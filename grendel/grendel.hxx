#ifndef __GRENDEL_PLANNER__
#define __GRENDEL_PLANNER__

#include <fond_inst.hxx>
#include <common.hxx>
#include <gp_atoms.hxx>
#include <open_list.hxx>
#include <ext_math.hxx>
#include <ex_policy.hxx>
#include <closed_list.hxx>
#include <hash_table.hxx>
#include <proc_nd_regression.hxx>
#include <search_node.hxx>
#include <ag_check_node.hxx>
#include <h1.hxx>
#include <rp.hxx>
#include <stack>
#include <set>
#include <deque>
#include <march_tree.hxx>

namespace grendel {

	template <typename Mutex_Heuristic>
	class Grendel_Planner {

		typedef  aptk::search::Closed_List< Search_Node >			AStar_Closed_List;
		typedef  Search_Node::Open_List					AStar_Strong_Open_List;
		typedef  Search_Node::Open_List					AStar_Weak_Open_List;
	public:
		Grendel_Planner( const FOND_Model& model, const Mutex_Heuristic& h ) 
		: m_model( model ), m_mutex_func( h ), m_gen_count(0), m_exp_count(0), m_strong_cyclic_checks(0), m_max_cost_to_go( 0 ),
		m_fwd_h( model ), m_rp_h( model ), m_max_missing_size(0), m_weight( 1.0f ), m_weight_2(1.0f), m_tree( model ) {
			m_vtable_entry.hash_table = &m_vtable;
		}

		~Grendel_Planner() {
			#ifdef DEBUG	
			for ( typename AStar_Closed_List::iterator i = m_closed.begin();
				i != m_closed.end(); i++ ) {
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			m_closed.clear();

			std::cout << "Checking for duplicates in weak open hash:" << std::endl;
			for ( typename AStar_Closed_List::iterator i = m_weak_open_hash.begin();
				i != m_weak_open_hash.end(); i++ ) {
				for ( auto j = m_weak_open_hash.begin(); j != m_weak_open_hash.end(); j++ ) {
					if ( i == j ) continue;
					if (i->second == j->second) {
						std::cout << "Duplicate node found in Weak Hash:" << std::endl;
						i->second->print_node( std::cout, m_model );
					}
				}
			}
	
			for ( typename AStar_Closed_List::iterator i = m_weak_open_hash.begin();
				i != m_weak_open_hash.end(); i++ ) {
				if ( i->second->hash_table !=  &m_weak_open_hash ) continue;
				assert( i->second->hash_table == &m_weak_open_hash );
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			m_weak_open_hash.clear();
			#endif
		}

		void	start() {
			DNF_Clause ext_goal;
			std::cout << "Completing negation: " << std::endl;
			m_mutex_func.add_consequences( m_model.goal, ext_goal );

			std::cout << "Goal (extended): " << std::endl;
			ext_goal.write( std::cout, m_model, true );
			std::cout << std::endl;

			m_mem_used = 0;
			m_init_is_dead_end = false;
			m_root = new Search_Node( ext_goal, -1 );
			m_root->strong = true;
			m_root->critical_path = true;
			std::cout << "Initial state" << std::endl;
			m_model.init.write( std::cout, m_model, true );
			std::cout << "Computing the additive heuristic..." << std::endl;
			m_fwd_h.full_eval( m_model.init, m_root->state );
			std::cout << "Computing best supporters for r.p.heuristic..." << std::endl;
			m_rp_h.compute_best_supporters( m_fwd_h );
			for ( Lit l : m_root->state )
				m_novelty.add(l);	
			eval( m_root );
			m_root->compute_hash( m_model );
			std::cout << "Root heuristic value: " << m_root->hn << std::endl;
			m_best_dist_to_init = infty;
			m_solution = nullptr;
			m_changed = false;
			close(m_root);
			open_strong_node(m_root);
		}

		bool	
		find_strong_cyclic_plan( Explicit_Policy& plan ) {
			if ( m_root->hn == infty ) return false;
			bool solved = do_search(plan);
			return solved;
		} 

		void		set_weight( float w ) { m_weight = w; }
		void		set_weight_2( float w ) { m_weight_2 = w; }

		void		inc_gen() { m_gen_count++; }
		void		inc_exp() { m_exp_count++; }
		void		inc_strong_cyclic_check() { m_strong_cyclic_checks++; }

		unsigned	generated() const { return m_gen_count; }
		unsigned	expanded() const { return m_exp_count; }
		unsigned	strong_cyclic_checks() const { return m_strong_cyclic_checks; }
		unsigned	march_tree_size() const { return m_tree.num_nodes(); }
		float		min_cost_to_go() const { return m_min_cost_to_go; }
		float		max_cost_to_go() const { return m_max_cost_to_go; }
		unsigned	dead_ends() const { return m_dead_ends.size(); }
		unsigned	max_missing_size() const { return m_max_missing_size; }
		unsigned	dead_end_action_pairs() const {
			unsigned count = 0;
			for ( auto entry = m_weak_open_hash.begin(); entry != m_weak_open_hash.end(); entry++ ) {
				if ( entry->second->is_dead_end ) count++;
			}
			return count;
		}
		unsigned	solved() const { return m_vtable.size(); }
		size_t		mem_used() const { return m_mem_used; }

		void		
		extract_plan( Explicit_Policy& plan ) {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				plan.add( it->second->state, it->second->action_idx, it->second->gn );
			}
		}

	protected:

		bool 			
		close( Search_Node* n ) { 
			//std::cout << "CLOSED stats: ";
			m_best_dist_to_init = std::min( m_best_dist_to_init, n->hn );
			auto other = m_closed.retrieve( n );
			if ( other != nullptr ) {
				#ifdef DEBUG
				std::cout << "There's another node like this in CLOSED: " << std::endl;
				other->print_node( std::cout, m_model, true );
				#endif
				return false;	
			}
			#ifdef DEBUG
			std::cout << "Inserting into CLOSED" << std::endl;
			#endif
			m_closed.put(n);
			
			#ifdef DEBUG
			std::cout << "Updating max cost to go" << std::endl;
			#endif
			m_max_cost_to_go = std::max( n->gn, m_max_cost_to_go );
			return true;
		}
	
		AStar_Closed_List&	
		closed() { 
			return m_closed; 
		}

		void	
		check_strong_dfs( Search_Node* root, bool relevant = false ) {
			#ifdef DEBUG
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Checking Strong Cyclic property: " << std::endl;
			root->print_node( std::cout, m_model, true );	
			#endif
			if( root->strong ) return;
			inc_strong_cyclic_check();

			m_tarjan_index = 0;
			#ifdef DEBUG
			float old_value = root->gn;
			#endif
			std::stack< AG_Check_Node* > S;
			AG_Closed_List C;
			//DNF_Clause context = ( is_goal( root->state ) ? m_model.init : root->state );
			AG_Check_Node* v0 = new AG_Check_Node( root, root->state /*context*/, m_model );
			v0->gn = root->gn;
			v0->hn = root->hn;
			strong_cyclic( v0, S, C );
			#ifdef DEBUG
			std::cout << "# Pairs in visited: " << C.size() << std::endl;
			#endif
			for ( auto x : C ) 
				delete x.second;
			#ifdef DEBUG
			std::cout << "Root node cost to go" << std::endl;
			std::cout << "Was: " << old_value << " is: " << root->gn << std::endl;
			std::cout << "Novelty: " << root->novelty << std::endl;
			#endif
		}

		Search_Node* 
		find_best_ag( const DNF_Clause& s, DNF_Clause& missing ) {
			missing.clear();
			Search_Node* 	best_ag = nullptr;
			for ( auto ag : m_closed ) {
				if ( best_ag != nullptr && aptk::dgeq( ag.second->gn, best_ag->gn ) ) continue;
				DNF_Clause subset;
				if ( s.intersection( ag.second->state, subset ) ) {
					if ( best_ag == nullptr || best_ag->gn > ag.second->gn ) {
						best_ag = ag.second;
						missing = subset;
					}
					else if ( aptk::dequal( best_ag->gn, ag.second->gn ) ) {
						if ( missing.size() > subset.size() ) {
							best_ag = ag.second;
							missing = subset;
						}
					}
				}
			}	
			#ifdef DEBUG
			std::cout << "======= Best AG search =============" << std::endl;
			std::cout << "Best AG cost: " << ( best_ag == nullptr ? infty : best_ag->gn ) << std::endl;
			std::cout << "Assumption literals: ";
			missing.write( std::cout, m_model, true );
			std::cout << std::endl;
			#endif
	
			return best_ag;	
		}		
		
		Search_Node* 	
		find_best_ef( const DNF_Clause& s, std::vector<int>& de_actions, DNF_Clause& assumption ) {
			assumption.clear();
			float best_ef_value =  infty;
			Search_Node*	best_ef = nullptr;
			for ( auto ef : m_weak_open_hash ) {
				if ( ef.second->loopy ) 
					continue;
				if ( ef.second->useless ) {
					continue;
				}
				if ( ef.second->gn > best_ef_value ) {
					continue;
				}
				DNF_Clause	subset;
				if ( !s.intersection( ef.second->state, subset ) ) {
					continue;
				}
				if ( ef.second->is_dead_end ) {
					de_actions.push_back( ef.second->action_idx );
					continue;
				}
				if ( best_ef_value > ef.second->gn ) {
					best_ef = ef.second;
					best_ef_value = ef.second->gn;
					assumption = subset;
					continue;
				}
				if ( aptk::dequal( best_ef_value, ef.second->gn ) ) {
					if ( subset.size() < assumption.size() ) {
						best_ef = ef.second;
						best_ef_value = ef.second->gn;
						assumption = subset;
					}
				}						
			}
			#ifdef DEBUG
			std::cout << "======= Best EF search =============" << std::endl;
			std::cout << "Best EF cost: " << best_ef_value << std::endl;
			std::cout << "Assumption literals: ";
			assumption.write( std::cout, m_model, true );
			std::cout << std::endl;

			#endif
			return best_ef;
		}

		Search_Node* 
		find_best_ag( const DNF_Clause& s, DNF_Clause& missing, std::list<Search_Node*>& relevant ) {
			missing.clear();
			Search_Node* 	best_ag = nullptr;
			for ( auto ag : relevant ) {
				if ( !ag->strong ) continue;
				if ( best_ag != nullptr && aptk::dgeq( ag->gn, best_ag->gn ) ) continue;
				DNF_Clause subset;
				if ( s.intersection( ag->state, subset ) ) {
					if ( best_ag == nullptr || best_ag->gn > ag->gn ) {
						best_ag = ag;
						missing = subset;
					}
					else if ( aptk::dequal( best_ag->gn, ag->gn ) ) {
						if ( missing.size() > subset.size() ) {
							best_ag = ag;
							missing = subset;
						}
					}
				}
			}	
			#ifdef DEBUG
			std::cout << "======= Best AG search =============" << std::endl;
			std::cout << "Best AG cost: " << ( best_ag == nullptr ? infty : best_ag->gn ) << std::endl;
			std::cout << "Assumption literals: ";
			missing.write( std::cout, m_model, true );
			std::cout << std::endl;
			#endif
	
			return best_ag;	
		}		
		
		Search_Node* 	
		find_best_ef( const DNF_Clause& s, std::vector<int>& de_actions, DNF_Clause& assumption, std::list<Search_Node*>& relevant ) {
			assumption.clear();
			float best_ef_value =  infty;
			Search_Node*	best_ef = nullptr;
			for ( auto ef : relevant ) {
				if ( ef->strong ) continue;
				if ( ef->gn > best_ef_value ) {
					continue;
				}
				DNF_Clause	subset;
				if ( !s.intersection( ef->state, subset ) ) {
					continue;
				}
				if ( ef->is_dead_end ) {
					de_actions.push_back( ef->action_idx );
					continue;
				}
				if ( best_ef_value > ef->gn ) {
					best_ef = ef;
					best_ef_value = ef->gn;
					assumption = subset;
					continue;
				}
				if ( aptk::dequal( best_ef_value, ef->gn ) ) {
					if ( subset.size() < assumption.size() ) {
						best_ef = ef;
						best_ef_value = ef->gn;
						assumption = subset;
					}
				}						
			}
			#ifdef DEBUG
			std::cout << "======= Best EF search =============" << std::endl;
			std::cout << "Best EF cost: " << best_ef_value << std::endl;
			std::cout << "Assumption literals: ";
			assumption.write( std::cout, m_model, true );
			std::cout << std::endl;

			#endif
			return best_ef;
		}


		void
		complete( const DNF_Clause& s, DNF_Clause& s_c ) {
			DNF_Clause ext_s;
			m_mutex_func.add_consequences( s, ext_s );
			s_c = std::move(ext_s);
		}

		unsigned
		select_best_greedy_policy_heuristic_and_critical_path( std::vector<Search_Node* >& greedy_policies ) {
			unsigned best_greedy_policy = 0;
			float	 best_h = greedy_policies[0]->hn;
			bool	 is_critical = greedy_policies[0]->critical_path;
			for ( unsigned i = 1; i < greedy_policies.size(); i++ ) {
				if ( best_h > greedy_policies[i]->hn ) {
					best_greedy_policy = i;
					best_h = greedy_policies[i]->hn;
					is_critical = greedy_policies[i]->critical_path;
					continue;
				}
				if ( aptk::dequal( best_h, greedy_policies[i]->hn )
							&& !is_critical ) {	
					if ( greedy_policies[i]->critical_path ) {
						best_greedy_policy = i;
						is_critical = true;
					}
				}
			}
			return best_greedy_policy;
		}

		unsigned
		select_best_greedy_policy_crit_path( std::vector<Search_Node* >& greedy_policies ) {
			unsigned best_greedy_policy = 0;
			for ( unsigned i = 0; i < greedy_policies.size(); i++ ) {
				if ( greedy_policies[i]->critical_path )
					return i;
			}
			return best_greedy_policy;

		}

		void
		make_successors( AG_Check_Node* v, AG_Closed_List& C  ) {
			const 	FOND_Model::Action& a = *(m_model.actions[v->policy_entry->action_idx]);
			v->delete_succ.resize( a.effects.size() );
			v->deadend_succ.resize( a.effects.size() );
			for ( unsigned k = 0; k < a.effects.size(); k++ ) { 
				v->delete_succ[k] = true;
				v->deadend_succ[k] = false;
			}
			v->successors.resize( a.effects.size() );
			v->assumptions.resize( a.effects.size() );
			for ( unsigned k = 0; k < a.effects.size(); k++ ) {
				v->successors[k] = nullptr;
				auto eff = a.effects[k];
				DNF_Clause incomplete_successor, successor;
				#ifdef DEBUG
				bool 	valid = 
				#endif
				v->context.apply( a.precondition, eff, incomplete_successor );
				complete( incomplete_successor, successor );
				assert(valid);
				#ifdef DEBUG
				std::cout << "========= SUCCESSOR =============" << std::endl;
				successor.write( std::cout, m_model );
				std::cout << std::endl;
				#endif
				if ( is_dead_end( successor ) ) {
					#ifdef DEBUG
					std::cout << "======== Stumbled upon DEAD END ==========" << std::endl;
					#endif
					v->deadend_succ[k] = true;
					v->policy_entry->is_dead_end = true;
					m_changed = true;
					break;
				}

				std::vector<int> 	de_actions;

				if ( successor.implies( m_model.goal ) ) {
					#ifdef DEBUG
					std::cout << "========= GOAL REACHED =============" << std::endl;
					#endif
					v->successors[k] = new AG_Check_Node( m_root, successor, m_model );
					v->successors[k]->hn = v->hn + a.cost;
					v->successors[k]->gn = 0.0f;
					v->successors[k]->parent = v;
					v->AG_connected = true;
					continue;
				}
			
				if ( v->successors[k] == nullptr ) {
					for ( auto visited : C ) 
						if ( successor.implies( visited.second->context) ) {
							#ifdef DEBUG
							std::cout << "========= FOUND in VISITED =============" << std::endl;
							#endif
							v->successors[k] = visited.second;
							v->AG_connected = visited.second->AG_connected;
							v->delete_succ[k] = false; 
							v->gn = std::max( v->gn, a.cost + v->successors[k]->policy_entry->gn );
							break;
						}
				}
	
				if ( v->successors[k] == nullptr ) {
					DNF_Clause	assumption;
					std::list< Search_Node* > relevant;
					m_tree.retrieve( successor, relevant );
					//std::cout << relevant.size() << " Nodes retrieved from March Tree" << std::endl; 

					//Search_Node* best = find_best_ag( successor, assumption );
					Search_Node* best = find_best_ag( successor, assumption, relevant );
					std::vector<Search_Node* > greedy_policies;
					if ( best != nullptr ) {
						if ( assumption.empty() ) {
							greedy_policies.push_back( best );
						}
						else {
							#ifdef DEBUG
							std::cout << "Match found that requires splitting the set" << std::endl;
							#endif
							v->assumptions[k] = assumption;
							v->failed = true;
							break;
						}
					}
					if ( greedy_policies.empty() ) {
						assumption.clear();
						//best = find_best_ef( successor,  de_actions, assumption );
						best = find_best_ef( successor, de_actions, assumption, relevant );
						if ( best != nullptr ) {
							if ( assumption.empty() ) {
								greedy_policies.push_back( best );
							}
							else {
								#ifdef DEBUG
								std::cout << "Match found that requires splitting the set" << std::endl;
								#endif
								v->assumptions[k] = assumption;
								v->failed = true;
								break;
							}
						}
					}
					#ifdef DEBUG
					std::cout << "=============== # Dead-end actions: " << de_actions.size() << std::endl;
					#endif
					if ( !greedy_policies.empty() ) {
						unsigned best_greedy_policy =
								select_best_greedy_policy_heuristic_and_critical_path( greedy_policies );
								//select_best_greedy_policy_crit_path( greedy_policies );
								//0;
						#ifdef DEBUG
						std::cout << "Satisfied by state-action pair: " << std::endl;
						greedy_policies[best_greedy_policy]->print_node( std::cout, m_model, true );
						std::cout << "Greedy policies: " << greedy_policies.size() << std::endl;
						#endif
							
						v->successors[k] = new AG_Check_Node( greedy_policies[best_greedy_policy], successor, m_model );
						v->successors[k]->hn = v->hn + a.cost;
						v->successors[k]->gn = greedy_policies[best_greedy_policy]->gn;
						v->successors[k]->greedy_policies = greedy_policies.size();
						v->gn = std::max( v->gn, a.cost + v->successors[k]->policy_entry->gn );
						if ( v->successors[k]->policy_entry->strong )
							v->AG_connected = true;
					}
				}

				if ( v->successors[k] == nullptr ) {
					float min_cost = infty;
					DNF_Clause			reason;
					DNF_Clause			assumption;
					bool				dead_end = true;
					#ifdef DEBUG
					std::cout << "================= TERMINAL CHECK: # dead-actions: " << de_actions.size() << std::endl;
					#endif
					for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {
						if ( std::find( de_actions.begin(), de_actions.end(), i ) != de_actions.end() )
							continue;
						const DNF_Clause& prec = m_model.actions[i]->precondition;
						bool consistent = successor.intersection( prec, assumption );
						if ( !consistent ) continue;
						m_max_missing_size = std::max( m_max_missing_size, assumption.size() );
						#ifdef DEBUG
						std::cout << "Dead-end hypothesis POTENTIALLY rejected by action:" << std::endl;
						std::cout<< "\t" << m_model.actions[i]->name << std::endl;
						std::cout << "Precondition: ";
						prec.write( std::cout, m_model, true );
						std::cout << std::endl;
						for ( Lit l : prec ) {
							if ( successor.entails(~l) ) std::cout << "No ";
						}
						std::cout << std::endl;
						std::cout << "Missing literals: ";
						assumption.write( std::cout, m_model, true );
						std::cout << std::endl;
						#endif
						for ( Lit l : assumption ) 
							successor.add(l);
						float h_pre = m_mutex_func.eval( successor );
						if ( h_pre < infty ) {
							dead_end = false;
							break;
						}
						#ifdef DEBUG
						std::cout << "Intersection of successor and precondition isn't reachable from initial state:" << std::endl;
						std::cout<< "\t" << m_model.actions[i]->name << std::endl;
						std::cout << "\t h(succ & pre) = " << h_pre << std::endl;
						std::cout << "Successor /\\ Pre: ";
						successor.write( std::cout, m_model, true );
						std::cout << std::endl;
						#endif
					}
					v->failed = true;
					if ( dead_end ) {
						#ifdef DEBUG
						std::cout << "Successor is DEAD END" << std::endl;
						std::cout << "Reason: ";
						reason.write( std::cout, m_model, true );
						std::cout << std::endl;
						std::cout << "Recording dead-end:" << std::endl;
						successor.write( std::cout, m_model, true );
						std::cout << std::endl;
						std::cout << "Entry in the Weak Policy is a dead-end:" << std::endl;
						v->policy_entry->print_node( std::cout, m_model, true );
						#endif
						v->deadend_succ[k] = true;
						record_deadend( std::move(successor) );
						v->policy_entry->is_dead_end = true;
						m_changed = true;
						break;
					}
					v->policy_entry->novelty++;
					v->gn = std::max( v->gn, a.cost + min_cost );
				}
			}
			
		}

		void
		update_policy_node_strong( AG_Check_Node* n ) {
			if ( already_in_weak_frontier(n->policy_entry) )
				m_weak_open_hash.remove(n->policy_entry);
			n->policy_entry->detach();
		
			// 1. Check for literals in the context, which aren't in the node
			DNF_Clause missing;
			for ( Lit l : n->context ) {
				if ( !n->policy_entry->state.entails(l) )
					missing.add(l);
			}
	
			if ( missing.empty( ) ) // it is tight
			{
				n->policy_entry->strong = true;
				if ( close( n->policy_entry ) )
					open_strong_node(n->policy_entry);
				return;
			}

			// 2. Not "tight", there are states denoted by the state in n->policy_entry
			// which are inconsistent with the context where we have proved it to be
			// closed
			n->policy_entry->useless = true;
			// 1. Restricting original node
			DNF_Clause restricted_lits = n->policy_entry->state;
			restricted_lits.add( missing );
			Search_Node* restricted = new Search_Node( restricted_lits, n->policy_entry->action_idx, n->policy_entry->gn, n->policy_entry->parent, false );
			complete( restricted_lits, restricted->state );
			eval(restricted);
			restricted->compute_hash( m_model );
			restricted->strong = true;
			if ( close( restricted) ) {
				#ifdef DEBUG
				std::cout << "Node splitting" << std::endl;
				std::cout << "Restricted: " << std::endl;
				restricted->print_node( std::cout, m_model, true );
				#endif
				m_tree.add( restricted );
				open_strong_node(restricted);
			}
			else 
				delete restricted;
			// 2. Account for residual states
			for ( Lit l : missing ) {
				DNF_Clause residual = n->policy_entry->state;
				residual.add( ~l ); // we just negate one literal
				Search_Node* residual_l = new Search_Node( residual, n->policy_entry->action_idx, n->policy_entry->gn, n->policy_entry->parent, false );
				complete( residual, residual_l->state );
				eval( residual_l );
				residual_l->compute_hash( m_model );
				if ( ( m_weak_open_hash.retrieve(residual_l) == nullptr ) && ( residual_l->hn < infty ) ) {
					m_weak_open_hash.put(residual_l);
					m_tree.add( residual_l );
					#ifdef DEBUG
					std::cout << "Node splitting" << std::endl;
					std::cout << "Residual: " << std::endl;
					residual_l->print_node( std::cout, m_model, true );
					#endif
					m_gen_count++;
				}
				else
					delete residual_l;	
			}
			
		}

		bool	
		update_policy_node( AG_Check_Node* n, float new_cost ) {
			if ( n->policy_entry->is_dead_end ) return true;
			return false;
		}

		void
		split_weak_node( Search_Node* n, const DNF_Clause& current_set, const DNF_Clause& selected_subset ) {

			// 1. Restricting original node
			DNF_Clause restricted_lits = current_set;
			restricted_lits.add( selected_subset );
			Search_Node* restricted = new Search_Node( restricted_lits, n->action_idx, n->gn, n->parent, false );
			complete( restricted_lits, restricted->state );
			eval(restricted);
			if ( restricted->hn < infty ) { // reachable from initial state
				restricted->compute_hash( m_model );
				if ( m_weak_open_hash.retrieve(restricted) == nullptr ) {
					m_weak_open_hash.put(restricted);
					m_tree.add( restricted );
					m_gen_count++;
					#ifdef DEBUG
					std::cout << "Node splitting" << std::endl;
					std::cout << "Restricted: " << std::endl;
					restricted->print_node( std::cout, m_model, true );
					#endif
				}
				else 
					delete restricted;
			}
			else {
				delete restricted;
			}
			// 2. Account for residual states
			for ( Lit l : selected_subset ) {
				DNF_Clause residual = current_set;
				residual.add( ~l ); // we just negate one literal
				Search_Node* residual_l = new Search_Node( residual, n->action_idx, n->gn, n->parent, false );
				complete( residual, residual_l->state );
				eval( residual_l );
				if ( residual_l->hn < infty ) {
					residual_l->compute_hash( m_model );
					if ( (m_weak_open_hash.retrieve(residual_l) == nullptr) && (residual_l->hn < infty ) ) {
						m_weak_open_hash.put(residual_l);
						m_tree.add( residual_l );
						#ifdef DEBUG
						std::cout << "Node splitting" << std::endl;
						std::cout << "Residual: " << std::endl;
						residual_l->print_node( std::cout, m_model, true );
						#endif
						m_gen_count++;
					}
					else
						delete residual_l;	
				}
				else {
					delete residual_l;
				}
			}
			
		}

		bool
		strong_cyclic( AG_Check_Node* v, std::stack< AG_Check_Node* >& S, AG_Closed_List& C ) {	
			// Evaluate the successors
			// Tarjan's algorithm bookkeeping 
			if ( C.retrieve(v) != nullptr ) {
				#ifdef DEBUG
				std::cout << "Already in visited!" << std::endl;
				#endif
				v->is_duplicate = true;
				return false;
			}
			v->policy_entry->novelty = 0;
			S.push( v );
			C.put( v );
			v->index = m_tarjan_index;
			v->lowlink = m_tarjan_index;
			v->in_stack = true;
			m_tarjan_index++;
			#ifdef DEBUG
			std::cout << "================== Node enters the stack" << std::endl;
			/*
			std::cout << "Context: " << std::endl;
			v->context.write( std::cout, m_model, true );
			std::cout << std::endl;
			std::cout << "Policy Entry: " << std::endl;
			v->policy_entry->print_node( std::cout, m_model, true );
			*/
			std::cout << "Tarjan bookkeeping: index(v) = " << v->index << " lowlink(v) = " << v->lowlink << std::endl;
			#endif

			make_successors( v, C );
			const 	FOND_Model::Action& a = *(m_model.actions[v->policy_entry->action_idx]);
			bool failed = v->failed || v->policy_entry->is_dead_end;
			float max_succ_cost = 0.0f;
			float new_val = a.cost;
			if ( failed ) {
				// Undo Tarjan's algorithm bookkeeping
				S.pop( );
				C.remove( v );
				v->index = v->lowlink = std::numeric_limits<int>::max();
				v->in_stack = false;
				m_tarjan_index--;
			
				DNF_Clause assumptions_parent;
				for ( DNF_Clause& assumptions_eff : v->assumptions ) {
					for ( Lit l : assumptions_eff )
						assumptions_parent.add( l );	
				}	
	
				if ( assumptions_parent.empty() ) {
					#ifdef DEBUG
					std::cout << "================== Node got OUTSIDE of the policy" << std::endl;
					std::cout << "Context: " << std::endl;
					v->context.write( std::cout, m_model, true );
					std::cout << std::endl;
					std::cout << "Policy Entry: " << std::endl;
					v->policy_entry->print_node( std::cout, m_model, true );
					std::cout << "Tarjan bookkeeping: index(v) = " << v->index << " lowlink(v) = " << v->lowlink << std::endl;
					#endif
				} 
				else {
					split_weak_node( v->policy_entry, v->context, assumptions_parent );
					v->policy_entry->useless = true;
				}
				if ( v->parent != nullptr ) v->parent->policy_entry->novelty += v->policy_entry->novelty;	
			
				for ( unsigned k = 0; k < a.effects.size(); k++ )
					if ( v->delete_succ[k] ) delete v->successors[k];
				return true; 
			}
			
			for ( unsigned k = 0; k < a.effects.size(); k++ ) { 
				if ( v->successors[k]->policy_entry->strong ) {
					v->AG_connected = true;
					break;
				}
			}

			#ifdef DEBUG
			std::cout << "============= LOOKING FOR SCC " << std::endl;
			#endif
			bool flag = false;
			unsigned num_open = 0;
			for ( unsigned k = 0; k < a.effects.size(); k++ ) {
				auto w = v->successors[k];
				if ( w->policy_entry->strong ) {
					#ifdef DEBUG
					std::cout << "============= \\pi_{AG} reached " << std::endl;
					#endif
					continue;
				}
				v->delete_succ[k] = false;	
				#ifdef DEBUG
				std::cout << "=============================== RECURSION" << std::endl;
				std::cout << "Context: " << std::endl;
				w->context.write( std::cout, m_model, true );
				std::cout << std::endl;
				std::cout << "Policy entry:" << std::endl;
				w->policy_entry->print_node( std::cout, m_model, true );
				std::cout << "index(n) = " << w->index << std::endl;
				#endif
				if ( w->index == std::numeric_limits<int>::max() ) {
					#ifdef DEBUG
					std::cout << "=============================== DRILLING DOWN" << std::endl;
					#endif
					flag = flag ||	strong_cyclic( w, S, C );
					if ( w->is_duplicate )
						v->delete_succ[k] = true;
					else
						v->lowlink = std::min( v->lowlink, w->lowlink );
					if ( flag ) num_open++;
				}
				else if ( w->in_stack ) {
					v->lowlink = std::min(v->lowlink, w->index );
					#ifdef DEBUG
					std::cout << "=============================== SUCCESSOR LOOPS BACK!" << std::endl;
					#endif
				}
			}
	
			max_succ_cost = 0.0f;
			bool hits_dead_end = false;
			for ( unsigned k = 0; k < v->successors.size(); k++ ) {
				auto w = v->successors[k]; 
				if ( w->policy_entry->is_dead_end ) {
					hits_dead_end = true;
					#ifdef DEBUG
					std::cout << "Dead-end has been hit" << std::endl;
					#endif
					continue;
				}
				if ( w->policy_entry->loopy ) {
					hits_dead_end = true;
					#ifdef DEBUG
					std::cout << "Infinite loop has been hit" << std::endl;
					#endif
					continue;
				}
				if ( w->policy_entry->strong ) { 
					v->AG_connected = true;
					max_succ_cost = std::max(max_succ_cost, w->gn);
				}
			}
			#ifdef DEBUG
			std::cout << "New value: " << new_val << " g(v) = " << v->gn << " g(n) = " << v->policy_entry->gn << std::endl;
			#endif
			new_val =  a.cost + max_succ_cost;
			if ( flag  && !hits_dead_end) {
				#ifdef DEBUG
				std::cout << "Call to Strong Cyclic failed: " << std::endl;
				std::cout << "Reason: value changed!" << std::endl;
				std::cout << "Context: " << std::endl;
				v->context.write( std::cout, m_model, true );
				std::cout << std::endl;
				std::cout << "Policy Entry: " << std::endl;
				v->policy_entry->print_node( std::cout, m_model, true );
				std::cout << "Tarjan bookkeeping: index(v) = " << v->index << " lowlink(v) = " << v->lowlink << std::endl;
				#endif
				if ( v->parent != nullptr ) v->parent->policy_entry->novelty += v->policy_entry->novelty;	
				update_policy_node( v, new_val );
				v->policy_entry->notify_update();
				v->gn = new_val;
				// Cleanup
				for ( unsigned k = 0; k < v->successors.size(); k++ ) 
					if ( v->delete_succ[k] ) delete v->successors[k];
				m_changed = true;
				return true;
			}
			if ( hits_dead_end  ) {
				#ifdef DEBUG
				std::cout << "Call to Strong Cyclic failed: " << std::endl;
				std::cout << "Reason: infinite loop or dead-end" << std::endl;
				#endif
				
				v->policy_entry->useless = true;
				if ( v->parent != nullptr ) v->parent->policy_entry->novelty += v->policy_entry->novelty;	
				// Cleanup
				for ( unsigned k = 0; k < v->successors.size(); k++ ) 
					if ( v->delete_succ[k] ) delete v->successors[k];
				m_changed = true;
				return true;

			}
			if ( v->lowlink == v->index ) { // v is a root node of an SCC
				#ifdef DEBUG
				std::cout << "======================================= Extracting SCC" << std::endl;
				#endif
				SCC_Container	SCC;
				AG_Check_Node* w = nullptr;
				bool hits_AG = false;
				size_t scc_size = 0;	
				do {
					w = S.top();	
					S.pop();
					w->in_stack = false;
					SCC.insert( w );
					hits_AG = hits_AG || w->AG_connected;
					#ifdef DEBUG
					std::cout << "Member of SCC: " << std::endl;
					w->context.write( std::cout, m_model, true ); std::cout << std::endl;
					std::cout << "index(n) = " << w->index << "lowlink(n) = " << w->lowlink << std::endl;
					w->policy_entry->print_node( std::cout, m_model );
					#endif
					scc_size++;
				} while ( w != v);
				#ifdef DEBUG
				std::cout << "======================================= SCC found with: " << scc_size << " pairs in it" << std::endl;
				#endif
				m_changed = true;	
				if ( hits_AG  ) {
					// Move all the nodes to $pi_\AG$, and update their values
					#ifdef DEBUG
					std::cout << "SCC is connected with $\\pi_{AG}$" << std::endl;
					#endif	
					while ( !SCC.empty() ) {
						
						auto n = SCC.pop();
						#ifdef DEBUG
						std::cout << "Updating $\\pi_{AG}$" << std::endl;
						std::cout << "Context:" << std::endl;
						n->context.write( std::cout, m_model, true ); std::cout << std::endl;
						std::cout << "index(n) = " << n->index << std::endl;
						std::cout << "Corresponding policy entry" << std::endl;
						n->policy_entry->print_node( std::cout, m_model, true );
						#endif
						/*
						float new_cost = 0.0f;
						float cost_internal = 0.0f;
						float cost_external = 0.0f;
						for ( auto w : n->successors ) {
							assert( w != nullptr );
							assert( w->policy_entry != nullptr );
							if ( w->policy_entry->strong ) {
								if ( aptk::dleq(cost_external, w->policy_entry->gn) ) {
									cost_external = w->policy_entry->gn;
									n->policy_entry->parent = w->policy_entry;
								}
							}
							else {
								if ( aptk::dleq(cost_internal, w->policy_entry->gn) ) { 
									cost_internal = w->policy_entry->gn;
									n->policy_entry->parent = w->policy_entry;
								}
							}
						}
						*/
						for ( unsigned k = 0; k < n->successors.size(); k++ ) 
							if ( n->delete_succ[k] ) delete n->successors[k];
						/*
						#ifdef DEBUG
						std::cout << "Cost: Internal: " << cost_internal << " External: " << cost_external << std::endl;
						#endif	
						new_cost = m_model.actions[n->policy_entry->action_idx]->cost + std::max( cost_internal, cost_external);
						if ( n->policy_entry->strong ) {
							#ifdef DEBUG
							std::cout << "A node already strong found inside a SCC!" << std::endl;
							#endif
							if ( aptk::dless(new_cost, n->policy_entry->gn) )
							{
								#ifdef DEBUG
								std::cout << n->policy_entry->gn << " < " << new_cost << std::endl;
								#endif
							}
							continue;
							
						}
						*/
						assert( !n->policy_entry->strong );
						assert( n->policy_entry->parent != nullptr );
						// the node went into the strong node queue
						update_policy_node_strong( n );						
						
					}
					return false;
				}
				else {
					#ifdef DEBUG
					std::cout << "Infinite Loop, marking policy nodes as loopy" << std::endl;
					#endif

					while ( !SCC.empty() ) {
						auto n = SCC.pop();
						float new_cost = 0.0f;
						float cost_internal = 0.0f;
						float cost_external = 0.0f;
	
						for ( auto w : n->successors ) {
							assert( w != nullptr );
							assert( w->policy_entry != nullptr );
							if ( w->policy_entry->strong ) {
								if ( aptk::dleq(cost_external, w->policy_entry->gn) ) {
									cost_external = w->policy_entry->gn;
								}
							}
							else {
								if ( aptk::dleq(cost_internal, w->policy_entry->gn) ) { 
									cost_internal = w->policy_entry->gn;
								}
							}
						}
						new_cost = m_model.actions[n->policy_entry->action_idx]->cost + std::max( cost_internal, cost_external);

						for ( unsigned k = 0; k < n->successors.size(); k++ ) 
							if ( n->delete_succ[k] ) delete n->successors[k];
						update_policy_node( n, new_cost );
						n->policy_entry->notify_update();
						m_changed = true;
					}
					return true;
				}
			}
			#ifdef DEBUG
			std::cout << "================== Node is contained in a SCC" << std::endl;
			std::cout << "Context: " << std::endl;
			v->context.write( std::cout, m_model, true );
			std::cout << std::endl;
			std::cout << "Policy Entry: " << std::endl;
			v->policy_entry->print_node( std::cout, m_model, true );
			std::cout << "Tarjan bookkeeping: index(v) = " << v->index << " lowlink(v) = " << v->lowlink << std::endl;
			#endif

			return false;
		}

		float
		get_strong_fn() {
			return std::min( get_prim_strong_fn(), get_sec_strong_fn() );
		}

		float
		get_prim_strong_fn() {
			return ( m_strong_open.empty() ? infty : m_strong_open.min() );
		}

		float
		get_sec_strong_fn() {
			return (m_strong_open_sec.empty() ? infty : m_strong_open_sec.min());
		}

		float
		get_weak_fn() {
			return std::min( get_prim_weak_fn(), get_sec_weak_fn() );
		}

		float
		get_prim_weak_fn() {
			return ( m_weak_open.empty() ? infty : m_weak_open.min() );
		}

		float
		get_sec_weak_fn() {
			return (m_weak_open_sec.empty() ? infty : m_weak_open_sec.min());
		}

		int
		get_prim_weak_novelty() 
		{
			return ( m_weak_open.empty() ? std::numeric_limits<int>::max() : m_weak_open.first()->novelty );
		}

		int
		get_sec_weak_novelty() 
		{
			return ( m_weak_open_sec.empty() ? std::numeric_limits<int>::max() : m_weak_open_sec.first()->novelty );
		}

		Search_Node* 
		get_strong_node() {
			if ( aptk::dleq(get_prim_strong_fn(),get_sec_strong_fn()) )
				return get_prim_strong_node();
			else
				return get_sec_strong_node();
			assert( m_strong_open.empty() );
			assert( m_strong_open_sec.empty() );
			return nullptr;
		}

		Search_Node*
		get_weak_node() {
			assert( !m_weak_open.empty() || !m_weak_open_sec.empty() );
			if ( get_prim_weak_novelty() < get_sec_weak_novelty() )
				return get_prim_weak_node();
			if ( get_prim_weak_novelty() > get_sec_weak_novelty() )
				return get_sec_weak_node();
			// Same novelty on both queues
			if ( get_prim_weak_fn() < get_sec_weak_fn() )
				return get_prim_weak_node();
			if ( get_prim_weak_fn() > get_sec_weak_fn() )
				return get_sec_weak_node();
			// f(n)
			if ( m_weak_open.empty() )
				return get_sec_weak_node();
			return get_prim_weak_node();
		}

		void
		eval( Search_Node* n ) {
			n->hn = std::max( m_mutex_func.eval(  n->state ), m_rp_h.eval( m_fwd_h, n->state ) );//m_fwd_h.eval( n->state ) );
			n->fn = ( n->hn == infty ? infty : aptk::add( m_weight_2 * n->gn, m_weight*n->hn ) );
			n->notify_update();
		}

		void
		reevaluate( Search_Node* n) {
			n->fn = ( n->hn == infty ? infty : aptk::add( m_weight_2 * n->gn, m_weight*n->hn ) );
		}

		template <typename Queue>
		void
		print_best( std::ostream& os, Queue& q ) {
			if ( !q.empty() ) {
				os << "h(n) = " << q.first()->hn;
				os << " g(n) = " << q.first()->gn;
				os << " f(n) = " << q.first()->fn;
				os << std::endl;
				q.first()->print_node( os, m_model, true );
			}
		}

		bool	
		do_search(Explicit_Policy& plan ) {
			m_min_cost_to_go = infty;
			#ifdef DEBUG
			int 	iteration = 0;
			#endif
			do {
				#ifdef DEBUG
				std::cout << "======================= Iteration = # " << iteration++ << "=============================================" << std::endl;
				std::cout << "======================== Exploitation phase ==================================" << std::endl;
				#endif
				m_changed = false;
				Search_Node* head = nullptr;
				do {
					head = get_strong_node();
					if ( head != nullptr && (head->hn < infty ) ) {
						assert( head->strong );
						if ( is_goal(head, plan) ) {
							std::cout << "======================================================================================================" << std::endl;
							std::cout << "Init reached" << std::endl;
							head->print_node( std::cout, m_model, true );
							std::cout << "Strong Cyclic Policy found!" << std::endl;
							m_min_cost_to_go = std::min( m_min_cost_to_go, head->gn );
							return true;
						}
					
						expand_node( head );
					}
				} while ( head != nullptr );
				#ifdef DEBUG
				std::cout << "======================== Checking weak states ======================================================" << std::endl;
				std::cout << "# Pairs in the Weak Policy: " << m_weak_open_hash.size() << std::endl;
				#endif
				int	opened_weak_nodes = 0;
				int	useless_weak_nodes = 0;
				int	loopy_weak_nodes = 0;
				for ( auto weak_pair : m_weak_open_hash ) {
					auto n = weak_pair.second;
					assert( !n->strong );
					if ( n->in_open ) continue;
					if ( n->useless ) {
						useless_weak_nodes++;
						continue;
					}
					if ( n->loopy ) {
						loopy_weak_nodes++;
						continue;
					}
					if ( n->is_dead_end  ) {
						useless_weak_nodes++;
						continue;
					}
					//if ( n->speculative ) continue;
					if ( n->expanded ) continue;
					open_weak_node( n );
					opened_weak_nodes++;
				}
				#ifdef DEBUG
				std::cout << "# Useless Weak Nodes = " << useless_weak_nodes << std::endl;
				std::cout << "# Loopy Weak Nodes = " << loopy_weak_nodes << std::endl;
				std::cout << "# Opened Weak Nodes = " << opened_weak_nodes << std::endl;
				#endif
				int sc_checks_done = 0;
				#ifdef DEBUG
				std::cout << "Best in Primary WEAK: " << std::endl;
				print_best( std::cout, m_weak_open );
				std::cout << "Best in Secondary WEAK: " << std::endl;
				print_best( std::cout, m_weak_open_sec );
				#endif
				#ifdef DEBUG
				std::cout << "Best Strong f(n) = " << get_strong_fn() << std::endl;
				std::cout << "Best Weak f(n) = " << get_weak_fn() << std::endl;
				#endif
				while ( get_weak_fn() < get_strong_fn() ) {
					head = get_weak_node();
					if ( head->strong ) {
						continue;
					}
					if ( head->loopy ) {
						continue;
					}
					if ( head->is_dead_end ) {
						continue; // discovered to be a dead-end
					}

					#ifdef DEBUG
					std::cout << "======================================================================================================" << std::endl;
					std::cout << "Q(o,\\phi) = " << head->gn << " V(s0) = " <<  head->hn << std::endl;
					#endif
					check_strong_dfs( head );
					if ( !head->strong && !head->loopy && !head->useless && !head->is_dead_end )
						expand_node( head );
					sc_checks_done++;
					// needs to be updated
				}
				#ifdef DEBUG
				std::cout << "======================= Iteration Results =============================================================" << std::endl;
				std::cout << "S.C. checks done = " << sc_checks_done << std::endl;
				std::cout << "Longest Strong Cyclic Execution=" << m_max_cost_to_go  << std::endl;
				std::cout << "min h(n), n \\in pi_{AG}: " << m_best_dist_to_init << std::endl;
				std::cout << "Size of pi_{AG}: " << m_closed.size() << std::endl;
				std::cout << "Size of pi_{EF}: " << m_weak_open_hash.size() << std::endl;
				std::cout << "Best in Primary STRONG: " << std::endl;
				print_best( std::cout, m_strong_open );
				std::cout << "Best in Secondary STRONG: " << std::endl;
				print_best( std::cout, m_strong_open_sec );
				std::cout << "Deadends = " << m_dead_ends.size() << std::endl;
				#endif
			} while ( m_changed || !m_strong_open.empty() || !m_strong_open_sec.empty() );
			assert( m_strong_open.empty() );
			assert( m_strong_open_sec.empty() );
			return false;			
		}

		bool
		relevance_check( const DNF_Clause& s, const FOND_Model::Action& a ) const {
			for ( auto eff_it = a.effects.begin(); eff_it != a.effects.end(); eff_it++ ) {
				for ( auto eff_it2 = eff_it->begin(); eff_it2 != eff_it->end(); eff_it2++ )
					for ( auto eff_it3 = eff_it2->effect.begin(); eff_it3 != eff_it2->effect.end(); eff_it3++ ) {
						if ( s.contains( *eff_it3 ) ) return true;
						if ( a.effects.size() == 1 && eff_it2->condition.empty() && s.contains( ~(*eff_it3) ) ) return false;	
					}
			}

			return false; // action doesn't mention any of the lits in the formula to be regressed
		}

		bool 		
		is_closed( Search_Node* n ) { 
			Search_Node* n2 = this->closed().retrieve(n);
			return n2 != nullptr;	
		}
		
		bool
		extract_minimal_policy( Search_Node* n0, Explicit_Policy& plan ) {
			AG_Closed_List	visited;
			typedef std::deque< AG_Check_Node* > 	Open;
			AG_Check_Node* v0 = new AG_Check_Node( n0, m_model.init, m_model );
			Open	Q;
			
			Q.push_back( v0 );
			unsigned	verified_states = 0;
			bool		failed = false;

			while ( !Q.empty() ) {
				AG_Check_Node* v = Q.front();
				Q.pop_front();
				AG_Check_Node* duplicate = visited.retrieve( v );
				if ( duplicate != nullptr ) {
					#ifdef DEBUG
					std::cout << "Greedy policy loops!" << std::endl;
					#endif
					delete v;
					continue;	
				}

				visited.put( v );
				const 	FOND_Model::Action& a = *(m_model.actions[v->policy_entry->action_idx]);
				for ( unsigned k = 0; k < a.effects.size(); k++ ) { 
					auto eff = a.effects[k];
					DNF_Clause successor;
					bool valid = v->context.apply( a.precondition, eff, successor );
					if ( !valid ) {
						#ifndef DEBUG
						std::cout << "Greedy policy failed!" << std::endl;
						#endif
						failed = true;
						break;
					}
					if ( successor.implies( m_model.goal ) ) {
						#ifdef DEBUG
						std::cout << "Goal reached" << std::endl;
						#endif
						continue;
					}
					DNF_Clause missing;
					Search_Node* best = find_best_ag( successor, missing  );
					if ( best == nullptr ) {
						#ifdef DEBUG
						std::cout << "Greedy policy failed!" << std::endl;
						std::cout << "No applicable closed policy found!" << std::endl;
						std::cout << "Successor: " << std::endl;
						successor.write( std::cout, m_model, true );
						std::cout << std::endl; 
						std::cout << "Reached through: " << std::endl;
						v->policy_entry->print_node( std::cout, m_model, true );
						std::cout << std::endl;	
						#endif
						failed = true;
						break;
					}
					AG_Check_Node* nv = new AG_Check_Node( best, successor, m_model );
					verified_states++;
					Q.push_back( nv );
				}
				if ( failed ) break;
			}
			
			while ( !Q.empty() )	{
				AG_Check_Node* v = Q.front();
				Q.pop_front();
				delete v;
			}
			for ( auto x : visited ) {
				plan.add( x.second->policy_entry->state, x.second->policy_entry->action_idx, x.second->policy_entry->gn );
				delete x.second;
			}
			#ifdef DEBUG
			std::cout << "States verified: " << verified_states << std::endl;
			#endif
			return !failed;	
		}	

		bool
		is_goal( Search_Node* n0, Explicit_Policy& plan  ) {//const DNF_Clause& c ) {
			if ( !m_model.init.implies(n0->state) ) return false;
			assert (  extract_minimal_policy( n0, plan ) );
			plan.clear();
			for ( auto x : m_closed ) {
				if ( x.second->strong )
					plan.add( x.second->state, x.second->action_idx, x.second->gn );
			}
			return true;
		}
	
		Search_Node* 		
		get_prim_strong_node() {
			Search_Node* next = nullptr;
			if(! m_strong_open.empty() ) {
				next = m_strong_open.pop();
				//assert( (m_strong_open.empty() ? true : aptk::dleq( next->fn, m_strong_open.min() ) ) );
				next->in_open = false;
			}
			return next;
		}
		
		Search_Node* 		
		get_sec_strong_node() {
			Search_Node* next = nullptr;
			if(! m_strong_open_sec.empty() ) {
				next = m_strong_open_sec.pop();
				//assert( (m_strong_open_sec.empty() ? true : aptk::dleq( next->fn, m_strong_open_sec.min() ) ) );
				next->in_open = false;
			}
			return next;
		}

		Search_Node*
		get_prim_weak_node() {
			Search_Node* next = nullptr;
			if(! m_weak_open.empty() ) {
				next = m_weak_open.pop();
				//assert( (m_weak_open.empty() ? true : aptk::dleq( next->fn, m_weak_open.min() ) ) );
				next->in_open = false;
			}
			return next;			
		}

		Search_Node*
		get_sec_weak_node() {
			Search_Node* next = nullptr;
			if(! m_weak_open_sec.empty() ) {
				next = m_weak_open_sec.pop();
				//assert( (m_weak_open_sec.empty() ? true : aptk::dleq( next->fn, m_weak_open_sec.min() ) ) );
				next->in_open = false;
			}
			return next;			
		}
		void
		flush_weak_queues( ) {
			while ( !m_weak_open.empty() ) {
				auto n = m_weak_open.pop();
				n->in_open = false;
			}
			while ( !m_weak_open_sec.empty() ) {
				auto n = m_weak_open_sec.pop();
				n->in_open = false;
			}
		}

		void
		dispose ( Search_Node* n ) {
			m_garbage.insert(n);
		}

		void	 	
		open_strong_node( Search_Node *n ) {
			if ( n == m_root ) {
				m_strong_open.insert(n);
				n->in_open = true;
				return;
			}
			if ( n->hn < n->parent->hn ) {
				n->critical_path = true;
				m_strong_open.insert(n);
				n->in_open = true;
				#ifdef DEBUG
				std::cout << "Went to primary" << std::endl;
				#endif
			}
			else {
				m_strong_open_sec.insert(n);
				n->in_open = true;
				#ifdef DEBUG
				std::cout << "Went to secondary" << std::endl;
				#endif
			}
			return;
		}

		bool	
		is_mutex( const DNF_Clause& c ) const {
			return aptk::dequal(m_mutex_func.eval( c ), infty);
		}

		bool	
		is_mutex( const DNF_Clause& c, const DNF_Clause& d ) const {
			return aptk::dequal(m_mutex_func.eval( c, d ),infty);
		}

		void	 	
		open_weak_node( Search_Node *n ) {
			assert( n != nullptr );
			n->in_open = true;
			if ( n->parent == nullptr && n->critical_path ) {
				#ifdef DEBUG
				std::cout << "Goes into primary!" << std::endl;
				std::cout << "h(n) = " << n->hn << std::endl;
				#endif
				m_weak_open.insert(n);
				n->in_open = true;
				return;
			}
			if ( n->parent == nullptr && !n->critical_path ) {
				#ifdef DEBUG
				std::cout << "Goes into secondary!" << std::endl;
				std::cout << "h(n) = " << n->hn << std::endl;
				#endif
				m_weak_open_sec.insert(n);
				n->in_open = true;
				return;
			}
			if ( n->hn < n->parent->hn ) {	
				#ifdef DEBUG
				std::cout << "Goes into primary!" << std::endl;
				std::cout << "h(parent)= " << n->parent->hn;
				std::cout << "h(n) = " << n->hn << std::endl;
				#endif
				n->critical_path = true;
				m_weak_open.insert(n);
				n->in_open = true;
			}
			else {
				#ifdef DEBUG
				std::cout << "Goes into secondary!" << std::endl;
				std::cout << "h(parent)= " << n->parent->hn;
				std::cout << "h(n) = " << n->hn << std::endl;
				#endif
				m_weak_open_sec.insert(n);
				n->in_open = true;
			}
			
		}
		
		bool 			
		already_in_weak_frontier( Search_Node *n ) {
			assert( !n->speculative );
			assert( n->parent != nullptr );
			Search_Node *previous_copy = m_weak_open_hash.retrieve(n);
			if ( previous_copy != nullptr ) {
				#ifdef DEBUG
				std::cout << "Match found for state-action pair:" << std::endl;
				n->print_node( std::cout, m_model, true );
				std::cout << "Matching state-action pair: " << std::endl;
				previous_copy->print_node( std::cout, m_model, true );
				#endif
				return true;
			}
			return false;
		}

		void
		expand_node( Search_Node *head ) {
			//assert( head->strong );
			if ( head->expanded ) return;
			#ifdef DEBUG
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Expanding: (min cost to go = " << m_min_cost_to_go << ", max cost to go = " << m_max_cost_to_go << " )" << std::endl;
			head->print_node( std::cout, m_model, true );		
			#endif
			head->expanded = true;
			if ( aptk::dgeq( (head->gn+head->hn), m_min_cost_to_go) ) {
				#ifdef DEBUG
				std::cout << "Predecessors pruned: g(n) + h(n) > V(s0)" << std::endl;
				#endif 
				return;
			}
			inc_exp();
			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {	
				if ( !relevance_check( head->state, *(m_model.actions[i]) ) ) continue;

				// Miquel: July 2014: Check if we have strong regressor
				if ( head->strong ) {
					DNF_Clause strong_succ;
					lbool result = procedural_strong_regress( m_model, i, head->state, strong_succ );
				
					if ( result == l_True ) {
						/*
						bool novelty_1 = false;
						for ( Lit l : strong_succ )
							if ( !m_novelty.entails(l) ) {
								novelty_1 = true;
								break;
							}
						if ( !novelty_1 ) continue;
						for ( Lit l : strong_succ )
							m_novelty.add(l);
						*/
						Search_Node* n = new Search_Node( strong_succ, i, head->gn + m_model.actions[i]->cost, head, false );
						complete( strong_succ, n->state );
						eval(n);
						if ( n->hn == infty ) {
							delete n;
							continue;
						}
						if (aptk::dgeq( (n->gn+n->hn), m_min_cost_to_go) ) {
							delete n;
							continue;
						}
						n->compute_hash( m_model );
						if ( is_closed( n ) ) {
							#ifdef DEBUG
							std::cout << "Predecessor:" << std::endl;
							n->print_node( std::cout, m_model, true );
							std::cout << "Pruned because already in the Strong Cyclic Policy" << std::endl;
							#endif
	
							delete n;
							continue;
						}
						// Goes directly into strong cyclic policy if parent strong
						n->strong = true;
						if ( close(n) ) {
							m_tree.add(n);
							open_strong_node( n );
						}
						inc_gen();
						m_mem_used += n->nbytes();
						#ifdef DEBUG
						std::cout << "\t through " << m_model.actions[i]->name << " results in new STRONG predecessor " << std::endl;
						n->print_node( std::cout, m_model, true );
						std::cout << std::endl;
						#endif
						m_changed = true;
						continue;
					}
				}
				std::vector<DNF_Clause>  succ;
				procedural_strong_cyclic_regress2( m_model, i, head->state, succ );
				for ( auto it = succ.begin(); it != succ.end(); it++ ) {
					/*
					bool novelty_1 = false;
					for ( Lit l : *it )
						if ( !m_novelty.entails(l) ) {
							novelty_1 = true;
							break;
						}
					if ( !novelty_1 ) continue;
					for ( Lit l : *it )
						m_novelty.add(l);
					*/

					Search_Node* n = new Search_Node( *it, i, head->gn + m_model.actions[i]->cost, head, false );
					complete( *it, n->state );
			
					eval(n);
					if ( n->hn == infty ) {
						delete n;
						continue;
					}
					if (aptk::dgeq( (n->gn+n->hn), m_min_cost_to_go) ) {
						delete n;
						continue;
					}
					n->compute_hash( m_model );
					if ( is_closed( n ) ) {
						#ifdef DEBUG
						std::cout << "Predecessor:" << std::endl;
						n->print_node( std::cout, m_model);
						std::cout << "Pruned because already in the Strong Cyclic Policy" << std::endl;
						#endif

						delete n;
						continue;
					}

					if( already_in_weak_frontier(n) ) {
						#ifdef DEBUG
						std::cout << "Predecessor:" << std::endl;
						n->print_node( std::cout, m_model );
						std::cout << "Pruned because already in the Weak Policy" << std::endl;
						#endif
						delete n;
						continue;
					}
					open_weak_node( n );
					inc_gen();
					m_mem_used += n->nbytes();
					m_weak_open_hash.put(n);
					m_tree.add(n);
					#ifdef DEBUG
					std::cout << "\t through " << m_model.actions[i]->name << " results in new WEAK predecessor " << std::endl;
					n->print_node( std::cout, m_model, true );
					std::cout << "V(s) = " << n->gn << std::endl;
					std::cout << std::endl;
					#endif
					m_changed = true;
				}	
			} 
		}

		void
		record_deadend( DNF_Clause&& s ) {
			auto tmp = new Search_Node;
			tmp->state = s;
			tmp->hash_table = &m_dead_ends;
			tmp->compute_hash( m_model );
			auto entry = m_dead_ends.retrieve( tmp );
			if ( entry != nullptr ) {
				delete tmp;
				return;
			}
			m_dead_ends.put( tmp );
		}

		void
		record_deadend( const DNF_Clause& s ) {
			auto tmp = new Search_Node;
			tmp->state = s;
			tmp->hash_table = &m_dead_ends;
			tmp->compute_hash( m_model );
			auto entry = m_dead_ends.retrieve( tmp );
			if ( entry != nullptr ) {
				delete tmp;
				return;
			}
			m_dead_ends.put( tmp );
		}

		bool
		is_dead_end( const DNF_Clause& s ) {
			for ( auto it = m_dead_ends.begin();
				it != m_dead_ends.end();
				it++ )
				if ( it->second->state.implies(s) )
					return true;
			return false;
		}

	public:
		void
		print_values( std::ostream& os ) {
			for ( auto it = m_vtable.begin(); it != m_vtable.end(); it++ ) {
				it->second->state.write( os, m_model );
				os << " <- " << it->second->gn << std::endl;
			}
		}

		void
		print_deadends( std::ostream& os ) {
			for ( auto it = m_dead_ends.begin(); it != m_dead_ends.end(); it++ ) {
				it->second->state.write( os, m_model );
				os << std::endl;
			}	
		}

	private:
		const FOND_Model&			m_model;
		AStar_Strong_Open_List			m_strong_open;
		AStar_Strong_Open_List			m_strong_open_sec;
		AStar_Weak_Open_List			m_weak_open;
		AStar_Weak_Open_List			m_weak_open_sec;
		const Mutex_Heuristic&			m_mutex_func;
		unsigned				m_gen_count;
		unsigned				m_exp_count;
		unsigned				m_strong_cyclic_checks;
		AStar_Closed_List			m_closed;
		AStar_Closed_List			m_weak_open_hash;
		Search_Node*				m_root;
		Search_Node*				m_solution;
		size_t					m_mem_used;
		float					m_min_cost_to_go;
		AStar_Closed_List			m_vtable;
		Search_Node				m_vtable_entry;
		AStar_Closed_List			m_dead_ends;
		float					m_max_cost_to_go;
		bool					m_init_is_dead_end;
		float					m_best_dist_to_init;
		std::vector< Search_Node* >		m_init_nodes;
		H1_Heuristic< H_Max_Evaluation_Function >
							m_fwd_h;
		Relaxed_Plan_Heuristic< H1_Heuristic< H_Max_Evaluation_Function > >
							m_rp_h;
		std::set< Search_Node* >		m_garbage;
		bool					m_changed;
		size_t					m_max_missing_size;
		int					m_tarjan_index;
		float					m_weight;
		float					m_weight_2;
		March_Tree				m_tree;
		DNF_Clause				m_novelty;
	};

}

#endif // ucs.hxx

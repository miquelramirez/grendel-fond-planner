#ifndef __EXPLICIT_POLICY__
#define __EXPLICIT_POLICY__

#include <gp_atoms.hxx>
#include <fond_inst.hxx>

namespace grendel {

	class Explicit_Policy {
	public:
		struct State_Action_Pair {
			DNF_Clause	condition;
			int		action_index;
			float		cost_to_go;
		};

		typedef std::pair< DNF_Clause, int >	Entry;

		Explicit_Policy( const FOND_Model& model ) 
		: m_model( model ) {
		}

		~Explicit_Policy() {
		}

		void
		clear() { m_entries.clear(); }

		const FOND_Model::Action*	
		lookup( const DNF_Clause& state ) const;
	
		void	
		lookup( const DNF_Clause& state, 
			std::vector< const FOND_Model::Action* >& possible_actions ) const;
	
		void	
		add( const DNF_Clause& c, int action_index, float cost_to_go );

		void	
		write( std::ostream& os, bool human_readable = false ) const {
			if ( human_readable ) 
				write_human_readable( os );
			else
				write_raw( os );
		}	

		bool	verify( ); 

		unsigned	num_states() const { return m_entries.size(); }
		unsigned	num_visited_states() const { return m_visited; }
		unsigned	num_executions() const { return m_paths_to_goal; }
		float		min_cost() const { return m_min_cost; }
		float		max_cost() const { return m_max_cost; }

	protected:
		void	make_init_and_goal();
		void	write_human_readable( std::ostream& os ) const;
		void	write_raw( std::ostream& os ) const;
	private:
		const FOND_Model& 			m_model;
		std::vector< State_Action_Pair >	m_entries;
		unsigned				m_visited;
		float					m_min_cost;
		float					m_max_cost;
		unsigned				m_paths_to_goal;
	};
}

#endif

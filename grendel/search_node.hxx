#ifndef __GRENDEL_SEARCH_NODE__
#define __GRENDEL_SEARCH_NODE__

#include <fond_inst.hxx>
#include <gp_atoms.hxx>
#include <open_list.hxx>

namespace grendel
{

	class Search_Node {
	public:

		typedef DNF_Clause	State_Type;
		
		DNF_Clause			state;
		int				action_idx;
		float				hn;
		float				gn;
		float				fn;
		Search_Node*			parent;
		size_t				hash_key;
		bool				hash_up_to_date;
		bool				strong;
		void*				hash_table;
		bool				expanded;
		bool				is_dead_end;
		bool				speculative;
		bool				critical_path;
		bool				useless;
		bool				in_open;
		bool				loopy;
		size_t				width;
		int				novelty;

		typedef  Fibonacci_Open_List< Search_Node > 		Open_List;

		Open_List::Handle			heap_handle;
		Open_List*				current;

		Search_Node( );

		Search_Node( const DNF_Clause& c );

		Search_Node( const DNF_Clause& c, int _action_idx, float _gn = 0.0f, Search_Node* _parent = nullptr, bool _strong = true ); 

		~Search_Node();

		size_t	hash( ) const {
			return hash_key;
		}

		bool 	operator==( const Search_Node& o ) const {
			return action_idx == o.action_idx && state == o.state;
		}

		size_t nbytes() const {
			size_t mem_used = sizeof( Search_Node );
			return mem_used + state.nbytes();
		}

		bool	operator<( const Search_Node& b ) const {
			if ( strong ) {
				assert( b.strong ); // Make sure we compare strong and strong
				// novelty doesn't matter
				if ( fn > b.fn ) return true;
				if ( fn < b.fn ) return false;
				// f(n) is the same
				if ( hn > b.hn ) return true;
				if ( hn < b.hn ) return false;
				// h(n) is the same
				if ( gn > b.gn ) return true;
				return false;
			}
			if ( novelty > b.novelty ) return true;
			if ( novelty < b.novelty ) return false;
			// novelty is the same
			if ( fn > b.fn ) return true;
			if ( fn < b.fn ) return false;
			// f(n) is the same
			if ( hn > b.hn ) return true;
			if ( hn < b.hn ) return false;
			// h(n) is the same
			if ( gn > b.gn ) return true;
			return false;
		}

		void notify_update( ) {
			if ( !in_open ) return;
			assert( current != nullptr );
			current->update( this ); 
		}

		void detach() {
			if ( !in_open ) return;
			assert( current != nullptr );
			current->erase( this );
			current = nullptr;
			in_open = false;
		}

		void
		compute_hash( const FOND_Model& model  );

		void
		print_node( std::ostream& os, const FOND_Model& model, bool show_negative_lits = false );

	};
}

#endif // search_node.hxx

#ifndef __STRONG_CYCLIC_UNIFORM_COST_SEARCH_DOUBLE_QUEUE__
#define __STRONG_CYCLIC_UNIFORM_COST_SEARCH_DOUBLE_QUEUE__

#include <problem.hxx>
#include <gp_atoms.hxx>
#include <nd_regression.hxx>
#include <open_list.hxx>
#include <ext_math.hxx>
#include <ex_policy.hxx>
#include <closed_list.hxx>
#include <hash_table.hxx>
#include <nd_regression.hxx>

namespace grendel {

	namespace scucs_dq {
		class Node {
		public:

			typedef DNF_Clause	State_Type;
			
			DNF_Clause			state;
			int				action_idx;
			float				gn;
			Node*				parent;
			size_t				hash_key;
			bool				hash_up_to_date;
			int 				trials;
			bool				strong;
			bool				subsumed;
			void*				hash_table;
			
	
			Node( const DNF_Clause& c, int _action_idx, float _gn = 0.0f, Node* _parent = nullptr, bool _strong = true ) 
			: state(c), action_idx( _action_idx ),  gn(_gn), parent(_parent), trials(0), strong( _strong ), subsumed(false) {
				state.sort();	
				hash_up_to_date = false;
				hash_table = nullptr;
			}

			~Node() {
				assert( hash_table == nullptr );
				hash_up_to_date = false;
				gn = -1;
			}

			size_t	hash( ) const {
				return hash_key;
			}

			bool 	operator==( const Node& o ) const {
				return action_idx == o.action_idx && state == o.state;
			}
		};
	
		class Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				return aptk::dless( b->gn, a->gn );
			}
		};

		class Weak_Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				if ( b->trials > a->trials ) return false;
				if ( b->trials == a->trials ) 
					return aptk::dless( b->gn, a->gn );
				return true;
			}
		};


	}

	template <typename Mutex_Heuristic>
	class DQ_Strong_Cyclic_Uniform_Cost_Search {

		typedef  Open_List< scucs_dq::Comparer, scucs_dq::Node > 	UCS_Strong_Open_List;
		typedef  Open_List< scucs_dq::Weak_Comparer, scucs_dq::Node >	UCS_Weak_Open_List;
		typedef  aptk::search::Closed_List< scucs_dq::Node >		UCS_Closed_List;

	public:
		DQ_Strong_Cyclic_Uniform_Cost_Search( const Instance& model, const Mutex_Heuristic& h ) 
		: m_model( model ), m_mutex_func( h ), m_gen_count(0), m_exp_count(0) {

			for ( grendel::Atom p = 0; p < (int)m_model.n_atoms(); p++ ) {
				bool found = false;
				for ( auto init_it = m_model.init.literals.begin();
					init_it != m_model.init.literals.end() && !found; init_it++ ) {
					int idx = *init_it > 0 ? *init_it-1 : -*init_it-1;
					found = idx == p;
				}
					
				if ( found ) 
					m_init.add( mkLit(p,false) );
				else
					m_init.add( mkLit(p,true) );
			}

			m_goal = DNF_Clause( m_model.goal_literals );

		}

		~DQ_Strong_Cyclic_Uniform_Cost_Search() {
			for ( typename UCS_Closed_List::iterator i = m_closed.begin();
				i != m_closed.end(); i++ ) {
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			while ( !m_strong_open.empty() ) {
				scucs_dq::Node* n = m_strong_open.pop();
				n->hash_table = nullptr;
				delete n;
			}

			m_closed.clear();
			m_strong_open_hash.clear();

			while ( !m_weak_open.empty() ) {
				scucs_dq::Node* n = m_weak_open.pop();
				n->hash_table = nullptr;
				delete n;
			}

			m_weak_open_hash.clear();

		}

		void	start() {
			DNF_Clause ext_goal;
			m_mutex_func.add_consequences( m_goal, ext_goal );
			m_root = new scucs_dq::Node( ext_goal, -1 );
			compute_hash( m_root );
			m_solution = nullptr;
			open_strong_node( m_root );
			m_get_weak = false;
		}

		bool	
		find_strong_cyclic_plan( Explicit_Policy& plan ) {
			bool solved = false;
			
			if ( do_search( ) != nullptr ) {
	
				extract_plan( plan );
				solved = true;

			}

			return solved;
		} 

		void		inc_gen() { m_gen_count++; }
		void		inc_exp() { m_exp_count++; }

		unsigned	generated() const { return m_gen_count; }
		unsigned	expanded() const { return m_exp_count; }

		void		
		extract_plan( Explicit_Policy& plan ) {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				plan.add( it->second->state, it->second->action_idx );
			}
		}

	protected:

		bool
		check_delete_precondition(scucs_dq::Node* n ) const {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				if ( it->second == n ) return true;
			}
			for ( auto it = m_strong_open_hash.begin(); it != m_strong_open_hash.end(); it++ ) {
				if ( it->second == n ) return true;
			}
			for ( auto it = m_weak_open_hash.begin(); it != m_weak_open_hash.end(); it++ ) {
				if ( it->second == n ) return true;
			}
			return false;
		}

		void 			
		close( scucs_dq::Node* n ) {  
			m_closed.put(n); 
		}
	
		UCS_Closed_List&	
		closed() { 
			return m_closed; 
		}
	
		UCS_Closed_List&	
		open_hash() 	{ 
			return m_strong_open_hash; 
		}

		bool
		check_successors( scucs_dq::Node* n, std::vector<scucs_dq::Node* >& replacements, bool& delete_node )  {
			const Instance::Action& a = *(m_model.actions[n->action_idx]);
			assert( !a.ndeffects.empty() ); // predecessors of det actions shouldn't turn up here
			const size_t num_effs = a.ndeffects[0].effs.size();
			DNF_Clause   res_state;
		
	
			std::vector< DNF_Clause > forced_lits;
			for ( unsigned k = 0; k < num_effs; k++ )
			{
				res_state.clear();	
				bool res = n->state.apply( a, k, res_state );
				assert( res ); // actions should be applicable!
				/*
				std::cout << "Checking successor :" << std::endl;
				res_state.write( std::cout, m_model );
				std::cout << std::endl;
				*/
				if ( !implied_by_closed( res_state, forced_lits  ) ) {
					// Checking whether there exists a weak plan with h^2 might
					// be sufficient
					return false;
				}
			}
			bool all_empty = true;
			for ( auto it = forced_lits.begin(); it != forced_lits.end(); it++ ) {
				if ( !it->empty() ) {
					all_empty = false;
					break;
				}
			}
			if ( all_empty ) {
				delete_node = false;
				n->strong = true;
				replacements.push_back( n );
				//std::cout << "All cases are empty" << std::endl;
				return true;
			}

			// Check that none of the forced conjunctions yields a mutex state!
			for ( auto it = forced_lits.begin(); it != forced_lits.end(); it++ ) {
				if ( is_mutex( n->state, *it ) ) {
					//std::cout << "State is mutex with forced literal set: ";
					//it->write( std::cout, m_model );
					//std::cout << std::endl;
					return false;	
				}
			}
			// Create new nodes
			delete_node = true;
			for ( auto it = forced_lits.begin(); it != forced_lits.end(); it++ ) {
				if ( it->empty() ) 
					continue;
				DNF_Clause new_state(n->state);
				new_state.add( *it );
				DNF_Clause ext_new_state;
				m_mutex_func.add_consequences( new_state, ext_new_state );
				scucs_dq::Node* n2 = new scucs_dq::Node( ext_new_state, n->action_idx, n->gn, n->parent, false ); 
				compute_hash( n2 );
				replacements.push_back( n2 );
			}		

			
			return true;
		}
		
		bool
		implied_by_closed( const DNF_Clause& c, std::vector<DNF_Clause>& missing ) const {
			DNF_Clause tmp;
			bool implied = false;
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				tmp.clear();
				if ( it->second->state.implies( c, tmp ) ) {
					if ( is_mutex( c, tmp ) ) continue;
					/*
					std::cout << "\t\tImplied by: " << std::endl;
					std::cout << "\t\t";
					it->second->state.write( std::cout, m_model );
					if ( it->second->action_idx == -1 ) 
						std::cout << " -> (GOAL)" << std::endl;
					else
						std::cout << " -> " << *(m_model.actions[it->second->action_idx]->name) << std::endl;
					std::cout << "\tMissing lits: ";
					tmp.write( std::cout, m_model );
					std::cout << std::endl;
					*/
					missing.push_back( tmp );
					implied = true;
				}
			}
			return implied;
		}

		scucs_dq::Node* 
		get_node() {
			scucs_dq::Node* n = nullptr;
			if ( !m_strong_open.empty() )
				return get_strong_node();
			if ( !m_weak_open.empty() )
				return get_weak_node();
		/*
			if ( !m_get_weak ) {
				m_get_weak = true;
				if ( !m_strong_open.empty() ) 
					return get_strong_node();
			}
			assert( m_get_weak );
			m_get_weak = false;
			if ( !m_weak_open.empty() )
				return get_weak_node();
			if ( !m_strong_open.empty() )
				return get_strong_node();
		*/
			return n;
		}

		scucs_dq::Node*	
		do_search() {
			scucs_dq::Node* head = get_node();
			int 	max_trials = 0;
			size_t	weak_open_size = 0;
			while(head) {
				if ( head->strong ) {	
					if(is_goal(head->state)) {
						std::cout << "======================================================================================================" << std::endl;
						std::cout << "Init reached ";
						head->state.write( std::cout, m_model );
						std::cout << ", execution length is: " << head->gn << std::endl;
						close(head);
						return head;
					}
			
					close(head);
					process_strong_node(head);
					head = get_node();
					continue;
				}
				process_weak_node( head );
				if ( 	m_strong_open.empty() 
					&& m_weak_open.first()->trials > max_trials ) {
					max_trials =  m_weak_open.first()->trials;
					std::cout << "Max trials = " << max_trials << std::endl;
					std::cout << "Fixed point: old size = " << weak_open_size << " current size = " << m_weak_open_hash.size() << std::endl;

					if ( weak_open_size == m_weak_open_hash.size() ) {
						break; // Fixed Point!
					}
					weak_open_size = m_weak_open_hash.size();
				}
				head = get_node();
			}
			return nullptr;			
		}

		void
		print_node( std::ostream& os, scucs_dq::Node* n ) {
			os << "\t";
			n->state.write( os, m_model);
			os << std::endl;
			if ( n->action_idx == -1 ) 
				os << "-> (GOAL)" << std::endl;
			else
				os << "-> " << *(m_model.actions[n->action_idx]->name) << std::endl;

			os << " g(n) = " << n->gn << " h(n) = " << m_mutex_func.eval( n->state ) << " trials(n) = " << n->trials << std::endl;
				
		}

		void
		process_weak_node( scucs_dq::Node* head ) {
			
			// Delete subsumed nodes lazily
			if ( head->subsumed ) {
				//std::cout << "Deleting subsumed" << std::endl;
				delete head;
				return;
			}
			/*
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Expanding potential strong cyclic: " << std::endl;
			print_node( std::cout, head );	
			*/			
			std::vector<scucs_dq::Node*> cases;
			bool 	delete_original = false;

			if ( is_closed( head ) ) {
				delete head;
				return;
			}

			bool all_succ_strong = check_successors( head, cases, delete_original );
			if ( all_succ_strong ) {
				if ( cases.size() == 1 ) {
					float LB_parent = m_mutex_func.eval(cases[0]->parent->state);
					if ( is_mutex( cases[0]->state ) )
						delete cases[0];
					else if ( m_mutex_func.eval( cases[0]->state ) > LB_parent )
						delete cases[0];
					else if ( is_closed( cases[0] ) )
						delete cases[0];
					else if ( previously_hashed( cases[0] ) )
						delete cases[0];
					else {

						open_strong_node( cases[0] );
						/*
						std::cout << "State action pair: " << std::endl;
						std::cout << "\t";
						cases[0]->state.write( std::cout, m_model );
						if ( cases[0]->action_idx == -1 ) 
							std::cout << " -> (GOAL)" << std::endl;
						else
							std::cout << " -> " << *(m_model.actions[cases[0]->action_idx]->name) << std::endl;
						std::cout << "h(n) = " << m_mutex_func.eval(cases[0]->state);
						std::cout << " h(parent) = " << m_mutex_func.eval(cases[0]->parent->state) << std::endl;
						std::cout << std::endl;

						std::cout << "Goes into STRONG open" << std::endl;
						*/
					}
				}
				else {
					std::vector<scucs_dq::Node*> cases_clean;
					for ( auto it = cases.begin(); it != cases.end(); it++ ) {
						float LB_parent = m_mutex_func.eval((*it)->parent->state);
						if ( is_mutex( (*it)->state ) )
							delete *it;
						else if ( m_mutex_func.eval( (*it)->state ) > LB_parent )
							delete *it;
						else if ( is_closed( *it ) )
							delete *it;
						else if ( weak_previously_hashed( *it ) )
							delete *it;
						else {
							cases_clean.push_back( *it );
						}
					}
					std::vector< scucs_dq::Node* > minimal_set;
					if ( cases_clean.size() == 1 ) {
						minimal_set.push_back( cases_clean[0] );
						cases_clean.clear();
					}
					for ( auto it = cases_clean.begin(); it != cases_clean.end(); it++ ) {
						if ( (*it)->subsumed ) continue;
						float hn = m_mutex_func.eval( (*it)->state );
						DNF_Clause merged_state;
						merged_state.add( (*it)->state );
						unsigned merge_count = 0;
						auto it2 = it;
						it2++;
						for ( ; it2 != cases_clean.end(); it2++ ) {
							if ( (*it2)->subsumed ) continue;
							float hn2 = m_mutex_func.eval( (*it2)->state );
							float hmerge = m_mutex_func.eval( merged_state, (*it2)->state );
							if (  hmerge == hn &&  hmerge == hn2 ) {
								(*it2)->subsumed = true;
								merged_state.set_union( (*it2)->state );
								merge_count++;
							}
						}
						if ( merge_count == 0 ) {
							minimal_set.push_back( *it );
							continue;
						}
						scucs_dq::Node* new_node = new scucs_dq::Node( merged_state, (*it)->action_idx, (*it)->gn, (*it)->parent, false );
						compute_hash( new_node );
						minimal_set.push_back( new_node );
						(*it)->subsumed = true;
					}
					// Clean up subsumed
					for ( auto it = cases_clean.begin(); it != cases_clean.end(); it++ )
						if ( (*it)->subsumed ) delete *it;
					// Get into WEAK OPEN
					for ( auto it = minimal_set.begin(); it != minimal_set.end(); it++ ) {
						if ( weak_previously_hashed( *it ) ) 
							delete *it;
						else
							open_weak_node( *it );
					}
					
				}

				if ( delete_original )
					delete head;
			}
			else {
				//std::cout << "Delaying expansion" << std::endl;
				head->trials++;
				assert( m_weak_open_hash.retrieve( head ) == nullptr );
				open_weak_node( head );
			}
		}
		
		

		scucs_dq::Node*
		merge_equivalent_states( scucs_dq::Node* n ) {
			float hn = m_mutex_func.eval( n->state );
			DNF_Clause merged_state;
			merged_state.add( n->state );
			unsigned merge_count = 0;
			//std::cout << "Weak state merging has to check: " << m_weak_open_hash.size() << " states" << std::endl;
			for ( auto it = m_weak_open_hash.begin(); it != m_weak_open_hash.end(); it++ ) {
				scucs_dq::Node* n2 = it->second;
				if ( n2->subsumed ) continue;
				if ( n->gn != n2->gn ) continue;
				float hn2 = m_mutex_func.eval( n2->state );
				float hmerge = m_mutex_func.eval( merged_state, n2->state );
				if (  hmerge == hn &&  hmerge == hn2 ) {
					n2->subsumed = true;
					/*
					std::cout << "\t Subsuming:" << std::endl;
					print_node( std::cout, n2 );
					*/
					merged_state.set_union( n2->state );
					merge_count++;
				}
			}
			//std::cout << "Merged " << merge_count << " states in WEAK open" << std::endl;
			if ( merge_count == 0 ) return nullptr;
			scucs_dq::Node* new_node = new scucs_dq::Node( merged_state, n->action_idx, n->gn, n->parent, false );
			compute_hash( new_node );
			/*
			std::cout << "\tResulting WEAK node:" << std::endl;
			print_node( std::cout, new_node );
			*/
			return new_node;
		}
		
		void
		compute_hash( scucs_dq::Node* n ) {
			aptk::Hash_Key hasher;
			for ( auto it = n->state.begin(); it != n->state.end(); it++ )
				hasher.add( toInt(*it) );
			hasher.add( 2*m_model.n_atoms() + n->action_idx );
			n->hash_key = (size_t)hasher;
			n->hash_up_to_date = true;
		}		

		void 			
		process_strong_node(  scucs_dq::Node *head ) {

			inc_exp();
			/*
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Expanding strong cyclic: " << std::endl;
			print_node( std::cout, head );		
			*/
			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {	

				std::vector<DNF_Clause>  succ;
				lbool res = strong_cyclic_regress( m_model, i, head->state, succ );

				//const Instance::Action& a = *m_model.actions[i];
				
				assert( res != l_Undef );
				
				if ( res == l_False ) {
					//std::cout << "\t through " << *(a.name) << " results in FALSE" << std::endl;
					for ( auto it = succ.begin(); it != succ.end(); it++ ) {
						if ( is_mutex(*it) ) {
							//std::cout << "\t through " << *(a.name) << " results in ";
							//it->write( std::cout, m_model );
							//std::cout << " which is mutex" << std::endl;
							continue;
						}

						DNF_Clause ext_succ;
						m_mutex_func.add_consequences( *it, ext_succ );						

						scucs_dq::Node* n = new scucs_dq::Node( ext_succ, i, head->gn + m_model.actions[i]->cost, head, false );
						compute_hash( n );
						if ( is_closed( n ) ) {
							//std::cout << "\t through " << *(a.name) << " already in closed! " << std::endl;
							delete n;
							continue;
						}
						if( weak_previously_hashed(n) ) {
							//std::cout << "\t through " << *(a.name) << " already in WEAK open! " << std::endl;
							delete n;
						}
						else {
							//std::cout << "\t through " << *(a.name) << " results in new WEAK predecessor " << std::endl;
							//std::cout << "\t";
							//it->write( std::cout, m_model );
							//std::cout << std::endl;
							open_weak_node( n );
						}
					}	
					continue;
				}

				for ( auto it = succ.begin(); it != succ.end(); it++ ) {
					if ( is_mutex(*it) ) {
						//std::cout << "\t through " << *(a.name) << " results in mutex " << std::endl;
						//it->write( std::cout, m_model );
						//std::cout << " which is mutex" << std::endl;
						continue;
					}
					DNF_Clause ext_succ;
					m_mutex_func.add_consequences( *it, ext_succ );						
					scucs_dq::Node* n = new scucs_dq::Node( ext_succ, i, head->gn + m_model.actions[i]->cost, head );
					compute_hash( n );

					if ( is_closed( n ) ) {
						//std::cout << "\t through " << *(a.name) << " already in CLOSED! " << std::endl;
						delete n;
						continue;
					}
					if( previously_hashed(n) ) {
						//std::cout << "\t through " << *(a.name) << " already in STRONG open! " << std::endl;
						delete n;
					}
					else {
						//std::cout << "\t through " << *(a.name) << " results in STRONG predecessor " << std::endl;
						//std::cout << "\t";
						//it->write( std::cout, m_model );
						//std::cout << std::endl;

						open_strong_node(n);	
					}
				}	

			} 
		}

		bool 		
		is_closed( scucs_dq::Node* n ) 	{ 
			scucs_dq::Node* n2 = this->closed().retrieve(n);
	
			if ( n2 != NULL ) {
				if ( n2->gn <= n->gn ) {
					// The node we generated is a worse path than
					// the one we already found
					return true;
				}
				// Otherwise, we put it into Open and remove
				// n2 from closed
				this->closed().erase( this->closed().retrieve_iterator( n2 ) );
			}
			return false;
		}
		
		bool
		is_goal( const DNF_Clause& c ) {
			bool satisfied = true;
			for ( auto it = c.begin(); it != c.end(); it++ ) {
				if ( m_init.entails( ~(*it) ) ) {
					satisfied = false;
					break;
				}
			}
			return satisfied;
		}
	
		scucs_dq::Node* 		
		get_strong_node() {
			scucs_dq::Node* next = nullptr;
			if(! m_strong_open.empty() ) {
				next = m_strong_open.pop();
				assert( next->hash_table == &m_strong_open_hash );
				assert( m_strong_open_hash.retrieve_iterator( next) != m_strong_open_hash.end() );
				m_strong_open_hash.erase( m_strong_open_hash.retrieve_iterator( next) );
				next->hash_table = nullptr;
			}
			return next;
		}

		scucs_dq::Node*
		get_weak_node() {
			scucs_dq::Node* next = nullptr;
			if(! m_weak_open.empty() ) {
				next = m_weak_open.pop();
				assert( next->hash_table == &m_weak_open_hash );
				assert( m_weak_open_hash.retrieve_iterator( next) != m_weak_open_hash.end() );
				m_weak_open_hash.erase( m_weak_open_hash.retrieve_iterator( next ) );
				next->hash_table = nullptr;
			}
			return next;			
		}

		void	 	
		open_strong_node( scucs_dq::Node *n ) {
			m_strong_open.insert(n);
			assert( m_strong_open_hash.retrieve_iterator( n ) == m_strong_open_hash.end() );
			m_strong_open_hash.put(n);
			/*
			std::cout << "STRONG OPEN STATS:";
			m_strong_open_hash.print_stats( std::cout );
			*/
			assert(  m_strong_open_hash.retrieve_iterator( n ) != m_strong_open_hash.end() );
			inc_gen();
		}

		bool	
		is_mutex( const DNF_Clause& c ) const {
			return m_mutex_func.eval( c ) == infty;
		}

		bool	
		is_mutex( const DNF_Clause& c, const DNF_Clause& d ) const {
			return m_mutex_func.eval( c, d ) == infty;
		}

		bool 			
		previously_hashed( scucs_dq::Node *n ) {
			scucs_dq::Node *previous_copy = NULL;
			if( (previous_copy = m_strong_open_hash.retrieve(n)) ) {
			
				if(n->gn < previous_copy->gn)
				{
					previous_copy->parent = n->parent;
					previous_copy->action_idx = n->action_idx;
					previous_copy->gn = n->gn;
				}
				return true;
			}

			return false;
		}
		
		void	 	
		open_weak_node( scucs_dq::Node *n ) {
			scucs_dq::Node* merged = merge_equivalent_states(n);
			if ( merged == nullptr ) {
				m_weak_open.insert(n);
				assert( m_weak_open_hash.retrieve_iterator( n ) == m_weak_open_hash.end() );

				m_weak_open_hash.put(n);
				/*
				std::cout << "WEAK OPEN STATS:";
				m_weak_open_hash.print_stats( std::cout );
				*/
				assert( m_weak_open_hash.retrieve_iterator( n ) != m_weak_open_hash.end() );

				inc_gen();
				return;
			}
			delete n;
			if ( m_weak_open_hash.retrieve_iterator( merged ) != m_weak_open_hash.end() ) {
				delete merged;
				return;
			}
			m_weak_open.insert(merged);
			assert( m_weak_open_hash.retrieve_iterator( merged ) == m_weak_open_hash.end() );
			m_weak_open_hash.put(merged);
			/*
			std::cout << "WEAK OPEN STATS:";
			m_weak_open_hash.print_stats( std::cout );
			*/
			assert( m_weak_open_hash.retrieve_iterator( merged ) != m_weak_open_hash.end() );
		}
		
		bool 			
		weak_previously_hashed( scucs_dq::Node *n ) {
			scucs_dq::Node *previous_copy = nullptr;
			if( (previous_copy = m_weak_open_hash.retrieve(n)) ) {
			
				if(n->gn < previous_copy->gn)
				{
					previous_copy->parent = n->parent;
					previous_copy->action_idx = n->action_idx;
					previous_copy->gn = n->gn;
				}
				return true;
			}

			return false;
		}

	private:
		const Instance&				m_model;
		UCS_Strong_Open_List			m_strong_open;
		UCS_Weak_Open_List			m_weak_open;
		const Mutex_Heuristic&			m_mutex_func;
		unsigned				m_gen_count;
		unsigned				m_exp_count;
		UCS_Closed_List				m_closed;
		UCS_Closed_List				m_strong_open_hash;
		UCS_Closed_List				m_weak_open_hash;
		scucs_dq::Node*				m_root;
		scucs_dq::Node*				m_solution;
		DNF_Clause				m_init;
		DNF_Clause				m_goal;
		bool					m_get_weak;
	};

}

#endif // ucs.hxx

#ifndef __PDDL_TO_SMV__
#define __PDDL_TO_SMV__

#include <problem.hxx>
#include <string>

namespace grendel {

	class H2_Heuristic;

	void translate_to_smv( const Instance& task, std::string out_filename ); 
	void translate_to_smv( const Instance& task, const H2_Heuristic& h, std::string out_filename );
}


#endif //pddl_to_smv.hxx

#include <pddl_to_smv.hxx>
#include <fstream>
#include <cassert>
#include <algorithm>
#include <h2.hxx>
#include <gp_atoms.hxx>

namespace grendel {

	std::string g_smv_true = "\"T\"";
	std::string g_smv_false = "\"F\"";

	class SMV_Module {
	public:

		SMV_Module( const Instance& task, std::string filename ) 
		: m_task( task ), m_filename( filename ) {
		}

		~SMV_Module() {
		}
		
		void		make_vars();
		void		write() 	const;
		void		write( const H2_Heuristic& h ) 	const;

	protected:
		
		void		write_SMV_header( std::ofstream& out ) const;
		void		write_action_vars( std::ofstream& out ) const;
		void		write_atom_vars( std::ofstream& out ) const;
		void		write_init( std::ofstream& out ) const;
		void		write_preconditions( std::ofstream& out ) const;
		void		write_transition_function( std::ofstream& out ) const;
		void		write_transition_function( const H2_Heuristic& h, std::ofstream& out ) const;

		void		write_evolution_rules( std::ofstream& out ) const;
		void		write_goal( std::ofstream& out ) const;

		void		write_frame_effects( std::vector<int>& frame, std::ofstream& out ) const;

		void		write_action_effect( const Instance::Action& a, unsigned k, std::ofstream& out ) const;

		void		write_action_effect( const H2_Heuristic& h, const Instance::Action& a, unsigned k, std::ofstream& out ) const;

		void		write_conjunction( const index_set& lits, bool next, std::ofstream& out ) const;
		void		write_conjunction( const DNF_Clause& lits, bool next, std::ofstream& out ) const;

		void 		encode_atom_evolution_rule( int action_idx, int atom_idx, std::ofstream& out ) const;
	private:
		const 		Instance& 	m_task;
		std::string			m_filename;
		std::vector< std::string >	m_action_vars;
		std::vector< std::string >	m_atom_vars;
		std::vector< std::string >	m_prec_vars;
	};

	void	
	SMV_Module::make_vars() {

		// Action vars
		for ( auto it = m_task.actions.begin(); it != m_task.actions.end(); it++ ) {
			const Instance::Action& a = **it;
			std::string smv_var_name = "\"" + a.name->to_string() + "\"";
			m_action_vars.push_back( smv_var_name );
			smv_var_name = "\"" + a.name->to_string() + "@pc\"";
			m_prec_vars.push_back( smv_var_name );
		}	

		// Atom vars
		for ( auto it = m_task.atoms.begin(); it != m_task.atoms.end(); it++ ) {
			const Instance::Atom& a = **it;
			std::string smv_var_name = "\"" + a.name->to_string() + "\"";
			m_atom_vars.push_back( smv_var_name );
		}			

	}

	void	
	SMV_Module::write_SMV_header( std::ofstream& out ) const {
		out << "--PROBLEM--" << std::endl;
		out << "MODULE main" << std::endl;
		out << "DOMAINNAME " << m_task.get_domain_name() << std::endl;
		out << "PROBLEMNAME " << m_task.get_problem_name() << std::endl;
		out << "PROBLEMDOMAIN " << m_task.get_domain_name() << std::endl;	
	}

	void	
	SMV_Module::write_action_vars( std::ofstream& out ) const {
		out << "IVAR" << std::endl;
		out << "\taction: {" << std::endl;
		for ( unsigned k = 0; k < m_action_vars.size(); k++ ) {
			out << "\t\t" << m_action_vars[k];
			if ( k < m_action_vars.size() - 1 )
				out << ",";
			out << std::endl;
		}
		out << "\t};" << std::endl;
	}

	void	
	SMV_Module::write_atom_vars( std::ofstream& out ) const {
		out << "VAR" << std::endl;
		// Syntax: <var name>: boolean;
		for ( unsigned k = 0; k < m_atom_vars.size(); k++ ) {
			out << "\t\t" << m_atom_vars[k] << ": boolean;" << std::endl;
		}
	}

	void	
	SMV_Module::write_init( std::ofstream& out ) const {
		// Syntax: ( (<var name 1>=value) & ... & (<var name n>=value) )
		out << "INIT" << std::endl;
		out << "(";
		for ( int p = 0; p < (int)m_task.n_atoms(); p++ ) {
			bool found = false;
			for ( auto it = m_task.init.literals.begin(); it != m_task.init.literals.end() && !found; it++ ) {
				int idx = *it > 0 ? *it-1 : -*it-1;
				found = idx == p;
			}
			const std::string var_name = m_atom_vars[ p ];
			if ( found ) 
				out << " " << var_name << " ";
			else
				out << " !" << var_name << " ";
			if ( p < m_task.n_atoms() - 1 )
				out << " &";
			out << " ";
		}
		out << ")" << std::endl;
	}

	void		
	SMV_Module::write_conjunction( const index_set& lits, bool next, std::ofstream& out ) const {
		unsigned k = 0;	
		for ( auto it = lits.begin(); it != lits.end(); it++ ) {
			int 	atom_idx = *it > 0 ? *it - 1 : -*it - 1;
			bool	truth_value = *it > 0 ? true : false;
			const std::string var_name = m_atom_vars[  atom_idx ];
			if ( !next ) 
				out << ( truth_value ? " " : " !") << var_name;
			else
				out << ( truth_value ? " " : " !") << "next(" << var_name << ")";
			if ( k < lits.size() - 1 )
				out << " &";
			out << " ";
			k++;
		}

	}

	void		
	SMV_Module::write_conjunction( const DNF_Clause& lits, bool next, std::ofstream& out ) const {
		unsigned k = 0;	
		for ( auto it = lits.begin(); it != lits.end(); it++ ) {
			int 	atom_idx = atom(*it);
			bool	truth_value = !sign(*it);
			const std::string var_name = m_atom_vars[  atom_idx ];
			if ( !next ) 
				out << ( truth_value ? " " : " !") << var_name;
			else
				out << ( truth_value ? " " : " !") << "next(" << var_name << ")";
			if ( k < lits.size() - 1 )
				out << " &";
			out << " ";
			k++;
		}

	}

	void	
	SMV_Module::write_preconditions( std::ofstream& out ) const {
		out << "DEFINE" << std::endl;
		// Syntax: <var name>:=( (<var name>=value) <op> ... );
		for ( unsigned k = 0; k < m_task.actions.size(); k++ ) {
			out << m_prec_vars[k] << ":=(";
			const Instance::Action& a = *(m_task.actions[k]);
			write_conjunction( a.precondition, false, out );
			out << ");" << std::endl;
		}
	}
	
	void
	SMV_Module::write_frame_effects( std::vector<int>& frame, std::ofstream& out ) const {
		unsigned k = 0;	
		for ( auto it = frame.begin(); it != frame.end(); it++ ) {
			int 	atom_idx = *it;
			const std::string var_name = m_atom_vars[  atom_idx ];
			out << " ( next(" << var_name << ")=" << var_name << ")";
			if ( k < frame.size() - 1 )
				out << " &";
			out << " ";
			k++;
		}
	}

	void
	SMV_Module::write_action_effect( const Instance::Action& a, unsigned index,  std::ofstream& out ) const {
		assert( a.when.empty() );
		if ( a.ndeffects.empty() ) {
			// Deterministic effects
			std::vector<int> frame_effects;
			if (!a.effect.empty() ) {
				out << "\t\t(" << std::endl;
				out << "\t\t";
				write_conjunction( a.effect, true, out );
				out << "\t\t)" << std::endl;
			}
			for ( int p = 0;  p < m_task.n_atoms(); p++ ) {
				int pos_eff = p+1;
				int neg_eff = -(p+1);
				if ( std::find( a.effect.begin(), a.effect.end(), pos_eff ) == a.effect.end()
					&& std::find( a.effect.begin(), a.effect.end(), neg_eff ) == a.effect.end() )
					frame_effects.push_back( p );
			}
			if (!frame_effects.empty()) {
				out << "\t\t&" << std::endl;
				out << "\t\t(" << std::endl;
				out << "\t\t";
				write_frame_effects( frame_effects, out );
				out << "\t\t)" << std::endl;

			}
		}
		else {
			out <<"\t\t(" << std::endl;
			for ( unsigned k = 0; k < a.ndeffects[0].effs.size(); k++ ) {
				std::vector<int> frame_effects;
				index_set combined =  a.ndeffects[0].effs[k];
				if (combined.empty())
					combined = a.precondition;
				for ( auto it = a.effect.begin(); it != a.effect.end(); it++ ) {
					combined.insert(*it);
				}
				out << "\t\t\t(";
				out << "\t\t\t";
				write_conjunction( combined, true, out );
				out << std::endl;
				out << "\t\t\t)" << std::endl;
				for ( int p = 0;  p < m_task.n_atoms(); p++ ) {
					int pos_eff = p+1;
					int neg_eff = -(p+1);
					if ( std::find( combined.begin(), combined.end(), pos_eff ) == combined.end()
						&& std::find( combined.begin(), combined.end(), neg_eff ) == combined.end() )
						frame_effects.push_back( p );
				}
				if (!frame_effects.empty()) {
					out << "\t\t\t&" << std::endl;
					out << "\t\t\t(" << std::endl;
					out << "\t\t\t";
					write_frame_effects( frame_effects, out );
					out << "\t\t\t)" << std::endl;
				}

				if ( k < a.ndeffects[0].effs.size() - 1 )
					out << "\t\t|" << std::endl;
			}
			out << "\t\t)" << std::endl;
		}
		/*
		if ( !a.when.empty() ) {
			if ( !a.effect.empty() || !a.ndeffects.empty()  )
				out <<"\t\t&" << std::endl;
			for ( unsigned k = 0; k < a.when.size(); k++ ) {
				out << "\t\t\t(" << std::endl;
				out << "\t\t\t";
				write_conjunction( a.when[k].condition, false, out );
				out << std::endl;
				out << "\t\t\t->" << std::endl;
				out << "\t\t\t";
				write_conjunction( a.when[k].effect, true, out );
				out << std::endl;
				out << "\t\t\t)" << std::endl;
				if ( k < a.when.size()-1 )
					out << "&" << std::endl;
			}
		}
		*/
		
	}

	void
	SMV_Module::write_action_effect( const H2_Heuristic& h, const Instance::Action& a, unsigned index,  std::ofstream& out ) const {
		assert( a.when.empty() );
		if ( a.ndeffects.empty() ) {
			// Deterministic effects
			DNF_Clause effects (a.effect);
			DNF_Clause closure_effs;
			h.add_consequences( effects, closure_effs );
			std::vector<int> frame_effects;
			
			if (!a.effect.empty() ) {
				out << "\t\t(" << std::endl;
				out << "\t\t";
				write_conjunction( closure_effs, true, out );
				out << "\t\t)" << std::endl;
			}
			for ( int p = 0;  p < m_task.n_atoms(); p++ ) {
				Lit pos_eff = mkLit( p, false );
				Lit neg_eff = mkLit( p, true );
				if ( !closure_effs.entails(pos_eff) && !closure_effs.entails(neg_eff) )
					frame_effects.push_back( p );
			}
			if (!frame_effects.empty()) {
				out << "\t\t&" << std::endl;
				out << "\t\t(" << std::endl;
				out << "\t\t";
				write_frame_effects( frame_effects, out );
				out << "\t\t)" << std::endl;

			}
		}
		else {
			out <<"\t\t(" << std::endl;
			for ( unsigned k = 0; k < a.ndeffects[0].effs.size(); k++ ) {
				DNF_Clause effects (a.effect);
				if ( a.ndeffects[0].effs[k].empty() ) {
					DNF_Clause prec( a.precondition );
					effects.add(prec);
				}
				else {
					DNF_Clause ndeff( a.ndeffects[0].effs[k] );
					effects.add( ndeff );
				}
				DNF_Clause closure_effs;
				h.add_consequences( effects, closure_effs );
				std::vector<int> frame_effects;

				out << "\t\t\t(";
				out << "\t\t\t";
				write_conjunction( closure_effs, true, out );
				out << std::endl;
				out << "\t\t\t)" << std::endl;
				for ( int p = 0;  p < m_task.n_atoms(); p++ ) {
					Lit pos_eff = mkLit( p, false );
					Lit neg_eff = mkLit( p, true );
					if ( !closure_effs.entails(pos_eff) && !closure_effs.entails(neg_eff) )
						frame_effects.push_back( p );
				}
				if (!frame_effects.empty()) {
					out << "\t\t\t&" << std::endl;
					out << "\t\t\t(" << std::endl;
					out << "\t\t\t";
					write_frame_effects( frame_effects, out );
					out << "\t\t\t)" << std::endl;
				}

				if ( k < a.ndeffects[0].effs.size() - 1 )
					out << "\t\t|" << std::endl;
			}
			out << "\t\t)" << std::endl;
		}
		/*
		if ( !a.when.empty() ) {
			if ( !a.effect.empty() || !a.ndeffects.empty()  )
				out <<"\t\t&" << std::endl;
			for ( unsigned k = 0; k < a.when.size(); k++ ) {
				out << "\t\t\t(" << std::endl;
				out << "\t\t\t";
				write_conjunction( a.when[k].condition, false, out );
				out << std::endl;
				out << "\t\t\t->" << std::endl;
				out << "\t\t\t";
				write_conjunction( a.when[k].effect, true, out );
				out << std::endl;
				out << "\t\t\t)" << std::endl;
				if ( k < a.when.size()-1 )
					out << "&" << std::endl;
			}
		}
		*/
		
	}

	void	
	SMV_Module::write_transition_function( std::ofstream& out ) const {
		out << "TRANS" << std::endl;
		for ( unsigned k = 0; k < m_task.actions.size(); k++ ) {
			out << "(" << std::endl;
			out << "\t( ( action=" << m_action_vars[k] << "  &  " << m_prec_vars[k] << " )" << std::endl;
			out << "\t  ->" << std::endl;
			write_action_effect( *(m_task.actions[k]) , k, out );
			out << "\t)" << std::endl;
			out << "\t&" << std::endl;
			out << "\t( action= " << m_action_vars[k] << "  &  !" << m_prec_vars[k] << " -> FALSE )" << std::endl;
			out << " )" << std::endl;
			if ( k < m_task.actions.size() - 1 )
				out << "&" << std::endl;
		}
	}

	void	
	SMV_Module::write_transition_function( const H2_Heuristic& h, std::ofstream& out ) const {
		out << "TRANS" << std::endl;
		for ( unsigned k = 0; k < m_task.actions.size(); k++ ) {
			out << "(" << std::endl;
			out << "\t( ( action=" << m_action_vars[k] << "  &  " << m_prec_vars[k] << " )" << std::endl;
			out << "\t  ->" << std::endl;
			write_action_effect( h, *(m_task.actions[k]) , k, out );
			out << "\t)" << std::endl;
			out << "\t&" << std::endl;
			out << "\t( action= " << m_action_vars[k] << "  &  !" << m_prec_vars[k] << " -> FALSE )" << std::endl;
			out << " )" << std::endl;
			if ( k < m_task.actions.size() - 1 )
				out << "&" << std::endl;
		}
	}

	void
	SMV_Module::encode_atom_evolution_rule( int action_idx, int atom_idx, std::ofstream& out ) const {

		int p = atom_idx + 1;
		int not_p = -( atom_idx + 1 );

		const Instance::Action& a = *(m_task.actions[action_idx]);
		assert( a.when.empty() );
		// eff(a) entails p
		bool affected = false;
		if ( std::find( a.effect.begin(), a.effect.end(), p ) != a.effect.end() ) {
			assert( std::find( a.effect.begin(), a.effect.end(), not_p ) == a.effect.end() );
			out << "(action = " << m_action_vars[action_idx] << ") & " << m_prec_vars[action_idx] << ": TRUE;" << std::endl;
			affected = true;
		} 
		if ( std::find( a.effect.begin(), a.effect.end(), not_p ) != a.effect.end() ) {
			assert( std::find( a.effect.begin(), a.effect.end(), p ) == a.effect.end() );
			out << "(action = " << m_action_vars[action_idx] << ") & " << m_prec_vars[action_idx] << ": FALSE;" << std::endl;
			affected = true;
		}

		// Easy case, no n.d. effects, no cond. effects
		if ( !affected && a.ndeffects.empty() ) {
			const index_set& pre = a.precondition;
			auto p_it = std::find( pre.begin(), pre.end(), p );
			auto not_p_it = std::find( pre.begin(), pre.end(), not_p );

			if ( p_it == pre.end() && not_p_it == pre.end() )
				return;
			out << "(action = " << m_action_vars[action_idx] << ") & " << m_prec_vars[action_idx] << ":";
			if ( p_it != pre.end() && not_p_it == pre.end() )
				out << " TRUE";
			else if ( p_it == pre.end() && not_p_it == pre.end() ) 
				out << " FALSE";
			else 
				assert( false );
			out << ";" << std::endl;
			return;
		}
		if ( affected || a.ndeffects.empty() ) return;
		std::vector< int > eff_values;

		bool made_true = false;
		bool made_false = false;
		bool unmentioned = false;

		for ( unsigned k = 0; k < a.ndeffects[0].effs.size(); k++ ) {
			index_set eff = ( a.ndeffects[0].effs[k].empty() ? a.precondition :  a.ndeffects[0].effs[k] );
			auto p_it = std::find( eff.begin(), eff.end(), p );
			auto not_p_it = std::find( eff.begin(), eff.end(), not_p );
			if ( p_it == eff.end() && not_p_it == eff.end() ) {
				unmentioned = true;
				const index_set& pre = a.precondition;
				auto p_it = std::find( pre.begin(), pre.end(), p );
				auto not_p_it = std::find( pre.begin(), pre.end(), not_p );
				if ( !made_true && p_it != pre.end() ) {
					eff_values.push_back( 1 );
					made_true = true;
				}
				if ( !made_false && not_p_it != pre.end() ) {
					eff_values.push_back( 0 );
					made_false = true;
				}

			}
			else if ( p_it != eff.end() && not_p_it == eff.end() ) {
				if ( !made_true ) {
					eff_values.push_back( 1 );
					made_true = true;
				}
			}
			else if ( p_it == eff.end() && not_p_it != eff.end() ) {
				if ( !made_false ) {
					eff_values.push_back( 0 );
					made_false = true;
				}
			}
			else
				assert(false); // Inconsistent problem!
		}

		if ( unmentioned & !made_true & !made_false ) {
			const index_set& pre = a.precondition;
			auto p_it = std::find( pre.begin(), pre.end(), p );
			auto not_p_it = std::find( pre.begin(), pre.end(), not_p );
			unmentioned = made_true = made_false = false;

			if ( p_it == pre.end() && not_p_it == pre.end() ) {
				return;
			}
			else if ( p_it != pre.end() && not_p_it == pre.end() ) {
				if ( !made_true ) {
					eff_values.push_back( 1 );
					made_true = true;
				}
			}
			if ( p_it == pre.end() && not_p_it == pre.end() ) {
				if ( !made_false ) {
					eff_values.push_back( 0 );
					made_false = true;
				}
			}
		}
		
		out << "(action = " << m_action_vars[action_idx] << ") & " << m_prec_vars[action_idx] << ":";
		if ( eff_values.size() == 1 ) {
			if ( eff_values[0] == 0 )
				out << " FALSE";
			else if ( eff_values[0] == 1 )
				out << " TRUE";
			else
				out << " " << m_atom_vars[atom_idx];
			out << ";" << std::endl;
		}
		else {

			out << "{";
			for ( unsigned k = 0; k < eff_values.size(); k++ ) {
				if ( eff_values[k] == 0 )
					out << " FALSE";
				else if ( eff_values[k] == 1 )
					out << " TRUE";
				else
					out << " " << m_atom_vars[atom_idx];
				out << " ";
				if ( k < eff_values.size() - 1 )
					out << ",";
			}
			out << "};" << std::endl;
		}
	}

	void
	SMV_Module::write_evolution_rules( std::ofstream& out ) const {
		for ( unsigned atom_idx = 0; atom_idx < m_task.atoms.size(); atom_idx++ ) {
			out << "ASSIGN next(" << m_atom_vars[atom_idx] << ") :=" << std::endl;
			out << "case" << std::endl;
			for ( unsigned k = 0; k < m_task.actions.size(); k++ ) {
				encode_atom_evolution_rule( k, atom_idx, out );
			} 
			out << "1 : " << m_atom_vars[atom_idx] << ";" << std::endl;
			out << "esac;" << std::endl;
		}
	}

	void	
	SMV_Module::write_goal( std::ofstream& out ) const {
		out << "FULL_OBS_STRONG_CYCLIC_GOAL" << std::endl;
		// Syntax: ( (<var name 1>=value) & ... & (<var name n>=value) )
		out << "(";
		unsigned k = 0;
		for ( auto it = m_task.goal_literals.begin(); it != m_task.goal_literals.end(); it++ ) {
			int 	atom_idx = *it > 0 ? *it - 1 : -*it - 1;
			bool	truth_value = *it > 0 ? true : false;
			const std::string var_name = m_atom_vars[  atom_idx ];
			out << ( truth_value ? " " : " !" ) << var_name;
			if ( k < m_task.goal_literals.size() - 1 )
				out << " &";
			out << " ";
			k++;
		}
		out << ")" << std::endl;

	}

	void	
	SMV_Module::write() const {
		std::ofstream	output_stream( m_filename.c_str() );

		write_SMV_header( output_stream );
		write_action_vars( output_stream );
		write_atom_vars( output_stream );
		write_init( output_stream );
		write_preconditions( output_stream );
		write_transition_function( output_stream );
		//write_evolution_rules( output_stream );
		write_goal( output_stream );
	}

	void	
	SMV_Module::write(const H2_Heuristic& h ) const {
		std::ofstream	output_stream( m_filename.c_str() );

		write_SMV_header( output_stream );
		write_action_vars( output_stream );
		write_atom_vars( output_stream );
		write_init( output_stream );
		write_preconditions( output_stream );
		write_transition_function( h, output_stream );
		//write_evolution_rules( output_stream );
		write_goal( output_stream );
	}

	void 
	translate_to_smv( const Instance& task, std::string out_filename ) {

		SMV_Module module( task, out_filename );
	
		module.make_vars();		

		module.write();
	}

	void
	translate_to_smv( const Instance& task, const H2_Heuristic& h, std::string out_filename  ) {
		SMV_Module module( task, out_filename );
	
		module.make_vars();		

		module.write(h);

	}
}

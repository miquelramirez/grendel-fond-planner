#ifndef __STRONG_CYCLIC_UNIFORM_COST_SEARCH__
#define __STRONG_CYCLIC_UNIFORM_COST_SEARCH

#include <problem.hxx>
#include <gp_atoms.hxx>
#include <nd_regression.hxx>
#include <open_list.hxx>
#include <ext_math.hxx>
#include <ex_policy.hxx>
#include <closed_list.hxx>
#include <hash_table.hxx>
#include <nd_regression.hxx>

namespace grendel {

	namespace scucs {
		class Node {
		public:

			typedef DNF_Clause	State_Type;
			
			DNF_Clause			state;
			int				action_idx;
			float				gn;
			Node*				parent;
			size_t				hash_key;
			int 				trials;
			
	
			Node( const DNF_Clause& c, int _action_idx, float _gn = 0.0f, Node* _parent = nullptr ) 
			: state(c), action_idx( _action_idx ),  gn(_gn), parent(_parent), trials(0) {
					
				aptk::Hash_Key hasher;
				for ( auto it = c.begin(); it != c.end(); it++ )
					hasher.add( toInt(*it) );
				hash_key = (size_t)hasher;
			}

			~Node() {
				gn = -1;
			}

			size_t	hash( ) const {
				return hash_key;
			}

			bool 	operator==( const Node& o ) const {
				return state == o.state;
			}
		};
	
		class Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				return aptk::dless( b->gn, a->gn );
			}
		};

		class Weak_Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				if ( b->trials < a->trials ) return false;
				if ( b->trials == a->trials ) 
					return aptk::dless( b->gn, a->gn );
				return true;
			}
		};


	}

	template <typename Mutex_Heuristic>
	class Strong_Cyclic_Uniform_Cost_Search {

		typedef  Open_List< scucs::Comparer, scucs::Node > 	UCS_Open_List;
		typedef  Open_List< scucs::Weak_Comparer, scucs::Node >	UCS_Weak_Open_List;
		typedef  aptk::search::Closed_List< scucs::Node >		UCS_Closed_List;

	public:
		Strong_Cyclic_Uniform_Cost_Search( const Instance& model, const Mutex_Heuristic& h ) 
		: m_model( model ), m_mutex_func( h ), m_gen_count(0), m_exp_count(0) {

			for ( grendel::Atom p = 0; p < (int)m_model.n_atoms(); p++ ) {
				bool found = false;
				for ( auto init_it = m_model.init.literals.begin();
					init_it != m_model.init.literals.end() && !found; init_it++ ) {
					int idx = *init_it > 0 ? *init_it-1 : -*init_it-1;
					found = idx == p;
				}
					
				if ( found ) 
					m_init.add( mkLit(p,false) );
				else
					m_init.add( mkLit(p,true) );
			}

			m_goal = DNF_Clause( m_model.goal_literals );

		}

		~Strong_Cyclic_Uniform_Cost_Search() {
			for ( typename UCS_Closed_List::iterator i = m_closed.begin();
				i != m_closed.end(); i++ ) {
				delete i->second;
			}
			
			while ( !m_open.empty() ) {
				scucs::Node* n = m_open.pop();
				delete n;
			}

			m_closed.clear();
			m_open_hash.clear();
		}

		void	start() {
			DNF_Clause ext_goal;
			m_mutex_func.add_consequences( m_goal, ext_goal );
			m_root = new scucs::Node( ext_goal, -1 );
			m_solution = nullptr;
			open_subgoal_node( m_root );
		}

		bool	
		find_strong_cyclic_plan( Explicit_Policy& plan ) {
			bool solved = false;
			scucs::Node* head = get_subgoal();
			scucs::Node* last = nullptr;

			while ( head != nullptr ) {
				if ( !m_open.empty() ) {
					while ( !m_open.empty() ) {
						scucs::Node* n = m_open.pop();
						assert( !check_delete_precondition(n) );
						delete n;
					}
				
					m_open_hash.clear();
				}
				std::cout << "Expanding potential strong cyclic: ";
				head->state.write( std::cout, m_model);
				std::cout << " g(n) = " << head->gn << " trials(n) = " << head->trials;
					if ( head->action_idx == -1 ) 
						std::cout << " -> (GOAL)" << std::endl;
					else
						std::cout << " -> " << *(m_model.actions[head->action_idx]->name) << std::endl;
				
				if ( head->state.satisfies(m_goal) ) {
					open_node( head );
					scucs::Node* successful_execution = do_search();
					if ( successful_execution != nullptr ) {
						solved = true;
						break;
					}
				}
				else {
					std::vector<scucs::Node*> cases;
					bool 	delete_original = false;

					bool all_succ_strong = check_successors( head, cases, delete_original );
					if ( all_succ_strong ) {
						std::cout << "All successors are strong " << std::endl;
						for ( auto it = cases.begin(); it != cases.end(); it++ )
							open_node( *it );
	
						std::cout << "Considering " << cases.size() << " cases" << std::endl;
						delete head;
	
						scucs::Node* successful_execution = do_search();
						if ( successful_execution != nullptr ) {
							solved = true;
							break;
						}
					}
					else {
						std::cout << "Delaying expansion" << std::endl;
						head->trials++;
						assert( m_open_subgoals_hash.retrieve( head ) == nullptr );
						open_subgoal_node( head );
						last = head;
					}
				}
				head = get_subgoal();
				if ( last == head )
					return false;
			}
			
			extract_plan( plan );

			return solved;
		} 

		void		inc_gen() { m_gen_count++; }
		void		inc_exp() { m_exp_count++; }

		unsigned	generated() const { return m_gen_count; }
		unsigned	expanded() const { return m_exp_count; }

		void		
		extract_plan( Explicit_Policy& plan ) {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				plan.add( it->second->state, it->second->action_idx );
			}
		}

	protected:

		bool
		check_delete_precondition(scucs::Node* n ) const {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				if ( it->second == n ) return true;
			}
			for ( auto it = m_open_hash.begin(); it != m_open_hash.end(); it++ ) {
				if ( it->second == n ) return true;
			}
			for ( auto it = m_open_subgoals_hash.begin(); it != m_open_subgoals_hash.end(); it++ ) {
				if ( it->second == n ) return true;
			}
			return false;
		}

		void 			
		close( scucs::Node* n ) {  
			m_closed.put(n); 
		}
	
		UCS_Closed_List&	
		closed() { 
			return m_closed; 
		}
	
		UCS_Closed_List&	
		open_hash() 	{ 
			return m_open_hash; 
		}

		bool
		check_successors( scucs::Node* n, std::vector<scucs::Node* >& replacements, bool& delete_node )  {
			const Instance::Action& a = *(m_model.actions[n->action_idx]);
			assert( !a.ndeffects.empty() ); // predecessors of det actions shouldn't turn up here
			const size_t num_effs = a.ndeffects[0].effs.size();
			DNF_Clause   res_state;
		
	
			std::vector< DNF_Clause > forced_lits;
			for ( unsigned k = 0; k < num_effs; k++ )
			{
				res_state.clear();	
				bool res = n->state.apply( a, k, res_state );
				assert( res ); // actions should be applicable!
				std::cout << "Checking successor :" << std::endl;
				res_state.write( std::cout, m_model );
				std::cout << std::endl;
			
				if ( !implied_by_closed( res_state, forced_lits ) ) {
					// Checking whether there exists a weak plan with h^2 might
					// be sufficient
					return false;
				}
			}
			bool all_empty = true;
			for ( auto it = forced_lits.begin(); it != forced_lits.end(); it++ ) {
				if ( !it->empty() ) {
					all_empty = false;
					break;
				}
			}
			if ( all_empty ) {
				std::cout << "All cases are empty: this SHOULD NOT BE HAPPENING!" << std::endl;
				return false;
			}

			// Check that none of the forced conjunctions yields a mutex state!
			for ( auto it = forced_lits.begin(); it != forced_lits.end(); it++ ) {
				if ( is_mutex( n->state, *it ) ) {
					std::cout << "State is mutex with forced literal set: ";
					it->write( std::cout, m_model );
					std::cout << std::endl;
					return false;	
				}
			}
			// Create new nodes
			delete_node = true;
			for ( auto it = forced_lits.begin(); it != forced_lits.end(); it++ ) {
				if ( it->empty() ) 
					continue;
				DNF_Clause new_state(n->state);
				new_state.add( *it );
				DNF_Clause ext_new_state;
				m_mutex_func.add_consequences( new_state, ext_new_state );
				scucs::Node* n2 = new scucs::Node( ext_new_state, n->action_idx, n->gn, n->parent ); 
				replacements.push_back( n2 );
			}		

			
			return true;
		}
		
		bool
		implied_by_closed( const DNF_Clause& c, std::vector<DNF_Clause>& missing ) const {
			DNF_Clause tmp;
			bool implied = false;
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				tmp.clear();
				if ( it->second->state.implies( c, tmp ) ) {
					if ( is_mutex( c, tmp ) ) continue;
					std::cout << "\t\tImplied by: " << std::endl;
					std::cout << "\t\t";
					it->second->state.write( std::cout, m_model );
					if ( it->second->action_idx == -1 ) 
						std::cout << " -> (GOAL)" << std::endl;
					else
						std::cout << " -> " << *(m_model.actions[it->second->action_idx]->name) << std::endl;
					std::cout << "\tMissing lits: ";
					tmp.write( std::cout, m_model );
					std::cout << std::endl;
					missing.push_back( tmp );
					implied = true;
				}
			}
			return implied;
		}

		scucs::Node*	
		do_search() {
			scucs::Node* head = get_node();
			while(head) {
	
				if(is_goal(head->state)) {
					std::cout << "Init reached ";
					head->state.write( std::cout, m_model );
					std::cout << ", execution length is: " << head->gn << std::endl;
					close(head);
					return head;
				}
		
				close(head);
				process(head);
				head = get_node();
			}
			return nullptr;			
		}

		void 			
		process(  scucs::Node *head ) {

			inc_exp();

			std::cout << "Expanding strong cyclic: ";
			head->state.write( std::cout, m_model);
			std::cout << " g(n) = " << head->gn << std::endl;

			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {	

				std::vector<DNF_Clause>  succ;
				lbool res = strong_cyclic_regress( m_model, i, head->state, succ );

				const Instance::Action& a = *m_model.actions[i];
				
				assert( res != l_Undef );
				
				if ( res == l_False ) {
					//std::cout << "\t through " << *(a.name) << " results in FALSE" << std::endl;
					for ( auto it = succ.begin(); it != succ.end(); it++ ) {
						if ( is_mutex(*it) ) {
							//std::cout << "\t through " << *(a.name) << " results in ";
							//it->write( std::cout, m_model );
							//std::cout << " which is mutex" << std::endl;
							continue;
						}

						DNF_Clause ext_succ;
						m_mutex_func.add_consequences( *it, ext_succ );						

						scucs::Node* n = new scucs::Node( ext_succ, i, head->gn + m_model.actions[i]->cost, head );
						if ( is_closed( n ) ) {
							delete n;
							continue;
						}
						if( subgoal_previously_hashed(n) ) {
							delete n;
						}
						else {
							std::cout << "\t through " << *(a.name) << " results in new weak predecessor ";
							it->write( std::cout, m_model );
							std::cout << std::endl;
	
							open_subgoal_node(n);	
						}
					}	
					continue;
				}

				for ( auto it = succ.begin(); it != succ.end(); it++ ) {
					if ( is_mutex(*it) ) {
						//std::cout << "\t through " << *(a.name) << " results in ";
						//it->write( std::cout, m_model );
						//std::cout << " which is mutex" << std::endl;
						continue;
					}
					DNF_Clause ext_succ;
					m_mutex_func.add_consequences( *it, ext_succ );						
					scucs::Node* n = new scucs::Node( ext_succ, i, head->gn + m_model.actions[i]->cost, head );

					if ( is_closed( n ) ) {
						delete n;
						continue;
					}
					if( previously_hashed(n) ) {
						delete n;
					}
					else {
						std::cout << "\t through " << *(a.name) << " results in strong predecessor ";
						it->write( std::cout, m_model );
						std::cout << std::endl;

						open_node(n);	
					}
				}	

			} 
		}

		bool 		
		is_closed( scucs::Node* n ) 	{ 
			scucs::Node* n2 = this->closed().retrieve(n);
	
			if ( n2 != NULL ) {
				if ( n2->gn <= n->gn ) {
					// The node we generated is a worse path than
					// the one we already found
					return true;
				}
				// Otherwise, we put it into Open and remove
				// n2 from closed
				this->closed().erase( this->closed().retrieve_iterator( n2 ) );
			}
			return false;
		}
		
		bool
		is_goal( const DNF_Clause& c ) {
			bool satisfied = true;
			for ( auto it = c.begin(); it != c.end(); it++ ) {
				if ( m_init.entails( ~(*it) ) ) {
					satisfied = false;
					break;
				}
			}
			return satisfied;
		}
	
		scucs::Node* 		
		get_node() {
			scucs::Node* next = nullptr;
			if(! m_open.empty() ) {
				next = m_open.pop();
				assert( m_open_hash.retrieve_iterator( next) != m_open_hash.end() );
				m_open_hash.erase( m_open_hash.retrieve_iterator( next) );
			}
			return next;
		}

		scucs::Node*
		get_subgoal() {
			scucs::Node* next = nullptr;
			if(! m_open_subgoals.empty() ) {
				next = m_open_subgoals.pop();
				assert( m_open_subgoals_hash.retrieve_iterator( next) != m_open_subgoals_hash.end() );
				m_open_subgoals_hash.erase( m_open_subgoals_hash.retrieve_iterator( next) );
			}
			return next;			
		}

		void	 	
		open_node( scucs::Node *n ) {
			m_open.insert(n);
			m_open_hash.put(n);
			inc_gen();
		}

		bool	
		is_mutex( const DNF_Clause& c ) const {
			return m_mutex_func.eval( c ) == infty;
		}

		bool	
		is_mutex( const DNF_Clause& c, const DNF_Clause& d ) const {
			return m_mutex_func.eval( c, d ) == infty;
		}

		bool 			
		previously_hashed( scucs::Node *n ) {
			scucs::Node *previous_copy = NULL;
			if( (previous_copy = m_open_hash.retrieve(n)) ) {
			
				if(n->gn < previous_copy->gn)
				{
					previous_copy->parent = n->parent;
					previous_copy->action_idx = n->action_idx;
					previous_copy->gn = n->gn;
				}
				return true;
			}

			return false;
		}
		
		void	 	
		open_subgoal_node( scucs::Node *n ) {
			m_open_subgoals.insert(n);
			m_open_subgoals_hash.put(n);
		}
		
		bool 			
		subgoal_previously_hashed( scucs::Node *n ) {
			scucs::Node *previous_copy = NULL;
			if( (previous_copy = m_open_subgoals_hash.retrieve(n)) ) {
			
				if(n->gn < previous_copy->gn)
				{
					previous_copy->parent = n->parent;
					previous_copy->action_idx = n->action_idx;
					previous_copy->gn = n->gn;
				}
				return true;
			}

			return false;
		}

	private:
		const Instance&				m_model;
		UCS_Open_List				m_open;
		UCS_Weak_Open_List			m_open_subgoals;
		const Mutex_Heuristic&			m_mutex_func;
		unsigned				m_gen_count;
		unsigned				m_exp_count;
		UCS_Closed_List				m_closed;
		UCS_Closed_List				m_open_hash;
		UCS_Closed_List				m_open_subgoals_hash;
		scucs::Node*				m_root;
		scucs::Node*				m_solution;
		DNF_Clause				m_init;
		DNF_Clause				m_goal;
	};

}

#endif // ucs.hxx

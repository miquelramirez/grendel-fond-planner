#include <smv_patrizi.hxx>
#include <sstream>

namespace grendel {

	const std::string g_start_action = "smv_start_action";
	const std::string g_stop_action = "smv_stop_action";

	void	
	Patrizi_SMV_Module::make_vars() {

		// Action vars
		for ( auto action_ptr : m_task.actions ) {
			const FOND_Model::Action& a = *action_ptr;
			std::string smv_var_name = "\"" + a.name + "\"";
			m_action_vars.push_back( smv_var_name );
			m_action_effs.push_back( std::vector<std::string>() );
			for ( unsigned i = 0; i < a.effects.size(); i++ ) {
				std::string smv_eff_name =  "\"" + a.name + "@eff"
				std::stringstream buffer;
				buffer << "_" << i;
				smv_eff_name += buffer.str();	
				smv_eff_name += "\"";
				m_action_effs.back().push_back( smv_eff_name );
			}
			smv_var_name = "\"" + a.name + "@pc\"";
			m_prec_vars.push_back( smv_var_name );
		}	

		// Atom vars
		for ( auto atom_ptr : m_task.atoms ) {
			const FOND_Model::Atom& a = *atom_ptr;
			std::string smv_var_name = "\"" + a.name + "\"";
			m_atom_vars.push_back( smv_var_name );
		}			

	}

	void
	Patrizi_SMV_Module::write_main_module( std::ostream& os ) const {
		os << "MODULE main" << std::endl;
		os << "\tVAR" << std::endl;
		os << "\t\tenvironment : system environment_module(agent);" << std::endl;
		os << "\t\tagent : system system_module(environment);" << std::endl;
		os << "\tDEFINE" << std::endl;
		os << "\t\tjx := agent.jx;" << std::endl;
		os << "-- end main " << std::endl;
	}

	void
	Patrizi_SMV_Module::write_action_list( std::ostream& os, std::string prefix ) const {
		unsigned k = 0;
		os << prefix;
		for ( auto act_val : m_action_vars ) {
			os << act_val << ", ";
			if ( !((k+1) % 5) && k < m_action_vars.size()-1 )
				os << std::endl << prefix;
			k++;
		}
		os << g_stop_action << "," << std::endl;
		os << g_start_action; 
	}

	void
	Patrizi_SMV_Module::write_smv_system( std::ostream& os ) const {
		os << "MODULE system_module(env)" << std::endl;
		os << "\tVAR" << std::endl;
		os << "\taction : {" << std::endl;
		write_smv_action_list( os, "\t\t\t\t" );
		os << "\t};" << std::endl;
		os << "\t\tjx: 1..1;" << std::endl;
		os << "\tINIT" << std::endl;
		os << "\t\taction = " << g_start_action << std::endl;
		os << "\tTRANS" << std::endl;
		os << "-- Special actions:" << std::endl;
		os << "\t\t" << "next(action) != " << g_start_action << " &" << std::endl;
		os << "\t\t" << "((next(action) = " << g_stop_action << ") <-> " << std::endl;
		if ( m_task.goal.empty() ) {
			os << "\t\t\t" << "TRUE";
		}
		else {
			os << "\t\t\t" << "(";
			for ( unsigned k = 0; k < m_task.goal.size(); k++ ) {
				Lit l = m_task.goal[k];
				os << sign(l) ? "!" : "";
				os << "next(env." << m_atom_vars[atom(l)] << ")";
				if ( k < m_task.goal.size() -1 )
					os << " & ";
			}
			os << ")" << std::endl;
		}
		os << "\t\t" << ")&" << std::endl;
		os << "\t\t" << "-- Action preconditions" << std::endl;
		os << "\t\t" << "case" << std::endl;
		for ( unsigned i = 0; i < m_task.actions.size(); i++ ) {
			auto a = m_task.actions[i];
			if ( a->precondition.empty() ) continue;
			os << "\t\t\t" << "next(action) = " << m_action_vars[i] << " : ";
			for ( unsigned k = 0; k < a->precondition.size(); k++ ) {
				Lit l = a->precondition[k];
				os << sign(l) ? "!" : "";
				os << "next(env." << m_atom_vars[atom(l)] << ")";
				if ( k < a->precondition.size()-1 )
					os << " & ";
			}	
			os << ";" << std::endl;
		}
		os << "\t\t" << "TRUE: TRUE; -- for special actions" << std::endl;
		os << "\t\t" << "esac" << std::endl;
		os << "\t\t" << "-- justice requirements: problem goal" << std::endl;
		os << "\t\t" << "JUSTICE" << std::endl;
		if ( m_task.goal.empty() ) {
			os << "\t\t\t" << "TRUE";
		}
		else {
			os << "\t\t\t" << "(";
			for ( unsigned k = 0; k < m_task.goal.size(); k++ ) {
				Lit l = m_task.goal[k];
				os << sign(l) ? "!" : "";
				os << "next(env." << m_atom_vars[atom(l)] << ")";
				if ( k < m_task.goal.size() -1 )
					os << " & ";
			}
			os << ")" << std::endl;
		}
		os << "-- end system_module" << std::endl;
	
	}

	void
	Patrizi_SMV_Module::write_smv_environment( std::ostream& os ) const {
		os << "MODULE environment_module(ag)" << std::endl;
		os << "\tVAR" << std::endl;
		os << "\t\t" << "-- domain fluents" << std::endl;
		for ( auto atom : m_atom_vars ) {
			os << "\t\t\t" << atom << " : boolean;" << std::endl;
		}
		os << "\t\t" << "-- effects" << std::endl;
		os << "\t\t\t" << "init@eff" << " : boolean;" << std::endl;
		for ( auto eff_list : m_eff_vars ) 
			for ( auto eff_var : eff_list )
				os << "\t\t\t" << eff_var << " : boolean;" << std::endl;
		os << std::endl;
		os << "\tINIT" << std::endl;
		os << "\t\t" << "-- all fluents are initially set to false. They will be correctly initialized at the first step by smv_start_action" << std::endl;
		os << "\t\t\t";
		unsigned counter = 0;
		for ( auto atom: m_atom_vars ) {
			os << "!" << atom;
			if ( counter < m_atom_vars.size() -1 )
				os << " & ";
			if ( (counter+1)%5 == 0 )
				os << std::endl << "\t\t\t";
			counter++;
		}
		for ( auto eff_list : m_eff_vars ) 
			for ( auto eff_var : eff_list ) {
				os << "!" << eff_var << std::endl;
			}
	
		os << std::endl << std::endl;
		os << "TRANS" << std::endl;
		os << "\t-- effect rule encoding" << std::endl;
		for ( unsigned i = 0; i < m_task.actions.size(); i++ ) {
			auto ai = m_task.actions[i];
			for ( unsigned j = 0; j < m_eff_vars[i].size(); j++ ) {
				os << "\t\t\t" << "(";
				if ( ai.effects[j].condition.emtpy() ) {
					os << "next(" << m_eff_vars[i][j] << ")";
				}
				else {
					os << "(" << "next(" << m_eff_vars[i][j] << ")";
					os << " & ";
					for ( unsigned k = 0; k < ai.effects[j].condition.size(); k++ ) {
							os << sign(ai.effects[j].condition[k]) ? "!" : "";
							os << m_atom_vars[atom( ai.effects[j].condition[k] )];
						if ( k < ai.effects[j].condition.size() - 1 )
							os << " & ";
					}
					os << ")";
				}
				os << "->" << std::endl;
				os << "\t\t\t\t" << "(";
				for ( unsigned k = 0; k < ai.effects[j].effect.size(); k++ ) {
					os << sign(ai.effects[j].effect[k]) ? "!" : "";
					os << "next(" <<  m_atom_vars[atom( ai.effects[j].effect[k] )] << ")";
					if ( k < ai.effects[j].effect.size() - 1 )
						os << " & ";
				}
				os << ")" << std::endl;
	
				os << "\t\t\t" << ")" << std::endl;
				os << "\t\t\t" << " & " << std::endl;
			}
		}
		os << "\t-- actions trigger effects" << std::endl;
		os << "\t\t\tcase" << std::endl;
		for ( unsigned i = 0; i < m_task.actions.size(); i++ ) {
			os << "\t\t\tag.action = " << m_actions_vars[i] << " : ";
			// non-deterministic effects
			os << "{";
			os << "} & ";
			os << ";" << std::endl
		}
		os << "\t\t\tesac";
		os << "\tJUSTICE" << std::endl;
		os << "\t\tTRUE" << std::endl;
		os << "-- end environment_module" << std::endl;
	}

	void
	Patrizi_SMV_Module::write() const {

		std::ofstream	stream( m_filename.c_str() );
		
		write_main_module( stream );
		stream << std::endl;
		
		write_smv_system( stream );
		stream << std::endl;

		write_smv_environment( stream );
		stream << std::endl;
	}	

}

#ifndef __SMV_PATRIZI__
#define __SMV_PATRIZI__

#include <fond_inst.hxx>
#include <gp_atoms.hxx>

namespace grendel {
	
	class Patrizi_SMV_Module {
	public:
		
		Patrizi_SMV_Module( const FOND_Model& task, std::string filename ) 
		: m_task( task ), m_filename( filename ) {
		}

		~Patrizi_SMV_Module() {
		}
		
		void		make_vars();
		void		write() 	const;

	protected:

		void		write_main_module( std::ostream& os ) const;
		void		write_smv_system( std::ostream& os ) const;
		void		write_smv_environment( std::ostream& os ) const;
		void		write_action_list( std::ostream& os, std::string prefix ) const;

		
	private:
		const 		FOND_Model& 			m_task;
		std::string					m_filename;
		std::vector< std::string >			m_action_vars;
		std::vector< std::string >			m_atom_vars;
		std::vector< std::string >			m_prec_vars;
		std::vector< std::vector< std::string > >	m_eff_vars;
		
	};

}

#endif // smv_patrizi.hxx

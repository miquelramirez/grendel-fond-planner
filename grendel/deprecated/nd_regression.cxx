#include <nd_regression.hxx>
#include <cassert>
#include <algorithm>

namespace grendel {
	enum 	NodeType { NOT, AND, OR, LIT, VAL };

	class 	Node {
	public:
		NodeType	type;
		Node*		lhs;
		Node*		rhs;
		Lit		lit;
		lbool		value;

		// NOT, AND, OR
		Node( NodeType _type )
		: type( _type ), lhs( nullptr ), rhs( nullptr ), value( l_Undef ) {
		
		}
		
		Node( Lit l )
		: type( LIT ), lhs( nullptr ), rhs( nullptr ), lit(l), value( l_Undef ) {
		}

		Node( lbool v )
		: type( VAL ), lhs( nullptr ), rhs( nullptr ), value(v) {
		}

		Node( const Node& n ) {
			type = n.type;
			lit = n.lit;
			value = n.value;
			lhs = rhs = nullptr;
		}

		void	promote( NodeType _type ) {
			type = _type;
		}

		void	promote( Lit l ) {
			type = LIT;
			lit = l;
			assert( rhs == nullptr );
			assert( lhs == nullptr );
		}

		void	promote( lbool v ) {
			type = VAL;
			value = v;
			assert( rhs == nullptr );
			assert( lhs == nullptr );
		}
		
	};

	class Regression_Formula {
	public:
		Regression_Formula( const Instance::Action& a, unsigned nd_eff_idx, const DNF_Clause& rgr );
		Regression_Formula( const Instance::Action& a, const DNF_Clause& rgr );
		Regression_Formula( NodeType op, Regression_Formula& negated );
		Regression_Formula( NodeType op, Regression_Formula& lhs, Regression_Formula& rhs );

		~Regression_Formula();

		void expand();
		
		void simplify();

		void simplify( DNF_Clause& final );

		Node* copy( Regression_Formula& f );	
	
		Node* 	make_node_from_lit( Lit l, Node* parent = nullptr, bool is_lhs = true );

		void	make_conjunctive_subformula( DNF_Clause& c, Node* parent );

		void	substitute_terms_by_expansion_formula( 	std::vector< Node* >& terms_to_subst,
								std::vector< Node* >& terms_to_expand );

		void	expand_evaluation_formulas( std::vector< Node* >& terms_to_expand );

		size_t	size() const { return count_symbols(); }

		void	write( std::ostream& os, const Instance& i ) const;

		void	to_NNF();
		
		void	to_DNF();
	
		void	to_CNF();

		lbool	extract_clauses( std::vector< DNF_Clause > & clauses );

	protected:

		Node* 	simplify_rec( Node* n );

		size_t 	count_symbols() const { return ( m_f_nodes.empty() ? 0 : count_symbols_rec( m_f_nodes[0] ) ); }

		size_t	count_symbols_rec( Node* n ) const;

		void	write_rec( std::ostream& os, const Instance& i, Node* n, int level ) const;

		Node*	NNF( Node* n );
	
		Node*	DNF( Node* n );

		Node*	CNF( Node* n );

		Node*	copy_rec( Node* orig );

		Node*	distribute_DNF( Node* n1, Node* n2 );

		Node* 	distribute_CNF( Node* n1, Node* n2 );

		void	extract_clause( Node* n, DNF_Clause& c );

		void	extract_clauses( Node* n, std::vector< DNF_Clause >& clauses );
	private:
		
		DNF_Clause 							m_regressed;
		DNF_Clause							m_precondition;
		DNF_Clause							m_conj_effect;
		std::vector< std::pair< DNF_Clause, DNF_Clause > >		m_cond_effects;
		DNF_Clause							m_nd_conj_effect;
		std::vector< Node* >						m_f_nodes;
	};

	Regression_Formula::Regression_Formula(	 const Instance::Action& a, 
						unsigned nd_eff_idx, 
						const DNF_Clause& rgr ) 
	: m_regressed( rgr ), 
	m_precondition( a.precondition ),
	m_conj_effect( a.effect ), 
	m_nd_conj_effect( a.ndeffects[0].effs[nd_eff_idx] ) {

		for ( auto it = a.when.begin(); it != a.when.end(); it++ ) {
			m_cond_effects.push_back( 
				std::make_pair( DNF_Clause( it->condition ), DNF_Clause( it->effect ) ) );
		}

	}

	Regression_Formula::Regression_Formula(	 const Instance::Action& a, 
						const DNF_Clause& rgr ) 
	: m_regressed( rgr ), 
	m_precondition( a.precondition ),
	m_conj_effect( a.effect ) {

		for ( auto it = a.when.begin(); it != a.when.end(); it++ ) {
			m_cond_effects.push_back( 
				std::make_pair( DNF_Clause( it->condition ), DNF_Clause( it->effect ) ) );
		}

	}

	Regression_Formula::Regression_Formula( NodeType op, Regression_Formula& f ) {
		assert( op == NOT );
		Node* root = new Node(NOT);
		m_f_nodes.push_back( root );
		Node* lhs = copy( f );
		assert( lhs != nullptr );
		root->lhs = lhs;
		simplify();	
	}

	Regression_Formula::Regression_Formula( NodeType op, Regression_Formula& lhs_f, Regression_Formula& rhs_f ) {
		assert( op == AND || op == OR );
		Node* root = new Node(op);
		m_f_nodes.push_back( root );
		Node* lhs = copy( lhs_f );
		assert( lhs != nullptr );
		root->lhs = lhs;
		Node* rhs = copy( rhs_f );
		assert( lhs != nullptr );
		root->rhs = rhs;
		simplify();	
	}

	Regression_Formula::~Regression_Formula() {
		for ( auto it = m_f_nodes.begin(); it != m_f_nodes.end(); it++ )
			delete *it;
	}

	Node*	Regression_Formula::copy( Regression_Formula& f ) {
		if ( f.m_f_nodes.empty() ) return nullptr;
		return copy_rec( f.m_f_nodes[0] );	
	}

	Node*	Regression_Formula::copy_rec( Node* orig ) {
		if ( orig == nullptr ) return nullptr;
		Node* n = new Node( *orig );
		m_f_nodes.push_back( n );
		n->lhs = copy_rec( orig->lhs );
		n->rhs = copy_rec( orig->rhs );
		return n;
	}

	void	Regression_Formula::to_NNF() {
		if ( m_f_nodes.empty() ) return;
		if ( m_f_nodes[0]->type == VAL )
			return; // already in DNF

		NNF( m_f_nodes[0] );
	}

	void	Regression_Formula::to_DNF() {
		if ( m_f_nodes.empty() ) return;
		if ( m_f_nodes[0]->type == VAL )
			return; // already in DNF
		to_NNF();
		DNF( m_f_nodes[0] );
	}

	Node*	Regression_Formula::NNF( Node* n ) {

		assert( n != nullptr );		
		assert( n->type != VAL ); // there shouldn't be any values around
		
		if ( n->type == LIT ) 
			return n;
		if ( n->type == NOT ) {

			if ( n->lhs->type == LIT ) 
				return n;
			if ( n->lhs->type == NOT )
				return NNF( n->lhs->lhs );
			if ( n->lhs->type == AND || n->lhs->type == OR  ) {
				Node* result = n->lhs;

				result->type = ( result->type == OR ? AND : OR );

				Node* old_lhs = result->lhs;
				Node* old_rhs = result->rhs;				

				result->lhs = new Node( NOT );
				m_f_nodes.push_back( result->lhs );

				result->lhs->lhs = old_lhs;
				result->lhs = NNF( result->lhs );

				result->rhs = new Node( NOT );
				m_f_nodes.push_back( result->rhs );
				
				result->rhs->lhs = old_rhs;
				result->rhs = NNF( result->rhs );

				return result;	
			}

			assert( false ); // it's a value!
		}

		assert( n->type == AND || n->type == OR );
		n->lhs = NNF( n->lhs );
		n->rhs = NNF( n->rhs );		

		return n;
	}

	Node* 	Regression_Formula::DNF( Node* n ) {
		assert( n->type != VAL );
		if ( n->type == LIT || n->type == NOT ) return n;
		if ( n->type == OR ) {
			n->lhs = DNF( n->lhs );
			n->rhs = DNF( n->rhs );
			return n;
		}
		assert( n->type == AND );
		Node* n2 = distribute_DNF( DNF( n->lhs ), DNF( n->rhs ) );
		if ( n2->type == AND ) {
			n->lhs = n2->lhs;
			n->rhs = n2->rhs;
			return n;
		}
		assert( n2->type == OR );
		n->type = OR;
		n->lhs = n2->lhs;
		n->rhs = n2->rhs;
		return n;
	}

	Node*	Regression_Formula::distribute_DNF( Node* n1, Node* n2 ) {

		if ( n1->type == OR ) {
			Node* n = new Node( OR );
			m_f_nodes.push_back( n );
			n->lhs = distribute_DNF( n1->lhs, n2 );
			n->rhs = distribute_DNF( n1->rhs, n2 );
			return n;
		}
		if ( n2->type == OR) {
			Node* n = new Node( OR );
			m_f_nodes.push_back( n );
			n->lhs = distribute_DNF( n1, n2->lhs );
			n->rhs = distribute_DNF( n1, n2->rhs );
			return n;
		}

		Node* n = new Node( AND );
		m_f_nodes.push_back( n );
		n->lhs = n1;
		n->rhs = n2;
		return n;
	}

	lbool	Regression_Formula::extract_clauses( std::vector< DNF_Clause >& clauses ) {
		Node* root = m_f_nodes[0];
		if ( root->type == VAL ) {
			return root->value;
		}
		extract_clauses( root, clauses );
		
		return (clauses.empty() ? l_False : l_Undef);
	}

	void	Regression_Formula::extract_clause( Node* n, DNF_Clause& c ) {
		assert( n->type != OR ); // Formula isn't in DNF
		assert( n->type != VAL );
		if ( n->type == LIT ) {
			if ( !c.entails( n->lit ) )
				c.add( n->lit );
			return;
		}
		if ( n->type == NOT ) {
			Lit l = ~(n->lhs->lit);
			if ( !c.entails( l ) )
				c.add( l );
			return;
		}
		assert( n->type == AND );
		extract_clause( n->lhs, c );
		extract_clause( n->rhs, c );
	}

	void	Regression_Formula::extract_clauses( Node* n, std::vector< DNF_Clause >& clauses ) {
		assert( n->type != VAL );
		if ( n->type != OR ) {
			DNF_Clause c;
			extract_clause( n, c );
			if ( !c.inconsistent() ) {
				bool duplicate = false;
				for ( auto it = clauses.begin(); it != clauses.end(); it++ )
					if ( c == *it ) {
						duplicate = true;
						break;
					}

				if ( !duplicate ) clauses.push_back( c );
			}
			return;
		}
		extract_clauses( n->lhs, clauses );
		extract_clauses( n->rhs, clauses );	
	}

	void	Regression_Formula::simplify() {
		if ( m_f_nodes.empty() ) return;
		Node* n = simplify_rec( m_f_nodes[0] );	
		m_f_nodes[0]->type = n->type;
		m_f_nodes[0]->value = n->value;
		m_f_nodes[0]->lhs = n->lhs;
		m_f_nodes[0]->rhs = n->rhs;
		m_f_nodes[0]->lit = n->lit;
	}

	Node*	Regression_Formula::simplify_rec( Node* n ) {
		
		if ( n == nullptr || n->type == LIT || n->type == VAL )
			return n;

		Node* lhs = simplify_rec( n->lhs );
		Node* rhs = simplify_rec( n->rhs );	

		if ( n->type == AND ) {
			if ( lhs->type == VAL ) {
				if ( lhs->value == l_True ) { 
					if ( rhs->type == VAL ) {
						if ( rhs->value == l_False ) {
							// True & False = False
							n->type = VAL;
							n->value = l_False;
							n->lhs = nullptr;
							n->rhs = nullptr;
							return n;
						}
						// Must be True
						assert( rhs->value == l_True );
						// True & True = True
						n->type = VAL;
						n->value = l_True;
						n->lhs = nullptr;
						n->rhs = nullptr;
						return n;
					}
					assert( rhs->type != VAL );
					// True & \phi = \phi
					return rhs;
				}
				assert( lhs->value == l_False );
				// False & whatever = False
				n->type = VAL;
				n->value = l_False;
				n->lhs = n->rhs = nullptr;
				return n;
			}
			if ( rhs->type == VAL ) { // \phi & value

				if ( rhs->value == l_True ) { 
					if ( lhs->type == VAL ) {
						if ( lhs->value == l_False ) {
							// False & True = False
							n->type = VAL;
							n->value = l_False;
							n->lhs = nullptr;
							n->rhs = nullptr;
							return n;
						}
						// Must be True
						assert( lhs->value == l_True );
						// True & True = True
						n->type = VAL;
						n->value = l_True;
						n->lhs = nullptr;
						n->rhs = nullptr;
						return n;
					}
					assert( lhs->type != VAL );
					// \phi & True = \phi
					return lhs;
				}
				assert( rhs->value == l_False );
				// whatever & False = False
				n->type = VAL;
				n->value = l_False;
				n->lhs = n->rhs = nullptr;
				return n;
			}
			
			// Check for literals
			// 1. l & ~l
			// 2. l & l		
			if ( lhs->type == LIT && rhs->type == LIT ) {
				if ( lhs->lit == ~rhs->lit ) {
					// l & ~l = False
					n->type = VAL;
					n->value = l_False;
					n->lhs = nullptr;
					n->rhs = nullptr;
					return n;
				}
				if ( lhs->lit == rhs->lit ) {
					// l & l = l
					n->type = LIT;
					n->lit = lhs->lit;
					n->lhs = nullptr;
					n->rhs = nullptr;
					return n;
				}
			}

			// No simplification possible
			if ( rhs != n->rhs ) n->rhs = rhs;
			if ( lhs != n->lhs ) n->lhs = lhs;
			return n;
		}
		if ( n->type == OR ) {
			// Check for values
			if ( lhs->type == VAL ) {
				if ( lhs->value == l_True ) { 
					if ( rhs->type == VAL ) {
						if ( rhs->value == l_False ) {
							// True || False = True
							n->type = VAL;
							n->value = l_True;
							n->lhs = n->rhs = nullptr;
							return n;
						}
						// Must be True
						assert( rhs->value == l_True );
						// True || True = True
						n->type = VAL;
						n->value = l_True;
						n->lhs = n->rhs = nullptr;
						return n;
					}
					assert( rhs->type != VAL );
					// True || \phi = True
					n->type = VAL;
					n->value = l_True;
					n->lhs = n->rhs = nullptr;
					return n;
				}
				assert( lhs->value == l_False );
				// False || whatever = whatever
				return rhs;
			}
			if ( rhs->type == VAL ) { // \phi || value

				if ( rhs->value == l_True ) { 
					if ( lhs->type == VAL ) {
						if ( lhs->value == l_False ) {
							// False || True = True
							n->type = VAL;
							n->value = l_True;
							n->lhs = n->rhs = nullptr;
							return n;
						}
						// Must be True
						assert( lhs->value == l_True );
						// True || True = True
						n->type = VAL;
						n->value = l_True;
						n->lhs = n->rhs = nullptr;
						return n;
					}
					assert( lhs->type != VAL );
					// \phi || True = True
					n->type = VAL;
					n->value = l_True;
					n->lhs = n->rhs = nullptr;
					return n;
				}
				assert( rhs->value == l_False );
				// whatever || False = whatever
				return lhs;
			}

			// Check for literals
			// 1. l || ~l
			// 2. l || l

			if ( lhs->type == LIT && rhs->type == LIT ) {
				if ( lhs->lit == ~rhs->lit ) {
					// l || ~l = True
					n->type = VAL;
					n->value = l_True;
					n->lhs = n->rhs = nullptr;
					return n;
				}
				if ( lhs->lit == rhs->lit ) {
					// l || l = l
					n->type = LIT;
					n->lit = lhs->lit;
					n->lhs = n->rhs = nullptr;
					return n;
				}
			}
		// No simplification possible
			if ( rhs != n->rhs ) n->rhs = rhs;
			if ( lhs != n->lhs ) n->lhs = lhs;
			return n;
		}

		assert( n->type == NOT );
		assert( n->rhs == nullptr );
		assert( rhs == nullptr );
		// Check for values
		if ( lhs->type == VAL ) {
			n->type = VAL;
			assert( lhs->value != l_Undef );
			n->value = ( lhs->value == l_True ? l_False : l_True );
			n->lhs = n->rhs = nullptr;
			return n;	
		}		

		// Check for double negation
		if ( lhs->type == NOT ) {	
			return lhs;
		}		

		// No simplification possible
		if ( lhs != n->lhs ) n->lhs = lhs;
		return n;
	}
	
	size_t	Regression_Formula::count_symbols_rec( Node* n ) const {
		if ( n == nullptr ) return 0;
		return 1 + count_symbols_rec( n->lhs ) + count_symbols_rec( n->rhs );
	}

	void	Regression_Formula::expand() {
		// Expand according to Definition 3 and 7 in 
		// "Regression for Classical and Nondeterministic Planning"
		// Jussi Rintanen, ECAI 2008	
		
		// Root node
		Node* root = new Node(AND);
		bool regressing_singleton = m_regressed.size() == 1;
		std::vector<Node*> subst_terms;

		// Add precondition
		Node* parent = root;
		m_f_nodes.push_back( root );

		for ( size_t k = 0; k < m_precondition.size(); k++ ) {
			Lit l = m_precondition[k];
			make_node_from_lit( l, parent );

			if ( k < m_precondition.size() - 1 ) {
				parent->rhs = new Node( AND );
				m_f_nodes.push_back( parent->rhs );
				parent = parent->rhs;
			}
		}

		
		if ( regressing_singleton ) {
			Lit l = m_regressed[0];
			Node* l_node = make_node_from_lit( l, parent, false );
			subst_terms.push_back( l_node );
		}
		else {
			parent->rhs = new Node(AND); 
			m_f_nodes.push_back( parent->rhs );
			parent = parent->rhs;
			for ( size_t k = 0; k < m_regressed.size(); k++ ) {
				
				if ( k == m_regressed.size() - 2 ) {
					// Only two variables left
					Lit lhs_lit = m_regressed[k];
					Lit rhs_lit = m_regressed[k+1];

					Node* lhs_child = make_node_from_lit( lhs_lit, parent );
					subst_terms.push_back( lhs_child );
					Node* rhs_child = make_node_from_lit( rhs_lit, parent, false );					
					subst_terms.push_back( rhs_child );
					break;
				}
				
				Lit l = m_regressed[k];
				Node* lhs_child = make_node_from_lit( l, parent );
				subst_terms.push_back( lhs_child );

				parent->rhs = new Node( AND );
				m_f_nodes.push_back( parent->rhs );
				parent = parent->rhs;
			}
		}

		if ( m_conj_effect.empty() && m_nd_conj_effect.empty() && m_cond_effects.empty() ) 
			return;

		// By now all the terms in the regressed formula are being pointed
		// by the contents of subst_terms vector

		std::vector<Node*> expand_terms;	
		substitute_terms_by_expansion_formula( subst_terms, expand_terms );

		// Apply E_a(e) substitution/expansion
		expand_evaluation_formulas( expand_terms );
	}

	void	Regression_Formula::expand_evaluation_formulas( std::vector< Node* >& terms_to_expand ) {
		
		for ( auto it = terms_to_expand.begin(); it != terms_to_expand.end(); it++ ) {

			Node* n = *it;
			Lit l = ( n->type == NOT ? ~(n->lhs->lit) : n->lit );
			// Apply evaluation formula for conjunctive effects
			lbool	value = l_False;
			
			if ( std::find( m_conj_effect.begin(), m_conj_effect.end(), l ) 
				!= m_conj_effect.end() )
				value = l_True;
			
			if ( value == l_False 
				&& ( 	std::find( m_nd_conj_effect.begin(), m_nd_conj_effect.end(),l )
					!= m_nd_conj_effect.end() ) )
				value = l_True; 
			
			// if after evaluating E_a(e) on conjunctive effects we are left with
			// with a l_True value, or they're empty, we're done
			if ( value == l_True ) {
				n->type = VAL;
				n->value = l_True;
				continue;
			}

			// Apply evaluation formula for conditional effects
			// E_a( c -> e ) = c & E_a(e)
			// Note that e must be a conjunction of literals, so for each c.e.
			// we need to first check whether E_a(e) evaluates to true or false.
			// In the case it evaluates to false (i.e. the c.e. cannot have been fired)
	
			std::vector<DNF_Clause*> fired_conds;

			for ( auto ce_it = m_cond_effects.begin(); ce_it != m_cond_effects.end(); ce_it++ ) {

				DNF_Clause& ce_eff = ce_it->second;
				
				lbool	ce_eff_value = l_False;
		
				if ( std::find( ce_eff.begin(), ce_eff.end(), l ) 
					!= ce_eff.end() )
					ce_eff_value = l_True;
				
				if ( ce_eff_value == l_False ) continue; // ce didn't fire

				fired_conds.push_back( &(ce_it->first ) );
			}

			// No conditional effects were fired

			if ( fired_conds.empty() ) {
				n->type = VAL;
				n->value = value;
				continue;
			}
			
			// Here comes the hard case, when the conditional effect has been fired.
			// We need to turn the current formula node into a conjunction of literals.

			if ( fired_conds.size() == 1 ) {
				n->type = AND;
				make_conjunctive_subformula( *fired_conds[0], n );
				// and we're done
				continue;
			}

			// We end up with a DNF formula
			n->type = OR;
			Node* parent = n;

			for ( size_t k = 0; k < fired_conds.size(); k++ ) {

				if ( k == fired_conds.size() - 2 ) {
					DNF_Clause& cond_k1 = *(fired_conds[k]);
					
					if ( cond_k1.size() == 1 ) {
						make_node_from_lit( cond_k1[0], parent );
					}
					else {
						Node* lhs_child = new Node( AND );
						m_f_nodes.push_back( lhs_child );
						make_conjunctive_subformula( cond_k1, lhs_child );
						parent->lhs = lhs_child;
					}

					DNF_Clause& cond_k2 = *(fired_conds[k+1]);

					if ( cond_k2.size() == 1 ) {
						make_node_from_lit( cond_k2[0], parent, false );
					}
					else {
						Node* rhs_child = new Node( AND );
						m_f_nodes.push_back( rhs_child );
						make_conjunctive_subformula( cond_k2, rhs_child );
						parent->rhs = rhs_child;
					}

					break;	
				}
				
				DNF_Clause& cond_k = *(fired_conds[k]);
				
				if ( cond_k.size() == 1 ) {
					make_node_from_lit( cond_k[0], parent );
				}
				else {
					Node* lhs_child = new Node( AND );
					m_f_nodes.push_back( lhs_child );
					make_conjunctive_subformula( cond_k, lhs_child );
					parent->lhs = lhs_child;
				}
				
				parent->rhs = new Node( OR );
				m_f_nodes.push_back( parent->rhs );
				parent = parent->rhs;
			}
		}

	}

	void	Regression_Formula::make_conjunctive_subformula( DNF_Clause& c, Node* parent ) {

		for ( size_t k = 0; k < c.size(); k++ ) {
			
			if ( k == c.size() - 2 ) {
				// Only two variables left
				Lit lhs_lit = c[k];
				Lit rhs_lit = c[k+1];

				make_node_from_lit( lhs_lit, parent );
				make_node_from_lit( rhs_lit, parent, false );					
				break;
			}
			
			Lit l = c[k];
			make_node_from_lit( l, parent );
			
			parent->rhs = new Node( AND );
			m_f_nodes.push_back( parent->rhs );
			parent = parent->rhs;
		}

	}

	void	Regression_Formula::substitute_terms_by_expansion_formula( 	
			std::vector< Node* >& terms_to_subst,
			std::vector< Node* >& terms_to_expand ) {

		
		for ( auto it = terms_to_subst.begin(); it != terms_to_subst.end(); it++ ) {
			
			Node* n = *it;

			assert( n->type == LIT || n->type == NOT);

			// 1. Keep the atom
			Lit a = ( n->type == NOT ? ~(n->lhs->lit) : n->lit);
			
			// 2. Change node into OR
			n->type = OR;
			n->lhs = nullptr;

			// 3. Make lhs
			Node* lhs_child = make_node_from_lit( a, n ); // here goes the E_{a}(e) expansion
			terms_to_expand.push_back( lhs_child );

			// 4. Make rhs
			Node* rhs_child = new Node( AND );
			m_f_nodes.push_back( rhs_child );
			n->rhs = rhs_child;

			make_node_from_lit( a, rhs_child );
			
			Node* n2 = new Node(NOT);
			m_f_nodes.push_back( n2 );
			// here goes the E_{\neg a}( e ) expansion
			n2->lhs = new Node( ~a );
			m_f_nodes.push_back( n2->lhs );
			terms_to_expand.push_back( n2->lhs );

			rhs_child->rhs = n2;
		}	

	}

	Node* Regression_Formula::make_node_from_lit( Lit l, Node* parent, bool is_lhs ) {

		Node* lit_node = nullptr;

		if ( sign(l) ) {
			lit_node = new Node( NOT );
			m_f_nodes.push_back( lit_node );
			lit_node->lhs = new Node( ~l );
			m_f_nodes.push_back( lit_node->lhs );

			if ( parent != nullptr ) {
				if ( is_lhs ) 	{
					assert( parent->lhs == nullptr );
					parent->lhs = lit_node;
				}
				else	{
					assert( parent->rhs == nullptr );
					parent->rhs = lit_node;
				}
			}
			assert( !sign(lit_node->lhs->lit) );
			return lit_node;
		}
		assert( !sign(l));
		lit_node = new Node( l );
		m_f_nodes.push_back( lit_node );

		if ( parent != nullptr ) {
			if ( is_lhs ) 	{
				assert( parent->lhs == nullptr );
				parent->lhs = lit_node;
			}
			else	{
				assert( parent->rhs == nullptr );
				parent->rhs = lit_node;
			}
		}

		assert( !sign(lit_node->lit) );
		return lit_node;
	}

	void	Regression_Formula::write( std::ostream& os, const Instance& i ) const {
		os << "Formula: " << std::endl;
		if ( m_f_nodes.empty() ) { 
			os << "<empty>" << std::endl;
			return;
		}	
		write_rec( os, i, m_f_nodes[0], 0);
	}

	void	Regression_Formula::write_rec( std::ostream& os, const Instance& i, Node* n, int level ) const {

		if ( n == nullptr ) return;

		for ( int i = 0; i < level; i++ ) os << "\t";
		
		if ( n->type == AND ) {
			os << "AND" << std::endl;
			write_rec( os, i, n->lhs, level + 1 );
			write_rec( os, i, n->rhs, level + 1 );
			return;
		}
		if ( n->type == OR ) {
			os << "OR" << std::endl;
			write_rec( os, i, n->lhs, level + 1 );
			write_rec( os, i, n->rhs, level + 1 );
			return;
		}
		if ( n->type == NOT ) {
			os << "NOT" << std::endl;
			write_rec( os, i, n->lhs, level + 1 );
			return;
		}
		if ( n->type == LIT ) {
			os << "LIT ";
			Atom idx = atom(n->lit);
			assert( (unsigned)idx < i.n_atoms() );
			const Instance::Atom& a = *(i.atoms[idx]);
 			assert( !sign(n->lit) );
			os << *(a.name) << std::endl;
			return;
		}
		assert( n->type == VAL );
		os << ( n->value == l_True ? "True" : "False" ) << std::endl;
	}

	lbool	strong_regress( const 			Instance& task, 
				int 			action_idx,
				const DNF_Clause&	input,
				DNF_Clause&		output ) {
		
		if ( input.empty() ) return l_False;
		if ( input.inconsistent() ) return l_False;

		const Instance::Action& action = *task.actions[ action_idx ];

		std::vector< Regression_Formula* > regression_results;
	
		if ( !action.ndeffects.empty() ) {
	
			for ( size_t i = 0; i < action.ndeffects[0].effs.size(); i++ ) {
	
				regression_results.push_back( new Regression_Formula( action, i, input ) );
				regression_results.back()->expand();
				//std::cout << "Regression through action " << *(action.name) << " " << i << "-th nd effect ";
				//std::cout << "resulted in formula with " << regression_results.back()->size() << " symbols" << std::endl;
				regression_results.back()->simplify();
				//std::cout << "\tsize after simplification: " << regression_results.back()->size() << " symbols" << std::endl;
				//regression_results.back()->write( std::cout, task );
			}
	
			assert( regression_results.size() >= 2 );
			Regression_Formula* conjoined = nullptr;
			conjoined = new Regression_Formula( AND, *regression_results[0], *regression_results[1] );

			for ( unsigned k = 2; k < regression_results.size(); k++ ) {
				Regression_Formula* old_conjoined = conjoined;
				conjoined = new Regression_Formula( AND, *old_conjoined, *regression_results[k] );
				delete old_conjoined;
			}

			conjoined->to_DNF();

			//std::cout << "Regression through action " << *(action.name) << " ";
			//std::cout << "resulted in formula with " << conjoined->size() << " symbols" << std::endl;
			//conjoined->write( std::cout, task );
			
			std::vector< DNF_Clause > outputs;
			lbool res = conjoined->extract_clauses( outputs );

			for ( auto it = regression_results.begin(); it != regression_results.end(); it++ )
				delete *it;
			
			delete conjoined;

			std::cout << "Regressing" << std::endl;
			std::cout << "\t";
			input.write( std::cout, task );
			std::cout << std::endl;
			std::cout << " through action " << *(action.name) << " ";
			std::cout << "resulted in ";
			if ( res == l_False ) {
				std::cout << "EMPTY set of states" << std::endl;
				return l_False;
			}
			else if ( res == l_True ) {
				std::cout << "UNIVERSAL set of states" << std::endl;
				return l_Undef;
			}
			else {
				std::cout << std::endl;
				for ( auto it = outputs.begin(); it != outputs.end(); it++ ) {
					std::cout << "\t";
					it->write( std::cout, task );
					std::cout << std::endl;
				}
			}

			assert( outputs.size() == 1 );
			output = outputs[0];
			return l_True;
		}

		regression_results.push_back( new Regression_Formula( action, input ) );
		regression_results.back()->expand();
		regression_results.back()->simplify();
		regression_results.back()->to_DNF();

		//std::cout << "Regression through deterministic action " << *(action.name) << "  ";
		//std::cout << "resulted in formula with " << regression_results.back()->size() << " symbols" << std::endl;
		//regression_results.back()->write( std::cout, task );

		// convert to disjunctive normal form
		regression_results.back()->to_DNF();
		std::vector< DNF_Clause > outputs;
		lbool res = regression_results.back()->extract_clauses( outputs );
		std::cout << "Regressing " << std::endl;
		std::cout << "\t";
		input.write( std::cout, task );
		std::cout << std::endl;

		std::cout << " through action " << *(action.name) << " ";
		std::cout << "resulted in ";
		if ( res == l_False ) {
			std::cout << "EMPTY set of states" << std::endl;
			return l_False;
		}
		else if ( res == l_True ) {
			std::cout << "UNIVERSAL set of states" << std::endl;
			return l_Undef;
		}
		else {
			std::cout << std::endl;
			for ( auto it = outputs.begin(); it != outputs.end(); it++ ) {
				std::cout << "\t";
				it->write( std::cout, task );
				std::cout << std::endl;
			}
		}

		for ( auto it = regression_results.begin(); it != regression_results.end(); it++ )
			delete *it;

		assert( outputs.size() == 1 );
		output = outputs[0];

		return l_True;
	}

	lbool	strong_cyclic_regress( 	const 				Instance& task, 
					int 				action_idx,
					const DNF_Clause&		input,
					std::vector<DNF_Clause>&	outputs ) {
		
		if ( input.empty() ) return l_False;
		if ( input.inconsistent() ) return l_False;

		const Instance::Action& action = *task.actions[ action_idx ];

		std::vector< Regression_Formula* > regression_results;
	
		if ( !action.ndeffects.empty() ) {
	
			for ( size_t i = 0; i < action.ndeffects[0].effs.size(); i++ ) {
				if ( action.effect.empty() && action.ndeffects[0].effs.at(i).empty()
					&& action.when.empty() ) continue;	
				regression_results.push_back( new Regression_Formula( action, i, input ) );
				regression_results.back()->expand();
				/*
				std::cout << "Regression through action " << *(action.name) << " " << i << "-th nd effect ";
				std::cout << "resulted in formula with " << regression_results.back()->size() << " symbols" << std::endl;
				regression_results.back()->write( std::cout, task );
				*/
				regression_results.back()->simplify();
				/*
				std::cout << "\tsize after simplification: " << regression_results.back()->size() << " symbols" << std::endl;
				regression_results.back()->write( std::cout, task );
				*/
			}
	
			if ( regression_results.size() == 1 ) {

				regression_results.back()->to_DNF();

				// convert to disjunctive normal form
				lbool res = regression_results.back()->extract_clauses( outputs );

				if ( res == l_False ) {
					return l_False;
				}
				else if ( res == l_True ) {
					return l_Undef;
				}
				for ( auto it = regression_results.begin(); it != regression_results.end(); it++ )
					delete *it;
	
				assert( outputs.size() == 1 );

				return l_True;
			}

			Regression_Formula* conjoined = nullptr;
			conjoined = new Regression_Formula( AND, *regression_results[0], *regression_results[1] );

			for ( unsigned k = 2; k < regression_results.size(); k++ ) {
				Regression_Formula* old_conjoined = conjoined;
				conjoined = new Regression_Formula( AND, *old_conjoined, *regression_results[k] );
				delete old_conjoined;
			}

			conjoined->to_DNF();

			/*
			std::cout << "Regression through action " << *(action.name) << " ";
			std::cout << "resulted in formula with " << conjoined->size() << " symbols" << std::endl;
			conjoined->write( std::cout, task );
			*/
			lbool res = conjoined->extract_clauses( outputs );

			/* Compute the residuals */

			if ( res != l_True && res != l_False ) {
				//std::cout << "Computing residuals" << std::endl;
				for ( unsigned k = 0; k < regression_results.size(); k++ ) {
					std::vector< DNF_Clause > residuals;
					Regression_Formula* not_conjoined = new Regression_Formula( NOT, *conjoined );
					Regression_Formula* difference = new Regression_Formula( AND, *regression_results[k], *not_conjoined );
					difference->simplify();
					difference->to_DNF();
					/*
					std::cout << "Before DNF rewriting" << std::endl;
					difference->write( std::cout, task );
					std::cout << "After DNF rewriting" << std::endl;
					difference->write( std::cout, task );
					*/
					lbool res2 = difference->extract_clauses( residuals );
					/*
					std::cout << "Residuals for outcome #" << k << ":" << std::endl;
					std::cout << std::endl;
					for ( auto it = residuals.begin(); it != residuals.end(); it++ ) {
						std::cout << "\t";
						it->write( std::cout, task );
						std::cout << std::endl;
					}
					*/
					for ( auto r_it = residuals.begin(); r_it != residuals.end(); r_it++ ) {
						bool duplicate = false;
						for ( auto it = outputs.begin(); it != outputs.end(); it++ ) {
							if ( *r_it == *it ) {
								duplicate = true;
								break;
							}
						}
						if ( !duplicate ) outputs.push_back( *r_it );
					}
					delete not_conjoined;
					delete difference;
				}
			}
			/*
			std::cout << "Regressing" << std::endl;
			std::cout << "\t";
			input.write( std::cout, task );
			std::cout << std::endl;
			std::cout << " through action " << *(action.name) << " ";
			std::cout << "resulted in ";
			*/
			if ( res == l_False ) {
				//std::cout << "EMPTY set of states" << std::endl;
				for ( auto it = regression_results.begin(); it != regression_results.end(); it++ ) {
					//(*it)->write( std::cout, task );
					//std::cout << std::endl;
					(*it)->to_DNF();
					(*it)->extract_clauses( outputs );
				}

				for ( auto it = regression_results.begin(); it != regression_results.end(); it++ )
					delete *it;
			
				delete conjoined;


				return l_False;
			}
			else if ( res == l_True ) {
				//std::cout << "UNIVERSAL set of states" << std::endl;
				for ( auto it = regression_results.begin(); it != regression_results.end(); it++ )
					delete *it;
			
				delete conjoined;

				return l_Undef;
			}
			/*
			else {
				std::cout << std::endl;
				for ( auto it = outputs.begin(); it != outputs.end(); it++ ) {
					std::cout << "\t";
					it->write( std::cout, task );
					std::cout << std::endl;
				}
			}
			*/

			for ( auto it = regression_results.begin(); it != regression_results.end(); it++ )
				delete *it;
			
			delete conjoined;

			return l_True;
		}

		regression_results.push_back( new Regression_Formula( action, input ) );
		regression_results.back()->expand();
		regression_results.back()->simplify();
		regression_results.back()->to_DNF();

		//std::cout << "Regression through deterministic action " << *(action.name) << "  ";
		//std::cout << "resulted in formula with " << regression_results.back()->size() << " symbols" << std::endl;
		//regression_results.back()->write( std::cout, task );

		// convert to disjunctive normal form
		lbool res = regression_results.back()->extract_clauses( outputs );
		/*
		std::cout << "Regressing " << std::endl;
		std::cout << "\t";
		input.write( std::cout, task );
		std::cout << std::endl;

		std::cout << " through action " << *(action.name) << " ";
		std::cout << "resulted in ";
		*/

		if ( res == l_False ) {
			//std::cout << "EMPTY set of states" << std::endl;
			for ( auto it = regression_results.begin(); it != regression_results.end(); it++ )
				delete *it;

			return l_False;
		}
		else if ( res == l_True ) {
			//std::cout << "UNIVERSAL set of states" << std::endl;
			for ( auto it = regression_results.begin(); it != regression_results.end(); it++ )
				delete *it;

			return l_Undef;
		}
		/*
		else {
			std::cout << std::endl;
			for ( auto it = outputs.begin(); it != outputs.end(); it++ ) {
				std::cout << "\t";
				it->write( std::cout, task );
				std::cout << std::endl;
			}
		}
		*/
		for ( auto it = regression_results.begin(); it != regression_results.end(); it++ )
			delete *it;

		assert( outputs.size() == 1 );

		return l_True;
	}

}

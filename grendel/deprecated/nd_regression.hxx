#ifndef __ND_REGRESSION__
#define __ND_REGRESSION__

#include <problem.hxx>
#include <gp_atoms.hxx>

namespace grendel {

lbool	strong_regress( const 			Instance& task, 
			int 			action_idx,
			const DNF_Clause&	input,
			DNF_Clause&		output );

lbool	strong_cyclic_regress( const 				Instance& task, 
				int 				action_idx,
				const DNF_Clause&		input,
				std::vector<DNF_Clause>&	output );

}

#endif // nd_regression.hxx

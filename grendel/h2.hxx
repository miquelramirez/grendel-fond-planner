#ifndef __H_2__
#define __H_2__

#include <vector>
#include <deque>
#include <fond_inst.hxx>
#include <gp_atoms.hxx>
#include <bit_set.hxx>
#include <common.hxx>
#include <ext_math.hxx>

namespace grendel {

	namespace helper {
		inline int	pair_index( int p, int q ) {
			return ( p >= q ? p*(p+1)/2 + q : q*(q+1)/2 + p);
		}
	}

	class H2_Heuristic {
	public:
		H2_Heuristic( const FOND_Model& model )
		: m_model( model ) {
			unsigned F = DNF_Clause::num_lits();
			m_values.resize( (F*F + F)/2 );	
			m_consequences.resize( F );		
		}

		virtual ~H2_Heuristic() {
		}

		void setup( bool use_invariants = false ) {
			initialize();
			compute();
			extract_consequences_table();
		}

		const	LitVec& consequences( Lit l ) const {
			return m_consequences.at(toInt(l));
		}

		float&	value( Lit l1, Lit l2 ) {
			return value( toInt(l1), toInt(l2) );
		}

		float	value( Lit l1, Lit l2 ) const {
			return value( toInt(l1), toInt(l2) );
		}

		float&	value( int p, int q ) {
			assert( helper::pair_index(p,q) < (int)m_values.size() );
			return m_values[helper::pair_index(p,q)];			
		}

		float	value( int p, int q ) const {
			assert( helper::pair_index(p,q) < (int)m_values.size() );
			return m_values[helper::pair_index(p,q)];			
		}

		virtual void	eval( const DNF_Clause& c, float& h_val ) {
			h_val = 0.0f;
			for ( LitVec::const_iterator i = c.begin();
				i != c.end(); i++ ) {
				for ( LitVec::const_iterator j = i;
					j != c.end(); j++ ) {
					h_val = std::max( value( *i, *j ), h_val );
				}
			}
		}

		float	eval( const DNF_Clause& c ) const {
			float h_val = 0.0f;
			for ( LitVec::const_iterator i = c.begin();
				i != c.end(); i++ ) {
				for ( LitVec::const_iterator j = i;
					j != c.end(); j++ ) {
					h_val = std::max( value( *i, *j ), h_val );
				}
			}
			return h_val;
		}

		float	eval( const DNF_Clause& c, const DNF_Clause& d ) const {
			float h_val = 0.0f;
			for ( LitVec::const_iterator i = c.begin();
				i != c.end(); i++ ) {
				for ( LitVec::const_iterator j = i;
					j != c.end(); j++ ) {
					h_val = std::max( value( *i, *j ), h_val );
				}
				for ( auto j = d.begin(); j != d.end(); j++ )
					h_val = std::max( value( *i, *j ), h_val );
			}
			for ( auto i = d.begin(); i != d.end(); i++ ) {
				for ( auto j = i; j != d.end(); j++ ) {
					h_val = std::max( value( *i, *j), h_val );
				}
			}

			return h_val;
		}

		float 	eval( const DNF_Clause& a, const DNF_Clause& b, Lit p ) const {
			float v = value( p, p );
			if ( aptk::dequal(v, infty) ) return infty;

			for ( auto i = a.begin(); i != a.end() && v != infty; i++ ) {
				v = std::max( value( *i, p ), v );
				for ( auto j = i; j != a.end() && v != infty; j++ ) 
					v = std::max( value ( *i, *j ), v );
				for ( auto j = b.begin(); j != b.end() && v != infty ; j++ )
					v = std::max( value( *i, *j ), v );
			}

			for ( auto i = b.begin(); i != b.end() && v != infty; i++ ) {
				v = std::max( value( *i,p ), v );
				for ( auto j = i; j != b.end() && v != infty; j++ ) {
					v = std::max( value (*i, *j ), v  );
				}
			}
			
			return v;
			
		}

		void add_consequences( const DNF_Clause& c1, DNF_Clause& c2 ) const {
			bool changed;
			c2.add(c1);
			unsigned start = 0;
			DNF_Clause tmp;
			do {
				changed = false;
				for ( unsigned j = start; j != c2.size(); j++ ) {
					for ( auto con_it = m_consequences[toInt(c2[j])].begin();
						con_it != m_consequences[toInt(c2[j])].end(); con_it++ ) {
						if ( tmp.entails( *con_it ) || c2.entails( *con_it)  ) continue;
						tmp.add( *con_it );
						changed = true;
					}
				}
				start += c2.size();
				c2.add(tmp);
				tmp.clear();
			} while (changed);
		}

		void 	print( std::ostream& os ) const;
	
		void	compute_from_invariants();	

		//bool	interferes( const FOND_Model::Action& a, Lit l, int nd_eff_idx );
	
	protected:
		typedef		std::vector< std::pair< float, int> > Fired_Cond_Effects;

		void		initialize();
		void		compute();
		void		extract_consequences_table();

		/*
		bool 	process_effect( const FOND_Model::Action& a, 
					float v_eff,
					int nd_eff_idx = -1 );
	
		bool	process_effect( const FOND_Model::Action& a, 
					float v_eff, 
					const Fired_Cond_Effects& ceffs,
					int nd_eff_idx = -1 );

		bool	account_for_noops( const FOND_Model::Action& a,
					Lit li,
					int nd_eff_idx,
					int cond_eff_idx = -1 );	
		*/

		bool	process_effect( const FOND_Model::Action& prec,
					float h_pre, unsigned eff_idx );

		bool	account_for_noops( const FOND_Model::Action& eff, 
					Lit l, unsigned eff_idx, const std::vector<float>& v_effs );

		bool	interferes(const FOND_Model::Effect_Vec& eff, Lit lr, const std::vector<float>& v_effs);
	
	private:
		const FOND_Model& 	m_model;
		ValueTable		m_values;
		std::vector< LitVec>	m_consequences;
	};


}

#endif // h2.hxx

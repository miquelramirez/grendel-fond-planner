#ifndef __RAMIREZ_SMV_ENCODING__
#define __RAMIREZ_SMV_ENCODING__

#include <fond_inst.hxx>
#include <gp_atoms.hxx>

namespace grendel {

	// Extends Fabio Patrizi's ground PDDL to SMV program encoding
	class Ramirez_SMV_Program {
	public:

		Ramirez_SMV_Program( const FOND_Model& task, std::string filename ) 
		: m_task( task ), m_filename( filename ) {
			process_fond_task();
		}

		~Ramirez_SMV_Program() {
		}
		
		void		write() 	const;
		
	protected:

		void		write_main_module( std::ostream& os ) const;
		void		write_agent_module( std::ostream& os ) const;
		void		write_agent_action_variable( std::ostream& os ) const;
		void		write_agent_states( std::ostream& os ) const;
		void		write_agent_transition(std::ostream&) const;
		void		write_non_determinism_module( std::ostream& os ) const;
		void		write_action_effects( std::ostream& os ) const;
		void		write_non_deterministic_execution_rules( std::ostream& os ) const;
		void		write_environment_module( std::ostream& os ) const;
		void		write_state_variables( std::ostream& os ) const;
		void		write_environment_initial_conditions( std::ostream& os ) const;
		void		write_environment_invariants( std::ostream& os ) const;
		void		write_state_variable_transition_rules( std::ostream& os ) const;
		void		write_CTL_strong_cyclic_spec( std::ostream& os ) const;
		void		write_CTL_strong_spec( std::ostream& os ) const;
		void		write_CTL_weak_spec( std::ostream& os ) const;

		void		process_fond_task();
		void		process_atoms();
		void		process_actions();

		std::string	repr_lit( Lit l, std::string context = "" ) const {
			std::string tmp = sign(l) ? "!" : "";
			tmp += context +  m_atom_vars[atom(l)];
			return tmp;
		}

		std::string	repr_next_lit( Lit l, std::string context = "" ) const {
			std::string tmp = sign(l) ? "!" : "";
			tmp += "next(" + context + m_atom_vars[atom(l)] + ")";			
			return tmp;
		}

		std::string 	repr_formula(const DNF_Clause& phi, std::string context = "", bool next = false ) const;

	protected:		

		struct ActionEffect {
			unsigned a;
			unsigned e;
			unsigned d;
		};

		const 		FOND_Model& 			m_task;
		std::string					m_filename;
		std::vector< std::string >			m_action_lits;
		std::vector< std::string >			m_atom_vars;
		std::vector< std::vector<ActionEffect > >	m_atom_neg_eff;
		std::vector< std::vector<ActionEffect> >	m_atom_pos_eff;
		std::vector< std::string >			m_prec_vars;
		std::vector< std::vector< std::string > >	m_eff_lits;

		
	
	};

}

#endif // smv_ramirez.hxx

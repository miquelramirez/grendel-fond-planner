#ifndef __PROCEDURAL_ND_REGRESSION__
#define __PROCEDURAL_ND_REGRESSION__

#include <fond_inst.hxx>
#include <gp_atoms.hxx>

namespace grendel {

lbool	procedural_strong_cyclic_regress( 	const 				FOND_Model& task, 
						int 				action_idx,
						const DNF_Clause&		input,
						std::vector<DNF_Clause>&	output );

lbool	procedural_strong_cyclic_regress2( 	const 				FOND_Model& task, 
						int 				action_idx,
						const DNF_Clause&		input,
						std::vector<DNF_Clause>&	output );

lbool	procedural_strong_regress(	const 		FOND_Model& 	task,
					int				action_idx,
					const 		DNF_Clause&	input,
					DNF_Clause&			output );

}

#endif // nd_regression.hxx

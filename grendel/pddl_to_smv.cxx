#include <pddl_to_smv.hxx>
#include <fstream>
#include <cassert>
#include <algorithm>
#include <smv_direct.hxx>
#include <smv_ramirez.hxx>
#include <smv_mbp_ramirez.hxx>

namespace grendel {


	void 
	direct_translation( const FOND_Model& task, std::string out_filename ) {

		SMV_Module module( task, out_filename );
	
		module.make_vars();		

		module.write();
	}

	void
	direct_translation( const FOND_Model& task, const Rule_Based_H2_Heuristic& h, std::string out_filename  ) {
		SMV_Module module( task, out_filename );
	
		module.make_vars();		

		module.write(h);

	}

	void
	ramirez_translation( const FOND_Model& task, std::string out_filename ) {
		Ramirez_SMV_Program	the_program( task, out_filename );
		the_program.write();	
	}

	void
	ramirez_mbp_translation( const FOND_Model& task, std::string out_filename ) {
		Ramirez_MBP_SMV_Program	the_program( task, out_filename );
		the_program.write();	
	}

}

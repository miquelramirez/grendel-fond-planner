#include <ex_policy.hxx>
#include <closed_list.hxx>
#include <hash_table.hxx>
#include <deque>

namespace grendel {

	void	
	Explicit_Policy::add( const DNF_Clause& c, int action_index, float cost_to_go ) {
		State_Action_Pair sa;
		sa.condition = c;
		sa.action_index = action_index;
		sa.cost_to_go = cost_to_go;
		m_entries.push_back( sa );
	}
	
	const 	FOND_Model::Action*
	Explicit_Policy::lookup( const DNF_Clause& state_lits ) const {
		std::vector<const FOND_Model::Action*> possible_actions;		

		for ( const State_Action_Pair& sa : m_entries ) {
			if ( sa.action_index == -1 ) continue;
			const DNF_Clause& condition = sa.condition;
			bool satisfied = true;
			for ( auto it2 = condition.begin(); it2 != condition.end(); it2++ ) {
				if ( !state_lits.entails( *it2 ) ) {
					satisfied = false;
					break;
				}
			}
			if ( satisfied ) {
				possible_actions.push_back( m_model.actions[sa.action_index] );
			}
		}


		if ( !possible_actions.empty() ) {
			return possible_actions[0];
		}

		return nullptr;
	}

	void	Explicit_Policy::lookup( const DNF_Clause& state_lits, 
					std::vector< const FOND_Model::Action* >& possible_actions ) const {
		float min_cost_to_go = std::numeric_limits<float>::max();
		for ( const State_Action_Pair& sa : m_entries ) {
			if ( sa.action_index == -1 ) continue;
			const DNF_Clause& condition = sa.condition;
			bool satisfied = true;
			for ( auto it2 = condition.begin(); it2 != condition.end(); it2++ ) {
				if ( !state_lits.entails( *it2 ) ) {
					satisfied = false;
					break;
				}
			}
			if ( satisfied ) {
				if ( sa.cost_to_go < min_cost_to_go ) {
					min_cost_to_go = sa.cost_to_go;
					possible_actions.clear();
					possible_actions.push_back( m_model.actions[sa.action_index] );
				}
				else if ( fabs(min_cost_to_go - sa.cost_to_go) < 1e-7 )
					possible_actions.push_back( m_model.actions[sa.action_index] );
			}
		}
	}

	void
	Explicit_Policy::write_human_readable( std::ostream& os ) const {
		for ( const State_Action_Pair& sa : m_entries ) {
			os << "(:policy-rule" << std::endl;
			os << "\t:condition ";
			sa.condition.write_pddl( os, m_model );
			os << std::endl;
			os << "\t:action ";
			if ( sa.action_index == -1 ) 
				os << " (GOAL)"  << std::endl;
			else {
				const FOND_Model::Action& a = *(m_model.actions[ sa.action_index ]);
				os << a.name << std::endl;
			}
			os << ")" << std::endl;
		}
	}

	void
	Explicit_Policy::write_raw( std::ostream& os ) const {
		for ( const State_Action_Pair& sa : m_entries ) {
			const DNF_Clause& c = sa.condition;
			for ( unsigned i = 0; i < c.size(); i++ ) {
				os << effect( c[i] );
				if ( i < c.size() - 1 )
					os << ",";
			}	
			os << "|" << sa.action_index << "|" << sa.cost_to_go << std::endl;
		}
	}

	class Execution_Node {
	public:
		typedef DNF_Clause	State_Type;

		DNF_Clause					state;
		Execution_Node*					parent;
		std::vector<const FOND_Model::Action*>		actions;
		unsigned					action_index;
		float						gn;
		size_t						hash_key;
		bool						hash_up_to_date;
		void*						hash_table;

		Execution_Node( const DNF_Clause& c, const Explicit_Policy& policy,
				Execution_Node* n = nullptr, float accum_cost = 0.0f )
		: state(c), parent(n), action_index( 0 ),  gn( accum_cost ) {
				policy.lookup( state, actions );
				state.sort();
				aptk::Hash_Key hasher;
				for ( auto it = c.begin(); it != c.end(); it++ )
					hasher.add( toInt(*it) );
				hash_key = (size_t)hasher;
				hash_up_to_date = true;
				hash_table = nullptr;
		}

		~Execution_Node() {
			gn = -1;
		}

		size_t	hash( ) const {
			return hash_key;
		}

		bool 	operator==( const Execution_Node& o ) const {
			return state == o.state;
		}		
	};

	typedef  aptk::search::Closed_List< Execution_Node >		Execution_Closed_List;
	typedef  std::deque< Execution_Node* >				Execution_Open;

	bool
	Explicit_Policy::verify()  {
		Execution_Open 		open;
		Execution_Closed_List	closed;
		
		Execution_Node* initial = new Execution_Node( m_model.init, *this );
		m_visited = 0;
		m_min_cost = std::numeric_limits<float>::max();
		m_max_cost = 0.0f;
		m_paths_to_goal = 0;

		open.push_back( initial );
		while ( !open.empty() ) {

			Execution_Node* n = open.front();

			#ifdef DEBUG
			std::cout << "g(n) = " << n->gn << std::endl;
			std::cout << "State: ";
			n->state.write( std::cout, m_model, false );
			std::cout << std::endl;
			#endif

			if ( n->state.satisfies( m_model.goal ) ) {
				#ifdef DEBUG
				std::cout << "Goal reached: ";
				std::cout << std::endl;
				#endif
				m_min_cost = ( n->gn < m_min_cost ? n->gn : m_min_cost );
				m_max_cost = ( n->gn > m_max_cost ? n->gn : m_max_cost );	
				m_paths_to_goal++;
				open.pop_front(); // We're done with this guy
				continue;
			}
		
			if ( n->actions.empty() ) {
				#ifdef DEBUG
				std::cout << "Policy isn't closed, state is unaccounted for" << std::endl;
				#endif
				return false;

			}
			
			if ( n->action_index == n->actions.size() ) {
				#ifdef DEBUG
				std::cout << "No actions left" << std::endl;
				#endif
				// We're done with this guy
				open.pop_front();
				continue;
			}

			// Only count visited states once
			if ( n->action_index == 0 )  {
				m_visited++;			
				closed.put( n );
			}


			const FOND_Model::Action* a = n->actions[n->action_index];
			n->action_index++;

			#ifdef DEBUG
			std::cout  << " action = ";
			std::cout << a->name << ( a->effects.size() == 1 ? " [DETERMINISTIC] " : " ");
			std::cout << std::endl;
			std::cout << "Successors: " << std::endl;
			#endif

			for ( auto it = a->effects.begin(); it != a->effects.end(); it++ ) {
				DNF_Clause res_state;
				bool res = n->state.apply( a->precondition, *it, res_state );	
				assert( res );
				res_state.write( std::cout, m_model, false );
				if ( res_state == n->state ) {
					#ifdef DEBUG
					std::cout << " [CYCLE]" << std::endl;
					#endif 
					continue;
				}
				Execution_Node* n2 = new Execution_Node( res_state, *this, n,  n->gn + a->cost );
				std::cout << std::endl;
				if ( closed.retrieve( n2 ) != nullptr ) {
					#ifdef DEBUG
					std::cout << " [CYCLE]" << std::endl;
					#endif
					delete n2;
					continue;
				}
				#ifdef DEBUG
				std::cout << std::endl;
				#endif
				open.push_front( n2 );
			}
			#ifdef DEBUG
			std::cout << std::endl;
			#endif

		}

		std::cout << "Visited states: " << m_visited << std::endl;
		std::cout << "Min cost (without cycles): " << m_min_cost << std::endl;
		std::cout << "Max cost (without cycles): " << m_max_cost << std::endl;
		std::cout << "Acyclic executions to the goal: " << m_paths_to_goal << std::endl;

		return true;
	}
}

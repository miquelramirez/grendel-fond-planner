#include <proc_nd_regression.hxx>
#include <cassert>
#include <iostream>

namespace grendel {

	lbool
	regress( const DNF_Clause& prec, const FOND_Model::Effect_Vec& effs, const DNF_Clause& succ,
			DNF_Clause& pred ) {

		pred.add( prec );
		
		for ( auto it = succ.begin(); it != succ.end(); it++ ) {

			for ( auto eff_it = effs.begin(); eff_it != effs.end(); eff_it++ ) {
				assert( eff_it->condition.empty() );
				const DNF_Clause& effect = eff_it->effect;
				if ( effect.entails( *it ) ) // Effect makes true literal in successor formula
					continue;
				if ( effect.entails( ~(*it) ) || pred.entails( ~(*it) ) ) 
					return l_False;
				if ( pred.entails( *it ) )
					continue;
				pred.add( *it );
			}
		}

		return ( pred.empty() ? l_True : l_Undef );
	}

	lbool
	conjoin( const std::vector<DNF_Clause>&	terms, DNF_Clause& conjunction ) {
		lbool result = l_True;

		conjunction.add( terms[0] );
		
		for ( unsigned i = 1; i < terms.size(); i++ ) {
			for ( auto it = terms[i].begin(); it != terms[i].end(); it++ ) {
				if ( conjunction.entails( ~(*it) ) ) // p & ~p
					return l_False;
				if ( conjunction.entails( *it ) )
					continue;
				conjunction.add( *it );
			}	
		}

		return result;
	}

	lbool
	difference(  const DNF_Clause& lhs, const DNF_Clause& rhs, std::vector<DNF_Clause>& diff ) {
		lbool res = l_False;
		for ( auto it = rhs.begin(); it != rhs.end(); it++ ) {
			if ( lhs.entails( *it ) ) 
				continue; // lhs & ~c = false
			DNF_Clause term( lhs );
			term.add( ~(*it) );
			diff.emplace_back( term );
			res = l_True;
		}	
		return res;
	}

	lbool
	procedural_strong_cyclic_regress( 	const 				FOND_Model& task, 
						int 				action_idx,
						const DNF_Clause&		input,
						std::vector<DNF_Clause>&	outputs ) {

		const FOND_Model::Action& action = *(task.actions.at(action_idx));

		std::vector< DNF_Clause > regressed;
		bool	is_strong = true;
		for ( 	auto eff_it = action.effects.begin();
			eff_it != action.effects.end(); eff_it++ ) {
			DNF_Clause output;
			output.reserve( input.size() );
			lbool res = regress( action.precondition, *eff_it, input, output );
			if ( res == l_True ) 
				return l_Undef;
			if ( res == l_False ) {
				is_strong = false;
				continue;
			}
			regressed.emplace_back( output );	
		}

		if ( is_strong &&  regressed.size() == 1 ) {
			for ( auto it = regressed.begin(); it != regressed.end(); it++ )
				outputs.emplace_back( *it );
			
			return l_True;
		}

		if ( !is_strong ) { // one of the regressed terms turned out to be false
			for ( auto it = regressed.begin(); it != regressed.end(); it++ )
				outputs.emplace_back( *it );
			return l_False;
		}

		DNF_Clause conjoined;
		lbool res = conjoin( regressed, conjoined );
		if ( res == l_False ) { // conjoined formula isn't consistent
			outputs = regressed;
			return l_False;
		}
		
		outputs.emplace_back( conjoined );

		// Compute the residuals
		for ( auto it = regressed.begin(); it != regressed.end(); it++ ) {
			std::vector<DNF_Clause> residual;
			difference( *it, conjoined, residual );
			for ( unsigned i = 0; i < residual.size(); i++ )
				outputs.emplace_back( residual[i] );
		}
		
		return l_True;
	}

	lbool
	procedural_strong_cyclic_regress2( 	const 				FOND_Model& task, 
						int 				action_idx,
						const DNF_Clause&		input,
						std::vector<DNF_Clause>&	outputs ) {

		const FOND_Model::Action& action = *(task.actions.at(action_idx));

		std::vector< DNF_Clause > regressed;
		for ( 	auto eff_it = action.effects.begin();
			eff_it != action.effects.end(); eff_it++ ) {
			DNF_Clause output;
			output.reserve( input.size() );
			lbool res = regress( action.precondition, *eff_it, input, output );
			assert( res != l_True );
			if ( res == l_False )
				continue;
			bool dup = false;
			for ( const DNF_Clause& c : regressed ) {
				if ( output == c ) { 
					dup = true;
					break;
				}
			}
			if ( !dup )
				regressed.emplace_back( output );	
		}

		// Check for absorption
		for ( unsigned k = 0; k < regressed.size(); k++ ) {
			bool absorbed = false;
			for ( unsigned l = 0; l < regressed.size(); l++ ) {
				if ( k == l ) continue;
				if ( regressed[k].size() >= regressed[l].size() ) {
					if ( regressed[k].satisfies( regressed[l] ) ) {
						absorbed = true;
						break;
					}
				}	
			}
			if ( !absorbed ) { 
				DNF_Clause closed;
				task.close_under_axioms( regressed[k], closed );
				outputs.emplace_back( closed );
			}
		}

		return l_False;
	}

	lbool
	procedural_strong_regress(	const 		FOND_Model& 	task,
					int				action_idx,
					const 		DNF_Clause&	input,
					DNF_Clause&			output ) {

		const FOND_Model::Action& action = *(task.actions.at(action_idx));

		std::vector< DNF_Clause > regressed;
		for ( 	auto eff_it = action.effects.begin();
			eff_it != action.effects.end(); eff_it++ ) {
			DNF_Clause output;
			output.reserve( input.size() );
			lbool res = regress( action.precondition, *eff_it, input, output );
			assert( res != l_True );
			if ( res == l_False )
				return l_False;
			bool dup = false;
			for ( const DNF_Clause& c : regressed ) {
				if ( output == c ) { 
					dup = true;
					break;
				}
			}
			if ( !dup )
				regressed.emplace_back( output );	
		}

		DNF_Clause	tmp;
		lbool	conj_res = conjoin( regressed, tmp );
		if ( conj_res != l_False ) { 
			DNF_Clause closed;
			task.close_under_axioms( tmp, output );
		}

		return conj_res;

	}

}



/*
Lightweight Automated Planning Toolkit
Copyright (C) 2012
Miquel Ramirez <miquel.ramirez@rmit.edu.au>
Nir Lipovetzky <nirlipo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CLOSED_LIST__
#define __CLOSED_LIST__

#include <unordered_map>
#include <utility>
#include <iostream>

namespace aptk {

namespace search {

	template <typename Node>
	class Closed_List : public std::unordered_multimap< size_t, Node* > {

	public:

		typedef typename Node::State_Type						State;
		typedef typename std::unordered_multimap< size_t, Node* >::iterator 		iterator;
		typedef typename std::unordered_multimap< size_t, Node* >::const_iterator	const_iterator;
	
		Node*	retrieve( Node* n ) {
	
			std::pair< iterator, iterator > range = this->equal_range( n->hash() );
	
			if ( range.first != range.second ) {
				iterator it;
				for ( it = range.first; it != range.second; it++ ) {
					if ( *(it->second) == *n ) {
						assert( it->second->hash_key == n->hash_key );
						return it->second;
					}
				}
			}	
	
			return nullptr;	
		}
	
		iterator retrieve_iterator( Node* n ) {
	
			std::pair< iterator, iterator > range = this->equal_range( n->hash() );
			
			if ( range.first != range.second ) {
				iterator it;
				for ( it = range.first; it != range.second; it++ ) {
					if ( *(it->second) == *n ) {
						assert( it->second->hash_key == n->hash_key );
						return it;
					}
				}
			}
		
			return this->end();			
		}
	
		const_iterator retrieve_iterator( Node* n ) const {
			
			std::pair< iterator, iterator > range = this->equal_range( n->hash() );
	
			if ( range.first != range.second ) {
				const_iterator it;
				for ( it = range.first; it != range.second; it++ ) {
					assert( it->second->gn >= 0 );

					if ( *(it->second) == *n ) {
						assert( it->second->hash_key == n->hash_key );
						return it;
					}
				
				}
			}	
			return this->end();			
		}
	
		void	put( Node* n ) {
			#ifdef DEBUG
			auto it = retrieve_iterator(n);
			assert( it == this->end() );
			#endif
			this->insert( std::make_pair( n->hash(), n ) );
		}

		void	remove( Node* n ) {
			auto it = retrieve_iterator( n );
			assert( it != this->end() );
			this->erase( it );
		}
		
		void
		print_stats( std::ostream& os ) const {
			os << "Size = " << this->size();
			os << " Bucket Count = " << this->bucket_count();
			os << " Load Factor = " << this->load_factor();
			os << " Max Load Factor = " << this->max_load_factor();
			os << std::endl;
		}
	};
}

}

#endif // closed_list.hxx

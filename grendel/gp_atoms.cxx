#include <gp_atoms.hxx>
#include <fond_inst.hxx>
#include <iostream>

namespace grendel {

	unsigned DNF_Clause::m_num_lits = 0;

	class Lit_Comparer {
	public:
		bool operator()( const Lit& a, const Lit& b ) const {
			return toInt(b) > toInt(a);
		}
	};

	DNF_Clause::~DNF_Clause() {
	}

	void
	DNF_Clause::set_num_lits( unsigned sz ) {
		m_num_lits = sz;
	}

	unsigned
	DNF_Clause::num_lits( ) {
		return m_num_lits;
	}

	void	DNF_Clause::sort() {
		std::sort( m_lits.begin(), m_lits.end(), Lit_Comparer() );
	}

	void	DNF_Clause::write( std::ostream& os ) const {
		os << "DNF( ";
		for ( unsigned i = 0; i < m_lits.size(); i++ ) {
			os << ( sign(m_lits[i]) ? "~" : "" ) << atom(m_lits[i]);
			if ( i < m_lits.size()-1 )
				os << ", ";
		}
			
		os << ")";
	}

	void	DNF_Clause::write( std::ostream& os, const FOND_Model& task, bool write_negated ) const {
		os << "DNF( ";
		for ( unsigned i = 0; i < (unsigned)m_lits.size(); i++ ) {
			Atom idx = atom(m_lits[i]);
			assert( idx < (int)task.n_atoms() );
			const FOND_Model::Atom& a = *(task.atoms[idx]);
			if ( sign(m_lits[i]) ) {
				if ( !write_negated ) continue;
				os << "(not (" << a.name <<  "))";
			}
			else
				os << "(" << a.name << ")";
			if ( i < m_lits.size()-1 )
				os << ", ";
		}
			
		os << ")";

	}

	void	DNF_Clause::write_pddl( std::ostream& os, const FOND_Model& task ) const {
		if ( m_lits.size() > 1 )
			os << "(and ";
		for ( unsigned i = 0; i < (unsigned)m_lits.size(); i++ ) {
			Atom idx = atom(m_lits[i]);
			assert( idx < (int)task.n_atoms() );
			const FOND_Model::Atom& a = *(task.atoms[idx]);
			if ( sign(m_lits[i]) ) {
				os << "(not (" << a.name <<  "))";
			}
			else
				os << "(" << a.name << ")";
			if ( i < m_lits.size()-1 )
				os << " ";
		}
		if ( m_lits.size() > 1 )
			os << ")";

	}


	bool	DNF_Clause::inconsistent() const { 

		for ( unsigned i = 0; i < size(); i++ ) {

			Lit l_i = ~m_lits[i];
			if ( m_lits_bits.isset( toInt(l_i) ) )
				return true;
		}
		
		return false;
	}

	bool	DNF_Clause::intersection( const DNF_Clause& other, DNF_Clause& subset ) const {
		subset.clear();
		for ( Lit l : other ) {
			if ( entails( ~l ) ) return false;
			if ( !entails(l) ) subset.add(l);
		}
		return true;
	}

	bool	DNF_Clause::satisfies( const DNF_Clause& other ) const {
		for ( auto it = other.begin(); it != other.end(); it++ )
			if ( !entails( *it ) )
				return false;
		return true;
	}

	bool	DNF_Clause::satisfies2( const DNF_Clause& other ) const {
		for ( auto it = other.begin(); it != other.end(); it++ )
			if ( entails( ~(*it) ) )
				return false;
		return true;
	}

	bool	DNF_Clause::implies( const DNF_Clause& other ) const {

		for ( auto it = other.begin(); it != other.end(); it++ )
			if ( !entails(*it) ) return false;
		return true;
	}

	void	DNF_Clause::implication_check( const DNF_Clause& other, DNF_Clause& missing, bool& inconsistent, bool& implied ) const {
		inconsistent = false;
		for ( auto it = other.begin(); it != other.end(); it++ ) {
			if ( entails( ~(*it) )  )
			{
				inconsistent = true;
				implied = false;
				missing.clear();
				return;
			}
		}
	
		for ( auto it = begin(); it != end(); it++ )
			if ( !other.entails(*it) ) 
				missing.add( *it );
		
		implied = missing.empty();

	}

	void	
	DNF_Clause::add( const DNF_Clause& c ) {
		for ( auto it = c.begin(); it != c.end(); it++ )
			add(*it);
	}

	void
	DNF_Clause::set_union( const DNF_Clause& c ) {
		for ( auto it = c.begin(); it != c.end(); it++ )
			if (!entails(*it)) add(*it);
	}

}

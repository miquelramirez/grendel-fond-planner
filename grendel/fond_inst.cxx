#include <fond_inst.hxx>
#include <iostream>

namespace grendel {

	FOND_Model::~FOND_Model() {
	}

	FOND_Model::Atom&
	FOND_Model::new_atom( const std::string& name ) {
		Atom*	a = new Atom( name, atoms.size() );
		atoms.push_back( a );

		DNF_Clause::set_num_lits( 2*(atoms.size()) );
		init.resize_bitmap();
		goal.resize_bitmap();
		return *a;
	}
		
	FOND_Model::Action&
	FOND_Model::new_action( const std::string& name ) {
		Action* a = new Action( name, actions.size() );
		actions.push_back( a );

		return *a;
	}

	void
	FOND_Model::add_invariant( const DNF_Clause& inv ) {
		invariants.push_back( inv );
	}

	void
	FOND_Model::Action::print( std::ostream& os, const FOND_Model& model ) const {
		os << name << std::endl;
		os << "Precondition: ";
		precondition.write( os, model );
		os << std::endl;
		os << "Effects: " << std::endl;
		for ( const Effect_Vec& effs : effects ) {
			for ( const Effect& eff : effs ) {
				eff.condition.write( os, model );
				os << " -> " << std::endl;
				os << "\t";
				eff.effect.write( os, model );
				os << std::endl;
			}
		}
		os << std::endl;
		
	}

	void
	FOND_Model::Axiom::print( std::ostream& os, const FOND_Model& model ) const {
		os << "Axiom: ";
		os << "Head: ";
		head.write( os, model, true );
		os << " Body: ";
		body.write( os, model, true );
		os << std::endl;
	}
	
	void
	FOND_Model::complete_goal() {
		for ( Lit l : init ) {
			#ifdef DEBUG
			std::cout << "Testing " << ( sign(l) ? "(not (" : "(") << atoms[atom(l)]->name << std::endl;
			#endif

			bool flipped = false;
			for ( const Action* a : actions ) {
				for ( const Effect_Vec& nd_eff : a->effects ) {
					for ( const Effect& eff : nd_eff ) {
						if ( eff.effect.entails( ~l) ) {
							#ifdef DEBUG
							std::cout << ( sign(l) ? "(not (" : "(") << atoms[atom(l)]->name << ") flipped by ";
							std::cout << a->name << std::endl;
							#endif
							flipped = true;
							break;
						}	
					}
					if ( flipped ) break;
				}
				if ( flipped ) break;
			}
			if (!flipped ) {
				#ifdef DEBUG
				std::cout << "Added literal to goal!" << std::endl;
				#endif
				goal.add( l );
			}
		}	
	}

	void
	FOND_Model::add_axiom( const DNF_Clause& body, const DNF_Clause& head ) {
		Axiom* ax = new Axiom;
		ax->body = body;
		ax->head = head;
		axioms.push_back( ax );
	}

	void
	FOND_Model::close_under_axioms( const DNF_Clause& situation, DNF_Clause& closed ) const {
		
		bool changed;
		DNF_Clause  current = situation;
		
		do {
			changed = false;
			for ( auto axiom : axioms ) {
				if ( current.satisfies(  axiom->head ) )
					continue;
				if ( current.satisfies( axiom->body ) ) {
					changed = true;
					current.add( axiom->head );	
				}
			}

		} while( changed );
	
		closed = current;
	}

	
}

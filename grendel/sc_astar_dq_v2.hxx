#ifndef __STRONG_CYCLIC_ASTAR_SEARCH_DOUBLE_QUEUE_V2__
#define __STRONG_CYCLIC_ASTAR_SEARCH_DOUBLE_QUEUE_V2__

#include <fond_inst.hxx>
#include <common.hxx>
#include <gp_atoms.hxx>
#include <open_list.hxx>
#include <ext_math.hxx>
#include <ex_policy.hxx>
#include <closed_list.hxx>
#include <hash_table.hxx>
#include <proc_nd_regression.hxx>

namespace grendel {

	namespace scastar_dq {
		class Node {
		public:

			typedef DNF_Clause	State_Type;
			
			DNF_Clause			state;
			int				action_idx;
			float				hn;
			float				gn;
			float				fn;
			Node*				parent;
			size_t				hash_key;
			bool				hash_up_to_date;
			int 				trials;
			bool				strong;
			void*				hash_table;
			bool				expanded;

			Node( )
			: action_idx( -1 ), gn(0.0f), parent(nullptr), trials(0), strong(true), expanded(false) {
			}
	
			Node( const DNF_Clause& c )
			: state(c), action_idx( -1 ), gn(0.0f), parent(nullptr), trials(0), strong(true), expanded(false) {
			}	
	
			Node( const DNF_Clause& c, int _action_idx, float _gn = 0.0f, Node* _parent = nullptr, bool _strong = true ) 
			: state(c), action_idx( _action_idx ),  gn(_gn), parent(_parent), 
			trials(0), strong( _strong ), expanded( false ) {

				hash_up_to_date = false;
				hash_table = nullptr;
			}

			~Node() {
				assert( hash_table == nullptr );
				hash_up_to_date = false;
				gn = -1;
			}

			size_t	hash( ) const {
				return hash_key;
			}

			bool 	operator==( const Node& o ) const {
				return action_idx == o.action_idx && state == o.state;
			}

			size_t nbytes() const {
				size_t mem_used = sizeof( Node );
				return mem_used + state.nbytes();
			}
		};
	
		class Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				if( aptk::dgreater(a->fn, b->fn) ) return true;
				if ( aptk::dequal( a->fn, b->fn ) )
					return aptk::dgeq(a->gn, b->gn);
				return false;
			}
		};

		class Weak_Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				if ( a->trials > b->trials ) return true;
				if ( a->trials == b->trials ){
					if( aptk::dgreater(a->fn, b->fn) ) return true;
					if ( aptk::dequal( a->fn, b->fn ) )
						return aptk::dgeq(a->gn, b->gn);
				}
				return false;
			}
		};


	}

	template <typename Mutex_Heuristic>
	class DQ_Strong_Cyclic_AStar_Search {

		typedef  Open_List< scastar_dq::Comparer, scastar_dq::Node > 		AStar_Strong_Open_List;
		typedef  Open_List< scastar_dq::Weak_Comparer, scastar_dq::Node >	AStar_Weak_Open_List;
		typedef  aptk::search::Closed_List< scastar_dq::Node >			AStar_Closed_List;

	public:
		DQ_Strong_Cyclic_AStar_Search( const FOND_Model& model, const Mutex_Heuristic& h ) 
		: m_model( model ), m_mutex_func( h ), m_gen_count(0), m_exp_count(0), m_strong_cyclic_checks(0) {

		}

		~DQ_Strong_Cyclic_AStar_Search() {
			for ( typename AStar_Closed_List::iterator i = m_closed.begin();
				i != m_closed.end(); i++ ) {
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			m_closed.clear();
			
			for ( typename AStar_Closed_List::iterator i = m_weak_open_hash.begin();
				i != m_weak_open_hash.end(); i++ ) {
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			m_weak_open_hash.clear();

		}

		void	start() {
			DNF_Clause ext_goal;
			m_mutex_func.add_consequences( m_model.goal, ext_goal );

			std::cout << "Goal (extended): " << std::endl;
			ext_goal.write( std::cout, m_model );
			std::cout << std::endl;

			m_mem_used = 0;
			m_root = new scastar_dq::Node( ext_goal, -1 );
			m_root->strong = true;
			eval( m_root );
			compute_hash( m_root );
			std::cout << "Root heuristic value: " << m_root->hn << std::endl;
			m_solution = nullptr;
			open_strong_node( m_root );
		}

		bool	
		find_strong_cyclic_plan( Explicit_Policy& plan ) {
			bool solved = do_search();
			if ( solved )
				extract_plan( plan );
			return solved;
		} 

		void		inc_gen() { m_gen_count++; }
		void		inc_exp() { m_exp_count++; }
		void		inc_strong_cyclic_check() { m_strong_cyclic_checks++; }

		unsigned	generated() const { return m_gen_count; }
		unsigned	expanded() const { return m_exp_count; }
		unsigned	strong_cyclic_checks() const { return m_strong_cyclic_checks; }
		float		min_cost_to_go() const { return m_min_cost_to_go; }
		size_t		mem_used() const { return m_mem_used; }

		void		
		extract_plan( Explicit_Policy& plan ) {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				plan.add( it->second->state, it->second->action_idx, it->second->gn );
			}
		}

	protected:

		void 			
		close( scastar_dq::Node* n ) { 
			//std::cout << "CLOSED stats: "; 
			m_closed.put(n);
			update_value( n ); 
		}
	
		AStar_Closed_List&	
		closed() { 
			return m_closed; 
		}
	
		bool
		check_strong( scastar_dq::Node* root ) {
			inc_strong_cyclic_check();
			#ifdef DEBUG
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Checking Strong Cyclic property: " << std::endl;
			print_node( std::cout, root );		
			#endif

			std::queue< scastar_dq::Node*, std::list<scastar_dq::Node*> > to_check;
			AStar_Closed_List	seen;
			AStar_Closed_List	potentially_strong;
			to_check.push(root);
			seen.put( root );
			bool strong_component_reached = false;
			unsigned checked = 0;
			unsigned strong_hits =0;

			while ( !to_check.empty() ) {
				DNF_Clause   res_state;
				scastar_dq::Node* n = to_check.front();
				to_check.pop();
				potentially_strong.put(n);
				checked++;
				const FOND_Model::Action& a = *(m_model.actions[n->action_idx]);
				for ( auto eff_it = a.effects.begin(); eff_it != a.effects.end(); eff_it++ )
				{
					res_state.clear();
					n->state.apply( a.precondition, *eff_it, res_state );
					bool implied = false;

					for ( auto it = potentially_strong.begin(); it != potentially_strong.end(); it++ ) 
						if ( it->second->state.implies( res_state ) ) {
							implied = true;
							break;
						}
					if ( implied ) continue;

					for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) 
						if ( it->second->state.implies( res_state ) ) {	
							implied = true;
							strong_component_reached = true;
							strong_hits++;
							break;
						}
					if ( implied ) continue;
					
					for ( auto it = m_weak_open_hash.begin(); it != m_weak_open_hash.end(); it++ ) 
						if ( it->second->state.implies( res_state ) ) {
							if ( seen.retrieve( it->second ) == nullptr ) {
								#ifdef DEBUG
								std::cout << "Checking successor:" << std::endl;
								std::cout << "\t\t[" << it->first << ", " << it->second << "]";
								print_node( std::cout, it->second );
								#endif
								to_check.push( it->second );
								seen.put(it->second);
							}
							implied = true;
						}
					if ( !implied ) {
						#ifdef DEBUG
						std::cout << "Checked states: " << checked << std::endl;
						std::cout << "We got outside of the policy!" << std::endl;
						res_state.write( std::cout, m_model, false );
						std::cout << std::endl;
						#endif
						return false;
					}
				}
			}
			#ifdef DEBUG
			std::cout << "Checked states: " << checked << std::endl;
			#endif
			if ( !strong_component_reached ) { 
				#ifdef DEBUG
				std::cout << "Closed sub-policy not reaching STRONG component" << std::endl;
				#endif
				return false;
			}
			#ifdef DEBUG
			std::cout << "Closed sub-policy reaching STRONG component" << std::endl;
			std::cout << "New strong (s,a) pairs: " << std::endl;
			#endif
			for ( auto it = potentially_strong.begin(); it != potentially_strong.end(); it++ ) { 
				#ifdef DEBUG
				std::cout << "\t\t";
				it->second->state.write( std::cout, m_model, false );
				if ( it->second->action_idx == -1 ) 
					std::cout << " -> (GOAL)" << std::endl;
				else
					std::cout << " -> " << m_model.actions[it->second->action_idx]->name << std::endl;
				#endif
				it->second->strong = true;
				m_weak_open_hash.erase( m_weak_open_hash.retrieve_iterator(it->second) );
				open_strong_node( it->second );
			}
			return true;
		}

		bool
		implied_by_closed2( const DNF_Clause& c ) const {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				if ( it->second->state.implies( c ) ) {
					return true;
				}
			}
			return false;
		}

		scastar_dq::Node* 
		get_node() {
			scastar_dq::Node* n = nullptr;
			if ( !m_strong_open.empty() )
				return get_strong_node();
			if ( !m_weak_open.empty() )
				return get_weak_node();
			return n;
		}

		void
		eval( scastar_dq::Node* n ) {
			n->hn = m_mutex_func.eval( n->state );
			n->fn = n->hn;
			//n->fn = ( n->hn == infty ? infty : aptk::add( n->gn, 10.0f * n->hn ) );
		}

		bool	
		do_search() {
			bool init_hit = false;
			m_min_cost_to_go = infty;
			std::list< scastar_dq::Node* > weak_dead;
			bool changed;
			do {
				changed = false;
				scastar_dq::Node* head = get_node();
				while(head) {
					if ( is_goal(head->state) ) {
						head->expanded = true;
						#ifdef DEBUG
						std::cout << "======================================================================================================" << std::endl;
						std::cout << "Init reached" << std::endl;
						print_node( std::cout, head );
						#endif
						if ( head->strong ) {
							#ifdef DEBUG
							scastar_dq::Node* n = head->parent;
							while( n != nullptr ) {
								print_node( std::cout, n );
								n = n->parent;
							}
							#endif
							init_hit = true;
							m_min_cost_to_go = std::min( m_min_cost_to_go, head->gn );
						}
					}
					
					process_node( head );
		
					if ( !head->strong ) {	
						assert( head->expanded );
						weak_dead.push_back( head ); 
					}
	
					head = get_node();
				}
				#ifdef DEBUG	
				std::cout << "Mopping up weak states (" << weak_dead.size() << ")..." << std::endl;
				#endif
				for ( scastar_dq::Node* n : weak_dead ) {
					if ( n->strong ) continue;
					if ( aptk::dgeq((n->gn + n->hn), m_min_cost_to_go) )continue;
					if ( aptk::dgeq(n->gn, get_value(n) )) continue;
					#ifdef DEBUG
					std::cout << "======================================================================================================" << std::endl;
					std::cout << "Difference with min cost to go: " << (n->gn + n->hn) - m_min_cost_to_go << std::endl;
					std::cout << "V(s) = " << get_value(n) << std::endl;
					#endif
					if ( check_strong( n ) ) 
						changed = true;
				}
			} while ( changed );

			return init_hit;			
		}

		void
		print_node( std::ostream& os, scastar_dq::Node* n ) {
			os << "\t";
			n->state.write( os, m_model, false);
			os << std::endl;
			if ( n->action_idx == -1 ) 
				os << "-> (GOAL)" << std::endl;
			else
				os << "-> " << m_model.actions[n->action_idx]->name << std::endl;

			os << "f(n) = " << n->fn;
			os << " g(n) = " << n->gn;
			os << " h(n) = " << m_mutex_func.eval( n->state );
			os << " trials(n) = " << n->trials;
			os << " strong(n) = " << ( n->strong ? "yes" : "no" ) << std::endl;
				
		}

		void
		compute_hash( scastar_dq::Node* n ) {
			aptk::Hash_Key hasher;
			n->state.update_hash( hasher );
			hasher.add( DNF_Clause::num_lits() + ( n->action_idx == -1 ? m_model.actions.size() : n->action_idx ) );
			n->hash_key = (size_t)hasher;
			n->hash_up_to_date = true;
		}		

		bool
		relevance_check( const DNF_Clause& s, const FOND_Model::Action& a ) const {
			for ( auto eff_it = a.effects.begin(); eff_it != a.effects.end(); eff_it++ ) {
				for ( auto eff_it2 = eff_it->begin(); eff_it2 != eff_it->end(); eff_it2++ )
					for ( auto eff_it3 = eff_it2->effect.begin(); eff_it3 != eff_it2->effect.end(); eff_it3++ ) {
						if ( s.entails( *eff_it3 ) ) return true;
						if ( a.effects.size() == 1 && eff_it2->condition.empty() && s.entails( ~(*eff_it3) ) ) return false;	
					}
			}

			return false; // action doesn't mention any of the lits in the formula to be regressed
		}

		bool 		
		is_closed( scastar_dq::Node* n ) { 
			scastar_dq::Node* n2 = this->closed().retrieve(n);
			return n2 != nullptr;	
		}
		
		bool
		is_goal( const DNF_Clause& c ) {
			bool satisfied = true;
			for ( auto it = c.begin(); it != c.end(); it++ ) {
				if ( m_model.init.entails( ~(*it) ) ) {
					satisfied = false;
					break;
				}
			}
			return satisfied;
		}
	
		scastar_dq::Node* 		
		get_strong_node() {
			scastar_dq::Node* next = nullptr;
			if(! m_strong_open.empty() ) {
				next = m_strong_open.pop();
			}
			return next;
		}

		scastar_dq::Node*
		get_weak_node() {
			scastar_dq::Node* next = nullptr;
			if(! m_weak_open.empty() ) {
				next = m_weak_open.pop();
			}
			return next;			
		}

		void	 	
		open_strong_node( scastar_dq::Node *n ) {
			m_strong_open.insert(n);
			close(n);
			inc_gen();
			m_mem_used += n->nbytes();
		}

		bool	
		is_mutex( const DNF_Clause& c ) const {
			return m_mutex_func.eval( c ) == infty;
		}

		bool	
		is_mutex( const DNF_Clause& c, const DNF_Clause& d ) const {
			return m_mutex_func.eval( c, d ) == infty;
		}

		void	 	
		open_weak_node( scastar_dq::Node *n ) {	
			m_weak_open.insert(n);
			if (!already_in_weak_frontier(n) ) {
				m_weak_open_hash.put(n);
				inc_gen();
				m_mem_used += n->nbytes();
			}
		}
		
		bool 			
		already_in_weak_frontier( scastar_dq::Node *n ) {
			scastar_dq::Node *previous_copy = m_weak_open_hash.retrieve(n);
			return previous_copy != nullptr;
		}

		void
		expand_node( scastar_dq::Node *head ) {
			assert( !head->expanded );
			inc_exp();
			#ifdef DEBUG
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Expanding: (min cost to go = " << m_min_cost_to_go << ")" << std::endl;
			print_node( std::cout, head );		
			std::cout << "V(s) = " << get_value(head) << std::endl;
			#endif
			head->expanded = true;
			if ( aptk::dgeq( (head->gn+head->hn), m_min_cost_to_go) ) return;
			if ( !head->strong && aptk::dgeq(head->gn, get_value(head) )) return;
			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {	
				if ( !relevance_check( head->state, *(m_model.actions[i]) ) ) continue;

				std::vector<DNF_Clause>  succ;
				lbool res = procedural_strong_cyclic_regress( m_model, i, head->state, succ );
				assert( res != l_Undef );
				
				if ( res == l_False ) {
					for ( auto it = succ.begin(); it != succ.end(); it++ ) {
						scastar_dq::Node* n = new scastar_dq::Node( *it, i, head->gn + m_model.actions[i]->cost, head, false );
						eval(n);
						if ( n->hn == infty ) {
							delete n;
							continue;
						}
						if (aptk::dgeq( (n->gn+n->hn), m_min_cost_to_go) ) {
							delete n;
							continue;
						}

						DNF_Clause ext_succ;
						m_mutex_func.add_consequences( *it, ext_succ );
						n->state = std::move(ext_succ);
						
						if ( aptk::dgeq(n->gn, get_value(n) )) {
							delete n;
							continue;
						}
						
						compute_hash( n );
						if ( is_closed( n ) ) {
							delete n;
							continue;
						}
						if( already_in_weak_frontier(n) ) {
							delete n;
							continue;
						}
						open_weak_node( n );
						#ifdef DEBUG
						std::cout << "\t through " << m_model.actions[i]->name << " results in new WEAK predecessor " << std::endl;
						print_node( std::cout, n );
						std::cout << "V(s) = " << get_value( n ) << std::endl;
						std::cout << std::endl;
						#endif
					}	
					continue;
				}
				for ( auto it = succ.begin(); it != succ.end(); it++ ) {
					scastar_dq::Node* n = new scastar_dq::Node( *it, i, head->gn + m_model.actions[i]->cost, head, head->strong );
					eval(n);
					if ( n->hn == infty ) {
						delete n;
						continue;
					}
					if ( aptk::dgeq( (n->gn+n->hn), m_min_cost_to_go)) {
						delete n;
						continue;
					}
					DNF_Clause ext_succ;
					m_mutex_func.add_consequences( *it, ext_succ );
					n->state = std::move(ext_succ);
					if ( aptk::dgeq(n->gn, get_value(n) )) continue;
					if ( aptk::dgeq(n->gn, get_value(n) )) {
						delete n;
						continue;
					}

					compute_hash( n );
					if ( is_closed( n ) ) {
						delete n;
						continue;
					}
					if (n->strong) {
						open_strong_node(n);	
						#ifdef DEBUG
						std::cout << "\t through " << m_model.actions[i]->name << " results in STRONG predecessor " << std::endl;
						print_node( std::cout, n );
						std::cout << std::endl;
						std::cout << "V(s) = " << get_value( n ) << std::endl;
						std::cout << std::endl;
						#endif
					}
					else {
						open_weak_node(n);
						#ifdef DEBUG
						std::cout << "\t through " << m_model.actions[i]->name << " results in WEAK predecessor " << std::endl;
						print_node( std::cout, n );
						std::cout << std::endl;
						std::cout << "V(s) = " << get_value( n ) << std::endl;
						std::cout << std::endl;
						#endif
					}
				}	
			} 
		}

		bool
		process_node( scastar_dq::Node* n ) {
			
			if (!n->strong )
				if (!check_strong( n ))
					n->trials++;

			if (!n->expanded)
				expand_node( n );

			return true;	
		}

		void
		update_value( scastar_dq::Node* n ) {
			#ifdef DEBUG
			std::cout << "V(s) = " << get_value(n) << " V'(s) = " << n->gn << std::endl;
			#endif
			if ( aptk::dgeq( n->gn, get_value(n) ) ) return;
			#ifdef DEBUG
			std::cout << "Needs to be updated" << std::endl;
			#endif
			scastar_dq::Node* new_entry = new scastar_dq::Node( n->state );
			new_entry->gn = n->gn;
			compute_hash( new_entry );
			m_vtable.put( new_entry );
		}

		float	
		get_value( scastar_dq::Node* n ) {
			m_vtable_entry.state = std::move( n->state );
			compute_hash(&m_vtable_entry);
			auto entry = m_vtable.retrieve( &m_vtable_entry );
			n->state = std::move( m_vtable_entry.state );
			if ( entry == nullptr )
				return infty;	
			return entry->gn;
		}

	private:
		const FOND_Model&			m_model;
		AStar_Strong_Open_List			m_strong_open;
		AStar_Weak_Open_List			m_weak_open;
		const Mutex_Heuristic&			m_mutex_func;
		unsigned				m_gen_count;
		unsigned				m_exp_count;
		unsigned				m_strong_cyclic_checks;
		AStar_Closed_List			m_closed;
		AStar_Closed_List			m_weak_open_hash;
		scastar_dq::Node*			m_root;
		scastar_dq::Node*			m_solution;
		size_t					m_mem_used;
		float					m_min_cost_to_go;
		AStar_Closed_List			m_vtable;
		scastar_dq::Node			m_vtable_entry;
	};

}

#endif // ucs.hxx

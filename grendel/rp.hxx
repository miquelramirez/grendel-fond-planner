#ifndef __FF_RP_HEURISTIC__
#define __FF_RP_HEURISTIC__

#include <vector>
#include <queue>
#include <fond_inst.hxx>
#include <gp_atoms.hxx>

namespace grendel {

	template < typename Reachability_Heuristic >
	class Relaxed_Plan_Heuristic
	{

		class Goal_Node {
		public:
			
			Goal_Node( Lit p, float h )
				: m_fluent(p), m_h(h) {
			}
		
			Goal_Node( Goal_Node&& other ) 
				: m_fluent( other.m_fluent ), m_h( other.m_h ) {
				
			}
		
			Goal_Node( const Goal_Node& other ) 
				: m_fluent( other.m_fluent ), m_h( other.m_h ) {
			}
		
			Goal_Node&	operator=( Goal_Node&& other ) {
				m_fluent = other.m_fluent;
				m_h = other.m_h;
				return *this;
			}
		
			bool	operator>( const Goal_Node& n ) const {
				return m_h > n.m_h;
			}
		
			bool	operator<( const Goal_Node& n ) const {
				return m_h < n.m_h;
			}
		
			Lit	 	m_fluent;
			float		m_h;
		
		};

		typedef std::priority_queue<Goal_Node>				Goal_Queue;

	public:

		Relaxed_Plan_Heuristic( const FOND_Model& model )
			: m_model( model ) {
			m_best_supporter.resize( DNF_Clause::num_lits() );
			for ( unsigned i = 0; i < DNF_Clause::num_lits(); i++ )
				m_best_supporter[i] = nullptr;
			m_best_supporter_prob.resize( DNF_Clause::num_lits() );
			for ( unsigned i = 0; i < DNF_Clause::num_lits(); i++ )
				m_best_supporter_prob[i] = 0.0f;
			m_act_values.resize( model.n_actions() );
			for ( unsigned i = 0; i < model.n_actions(); i++ )
				m_act_values[i] = infty;
		}

		~Relaxed_Plan_Heuristic()
		{
		}

		void	compute_best_supporters( const Reachability_Heuristic& h ) {
			// 1. determine action values
			for ( unsigned i = 0; i < m_model.n_actions(); i++ ) {
				float h_precondition = h.eval( m_model.actions[i]->precondition );
				m_act_values[i] = h_precondition == infty ? infty : m_model.actions[i]->cost + h_precondition;
			}

			// 2. for each literal, determine best supporter
			std::vector< unsigned > adding_effs;
			for ( int i = 0; i < DNF_Clause::num_lits(); i++ ) {
				Lit p = toLit(i);
				for ( unsigned j = 0; j < m_model.n_actions(); j++ ) {
					if ( m_act_values[j] == infty ) continue;
					adding_effs.clear();
					m_model.actions[j]->adds( p, adding_effs );
					if ( adding_effs.empty() ) continue;
					float prob = float(adding_effs.size()) / float( m_model.actions[j]->effects.size() );
					if ( prob < m_best_supporter_prob[i] ) continue;
					if ( prob > m_best_supporter_prob[i] ) {
						m_best_supporter_prob[i] = prob;
						m_best_supporter[i] = m_model.actions[j];
						continue;
					}
					if ( m_act_values[j] > m_act_values[ m_best_supporter[i]->index ] )
						continue;
					m_best_supporter_prob[i] = prob;
					m_best_supporter[i] = m_model.actions[j];
				}
		 	}

		}

		float	eval( const Reachability_Heuristic& h, const DNF_Clause& G ) {
		
			std::vector<FOND_Model::Action*> relaxed_plan;
		
			for ( Lit g : G ) {
				if ( m_model.init.entails(g) ) continue;
				m_goal_queue.push( Goal_Node( g, h.eval( g ) ) );
			}	

			while ( !m_goal_queue.empty() ) {
				Goal_Node n = m_goal_queue.top();
				m_goal_queue.pop();
				if ( m_model.init.entails(n.m_fluent) ) continue;
				FOND_Model::Action* sup = m_best_supporter[ toInt( n.m_fluent ) ];
				assert( sup != nullptr );
				if ( std::find( relaxed_plan.begin(), relaxed_plan.end(), sup ) != relaxed_plan.end() ) continue;
				relaxed_plan.push_back( sup );
				for ( Lit prec : sup->precondition ) {
					m_goal_queue.push( Goal_Node( prec, h.eval(prec) ) );
				}
			}

			float rp_value = 0.0f;
			for ( FOND_Model::Action* a : relaxed_plan ) 
				rp_value += a->cost;
			
			return rp_value; 	
		}

	protected:

	private:
		
		const FOND_Model& 			m_model;
		std::vector< FOND_Model::Action* >	m_best_supporter;
		std::vector< float >			m_best_supporter_prob;
		std::vector< float >			m_act_values;
		Goal_Queue				m_goal_queue;
	};

}

#endif // rp.hxx

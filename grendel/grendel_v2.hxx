#ifndef __GRENDEL_PLANNER__
#define __GRENDEL_PLANNER__

#include <fond_inst.hxx>
#include <common.hxx>
#include <gp_atoms.hxx>
#include <open_list.hxx>
#include <ext_math.hxx>
#include <ex_policy.hxx>
#include <closed_list.hxx>
#include <hash_table.hxx>
#include <proc_nd_regression.hxx>
#include <stack>

namespace grendel {

	namespace scastar_dq {
		class Node {
		public:

			typedef DNF_Clause	State_Type;
			
			DNF_Clause			state;
			int				action_idx;
			float				hn;
			float				gn;
			float				fn;
			Node*				parent;
			size_t				hash_key;
			bool				hash_up_to_date;
			int 				trials;
			bool				strong;
			void*				hash_table;
			bool				expanded;
			bool				is_dead_end;
			bool				useless;
			bool				critical_path;

			Node( )
			: action_idx( -1 ), gn(0.0f), parent(nullptr), trials(0), strong(true), expanded(false), is_dead_end(false), useless( false ),
			critical_path( false ) {
			}
	
			Node( const DNF_Clause& c )
			: state(c), action_idx( -1 ), gn(0.0f), parent(nullptr), trials(0), strong(true), expanded(false), is_dead_end(false), useless( false ),
			critical_path( false ) {
			}	
	
			Node( const DNF_Clause& c, int _action_idx, float _gn = 0.0f, Node* _parent = nullptr, bool _strong = true ) 
			: state(c), action_idx( _action_idx ),  gn(_gn), parent(_parent), 
			trials(0), strong( _strong ), expanded( false ) , is_dead_end(false), useless( false ), critical_path( false ){
				hash_up_to_date = false;
				hash_table = nullptr;
			}

			~Node() {
				hash_up_to_date = false;
				gn = -1;
			}

			size_t	hash( ) const {
				return hash_key;
			}

			bool 	operator==( const Node& o ) const {
				return action_idx == o.action_idx && state == o.state;
			}

			size_t nbytes() const {
				size_t mem_used = sizeof( Node );
				return mem_used + state.nbytes();
			}
		};
	
		class Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				if( aptk::dgreater(a->fn, b->fn) ) return true;
				if ( aptk::dequal( a->fn, b->fn ) ) {
					if ( aptk::dgreater(a->hn, b->hn) ) return true;
					if ( aptk::dequal( a->hn, b->hn ) ) {
						if ( !a->critical_path && b->critical_path ) return true;
						return aptk::dgreater(a->gn, b->gn);
					}
				}
				return false;
			}
		};

		class Weak_Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				if ( a->trials < b->trials ) return true;
				if ( a->trials == b->trials ){
					if( aptk::dgreater(a->fn, b->fn) ) return true;
					if ( aptk::dequal( a->fn, b->fn ) ) {
						if ( aptk::dgreater(a->hn, b->hn) ) return true;
						if ( aptk::dequal( a->hn, b->hn ) ) {
							if ( !a->critical_path && b->critical_path ) return true;
							return aptk::dgreater(a->gn, b->gn);
						}
					}
				}
				return false;
			}
		};


	}

	template <typename Mutex_Heuristic>
	class Grendel_Planner {

		typedef  Open_List< scastar_dq::Comparer, scastar_dq::Node > 		AStar_Strong_Open_List;
		typedef  Open_List< scastar_dq::Weak_Comparer, scastar_dq::Node >	AStar_Weak_Open_List;
		typedef  aptk::search::Closed_List< scastar_dq::Node >			AStar_Closed_List;

	public:
		Grendel_Planner( const FOND_Model& model, const Mutex_Heuristic& h ) 
		: m_model( model ), m_mutex_func( h ), m_gen_count(0), m_exp_count(0), m_strong_cyclic_checks(0), m_max_cost_to_go( 0 ) {
			m_vtable_entry.hash_table = &m_vtable;
		}

		~Grendel_Planner() {
			#ifdef DEBUG	
			for ( typename AStar_Closed_List::iterator i = m_closed.begin();
				i != m_closed.end(); i++ ) {
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			m_closed.clear();

			std::cout << "Checking for duplicates in weak open hash:" << std::endl;
			for ( typename AStar_Closed_List::iterator i = m_weak_open_hash.begin();
				i != m_weak_open_hash.end(); i++ ) {
				for ( auto j = m_weak_open_hash.begin(); j != m_weak_open_hash.end(); j++ ) {
					if ( i == j ) continue;
					if (i->second == j->second) {
						std::cout << "Duplicate node found in Weak Hash:" << std::endl;
						print_node( std::cout, i->second );
					}
				}
			}
	
			for ( typename AStar_Closed_List::iterator i = m_weak_open_hash.begin();
				i != m_weak_open_hash.end(); i++ ) {
				if ( i->second->hash_table !=  &m_weak_open_hash ) continue;
				assert( i->second->hash_table == &m_weak_open_hash );
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			m_weak_open_hash.clear();
			#endif
		}

		void	start() {
			DNF_Clause ext_goal;
			m_mutex_func.add_consequences( m_model.goal, ext_goal );

			std::cout << "Goal (extended): " << std::endl;
			ext_goal.write( std::cout, m_model );
			std::cout << std::endl;

			m_mem_used = 0;
			m_init_is_dead_end = false;
			m_root = new scastar_dq::Node( ext_goal, -1 );
			m_root->strong = true;
			m_root->critical_path = true;
			eval( m_root );
			compute_hash( m_root );
			std::cout << "Root heuristic value: " << m_root->hn << std::endl;
			m_best_dist_to_init = m_root->hn;
			m_solution = nullptr;
			open_strong_node( m_root );
		}

		bool	
		find_strong_cyclic_plan( Explicit_Policy& plan ) {
			bool solved = do_search();
			extract_plan( plan );
			return solved;
		} 

		void		inc_gen() { m_gen_count++; }
		void		inc_exp() { m_exp_count++; }
		void		inc_strong_cyclic_check() { m_strong_cyclic_checks++; }

		unsigned	generated() const { return m_gen_count; }
		unsigned	expanded() const { return m_exp_count; }
		unsigned	strong_cyclic_checks() const { return m_strong_cyclic_checks; }
		float		min_cost_to_go() const { return m_min_cost_to_go; }
		float		max_cost_to_go() const { return m_max_cost_to_go; }
		unsigned	dead_ends() const { return m_dead_ends.size(); }
		unsigned	dead_end_action_pairs() const {
			unsigned count = 0;
			for ( auto entry = m_weak_open_hash.begin(); entry != m_weak_open_hash.end(); entry++ ) {
				if ( entry->second->is_dead_end ) count++;
			}
			return count;
		}
		unsigned	solved() const { return m_vtable.size(); }
		size_t		mem_used() const { return m_mem_used; }

		void		
		extract_plan( Explicit_Policy& plan ) {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				plan.add( it->second->state, it->second->action_idx, it->second->gn );
			}
		}

	protected:

		void 			
		close( scastar_dq::Node* n ) { 
			//std::cout << "CLOSED stats: "; 
			m_closed.put(n);
			if ( update_value( n ) ) {
				if ( aptk::dleq(n->hn, m_best_dist_to_init) && aptk::dgreater( n->hn + n->gn, m_max_cost_to_go ) ) {
					m_max_cost_to_go = n->gn + n->hn; 
					std::cout << "New upper bound on the cost to go: " << m_max_cost_to_go << std::endl;
					std::cout << "h(n) = " << n->hn << std::endl;
					std::cout << "g(n) = " << n->gn << std::endl;
					std::cout << "Critical path: " << (n->critical_path ? "yes" : "no" ) << std::endl;
					std::cout << "Mem used: " << mem_used() / (1024.*1024.) << " MBytes" << std::endl;
					std::cout << "Strong Cyclic State-Action pairs: " << m_closed.size() << std::endl;
					std::cout << "Dead end states: " << m_dead_ends.size() << std::endl;
				}
			}
			else {
				if ( aptk::dgeq(n->gn, get_value(n)) ) {
					#ifdef DEBUG
					std::cout << "Strong node NOT expanded because g(n)[" << n->gn << "] >= V(s)[" << get_value(n) << "]" << std::endl;
					#endif
					n->expanded = true; // Do not expand
				}
			}
		}
	
		AStar_Closed_List&	
		closed() { 
			return m_closed; 
		}

		const FOND_Model::Action* 
		get_action( scastar_dq::Node* n ) {
			if ( n->action_idx == -1 ) return nullptr;
			return m_model.actions[n->action_idx];
		}

		float
		get_covered_states_value( scastar_dq::Node* root ) {
			float max = 0.0f;
			float min = infty;
			for ( auto it = m_vtable.begin(); it != m_vtable.end(); it++ ) {
				if ( root->state.satisfies( it->second->state ) ) {
					#ifdef DEBUG
					std::cout << "\tCovering:" << std::endl;
					std::cout << "\t";
					it->second->state.write( std::cout, m_model, true );
					std::cout << std::endl;
					std::cout << "\tV(s) = " << it->second->gn << std::endl;
					#endif
					if ( it->second->gn > max ) 
						max = it->second->gn;
					if ( it->second->gn < min ) 
						min = it->second->gn;

				}	
			}

			for ( auto it = m_dead_ends.begin(); it != m_dead_ends.end(); it++ ) {
				if ( root->state.satisfies( it->second->state ) ) {
					#ifdef DEBUG
					std::cout << "\tCovering:" << std::endl;
					std::cout << "\t";
					it->second->state.write( std::cout, m_model, true );
					std::cout << std::endl;
					std::cout << "\tV(s) = " << infty << std::endl;
					#endif
					max = infty;
				}	
			}
			#ifdef DEBUG
			std::cout << "Min V(s) = " << min << std::endl;
			std::cout << "Max V(s) = " << max << std::endl; 
			#endif
			return min; 
		}

		class AG_Check_Node {
			public:
			typedef DNF_Clause	State_Type;

			AG_Check_Node( scastar_dq::Node* n, const DNF_Clause& ctx, const FOND_Model& model ) 
				: policy_entry( n ), context(ctx), index( no_idx ), lowlink( no_idx ), 
				parent( nullptr ), AG_connected(false), gn(0.0f), closed(false), hn(infty),
				the_model( model ) {
				aptk::Hash_Key hasher;
				context.update_hash( hasher );
				hasher.add( DNF_Clause::num_lits() + ( n->action_idx == -1 ? the_model.actions.size() : n->action_idx ) );
				hash_key = (size_t)hasher;
			}

			void 	compute_hash() {
				aptk::Hash_Key hasher;
				context.update_hash( hasher );
				hasher.add( DNF_Clause::num_lits() + ( policy_entry->action_idx == -1 ? the_model.actions.size() : policy_entry->action_idx ) );
				hash_key = (size_t)hasher;
			}
			
			size_t	hash( ) const {
				return hash_key;
			}

			bool 	operator==( const AG_Check_Node& o ) const {
				return context == o.context;
			}

			const int 		no_idx = std::numeric_limits<int>::max();
			scastar_dq::Node*	policy_entry;
			DNF_Clause		context;
			int			index;
			int			lowlink;
			AG_Check_Node*		parent;
			bool			AG_connected;
			float			gn;	
			bool			closed;
			size_t			hash_key;
			std::vector<bool>	closed_succ;
			float			hn;
			const			FOND_Model&	the_model;
		};

		typedef  aptk::search::Closed_List< AG_Check_Node >			AG_Closed_List;

		void	
		check_strong_dfs( scastar_dq::Node* root ) {
			#ifdef DEBUG
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Checking Strong Cyclic property: " << std::endl;
			print_node( std::cout, root, true );	
			#endif
			
			if( root->strong ) return;
			inc_strong_cyclic_check();

			int index = 0;
			std::stack< AG_Check_Node* > S;
			AG_Closed_List C;
			AG_Check_Node* v0 = new AG_Check_Node( root, root->state, m_model );
			v0->gn = root->gn;
			v0->hn = root->hn;
			strong_cyclic( v0, S, index, C );
		}

		bool
		check_terminality( const DNF_Clause& s, const std::vector<int>& action_mask, DNF_Clause& reason, int& reason_idx ) {
			#ifdef DEBUG
			std::cout << "Checking terminality: # dead-actions: " << action_mask.size() << std::endl;
			#endif
			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {
				if ( std::find( action_mask.begin(), action_mask.end(), i ) != action_mask.end() )
					continue;
				const DNF_Clause& prec = m_model.actions[i]->precondition;
				bool inconsistent = false;
				for ( Lit l : prec ) {
					if ( s.entails( ~l ) ) {
						inconsistent = true;
						reason.add(~l);
						break;
					}
				}
				if ( !inconsistent ) {
					#ifdef DEBUG
					std::cout << "Dead-end hypothesis rejected by action:" << std::endl;
					std::cout<< "\t" << m_model.actions[i]->name << std::endl;
					std::cout << "\t h(pre) = " << m_mutex_func.eval( prec ) << std::endl;
					#endif
					reason_idx = i;
					for ( Lit l : prec ) {
						if ( !s.is_defined(l) ) reason.add(l);
					}
					return false;
				}
			}
			return true;
		}

		scastar_dq::Node* 
		find_best_ag( const DNF_Clause& s, float accum, float& min_accum ) {
			scastar_dq::Node* best_ag = nullptr;
			for ( auto ag : m_closed ) {
				if ( ag.second->hn > accum ) {
					min_accum = std::min( min_accum, ag.second->hn );
					continue;
				}
				if ( best_ag != nullptr && aptk::dgeq( ag.second->gn, best_ag->gn ) ) continue;
				if ( s.satisfies( ag.second->state ) ) {
					if ( best_ag == nullptr || best_ag->gn > ag.second->gn )
						best_ag = ag.second;
				}
			}		
			return best_ag;	
		}		

		scastar_dq::Node*
		find_best_ef( const DNF_Clause& s, std::vector<int>& de_actions ) {
			scastar_dq::Node* best_ef = nullptr;
			for ( auto ef : m_weak_open_hash ) {
				if ( best_ef != nullptr && aptk::dgeq( ef.second->gn, best_ef->gn ) ) continue;
				if ( s.satisfies( ef.second->state ) ) {
					if ( ef.second->is_dead_end ) {
						de_actions.push_back( ef.second->action_idx );
						continue;
					}
					if ( best_ef == nullptr || best_ef->gn > ef.second->gn )
						best_ef = ef.second;
				}
			}
			return best_ef;	
		}

		void
		find_best_ef( const DNF_Clause& s, std::vector<int>& de_actions, std::vector< scastar_dq::Node* >& best_weak, float accum ) {
			assert( best_weak.empty() );
			float best_ef_cost = infty;
			for ( auto ef : m_weak_open_hash ) {
				if ( ef.second->hn > accum ) continue;
				if ( ef.second->useless ) continue;
				if ( s.satisfies( ef.second->state ) ) {
					if ( ef.second->is_dead_end ) {
						de_actions.push_back( ef.second->action_idx );
						continue;
					}
					if ( ef.second->gn < best_ef_cost ) {
						best_weak.clear();
						best_weak.push_back( ef.second );
						best_ef_cost = ef.second->gn;
						continue;
					}
					if ( aptk::dequal( ef.second->gn, best_ef_cost ) ) {
						best_weak.push_back( ef.second );
					}
				}
			}
			#ifdef DEBUG
			std::cout << "======= Best EF search =============" << std::endl;
			std::cout << "Best EF cost: " << best_ef_cost << std::endl;
			std::cout << "# Best EF pairs: " << best_weak.size() << std::endl;
			#endif
		}

		void
		strong_cyclic( AG_Check_Node* v, std::stack< AG_Check_Node* >& S, int index, AG_Closed_List& C ) {
			if ( v->hn > m_max_cost_to_go ) {
				#ifdef DEBUG
				std::cout << "This execution exceeds the max cost to go!" << std::endl; 
				#endif
				return;
			}	
			v->index = index;
			v->lowlink = index;
			S.push( v );
			index++;
			C.put( v );

			// Evaluate the successors
			const 	FOND_Model::Action& a = *(m_model.actions[v->policy_entry->action_idx]);
			v->closed_succ.resize( a.effects.size() );
			for ( unsigned k = 0; k < a.effects.size(); k++ ) 
				v->closed_succ[k] = false;
			for ( unsigned k = 0; k < a.effects.size(); k++ ) {
				auto eff = a.effects[k];
				DNF_Clause successor;
				bool 	valid = v->context.apply( a.precondition, eff, successor );
				assert(valid);
				if ( !valid ) {
					std::cerr << "This is a bug! - recompile in debug mode and check the assert" << std::endl;
					exit(1);
				}
				float 	succ_hn = std::max( v->hn + a.cost, m_mutex_func.eval( successor ) );
				assert( succ_hn != infty );

				float 	succ_gn = v->policy_entry->gn - a.cost;
				#ifdef DEBUG
				std::cout << "Resulting successor state:" << std::endl;
				successor.write( std::cout, m_model );
				std::cout << std::endl;
				std::cout << "h(n) = " << succ_hn << " g(n) = " << succ_gn;
				std::cout << " V(s) = " << get_value( successor ) << std::endl;
				#endif

				if ( is_dead_end( successor ) ) {
					#ifdef DEBUG
					std::cout << "Stumbled upon DEAD END" << std::endl;
					#endif
					v->policy_entry->is_dead_end = true;
					S.pop();
					C.remove(v);
					return;
				}

				if ( successor.satisfies( m_model.goal ) ) {
					#ifdef DEBUG
					std::cout << "Successor satisfies goal" << std::endl;
					#endif
					v->gn = std::max( v->gn, a.cost);
					v->closed_succ[k] = true;
					v->AG_connected = true;
					if ( v->policy_entry->parent == nullptr )
						v->policy_entry->parent = m_root;
					continue;
				}
				float min_hn = infty;
				scastar_dq::Node* best_ag = find_best_ag( successor, succ_hn, min_hn );
				
				if ( best_ag != nullptr ) {
					#ifdef DEBUG
					std::cout << "Successor satisfied by STRONG state-action pair: " << std::endl;
					print_node( std::cout, best_ag, true );
					#endif
					v->gn = std::max( v->gn, a.cost + best_ag->gn );
					v->closed_succ[k] = true;
					v->AG_connected = true;
					if ( v->policy_entry->parent == nullptr ) 
						v->policy_entry->parent = best_ag;
					continue;
				}
				if ( get_value(successor) != infty ) {
					#ifdef DEBUG
					std::cout << "State already SOLVED!" << std::endl;
					std::cout << "h(n) = " << min_hn << std::endl;
					#endif
					continue;
				}
				assert( get_value(successor) == infty );
				std::vector<int> de_actions;
				std::vector< scastar_dq::Node* > best_ef_vec;
				find_best_ef( successor, de_actions, best_ef_vec, succ_hn );			

				DNF_Clause 	reason;
				float 		best_ag_value = infty;
				std::vector< AG_Check_Node* > sub_graphs;
				for ( auto best_ef : best_ef_vec ) {
					#ifdef DEBUG
					std::cout << "Successor satisfied by WEAK state-action pair: " << std::endl;
					print_node( std::cout, best_ef, true );
					#endif
					AG_Check_Node* w = new AG_Check_Node( best_ef, successor, m_model );
					sub_graphs.push_back( w );
				}

				for ( AG_Check_Node* w : sub_graphs ) {
					AG_Check_Node* match = nullptr;
					for ( auto x : C ) {
						if ( x.second->context.satisfies( w->context ) ) {
							match = x.second;
							break;
						}
					}
					if ( match != nullptr ) {
						#ifdef DEBUG
						std::cout << "Successor is looping back!" << std::endl;
						#endif 
						v->lowlink = std::min(v->lowlink, match->index );
						v->closed_succ[k] = true;
						//const 	FOND_Model::Action& a = *(m_model.actions[v->policy_entry->action_idx]);
						//w->policy_entry->gn = std::max( w->policy_entry->gn, a.cost + match->gn ); 
						delete w;
						continue;
					}

					w->policy_entry->trials++;
					w->hn = succ_hn;
					w->gn = v->policy_entry->gn;
					w->parent = v;
					#ifdef DEBUG
					std::cout << "Recursion!" << std::endl;
					#endif
					strong_cyclic( w, S, index, C );
					if ( w->AG_connected && w->closed ) {
						bool valid = true;
						for ( Lit l : w->context )
							if ( successor.entails(~l) ) {
								#ifdef DEBUG
								std::cout << "==========================================================" << std::endl;
								std::cout << "Backpropagated literals contradict context!" << std::endl;
								std::cout << "Contradiction: ";
								std::cout << m_model.atoms[atom(l)]->name << std::endl;
								std::cout << "Backpropagated context:" << std::endl;
								w->context.write( std::cout, m_model, true );
								std::cout << std::endl;
								std::cout << "Applies to:" << std::endl;
								successor.write( std::cout, m_model );
								std::cout << std::endl;
								std::cout << "h(n) = " << succ_hn << " g(n) = " << succ_gn;
								std::cout << " V(s) = " << get_value( successor ) << std::endl;
								std::cout << "Outcome of:" << std::endl;
								print_node( std::cout, v->policy_entry, true );
								std::cout << "Context: " << std::endl;
								v->context.write( std::cout, m_model, true );
								std::cout << std::endl;
								std::cout << "==========================================================" << std::endl;
								#endif
								valid = false;
								break;
							}
						if ( valid ) {
							v->closed_succ[k] = true;
							v->AG_connected = true;
							if ( w->gn < best_ag_value ) {
								best_ag_value = w->gn;
								reason.clear();
								for ( Lit l : w->context ) {
									if ( !v->context.is_defined(l)  )
										reason.add(l);
								}
							}
						}
						else {
							de_actions.push_back( w->policy_entry->action_idx );
						}
					}
					if ( w->policy_entry->is_dead_end ) 
						de_actions.push_back( w->policy_entry->action_idx );
					delete w;
				}

				if ( v->AG_connected && v->closed_succ[k]) {
					#ifdef DEBUG
					std::cout << "Successor is hitting AG!" << std::endl;
					#endif 
					v->lowlink = v->index;
					v->gn = std::max( v->gn, a.cost + best_ag_value );
					for ( Lit l : reason )
						v->context.add(l);
					if ( !reason.empty() ) {
						C.remove(v);
						v->compute_hash();
						C.put(v);
					}
					continue;
				}

				if ( v->closed_succ[k] ) continue;

				bool dead_end = true;
				std::vector< DNF_Clause > options;
				std::vector< int >	  option_actions;
				#ifdef DEBUG
				std::cout << "Checking terminality: # dead-actions: " << de_actions.size() << std::endl;
				#endif
				for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {
					if ( std::find( de_actions.begin(), de_actions.end(), i ) != de_actions.end() )
						continue;
					const DNF_Clause& prec = m_model.actions[i]->precondition;
					bool inconsistent = false;
					for ( Lit l : prec ) {
						if ( successor.entails( ~l ) ) {
							inconsistent = true;
							if ( !v->context.entails(~l) )
								reason.add(~l);
							break;
						}
					}
					if ( !inconsistent ) {
						DNF_Clause option;
						DNF_Clause new_succ = successor;
						for ( Lit l : prec ) {
							new_succ.add(l);
							if ( !successor.is_defined(l) )
								option.add(l);
						}
						if ( m_mutex_func.eval( new_succ ) == infty )
							continue;
						#ifdef DEBUG
						std::cout << "Dead-end hypothesis rejected by action:" << std::endl;
						std::cout<< "\t" << m_model.actions[i]->name << std::endl;
						float h_pre = m_mutex_func.eval(prec);
						std::cout << "\t h(pre) = " << h_pre << std::endl;
						#endif
						if ( is_goal( new_succ ) ) { // This cannot be possible
							#ifdef DEBUG
							std::cout << "But this takes us to the initial state!" << std::endl;
							#endif
							continue;
						}
						float V = get_value(new_succ);
						float LB = get_lower_bound(new_succ);
						float new_succ_hn = std::max( succ_hn, m_mutex_func.eval( new_succ ) );
						if ( V != infty ) { // The state is already solved
							if ( new_succ_hn > LB ) {
								#ifdef DEBUG
								std::cout << "The state is already solved [V(s)=" << V << "]" << std::endl;
								std::cout << "But the precondition should be feasible" << std::endl;
								#endif
								options.emplace_back( option );
								option_actions.push_back( i );
								continue;
							}
							#ifdef DEBUG
							std::cout << "But this state is already solved V(s) = " << V << std::endl;
							std::cout << "Lower bound is: " << LB;
							std::cout << " vs " << new_succ_hn << std::endl;
							#endif
							continue;
						}

						auto new_weak_sa_pair = new scastar_dq::Node( new_succ );
						new_weak_sa_pair->action_idx = i;
						compute_hash( new_weak_sa_pair );
						new_weak_sa_pair->parent = nullptr;
						new_weak_sa_pair->hn = new_succ_hn;
						new_weak_sa_pair->gn = std::max( succ_gn, m_model.actions[i]->cost );
						if ( new_weak_sa_pair->hn + new_weak_sa_pair->gn > m_max_cost_to_go ) {
							#ifdef DEBUG
							std::cout << "New Weak pair exceeds max cost to go!" << std::endl;
							#endif
							delete new_weak_sa_pair;
						}
						else {
							new_weak_sa_pair->fn = new_weak_sa_pair->hn * 10.0f + new_weak_sa_pair->gn;
							new_weak_sa_pair->strong = false;
							if ( get_value( new_succ ) != infty || already_in_weak_frontier( new_weak_sa_pair, false ) ) {
								options.emplace_back( option );
								option_actions.push_back( i );
								delete new_weak_sa_pair;
								continue;
							}
							open_weak_node2( new_weak_sa_pair );
							#ifdef DEBUG
							std::cout << "Created new WEAK (s,a) pair:" << std::endl;
							print_node( std::cout, new_weak_sa_pair, true );
							#endif

						}
						dead_end = false;
					}
				}
				
				if ( dead_end ) {
					#ifdef DEBUG
					std::cout << "Successor is DEAD END" << std::endl;
					std::cout << "Reason: ";
					reason.write( std::cout, m_model, true );
					std::cout << std::endl;
					#endif
					if ( options.empty() ) {
						#ifdef DEBUG
						std::cout << "Recording dead-end:" << std::endl;
						successor.write( std::cout, m_model, true );
						std::cout << std::endl;
						std::cout << "Entry in the Weak Policy is a dead-end:" << std::endl;
						print_node( std::cout, v->policy_entry, true );
						#endif

						record_deadend( std::move(successor) );
						v->policy_entry->is_dead_end = true;
					}
					else {
						#ifdef DEBUG
						std::cout << "But we have options:" << std::endl;
						#endif
						int failed_opts = 0, useless_opts = 0;
						for ( unsigned k = 0; k < options.size(); k++ ) {
							auto option = options[k];
							#ifdef DEBUG
							std::cout << "Option #" << k+1 << ": ";
							option.write( std::cout, m_model, true );
							std::cout << std::endl;
							#endif
							DNF_Clause new_state = v->context;
							for ( Lit l : option )
								new_state.add(l);
							if ( is_goal( new_state ) ) {
								#ifdef DEBUG
								std::cout << "But this takes us to the initial state!" << std::endl;
								#endif
								failed_opts++;
								continue;
							}
							float V = get_value(new_state);
							float LB = get_lower_bound(new_state);
							if ( V != infty && aptk::dgeq(std::max(succ_gn, v->gn ), V) ) {
								#ifdef DEBUG
								std::cout << "But the option is already solved V(s) = " << V << std::endl;
								std::cout << "The g(n) for the new pair is: " << std::max(succ_gn, v->gn ) << std::endl;
								std::cout << "Lower bound is: " << LB;
								std::cout << " vs " << std::max( succ_hn, m_mutex_func.eval( new_state) ) << std::endl;
								#endif

								failed_opts++;
								continue;
							}
							auto new_weak_sa_pair = new scastar_dq::Node( new_state );
							new_weak_sa_pair->action_idx = v->policy_entry->action_idx;
							compute_hash( new_weak_sa_pair );
							assert( !is_goal(new_weak_sa_pair->state) );
							new_weak_sa_pair->parent = nullptr;
							new_weak_sa_pair->hn = std::max( succ_hn, m_mutex_func.eval( new_state ) );
							new_weak_sa_pair->gn = std::max( succ_gn, v->gn );
							new_weak_sa_pair->fn = new_weak_sa_pair->hn * 10.0f + new_weak_sa_pair->gn;
							new_weak_sa_pair->strong = false;
							auto other = m_weak_open_hash.retrieve( new_weak_sa_pair );
							if ( other == nullptr) {
								open_weak_node2( new_weak_sa_pair );
								v->policy_entry->useless = true;
								#ifdef DEBUG
								std::cout << "Created new WEAK (s,a) pair:" << std::endl;
								print_node( std::cout, new_weak_sa_pair, true );
								#endif
								#ifdef DEBUG
								std::cout << "Entry in the Weak policy is useless" << std::endl;
								print_node( std::cout, v->policy_entry, true );
								#endif
							}
							else {
								
								#ifdef DEBUG
								std::cout << "Option already in the Weak Policy" << std::endl;
								print_node( std::cout, other, true );	
								#endif
								if ( other->is_dead_end )
									failed_opts++;
								useless_opts++;	
								delete new_weak_sa_pair;
							}
						}
						if ( failed_opts == 0 && useless_opts == options.size() ) {
							#ifdef DEBUG
							std::cout << "All options failed" << std::endl;
							#endif
							#ifdef DEBUG
							std::cout << "Entry in the Weak policy is a useless" << std::endl;
							print_node( std::cout, v->policy_entry, true );
							#endif
							v->policy_entry->useless = true;
						}
						else if ( failed_opts == options.size() ) {
							#ifdef DEBUG
							std::cout << "All options failed" << std::endl;
							#endif
							record_deadend( std::move(successor) );
							v->policy_entry->is_dead_end = true;
							#ifdef DEBUG
							std::cout << "Entry in the Weak policy is a dead-end" << std::endl;
							print_node( std::cout, v->policy_entry, true );
							#endif
						}
					}
				}
				S.pop();
				C.remove(v);
				return;
			}
			
			v->closed = true;
			for ( bool b : v->closed_succ ) 
				v->closed = v->closed && b;

			if ( v->lowlink == v->index ) { // v is a root node of an SCC
				std::list< AG_Check_Node* > SCC;
				AG_Check_Node* w = nullptr;
				do {
					w = S.top();	
					S.pop();
					C.remove( w );
					SCC.push_back( w );
				} while ( w != v);
				#ifdef DEBUG
				std::cout << "SCC found with: " << SCC.size() << " pairs in it" << std::endl;
				#endif
				bool hits_AG = false;
				bool all_closed = true;
				for ( auto n : SCC ) {
					hits_AG = hits_AG || n->AG_connected;
					all_closed = all_closed && n->closed;
				}
				if ( hits_AG && all_closed ) {
					// Move all the nodes to $pi_\AG$, and update their values
					#ifdef DEBUG
					std::cout << "SCC is connected with $\\pi_{AG}$" << std::endl;
					#endif	
					for ( auto n : SCC ) {
						#ifdef DEBUG
						std::cout << "Processing member of SCC" << std::endl;
						n->context.write( std::cout, m_model, true ); std::cout << std::endl;
						std::cout << "h(n) = " << n->hn << " g(n) = " << n->gn << std::endl;
						std::cout << "Corresponding policy entry" << std::endl;
						print_node( std::cout, n->policy_entry, true );
						#endif
						if ( n->policy_entry->strong || get_value( n->policy_entry ) < infty )
							continue;
						m_weak_open_hash.remove( n->policy_entry );
						DNF_Clause reason;
						for ( Lit l : n->context ) {
							#ifdef DEBUG
							if ( n->policy_entry->state.entails(~l) ) {
								std::cout << "Contradiction: ";
								std::cout << m_model.atoms[atom(l)]->name << std::endl;
							}
							#endif
							assert( !n->policy_entry->state.entails(~l) );
							if ( !n->policy_entry->state.is_defined(l) )
								reason.add(l);
						}
						if ( reason.empty() ) {
							n->policy_entry->strong = true;
							n->policy_entry->gn = n->gn;
							n->policy_entry->hn = n->hn;
							n->policy_entry->fn = n->hn * 10.0f + n->gn;
							open_strong_node( n->policy_entry );
							#ifdef DEBUG
							std::cout << "Existing pair has the AG(EF\\G) property: " << std::endl;
							print_node( std::cout, n->policy_entry, true );
							#endif
						}
						else {
							#ifdef DEBUG
							std::cout << "Literals back-propagated:";
							reason.write( std::cout, m_model, true );
							std::cout << std::endl;
							#endif
							bool not_duplicated = false;
							for ( Lit l : reason ) {
								n->policy_entry->state.add( ~l );
								compute_hash( n->policy_entry );
								if ( !already_in_weak_frontier( n->policy_entry ) ) {
									not_duplicated = true;
									break;
								}
								n->policy_entry->state.remove( ~l );
							}
							if ( not_duplicated ) {
								eval( n->policy_entry );
								#ifdef DEBUG
								std::cout << "Previous (s,a) pair STRENGTHENED:" << std::endl;
								print_node( std::cout, n->policy_entry, true );
								#endif
							}
							else 
								n->policy_entry->useless = true;

							m_weak_open_hash.put( n->policy_entry );
							// Create new strong node
							auto new_strong_sa_pair = new scastar_dq::Node( n->context );
							new_strong_sa_pair->action_idx = a.index;
							compute_hash( new_strong_sa_pair );
							auto other = m_closed.retrieve( new_strong_sa_pair );
							if ( other != nullptr ) {
								for ( auto x : C ) {
									if (x.second->policy_entry == n->policy_entry )
										x.second->policy_entry = other;
								}
								n->policy_entry = other;
								delete new_strong_sa_pair;
							}
							else {
								new_strong_sa_pair->parent = nullptr;
								new_strong_sa_pair->gn = n->gn;
								new_strong_sa_pair->hn = n->hn;
								new_strong_sa_pair->fn = n->hn * 10.0f + n->gn;
								new_strong_sa_pair->strong = true;
								open_strong_node( new_strong_sa_pair );
								#ifdef DEBUG
								std::cout << "Created new STRONG (s,a) pair:" << std::endl;
								print_node( std::cout, new_strong_sa_pair, true );
								#endif
								for ( auto x : C ) {
									if (x.second->policy_entry == n->policy_entry )
										x.second->policy_entry = new_strong_sa_pair;
								}
								n->policy_entry = new_strong_sa_pair;
							}
						}
					}
				}
				if ( !hits_AG && all_closed )
					assert(false);
				for ( auto n : SCC ) {
					if ( n != v )
						delete n;
				}
			}

		}

		scastar_dq::Node* 
		get_node() {
			scastar_dq::Node* n = nullptr;
			float strong_fn = ( m_strong_open.empty() ? infty : m_strong_open.first()->fn );
			float weak_fn = ( m_weak_open.empty() ? infty : m_weak_open.first()->fn );
			if ( aptk::dgreater(strong_fn, weak_fn) ) {
				return get_weak_node();
			}
			if ( aptk::dless(strong_fn, weak_fn ) ) {
				return get_strong_node();
			}
			if ( !m_strong_open.empty() )
				return get_strong_node();
			if ( !m_weak_open.empty() )
				return get_weak_node();
			return n;
		}

		void
		eval( scastar_dq::Node* n ) {
			n->hn = m_mutex_func.eval( n->state );
			n->fn = ( n->hn == infty ? infty : aptk::add( n->gn, 10.0f*n->hn ) );
		}

		bool	
		do_search() {
			m_min_cost_to_go = infty;
			bool changed;
			do {
				changed = false;
				scastar_dq::Node* head = get_node();
				while(head) {
					if ( head->is_dead_end || head->useless ) {
						head = get_node();
						continue;
					}
					if ( is_goal(head->state) ) {
						std::cout << "======================================================================================================" << std::endl;
						std::cout << "Init reached" << std::endl;
						print_node( std::cout, head, true );
						std::cout << "Supporting execution:" << std::endl;
						scastar_dq::Node* tmp = head->parent;
						while ( tmp != nullptr ) {
							print_node( std::cout, tmp );
							tmp = tmp->parent;
						}	
						if ( head->hn > 0.0f ) {
							assert( false );
							head->useless = true;
							continue;
						}
						if ( head->strong ) {
							std::cout << "Strong Cyclic Policy found!" << std::endl;
							
							m_min_cost_to_go = std::min( m_min_cost_to_go, head->gn );
							return true;
						}
					}
				
					if ( head->strong ) {
						expand_node( head );
						head = get_node();
						continue;
					}
					if ( head->gn + head->hn >  m_max_cost_to_go ) {
						#ifdef DEBUG
						std::cout << "================================================================================" << std::endl;
						std::cout << "Node processing delayed due to g(n) + h(n) > UB(s0) [" << m_max_cost_to_go << "]" << std::endl;
						print_node( std::cout, head );
						#endif
						head = get_node();
						continue;
					}
					float min_covered_value = get_covered_states_value(head);

					if ( min_covered_value != infty ) {
						#ifdef DEBUG
						std::cout << "State-Action pair is NOT USEFUL" << std::endl;
						#endif
						head->useless = true;
						head = get_node();
						continue;
					}
					
					check_strong_dfs( head );
	
					head = get_node();
				}

				std::cout << "====================================================================================" << std::endl;
				std::cout << "Mopping up weak states..." << std::endl;
				std::cout << "Pairs in the Weak Policy: " << m_weak_open_hash.size() << std::endl;
				int	predicted_sc_checks = 0;
				int	useless_weak_nodes = 0;
				AStar_Weak_Open_List weak_dead;
				for ( auto weak_pair : m_weak_open_hash ) {
					auto n = weak_pair.second;
					if ( n->strong ) {
						#ifdef DEBUG
						std::cout << "====================================================================================" << std::endl;
						std::cout << "Strong node found in $\\pi_{EF}$!!!" << std::endl;
						std::cout << n << std::endl;
						print_node( std::cout, n, true );
						#endif
						assert( get_value(n) < infty );
						m_weak_open_hash.remove( n );
						continue;
					}
					if ( n->is_dead_end || n->useless ) {
						useless_weak_nodes++;
						continue;
					}
					if ( aptk::dgeq((n->gn + n->hn), m_min_cost_to_go) ) {
						#ifdef DEBUG
						std::cout << "====================================================================================" << std::endl;
						std::cout << "Weak condition-operator pair PRUNED" << std::endl;
						print_node( std::cout, n );
						std::cout << "g(n) + h(n) > min cost to go [" << m_min_cost_to_go << "]" << std::endl;
						#endif
						n->useless = true;
						useless_weak_nodes++;
						continue;
					}
					if ( aptk::dgreater(n->gn,  get_value(n)) || get_value(n) < infty ) {
						#ifdef DEBUG
						std::cout << "====================================================================================" << std::endl;
						std::cout << "Weak condition-operator pair PRUNED" << std::endl;
						print_node( std::cout, n );
						std::cout << "g(n) >= V(n)[" << get_value(n)<< "]"<< std::endl;
						#endif
						n->useless = true;
						useless_weak_nodes++;
						continue;
					}

					float min_covered_value = get_covered_states_value(n);
	
					if ( min_covered_value != infty ) {
						#ifdef DEBUG
						std::cout << "====================================================================================" << std::endl;
						std::cout << "Weak condition-operator pair PRUNED" << std::endl;
						print_node( std::cout, n );
						std::cout << "Q(o,\\phi) >= V(\\phi)[" << min_covered_value << "]"<< std::endl;
						#endif
						n->useless = true;
						useless_weak_nodes++;
						continue;
					}
					if ( n->gn > m_max_cost_to_go ) {
						#ifdef DEBUG
						std::cout << "====================================================================================" << std::endl;
						std::cout << "Processing delayed due to upper bound" << std::endl;
						print_node( std::cout, n );
						#endif
						continue;
					}
					if ( n->hn >  m_max_cost_to_go ) {
						#ifdef DEBUG
						std::cout << "================================================================================" << std::endl;
						std::cout << "Node processing delayed due to h(n) > UB(s0) [" << m_max_cost_to_go << "]" << std::endl;
						print_node( std::cout, n );
						#endif
						continue;
					}	

					weak_dead.insert( n );
					predicted_sc_checks++;
				}
				std::cout << "Useless Weak Nodes = " << useless_weak_nodes << std::endl;
				std::cout << "Pairs to check the strong cyclic property: " << predicted_sc_checks << std::endl;
				float old_max_cost_to_go = m_max_cost_to_go;
				float old_best_dist_to_init = m_best_dist_to_init;
				int max_num_trials = 0;
				int	sc_checks_done = 0;
				while ( !weak_dead.empty() ) {
					scastar_dq::Node* n = weak_dead.pop();
					#ifdef DEBUG
					std::cout << "======================================================================================================" << std::endl;
					std::cout << "Difference with max cost to go: " << m_max_cost_to_go - (n->gn + n->hn) << std::endl;
					std::cout << "V(s) = " << get_value(n) << std::endl;
					#endif
					if ( get_value(n) < infty ) {
						n->useless = true;
						#ifdef DEBUG
						std::cout << "Already solved!" << std::endl;
						#endif
						continue;
					}
					max_num_trials = std::max( max_num_trials, n->trials );
					check_strong_dfs( n );
					sc_checks_done++;
					if ( n->strong ) {
						std::cout << "====================================================================================" << std::endl;
						std::cout << "$\\pi_{AG}$ has been updated!" << std::endl;
						print_node( std::cout, n, true );
						break;
					}
				}
				std::cout << "====================================================================================" << std::endl;
				std::cout << "Old UB(s0)=" << old_max_cost_to_go << " New UB(s0) = " << m_max_cost_to_go << std::endl;
				std::cout << "Old Best Distance to Init= " << old_best_dist_to_init;
				std::cout << " New Best Distance to Init = " << m_best_dist_to_init << std::endl;
				std::cout << " Max # trials = " << max_num_trials << std::endl;
				std::cout << "S.C. checks done = " << sc_checks_done << std::endl;
				changed = !m_strong_open.empty();
			} while ( changed );

			return false;			
		}

		void
		print_node( std::ostream& os, scastar_dq::Node* n, bool show_negative_lits = false ) {
			os << "\t";
			n->state.write( os, m_model, show_negative_lits);
			os << std::endl;
			if ( n->action_idx == -1 ) 
				os << "-> (GOAL)" << std::endl;
			else
				os << "-> " << m_model.actions[n->action_idx]->name << std::endl;

			os << " address(n) = " << n;
			os << " f(n) = " << n->fn;
			os << " g(n) = " << n->gn;
			os << " h(n) = " << n->hn << std::endl;
			os << " trials(n) = " << n->trials;
			os << " strong(n) = " << ( n->strong ? "yes" : "no" );
			os << " deadend(n) = " << ( n->is_dead_end ? "yes" : "no" ) << std::endl;
			os << " critical(n) = " << ( n->critical_path ? "yes" : "no" ) << std::endl;
			os << " useless(n) = " << ( n->useless ? "yes" : "no" );
			os << " hashkey= " << n->hash_key << std::endl;
				
		}

		void
		compute_hash( scastar_dq::Node* n ) {
			aptk::Hash_Key hasher;
			n->state.update_hash( hasher );
			hasher.add( DNF_Clause::num_lits() + ( n->action_idx == -1 ? m_model.actions.size() : n->action_idx ) );
			n->hash_key = (size_t)hasher;
			n->hash_up_to_date = true;
		}		

		bool
		relevance_check( const DNF_Clause& s, const FOND_Model::Action& a ) const {
			for ( auto eff_it = a.effects.begin(); eff_it != a.effects.end(); eff_it++ ) {
				for ( auto eff_it2 = eff_it->begin(); eff_it2 != eff_it->end(); eff_it2++ )
					for ( auto eff_it3 = eff_it2->effect.begin(); eff_it3 != eff_it2->effect.end(); eff_it3++ ) {
						if ( s.entails( *eff_it3 ) ) return true;
						if ( a.effects.size() == 1 && eff_it2->condition.empty() && s.entails( ~(*eff_it3) ) ) return false;	
					}
			}

			return false; // action doesn't mention any of the lits in the formula to be regressed
		}

		bool 		
		is_closed( scastar_dq::Node* n ) { 
			scastar_dq::Node* n2 = this->closed().retrieve(n);
			return n2 != nullptr;	
		}
		
		bool
		is_goal( const DNF_Clause& c ) {
			return m_model.init.satisfies(c);
		}
	
		scastar_dq::Node* 		
		get_strong_node() {
			scastar_dq::Node* next = nullptr;
			if(! m_strong_open.empty() ) {
				next = m_strong_open.pop();
			}
			return next;
		}

		scastar_dq::Node*
		get_weak_node() {
			scastar_dq::Node* next = nullptr;
			if(! m_weak_open.empty() ) {
				next = m_weak_open.pop();
			}
			return next;			
		}

		void	 	
		open_strong_node( scastar_dq::Node *n ) {
			m_strong_open.insert(n);
			close(n);
			inc_gen();
			m_mem_used += n->nbytes();
		}

		bool	
		is_mutex( const DNF_Clause& c ) const {
			return m_mutex_func.eval( c ) == infty;
		}

		bool	
		is_mutex( const DNF_Clause& c, const DNF_Clause& d ) const {
			return m_mutex_func.eval( c, d ) == infty;
		}

		void
		open_weak_node2( scastar_dq::Node *n ) {
			assert( !already_in_weak_frontier(n) );	
			m_weak_open.insert(n);
			m_weak_open_hash.put(n);
			inc_gen();
			m_mem_used += n->nbytes();
		}

		void	 	
		open_weak_node( scastar_dq::Node *n ) {
			assert( !already_in_weak_frontier(n) );	
			m_weak_open.insert(n);
			if (!already_in_weak_frontier(n) ) {
				m_weak_open_hash.put(n);
				inc_gen();
				m_mem_used += n->nbytes();
			}
		}
		
		bool 			
		already_in_weak_frontier( scastar_dq::Node *n, bool update_cost_to_go = true ) {
			scastar_dq::Node *previous_copy = m_weak_open_hash.retrieve(n);
			if ( previous_copy != nullptr ) {
				#ifdef DEBUG
				std::cout << "Match found for state-action pair:" << std::endl;
				print_node( std::cout, n, true );
				std::cout << "Matching state-action pair: " << std::endl;
				print_node( std::cout, previous_copy, true );
				#endif
				if ( update_cost_to_go && previous_copy->gn > n->gn ) {
					previous_copy->gn = n->gn;
					previous_copy->fn = n->hn + 10.0f* n->gn;
					return true;
				}
			}
			return previous_copy != nullptr;
		}

		void
		expand_node( scastar_dq::Node *head ) {
			if( head->expanded ) return;
			#ifdef DEBUG
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Expanding: (min cost to go = " << m_min_cost_to_go << ", max cost to go = " << m_max_cost_to_go << " )" << std::endl;
			print_node( std::cout, head );		
			std::cout << "V(s) = " << get_value(head) << std::endl;
			#endif
			head->expanded = true;
			if ( aptk::dgeq( (head->gn+head->hn), m_min_cost_to_go) ) {
				#ifdef DEBUG
				std::cout << "Predecessors pruned: g(n) + h(n) > V(s0)" << std::endl;
				#endif 
				return;
			}
			if ( head->gn > get_value(head) ) { 
				#ifdef DEBUG
				std::cout << "Predecessors pruned: g(n) > V(s)" << std::endl;
				#endif
				return;
			}
			inc_exp();
			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {	
				if ( !relevance_check( head->state, *(m_model.actions[i]) ) ) continue;

				std::vector<DNF_Clause>  succ;
				procedural_strong_cyclic_regress2( m_model, i, head->state, succ );
				
				for ( auto it = succ.begin(); it != succ.end(); it++ ) {
					scastar_dq::Node* n = new scastar_dq::Node( *it, i, head->gn + m_model.actions[i]->cost, head, false );
					
					eval(n);
					if ( n->hn == infty ) {
						delete n;
						continue;
					}
					
					if (aptk::dgeq( (n->gn+n->hn), m_min_cost_to_go) ) {
						delete n;
						continue;
					}

										
					if ( aptk::dgeq(n->gn, get_value(n) )) {
						#ifdef DEBUG
						std::cout << "Predecessor:" << std::endl;
						print_node( std::cout, n );
						std::cout << "Pruned because V(s) [" << get_value(n) << "] >= g(n)" << std::endl;
						#endif
						delete n;
						continue;
					}
					if ( n->hn < head->hn ) n->critical_path = true;
					DNF_Clause ext_succ;
					m_mutex_func.add_consequences( *it, ext_succ );
					n->state = std::move(ext_succ);

					compute_hash( n );
					if ( is_closed( n ) ) {
						#ifdef DEBUG
						std::cout << "Predecessor:" << std::endl;
						print_node( std::cout, n );
						std::cout << "Pruned because already in the Strong Cyclic Policy" << std::endl;
						#endif

						delete n;
						continue;
					}
					if( already_in_weak_frontier(n) ) {
						#ifdef DEBUG
						std::cout << "Predecessor:" << std::endl;
						print_node( std::cout, n );
						std::cout << "Pruned because already in the Weak Policy" << std::endl;
						#endif
						delete n;
						continue;
					}
					open_weak_node( n );
					#ifdef DEBUG
					std::cout << "\t through " << m_model.actions[i]->name << " results in new WEAK predecessor " << std::endl;
					print_node( std::cout, n );
					std::cout << "V(s) = " << get_value( n ) << std::endl;
					std::cout << std::endl;
					#endif
				}	
			} 
		}

		bool	
		update_value( scastar_dq::Node* n ) {
			#ifdef DEBUG
			std::cout << "V(s) = " << get_value(n) << " V'(s) = " << n->gn << std::endl;
			#endif
			if ( aptk::dgeq( n->gn, get_value(n) ) ) {
				#ifdef DEBUG
				std::cout << "Potential cycle" << std::endl;
				#endif
				return false;
			}
			#ifdef DEBUG
			std::cout << "Needs to be updated" << std::endl;
			#endif
			scastar_dq::Node* new_entry = new scastar_dq::Node( n->state );
			new_entry->gn = n->gn;
			new_entry->hn = n->hn;
			compute_hash( new_entry );
			m_vtable.put( new_entry );
			return true;
		}

		float	
		get_lower_bound( DNF_Clause& s ) {
			m_vtable_entry.state = std::move( s );
			compute_hash(&m_vtable_entry);
			auto entry = m_vtable.retrieve( &m_vtable_entry );
			s = std::move( m_vtable_entry.state );
			if ( entry == nullptr )
				return infty;	
			return entry->hn;
		}

		float	
		get_value( DNF_Clause& s ) {
			m_vtable_entry.state = std::move( s );
			compute_hash(&m_vtable_entry);
			auto entry = m_vtable.retrieve( &m_vtable_entry );
			s = std::move( m_vtable_entry.state );
			if ( entry == nullptr )
				return infty;	
			return entry->gn;
		}

		float	
		get_lower_bound( scastar_dq::Node* n ) {
			m_vtable_entry.state = std::move( n->state );
			compute_hash(&m_vtable_entry);
			auto entry = m_vtable.retrieve( &m_vtable_entry );
			n->state = std::move( m_vtable_entry.state );
			if ( entry == nullptr )
				return infty;	
			return entry->hn;
		}

		float	
		get_value( scastar_dq::Node* n ) {
			m_vtable_entry.state = std::move( n->state );
			compute_hash(&m_vtable_entry);
			auto entry = m_vtable.retrieve( &m_vtable_entry );
			n->state = std::move( m_vtable_entry.state );
			if ( entry == nullptr )
				return infty;	
			return entry->gn;
		}

		void
		record_deadend( DNF_Clause&& s ) {
			auto tmp = new scastar_dq::Node;
			tmp->state = s;
			tmp->hash_table = &m_dead_ends;
			compute_hash( tmp );
			auto entry = m_dead_ends.retrieve( tmp );
			if ( entry != nullptr ) {
				delete tmp;
				return;
			}
			m_dead_ends.put( tmp );
		}

		void
		record_deadend( const DNF_Clause& s ) {
			auto tmp = new scastar_dq::Node;
			tmp->state = s;
			tmp->hash_table = &m_dead_ends;
			compute_hash( tmp );
			auto entry = m_dead_ends.retrieve( tmp );
			if ( entry != nullptr ) {
				delete tmp;
				return;
			}
			m_dead_ends.put( tmp );
		}

		bool
		is_dead_end( const DNF_Clause& s ) {
			for ( auto it = m_dead_ends.begin();
				it != m_dead_ends.end();
				it++ )
				if ( s.satisfies( it->second->state ) )
					return true;
			return false;
		}

	public:
		void
		print_values( std::ostream& os ) {
			for ( auto it = m_vtable.begin(); it != m_vtable.end(); it++ ) {
				it->second->state.write( os, m_model );
				os << " <- " << it->second->gn << std::endl;
			}
		}

		void
		print_deadends( std::ostream& os ) {
			for ( auto it = m_dead_ends.begin(); it != m_dead_ends.end(); it++ ) {
				it->second->state.write( os, m_model );
				os << std::endl;
			}	
		}

	private:
		const FOND_Model&			m_model;
		AStar_Strong_Open_List			m_strong_open;
		AStar_Weak_Open_List			m_weak_open;
		const Mutex_Heuristic&			m_mutex_func;
		unsigned				m_gen_count;
		unsigned				m_exp_count;
		unsigned				m_strong_cyclic_checks;
		AStar_Closed_List			m_closed;
		AStar_Closed_List			m_weak_open_hash;
		scastar_dq::Node*			m_root;
		scastar_dq::Node*			m_solution;
		size_t					m_mem_used;
		float					m_min_cost_to_go;
		AStar_Closed_List			m_vtable;
		scastar_dq::Node			m_vtable_entry;
		AStar_Closed_List			m_dead_ends;
		float					m_max_cost_to_go;
		bool					m_init_is_dead_end;
		float					m_best_dist_to_init;
	};

}

#endif // ucs.hxx

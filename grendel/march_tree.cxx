#include <grendel/march_tree.hxx>

namespace grendel {
	
	March_Tree::March_Tree( const FOND_Model& model ) 
	: m_model( model ){
		m_root = new Tree_Node( 0, nullptr );
	}

	March_Tree::~March_Tree() {
		for ( Tree_Node* n : m_tree_nodes )
			delete n;
	}

	void
	March_Tree::add( Search_Node* n ) {
		add( n, m_root, 0 );	
	}

	void
	March_Tree::add( Search_Node* n, Tree_Node* m, unsigned d ) {
		if ( m->the_atom == (int)m_model.n_atoms() ) { // leaf
			m->search_nodes.push_back( n );
			return;
		} 
		lbool truth_val = m->select_child( n->state );
		Tree_Node* next = m->get_child( truth_val );
		if ( next == nullptr ) {
			next = new Tree_Node( d+1, m );
			m_tree_nodes.push_back( next );
			m->set_child( truth_val, next );
		}
		add( n, next, d+1 );	
	}

	void
	March_Tree::retrieve( const DNF_Clause& s, std::list<Search_Node*>& nodes ) {

		retrieve( s, m_root, nodes ); 
	}

	void
	March_Tree::retrieve( const DNF_Clause& s, Tree_Node* m, std::list<Search_Node*>& nodes ) {
		if ( m->the_atom == (int)m_model.n_atoms() ) { // leaf
			for ( Search_Node* n : m->search_nodes ) {
				if ( n->loopy ) continue;
				if ( n->useless ) continue;
				nodes.push_back(n);
			}
			return; 
		}

		lbool truth_val = m->select_child( s );
		Tree_Node* next = m->get_child( truth_val );
		if ( next != nullptr ) {
			retrieve( s, next, nodes );
		}

		if ( truth_val != l_Undef ) {
			next = m->get_child( l_Undef );
			if ( next != nullptr )
				retrieve( s, next, nodes );
		}

	}
}

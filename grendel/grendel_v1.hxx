#ifndef __GRENDEL_PLANNER__
#define __GRENDEL_PLANNER__

#include <fond_inst.hxx>
#include <common.hxx>
#include <gp_atoms.hxx>
#include <open_list.hxx>
#include <ext_math.hxx>
#include <ex_policy.hxx>
#include <closed_list.hxx>
#include <hash_table.hxx>
#include <proc_nd_regression.hxx>

namespace grendel {

	namespace scastar_dq {
		class Node {
		public:

			typedef DNF_Clause	State_Type;
			
			DNF_Clause			state;
			int				action_idx;
			float				hn;
			float				gn;
			float				fn;
			Node*				parent;
			size_t				hash_key;
			bool				hash_up_to_date;
			int 				trials;
			bool				strong;
			void*				hash_table;
			bool				expanded;
			bool				is_dead_end;
			bool				useless;
			bool				critical_path;
			int				index;
			int				low;

			Node( )
			: action_idx( -1 ), gn(0.0f), parent(nullptr), trials(0), strong(true), expanded(false), is_dead_end(false), useless( false ),
			critical_path( false ) {
			}
	
			Node( const DNF_Clause& c )
			: state(c), action_idx( -1 ), gn(0.0f), parent(nullptr), trials(0), strong(true), expanded(false), is_dead_end(false), useless( false ),
			critical_path( false ) {
			}	
	
			Node( const DNF_Clause& c, int _action_idx, float _gn = 0.0f, Node* _parent = nullptr, bool _strong = true ) 
			: state(c), action_idx( _action_idx ),  gn(_gn), parent(_parent), 
			trials(0), strong( _strong ), expanded( false ) , is_dead_end(false), useless( false ), critical_path( false ){
				hash_up_to_date = false;
				hash_table = nullptr;
			}

			~Node() {
				hash_up_to_date = false;
				gn = -1;
			}

			size_t	hash( ) const {
				return hash_key;
			}

			bool 	operator==( const Node& o ) const {
				return action_idx == o.action_idx && state == o.state;
			}

			size_t nbytes() const {
				size_t mem_used = sizeof( Node );
				return mem_used + state.nbytes();
			}
		};
	
		class Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				if( aptk::dgreater(a->fn, b->fn) ) return true;
				if ( aptk::dequal( a->fn, b->fn ) ) {
					if ( aptk::dgreater(a->hn, b->hn) ) return true;
					if ( aptk::dequal( a->gn, b->hn ) ) {
						return aptk::dgreater(a->gn, b->gn);
					}
				}
				return false;
			}
		};

		class Weak_Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				//if ( a->trials > b->trials ) return true;
				//if ( a->trials == b->trials ){
					if( aptk::dgreater(a->fn, b->fn) ) return true;
					if ( aptk::dequal( a->fn, b->fn ) ) {
						if ( aptk::dgreater(a->hn, b->hn) ) return true;
						if ( aptk::dequal( a->gn, b->hn ) ) {
							return aptk::dgreater(a->gn, b->gn);
						}
					}
				//}
				return false;
			}
		};


	}

	template <typename Mutex_Heuristic>
	class Grendel_Planner {

		typedef  Open_List< scastar_dq::Comparer, scastar_dq::Node > 		AStar_Strong_Open_List;
		typedef  Open_List< scastar_dq::Weak_Comparer, scastar_dq::Node >	AStar_Weak_Open_List;
		typedef  aptk::search::Closed_List< scastar_dq::Node >			AStar_Closed_List;

	public:
		Grendel_Planner( const FOND_Model& model, const Mutex_Heuristic& h ) 
		: m_model( model ), m_mutex_func( h ), m_gen_count(0), m_exp_count(0), m_strong_cyclic_checks(0), m_max_cost_to_go( 0 ) {
			m_vtable_entry.hash_table = &m_vtable;
		}

		~Grendel_Planner() {
			#ifdef DEBUG	
			for ( typename AStar_Closed_List::iterator i = m_closed.begin();
				i != m_closed.end(); i++ ) {
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			m_closed.clear();

			std::cout << "Checking for duplicates in weak open hash:" << std::endl;
			for ( typename AStar_Closed_List::iterator i = m_weak_open_hash.begin();
				i != m_weak_open_hash.end(); i++ ) {
				for ( auto j = m_weak_open_hash.begin(); j != m_weak_open_hash.end(); j++ ) {
					if ( i == j ) continue;
					if (i->second == j->second) {
						std::cout << "Duplicate node found in Weak Hash:" << std::endl;
						print_node( std::cout, i->second );
					}
				}
			}
	
			for ( typename AStar_Closed_List::iterator i = m_weak_open_hash.begin();
				i != m_weak_open_hash.end(); i++ ) {
				if ( i->second->hash_table !=  &m_weak_open_hash ) continue;
				assert( i->second->hash_table == &m_weak_open_hash );
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			m_weak_open_hash.clear();
			#endif
		}

		void	start() {
			DNF_Clause ext_goal;
			m_mutex_func.add_consequences( m_model.goal, ext_goal );

			std::cout << "Goal (extended): " << std::endl;
			ext_goal.write( std::cout, m_model );
			std::cout << std::endl;

			m_mem_used = 0;
			m_init_is_dead_end = false;
			m_root = new scastar_dq::Node( ext_goal, -1 );
			m_root->strong = true;
			m_root->critical_path = true;
			eval( m_root );
			compute_hash( m_root );
			std::cout << "Root heuristic value: " << m_root->hn << std::endl;
			m_best_dist_to_init = m_root->hn;
			m_solution = nullptr;
			open_strong_node( m_root );
		}

		bool	
		find_strong_cyclic_plan( Explicit_Policy& plan ) {
			bool solved = do_search();
			extract_plan( plan );
			return solved;
		} 

		void		inc_gen() { m_gen_count++; }
		void		inc_exp() { m_exp_count++; }
		void		inc_strong_cyclic_check() { m_strong_cyclic_checks++; }

		unsigned	generated() const { return m_gen_count; }
		unsigned	expanded() const { return m_exp_count; }
		unsigned	strong_cyclic_checks() const { return m_strong_cyclic_checks; }
		float		min_cost_to_go() const { return m_min_cost_to_go; }
		float		max_cost_to_go() const { return m_max_cost_to_go; }
		unsigned	dead_ends() const { return m_dead_ends.size(); }
		unsigned	dead_end_action_pairs() const {
			unsigned count = 0;
			for ( auto entry = m_weak_open_hash.begin(); entry != m_weak_open_hash.end(); entry++ ) {
				if ( entry->second->is_dead_end ) count++;
			}
			return count;
		}
		unsigned	solved() const { return m_vtable.size(); }
		size_t		mem_used() const { return m_mem_used; }

		void		
		extract_plan( Explicit_Policy& plan ) {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				plan.add( it->second->state, it->second->action_idx, it->second->gn );
			}
		}

	protected:

		void 			
		close( scastar_dq::Node* n ) { 
			//std::cout << "CLOSED stats: "; 
			m_closed.put(n);
			if ( update_value( n ) ) {
				if ( aptk::dleq(n->hn, m_best_dist_to_init) && aptk::dgreater( n->hn + n->gn, m_max_cost_to_go ) ) {
					m_max_cost_to_go = n->gn + n->hn; 
					std::cout << "New upper bound on the cost to go: " << m_max_cost_to_go << std::endl;
					std::cout << "h(n) = " << n->hn << std::endl;
					std::cout << "g(n) = " << n->gn << std::endl;
					std::cout << "Critical path: " << (n->critical_path ? "yes" : "no" ) << std::endl;
					std::cout << "Mem used: " << mem_used() / (1024.*1024.) << " MBytes" << std::endl;
					std::cout << "Strong Cyclic State-Action pairs: " << m_closed.size() << std::endl;
					std::cout << "Dead end states: " << m_dead_ends.size() << std::endl;
				}
			}
			else {
				if ( aptk::dgeq(n->gn, get_value(n)) ) {
					#ifdef DEBUG
					std::cout << "Strong node NOT expanded because g(n)[" << n->gn << "] >= V(s)[" << get_value(n) << "]" << std::endl;
					#endif
					n->expanded = true; // Do not expand
				}
			}
		}
	
		AStar_Closed_List&	
		closed() { 
			return m_closed; 
		}

		class SC_Check_Node {
		public:
			SC_Check_Node(  const DNF_Clause& _context, scastar_dq::Node* sa_pair, SC_Check_Node* ptr_parent ) 
				: context( _context ), n( sa_pair ), parent( ptr_parent ) {
			}
			
			DNF_Clause		context;
			scastar_dq::Node* 	n;
			SC_Check_Node*		parent;
		};

		class SC_Check_Node_Queue : public std::queue< SC_Check_Node*, std::list<SC_Check_Node*> > {
		public:
			virtual ~SC_Check_Node_Queue() {
				while ( !this->empty() ) {
					auto n = this->front();
					delete n;
					this->pop();
				}
			}
		};

		const FOND_Model::Action* 
		get_action( scastar_dq::Node* n ) {
			if ( n->action_idx == -1 ) return nullptr;
			return m_model.actions[n->action_idx];
		}

		float
		get_covered_states_value( scastar_dq::Node* root ) {
			float max = 0.0f;
			float min = infty;
			for ( auto it = m_vtable.begin(); it != m_vtable.end(); it++ ) {
				if ( root->state.satisfies( it->second->state ) ) {
					#ifdef DEBUG
					std::cout << "\tCovering:" << std::endl;
					std::cout << "\t";
					it->second->state.write( std::cout, m_model, true );
					std::cout << std::endl;
					std::cout << "\tV(s) = " << it->second->gn << std::endl;
					#endif
					if ( it->second->gn > max ) 
						max = it->second->gn;
					if ( it->second->gn < min ) 
						min = it->second->gn;

				}	
			}

			for ( auto it = m_dead_ends.begin(); it != m_dead_ends.end(); it++ ) {
				if ( root->state.satisfies( it->second->state ) ) {
					#ifdef DEBUG
					std::cout << "\tCovering:" << std::endl;
					std::cout << "\t";
					it->second->state.write( std::cout, m_model, true );
					std::cout << std::endl;
					std::cout << "\tV(s) = " << infty << std::endl;
					#endif
					max = infty;
				}	
			}
			#ifdef DEBUG
			std::cout << "Min V(s) = " << min << std::endl;
			std::cout << "Max V(s) = " << max << std::endl; 
			#endif
			return min; 
		}

		scastar_dq::Node* 
		make_node_for_seen( const DNF_Clause& s, const FOND_Model::Action& a ) {
			auto tmp = new scastar_dq::Node;
			tmp->state = s;
			tmp->action_idx = a.index;
			compute_hash(tmp);
			return tmp;
		}

		void
		handle_deadend( scastar_dq::Node* root, const DNF_Clause& context, DNF_Clause& reason ) {

			DNF_Clause new_lits;
			for ( Lit l : reason ) {
				if ( context.entails(l) && !root->state.is_defined(l) ) {
					new_lits.add( l );
				} 

			}
	
			#ifdef DEBUG
			std::cout << "Literals inferred to be a possible reason leading to the dead-end:" << std::endl;
			new_lits.write( std::cout, m_model, true );
			std::cout << std::endl;
			#endif
			if ( !new_lits.empty() ) {
				auto new_weak_sa_pair = new scastar_dq::Node( root->state );
				for ( Lit l : new_lits ) {
					new_weak_sa_pair->state.add( ~l );
					break;
				}
				new_weak_sa_pair->gn = root->gn;
				new_weak_sa_pair->parent = root->parent;
				eval(new_weak_sa_pair);
				new_weak_sa_pair->action_idx = root->action_idx;
				new_weak_sa_pair->strong = false;
				
				compute_hash(new_weak_sa_pair);
				if ( !already_in_weak_frontier(new_weak_sa_pair, false) ) {
					open_weak_node2( new_weak_sa_pair );
					#ifdef DEBUG
					std::cout << "New WEAK (s,a) pair:" << std::endl;
					print_node( std::cout, new_weak_sa_pair, true );
					#endif
				}
				else {
					#ifdef DEBUG
					auto other = m_weak_open_hash.retrieve(new_weak_sa_pair);
					std::cout << "New WEAK (s,a) pair already in the Weak Policy Graph" << std::endl;
					print_node( std::cout, other, true );
					#endif	
					delete new_weak_sa_pair;
				}
				if ( m_weak_open_hash.retrieve( root ) != nullptr )
					m_weak_open_hash.erase( m_weak_open_hash.retrieve_iterator( root ) );
				// Record only the necessary literals
				// root->state = context; 
				for ( Lit l : new_lits ) {
					root->state.add( l );
				}
				eval( root );
				compute_hash( root );
				m_weak_open_hash.put( root );
			}
			#ifdef DEBUG
			std::cout << "Root was CHANGED to record the dead-end condition" << std::endl;
			print_node( std::cout, root, true );
			#endif
			root->is_dead_end = true;
			std::vector<int> de_actions;
			de_actions.push_back( root->action_idx );
			for ( auto it = m_weak_open_hash.begin(); it != m_weak_open_hash.end(); it++ ) {
				if ( root->state.satisfies( it->second->state ) ) {
					if ( it->second->is_dead_end ) 
						de_actions.push_back( it->second->action_idx );
				}
			}
			DNF_Clause cond_reason;
			if ( check_terminality( root->state, root->gn, de_actions, reason ) ) {
				#ifdef DEBUG
				std::cout << "Condition proved to be a dead-end!" << std::endl;
				std::cout << "Reason: ";
				cond_reason.write( std::cout, m_model, true );
				std::cout << std::endl;
				#endif
				record_deadend( root->state );
			}
			else {
				#ifdef DEBUG
				std::cout << "Condition cannot be proved to be a dead-end" << std::endl;
				std::cout << "Reason: ";
				cond_reason.write( std::cout, m_model, true );
				std::cout << std::endl;
				#endif
			}

		}

		bool
		check_terminality( const DNF_Clause& s, float gn, const std::vector<int>& action_mask, DNF_Clause& reason ) {
			#ifdef DEBUG
			std::cout << "Checking terminality: # dead-actions: " << action_mask.size() << std::endl;
			#endif
			bool dead_end = true;
			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {
				if ( std::find( action_mask.begin(), action_mask.end(), i ) != action_mask.end() )
					continue;
				const DNF_Clause& prec = m_model.actions[i]->precondition;
				bool inconsistent = false;
				for ( Lit l : prec ) {
					if ( s.entails( ~l ) ) {
						inconsistent = true;
						reason.add(~l);
						break;
					}
				}
				if ( !inconsistent ) {
					#ifdef DEBUG
					std::cout << "Dead-end hypothesis rejected by action:" << std::endl;
					std::cout<< "\t" << m_model.actions[i]->name << std::endl;
					#endif
					// We need to create a new weak node
					auto new_weak_sa_pair = new scastar_dq::Node( s );
					
					for ( Lit l : prec ) {
						reason.add(l);
						new_weak_sa_pair->state.add(l);
					}
					// Add as new weak nodes those corresponding
					dead_end = false;
					new_weak_sa_pair->parent = nullptr;
					new_weak_sa_pair->gn = gn;
					eval( new_weak_sa_pair );
					new_weak_sa_pair->action_idx = i;
					new_weak_sa_pair->strong = false;
					compute_hash( new_weak_sa_pair );
					assert( !already_in_weak_frontier(new_weak_sa_pair, false) );
					open_weak_node2( new_weak_sa_pair );
					#ifdef DEBUG
					std::cout << "New WEAK (s,a) pair:" << std::endl;
					print_node( std::cout, new_weak_sa_pair, true );
					#endif
					break;
				}
			}
			return dead_end;
		}

		typedef std::list< scastar_dq::Node* > State_Action_List;

		bool
		check_always_globally( 	scastar_dq::Node* root, 
					const DNF_Clause& context, AStar_Closed_List& seen, State_Action_List& cycling,
					scastar_dq::Node*& strong_node ) {
			assert( strong_node == nullptr );
			auto ctx_entry = make_node_for_seen( context );
			assert( seen.retrieve( ctx_entry ) == nullptr );
			seen.put( ctx_entry );
				
			float 			best_strong_cost = root->gn;
			const 			FOND_Model::Action& a = *(m_model.actions[root->action_idx]);
			DNF_Clause		reason_not_dead_end;
			DNF_Clause		reason_strong;
			State_Action_List 	w_cycling;

			std::vector<bool>	EF_goal;
			std::vector<bool>	EF_cycling;
			std::vector<bool>	EF_failed;
			std::vector<bool>	EF_deadend;
			EF_goal.resize( a.effects.size() );
			EF_failed.resize( a.effects.size() );
			EF_cycling.resize( a.effects.size() );
			EF_deadend.resize( a.effects.size() );
			for ( unsigned k = 0; k < a.effects.size(); k++ )
				EF_goal[k] = EF_failed[k] = EF_cycling[k] = EF_deadend[k] = false;
			
			for ( unsigned k = 0; k < a.effects.size(); k++ ) {
				auto eff = a.effects[k];
				DNF_Clause successor;
				bool 	valid = context.apply( a.precondition, eff, successor );
				assert(valid);
				if ( !valid ) {
					std::cerr << "This is a bug! - recompile in debug mode and check the assert" << std::endl;
					exit(1);
				}
				#ifdef DEBUG
				float 	succ_hn = m_mutex_func.eval( successor );
				assert( succ_hn != infty );
				#endif
				float 	succ_gn = root->gn - a.cost;

				#ifdef DEBUG
				std::cout << "Resulting successor state:" << std::endl;
				successor.write( std::cout, m_model );
				std::cout << std::endl;
				std::cout << "h(n) = " << succ_hn << " g(n) = " << succ_gn;
				std::cout << " V(s) = " << get_value( successor ) << std::endl;
				#endif

				if ( successor.satisfies( m_model.goal ) ) {
					#ifdef DEBUG
					std::cout << "Successor satisfies goal" << std::endl;
					#endif
					best_strong_cost = std::max(a.cost, best_strong_cost);
					if ( root->parent == nullptr ) {
						root->parent = m_root;
					}
					EF_failed[k] = false;
					EF_goal[k] = true;
					continue;
				}
				// Since we're adding weak nodes when leaving the policy, those
				// g(n) might be well way off the actual cost to go
				if ( aptk::dequal(succ_gn, 0.0f ) )
					succ_gn = a.cost;
				
				bool already_seen = false;
				for ( auto it = seen.begin(); it != seen.end(); it++ ) {
					if ( it->second->state.satisfies(successor) ) {
						already_seen = true;
						break;
					}
				}
				if ( already_seen ) {
					#ifdef DEBUG
					std::cout << "Successor Cycles Back" << std::endl;
					#endif
					EF_cycling[k] = true;
					EF_failed[k] = false;
					continue;
				}

				if ( is_dead_end( successor ) ) {
					#ifdef DEBUG
					std::cout << "Stumbled upon DEAD END" << std::endl;
					#endif
					DNF_Clause reason;
					handle_deadend( root, context, reason );
					EF_failed[k] = true;
					EF_deadend[k] = true;
					EF_goal[k] = false;
					break;
				}
				scastar_dq::Node* best_strong = nullptr;
				for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
					#ifdef DEBUG
					if ( it->second->is_dead_end ) {
						std::cout << "State-Action pair deemed to be STRONG, is now DEAD END" << std::endl;
						print_node( std::cout, it->second, true );
					}
					#endif
					assert( !it->second->is_dead_end );
					if ( successor.satisfies( it->second->state ) ) {
						#ifdef DEBUG
						std::cout << "Successor satisfied by STRONG state-action pair: " << std::endl;
						print_node( std::cout, it->second );
						#endif
						if ( best_strong != nullptr ) {
							if (it->second->gn < best_strong->gn ) {
								best_strong = it->second;
								best_strong_cost = std::max( a.cost + best_strong->gn, best_strong_cost );
							}
							if ( aptk::dequal( it->second->gn, best_strong->gn )
								&& it->second->hn < best_strong->gn )
								best_strong = it->second;
						}
						
						if ( best_strong == nullptr ) {
							best_strong = it->second;
							best_strong_cost = std::max( a.cost + best_strong->gn, best_strong_cost );
						}
					}
				}
				
				if ( best_strong != nullptr ) {
					if ( root->parent == nullptr ) {
						root->parent = best_strong;
					}
					EF_failed[k] = false;
					EF_goal[k] = true;
					for ( Lit l : context ) {
						if ( best_strong->state.entails(l) && !root->state.is_defined(l) )
							reason_strong.add(l);
					}
					continue;
				}
			
				std::list< scastar_dq::Node* > best_weak;
				std::vector< int > de_actions;	
				for ( auto it = m_weak_open_hash.begin(); it != m_weak_open_hash.end(); it++ ) {
					if ( it->second->is_dead_end ) { 
						if ( successor.satisfies( it->second->state ) ) 
							de_actions.push_back( it->second->action_idx );
						continue;
					}
					if ( successor.satisfies( it->second->state ) ) {
						#ifdef DEBUG
						std::cout << "Successor satisfied by WEAK state-action pair:" << std::endl;
						std::cout << "\t\t[" << it->first << ", " << it->second << "]";
						print_node( std::cout, it->second );
						#endif
						if ( !best_weak.empty() ) {
							if ( best_weak.front()->gn > it->second->gn ) 
								best_weak.clear();
							else if ( best_weak.front()->gn < it->second->gn )
								continue;
						}
						best_weak.push_back( it->second );
					}
				}
				#ifdef DEBUG
				std::cout << "# Best weak nodes: " << best_weak.size() << std::endl;
				#endif
				
				if ( best_weak.empty() ) { // No matching weak nodes, possible dead-end
					#ifdef DEBUG
					std::cout << "We got outside of the policy!" << std::endl;
					#endif
					EF_failed[k] = true;
					EF_goal[k] = false;
					DNF_Clause reason;
					bool dead_end = check_terminality( successor, succ_gn, de_actions, reason );
					if ( dead_end ) {
						#ifdef DEBUG
						std::cout << "Successor is DEAD END" << std::endl;
						#endif
						record_deadend( std::move(successor) );
						handle_deadend( root, context, reason );
						EF_deadend[k] = true;
					}
					if ( EF_deadend[k] ) break;
					for ( Lit l : reason ) {
						if ( !root->state.is_defined(l) ) //!context.is_defined(l) )
							reason_not_dead_end.add(l);
					}
					continue;
				}
				
				EF_failed[k] = true;
				float best_cost_to_go = infty;
				for ( scastar_dq::Node* w : best_weak ) {
					bool dominated = false;
					for ( scastar_dq::Node* w2 : best_weak ) {
						if ( w == w2 ) continue;
						if ( w->action_idx != w2->action_idx ) continue;
						if ( w->state.satisfies( w2->state ) ) {
							dominated = true;
							break;
						}
					}
					#ifdef DEBUG
					std::cout << "Recursion: Checking EF_failed goal for" << std::endl;
					print_node( std::cout, w );
					if ( !dominated ) {
						std::cout << "On context:" << std::endl;
						successor.write( std::cout, m_model, true );
						std::cout << std::endl;
					}
					else {
						std::cout << "Dominated by a weaker (s,a) pair" << std::endl;
					}
					#endif
					if ( dominated ) continue;
					if ( aptk::dgeq( w->gn, best_cost_to_go ) ) continue;
					scastar_dq::Node* new_strong_node = nullptr;
					bool is_cycling = check_always_globally( w, successor, seen, w_cycling, new_strong_node );
					if ( !is_cycling ) {
						if ( new_strong_node != nullptr ) {
							if ( root->parent == nullptr )
								root->parent = new_strong_node;
							best_cost_to_go = std::min( a.cost + new_strong_node->gn, best_cost_to_go );
							best_strong = new_strong_node;
							EF_cycling[k] = false;
							EF_goal[k] = true;
							EF_failed[k] = false;
						}
						if ( w->is_dead_end ) {
							de_actions.push_back( w->action_idx );
						}	
					}
					else {
						EF_cycling[k] = true;
						EF_failed[k] = false;
					}
				}

				if ( EF_goal[k] ) {
					best_strong_cost = std::max( best_strong_cost, best_cost_to_go );
					for ( Lit l : context ) {
						if ( best_strong->state.entails(l) && !root->state.is_defined(l) )
							reason_strong.add(l);
					}					
				}
				
				if ( EF_failed[k] ) {
					#ifdef DEBUG
					std::cout << "All possible weak nodes have failed" << std::endl;
					#endif
					DNF_Clause reason;
					bool dead_end = check_terminality( successor, succ_gn, de_actions, reason );
					if ( dead_end ) {
						#ifdef DEBUG
						std::cout << "Successor is DEAD END" << std::endl;
						#endif
						record_deadend( std::move(successor) );
						handle_deadend( root, context, reason );
						EF_deadend[k] = true;
					}
					if ( EF_deadend[k] ) break;
					for ( Lit l : reason ) {
						if ( context.entails(l) && !root->state.is_defined(l) ) //!context.is_defined(l) )
							reason_not_dead_end.add(l);
					}
				}
			}
			seen.erase( seen.retrieve_iterator( ctx_entry ) );
			bool is_dead_end = false;
			for ( bool b : EF_deadend ) {
				if ( b ) {
					is_dead_end = true;
				}
			}
			if ( is_dead_end ) {
				assert( root->is_dead_end );
				return false;
			}

			bool always_globally_holds = true;
			for ( bool b : EF_failed ) {
				if ( b ) {
					always_globally_holds = false;
					break;
				}
			}
			bool strong_component_reached = false;
			for ( bool b : EF_goal ) {
				if ( b ) {
					strong_component_reached = true;
					break;
				}
			}

			if ( !always_globally_holds ) {
				#ifdef DEBUG
				std::cout << "AG does NOT hold" << std::endl;
				std::cout << "Best Strong Cost: " << best_strong_cost << std::endl;
				print_node( std::cout, root );
				std::cout << "Reasons for not being a dead-end:" << std::endl;
				reason_not_dead_end.write( std::cout, m_model, true );
				std::cout << std::endl;
				#endif
				if ( root->gn < best_strong_cost ) {
					#ifdef DEBUG
					std::cout << "BACK UP: old Q(a,s) = " << root->gn << " new Q(a,s) = " << best_strong_cost << std::endl;
					#endif
					root->gn = best_strong_cost;
					root->fn = root->gn + 10.0f * root->hn;
				}
				for ( auto n : cycling ) {
					delete n;
				}
				cycling.clear();
				/*	
				for ( Lit l : context ) {
					if ( !root->state.is_defined( l ) )
						reason_not_dead_end.add(l);
				}		
				*/
				if ( !reason_not_dead_end.empty() ) {
					Lit split_lit = reason_not_dead_end[0];
					auto new_weak_sa_pair = new scastar_dq::Node( root->state );
					new_weak_sa_pair->state.add( ~split_lit );
					new_weak_sa_pair->gn = root->gn;
					new_weak_sa_pair->parent = root->parent;
					eval(new_weak_sa_pair);
					new_weak_sa_pair->action_idx = root->action_idx;
					new_weak_sa_pair->strong = false;
					compute_hash( new_weak_sa_pair );
					if ( !already_in_weak_frontier(new_weak_sa_pair, false) ) {
						open_weak_node2( new_weak_sa_pair );
						#ifdef DEBUG
						std::cout << "New WEAK (s,a) pair:" << std::endl;
						print_node( std::cout, new_weak_sa_pair, true );
						#endif
					}
					else {
						#ifdef DEBUG
						auto other = m_weak_open_hash.retrieve(new_weak_sa_pair);
						std::cout << "New WEAK (s,a) pair already in the Weak Policy Graph" << std::endl;
						print_node( std::cout, other, true );
						#endif	
						delete new_weak_sa_pair;
					}
					if ( m_weak_open_hash.retrieve(root) != nullptr )
						m_weak_open_hash.erase( m_weak_open_hash.retrieve_iterator( root ) );

					root->state.add(reason_not_dead_end); 
					eval( root );
					compute_hash( root );
					if (!already_in_weak_frontier(root, false) ) {
						m_weak_open_hash.put( root );
						#ifdef DEBUG
						std::cout << "Root was CHANGED to record the reason for not being a dead-end" << std::endl;
						print_node( std::cout, root, true );
						#endif
					}
					else {
						#ifdef DEBUG
						std::cout << "Changed Root is ALREADY in the Weak Policy Graph, with a better cost-to-go" << std::endl;
						#endif
						root->useless = true;
						return true;
					}
				}
	
				return false;
			}
			#ifdef DEBUG
			std::cout << "AG holds" << std::endl;
			#endif
			bool is_cycling = true;
			for ( unsigned k = 0; k < a.effects.size(); k++ )
				is_cycling = is_cycling && ( EF_cycling[k] || !EF_goal[k] );	
			
			if ( is_cycling ) {
				auto cycling_node = new scastar_dq::Node( context, a.index, root->gn, nullptr );
				compute_hash( cycling_node );
				eval( cycling_node );
				cycling_node->expanded = true;
				cycling.push_back( cycling_node );					
				return is_cycling;
			}	

			#ifdef DEBUG
			std::cout << "AGEF goal holds, backed up Q(a,s) = " << best_strong_cost << std::endl;
			std::cout << "Reason: ";
			reason_strong.write( std::cout, m_model, true );
			std::cout << std::endl;
			#endif
			if ( !reason_strong.empty() ) {
				strong_node = new scastar_dq::Node( root->state, a.index, best_strong_cost, nullptr ); 
				strong_node->state.add( reason_strong );
				compute_hash( strong_node );
				eval( strong_node );
				#ifdef DEBUG
				std::cout << "New Strong action pair:" << std::endl;
				print_node( std::cout, strong_node );
				#endif	
				auto other = m_closed.retrieve( strong_node );
				#ifdef DEBUG
				std::cout << "Whoah, found a matching STRONG node!:" << std::endl;
				print_node( std::cout, strong_node );
				#endif
				if ( other != nullptr && aptk::dleq(other->gn,  strong_node->gn) ) {
					#ifdef DEBUG
					std::cout << "Found this was already strong!" << std::endl;
					print_node( std::cout, other );
					#endif
					delete strong_node;
					strong_node = other;
				}
				else
					open_strong_node( strong_node );
				// Change the root
				if ( m_weak_open_hash.retrieve(root) != nullptr )
					m_weak_open_hash.erase( m_weak_open_hash.retrieve_iterator(root) );
				// Just one literal
				root->state.add( ~reason_strong[0] ); 
				eval( root );
				compute_hash( root );
				m_weak_open_hash.put( root );
				#ifdef DEBUG
				std::cout << "Root node was strengthened:" << std::endl;
				print_node( std::cout, root );
				#endif	
			}
			else {
				root->strong = true;
				root->gn = best_strong_cost;
				root->fn = aptk::add( root->hn * 10.0f, root->gn );
				strong_node = root;
				if( m_weak_open_hash.retrieve(root) != nullptr )
					m_weak_open_hash.erase( m_weak_open_hash.retrieve_iterator(root) );
				open_strong_node( root );
				#ifdef DEBUG
				std::cout << "New Strong action pair:" << std::endl;
				print_node( std::cout, root );
				#endif	
			}

			// close all cycling
			for ( scastar_dq::Node* n : cycling ) {
				#ifdef DEBUG
				std::cout << "State-action pair deemed to be STRONG because it's in a CYCLE: " << std::endl;
				print_node( std::cout, n );
				#endif
				open_strong_node( n );
			}
			cycling.clear();
			return false;
		}

		bool
		dfs( scastar_dq::Node* root, const DNF_Clause& context, AStar_Closed_List& visited  ) {
			const 	FOND_Model::Action& a = *(m_model.actions[root->action_idx]);
			auto ctx_entry = make_node_for_seen( context, a );
			float best_strong_cost = root->gn;
			ctx_entry->gn = root->gn;
			assert( visited.retrieve( ctx_entry ) == nullptr );
			visited.put( ctx_entry );

			std::vector<bool> connected;
			for ( unsigned k = 0; k < a.effects.size(); k++ )
				connected[k] = false;
			
			std::vector< AStar_Closed_List > sccs;
			sccs.resize( a.effects.size() );

			for ( unsigned k = 0; k < a.effects.size(); k++ ) {
				auto eff = a.effects[k];
				DNF_Clause successor;
				bool 	valid = context.apply( a.precondition, eff, successor );
				assert(valid);
				if ( !valid ) {
					std::cerr << "This is a bug! - recompile in debug mode and check the assert" << std::endl;
					exit(1);
				}
				#ifdef DEBUG
				float 	succ_hn = m_mutex_func.eval( successor );
				assert( succ_hn != infty );
				#endif
				float 	succ_gn = root->gn - a.cost;
				#ifdef DEBUG
				std::cout << "Resulting successor state:" << std::endl;
				successor.write( std::cout, m_model );
				std::cout << std::endl;
				std::cout << "h(n) = " << succ_hn << " g(n) = " << succ_gn;
				std::cout << " V(s) = " << get_value( successor ) << std::endl;
				#endif
				if ( successor.satisfies( m_model.goal ) ) {
					#ifdef DEBUG
					std::cout << "Successor satisfies goal" << std::endl;
					#endif
					best_strong_cost = std::max(a.cost, best_strong_cost);
					if ( root->parent == nullptr ) {
						root->parent = m_root;
					}
					connected[k] = true;
					continue;
				}
				bool already_seen = false;
				for ( auto it = visited.begin(); it != visited.end(); it++ ) {
					if ( it->second->state.satisfies(successor) ) {
						best_strong_cost = std::max( a.cost + it->second->gn, best_strong_cost );
						already_seen = true;
						break;
					}
				}
				if ( already_seen ) {
					#ifdef DEBUG
					std::cout << "Successor Cycles Back" << std::endl;
					#endif
					connected[k] = true;
					continue;
				}
				if ( is_dead_end( successor ) ) {
					#ifdef DEBUG
					std::cout << "Stumbled upon DEAD END" << std::endl;
					#endif
					DNF_Clause reason;
					handle_deadend( root, context, reason );
					connected[k] = false;
					break;
				}

				scastar_dq::Node* best_strong = nullptr;
				for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
					#ifdef DEBUG
					if ( it->second->is_dead_end ) {
						std::cout << "State-Action pair deemed to be STRONG, is now DEAD END" << std::endl;
						print_node( std::cout, it->second, true );
					}
					#endif
					assert( !it->second->is_dead_end );
					if ( successor.satisfies( it->second->state ) ) {
						#ifdef DEBUG
						std::cout << "Successor satisfied by STRONG state-action pair: " << std::endl;
						print_node( std::cout, it->second );
						#endif
						if ( best_strong != nullptr ) {
							if (it->second->gn < best_strong->gn ) {
								best_strong = it->second;
							}
							if ( aptk::dequal( it->second->gn, best_strong->gn )
								&& it->second->hn < best_strong->gn )
								best_strong = it->second;
						}
						
						if ( best_strong == nullptr ) 
							best_strong = it->second;
						
					}
				}
				
				if ( best_strong != nullptr ) {
					if ( root->parent == nullptr ) {
						root->parent = best_strong;
					}
					connected[k] = true;
					best_strong_cost = std::max( a.cost + best_strong->gn, best_strong_cost );
					continue;
				}

				// Now, check for failure
				std::vector<int> de_actions;
				bool is_failure = true;
				for ( auto it = m_weak_open_hash.begin(); it != m_weak_open_hash.end(); it++ ) {
					if ( it->second->is_dead_end ) { 
						if ( successor.satisfies( it->second->state ) ) 
							de_actions.push_back( it->second->action_idx );
						continue;
					}
					if ( successor.satisfies( it->second->state ) ) {
						#ifdef DEBUG
						std::cout << "Successor satisfied by WEAK state-action pair:" << std::endl;
						std::cout << "\t\t[" << it->first << ", " << it->second << "]";
						print_node( std::cout, it->second );
						#endif
						for ( auto n : visited )
							sccs[k].put(n.second);
						bool is_strong_cyclic =	dfs( it->second, successor, sccs[k] );
						for ( auto n : visited ) {
							sccs[k].remove(n.second);	
						}
						if ( is_strong_cyclic ) {
							is_failure = false;
							break;
						}
						// Clean-up
						for ( auto n : sccs[k] ) 
							delete n.second;
					}
				}

				if ( is_failure ) {
					DNF_Clause reason;
					bool dead_end = check_terminality( successor, succ_gn, de_actions, reason );
					if ( dead_end ) {
						#ifdef DEBUG
						std::cout << "Successor is DEAD END" << std::endl;
						#endif
						record_deadend( std::move(successor) );
						handle_deadend( root, context, reason );
					}
					break;
				}
				connected[k] = true;
			}

			bool all_good = true;
			for ( bool b : connected ) {
				all_good = all_good && b;
			}

			if ( !all_good ) {
				visited.remove( ctx_entry );
				delete ctx_entry;
				for ( AStar_Closed_List& sccs_i : sccs ) {
					for ( auto n : sccs_i )
						delete n.second;
				}	
				return false;
			}

			ctx_entry->gn = best_strong_cost;

			return true;
		}


		// Returns whether or not this 
		bool
		check_strong_dfs( scastar_dq::Node* root ) {
			assert( !root->strong );
			#ifdef DEBUG
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Checking Strong Cyclic property: " << std::endl;
			print_node( std::cout, root, true );	
			#endif
			
			float min_covered_value = get_covered_states_value(root);

			//if ( aptk::dgeq( root->gn, min_covered_value ) ) {
			//if ( root->gn > min_covered_value ) {
			if ( min_covered_value != infty ) {
				#ifdef DEBUG
				std::cout << "State-Action pair is NOT USEFUL" << std::endl;
				#endif
				return false;
			}

			inc_strong_cyclic_check();
			AStar_Closed_List	seen;
			DNF_Clause 		s0 = root->state;

			bool strong_cyclic = dfs( root, s0, seen ); 
			if ( strong_cyclic ) {
				root->strong = true;
				m_weak_open_hash.remove( root );
				open_strong_node( root );
				#ifdef DEBUG
				std::cout << "New strong node: " << std::endl;
				print_node( std::cout, root, true );
				#endif
				for ( auto n : seen ) {
					n.second->strong = true;
					open_strong_node( n.second );
					#ifdef DEBUG
					std::cout << "New strong node: " << std::endl;
					print_node( std::cout, n.second, true );
					#endif
				}
			} 

			#ifdef DEBUG
			std::cout << "Number of entries in seen: " << seen.size() << std::endl;
			#endif

			return true;
		}

		bool
		implied_by_closed2( const DNF_Clause& c ) const {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				if ( it->second->state.implies( c ) ) {
					return true;
				}
			}
			return false;
		}
		
		bool 
		is_redundant( const DNF_Clause& c ) const {
			if ( is_redundant( c, m_closed ) ) return true;
			if ( is_redundant( c, m_dead_ends ) ) return true;
			return false;
		}

		bool
		is_redundant( const DNF_Clause& c, const AStar_Closed_List& pi ) const {
			for ( auto it = pi.begin(); it != pi.end(); it++ ) {
				if ( c.satisfies( it->second->state ) ) return true;
			}
			return false;
		}
		
		bool
		is_redundant( scastar_dq::Node* n, const AStar_Closed_List& pi ) const {
			for ( auto it = pi.begin(); it != pi.end(); it++ ) {
				if ( n->state.satisfies( it->second->state )
					&& n->action_idx == it->second->action_idx ) return true;
			}
			return false;
		}

		scastar_dq::Node* 
		get_node() {
			scastar_dq::Node* n = nullptr;
			float strong_fn = ( m_strong_open.empty() ? infty : m_strong_open.first()->fn );
			float weak_fn = ( m_weak_open.empty() ? infty : m_weak_open.first()->fn );
			if ( aptk::dgreater(strong_fn, weak_fn) ) {
				return get_weak_node();
			}
			if ( aptk::dless(strong_fn, weak_fn ) ) {
				return get_strong_node();
			}
			if ( !m_strong_open.empty() )
				return get_strong_node();
			if ( !m_weak_open.empty() )
				return get_weak_node();
			return n;
		}

		void
		eval( scastar_dq::Node* n ) {
			n->hn = m_mutex_func.eval( n->state );
			n->fn = ( n->hn == infty ? infty : aptk::add( n->gn, 10.0f*n->hn ) );
		}

		bool	
		do_search() {
			m_min_cost_to_go = infty;
			AStar_Weak_Open_List weak_dead;
			bool changed;
			do {
				changed = false;
				scastar_dq::Node* head = get_node();
				while(head) {
					if ( head->is_dead_end ) {
						head = get_node();
						continue;
					}
					if ( is_goal(head->state) ) {
						#ifdef DEBUG
						std::cout << "======================================================================================================" << std::endl;
						std::cout << "Init reached" << std::endl;
						print_node( std::cout, head );
						#endif
						if ( head->strong ) {
							#ifdef DEBUG
							scastar_dq::Node* n = head->parent;
							while( n != nullptr ) {
								print_node( std::cout, n );
								n = n->parent;
							}
							#endif
							m_min_cost_to_go = std::min( m_min_cost_to_go, head->gn );
							return true;
						}
					}
					
					if (  process_node( head ) && !head->strong && !head->is_dead_end ) {
						#ifdef DEBUG
						std::cout << "Node goes to dead Weak node lists" << std::endl;
						#endif
						weak_dead.insert( head ); 
					}

					if ( is_goal(head->state) && head->is_dead_end ) {
						std::vector<int> de_actions;
						de_actions.push_back( head->action_idx );
						for ( auto it = m_weak_open_hash.begin(); it != m_weak_open_hash.end(); it++ ) {
							if ( m_model.init.satisfies( it->second->state ) && it->second->is_dead_end ) 
								de_actions.push_back( it->second->action_idx );
						}
						DNF_Clause reason;
						if ( check_terminality( m_model.init, head->gn, de_actions, reason ) ) {
							std::cout << "Initial state proved to be a dead-end!" << std::endl;
							m_init_is_dead_end = true;
							return false;
						}
					}

					head = get_node();
				}

				#ifdef DEBUG	
				std::cout << "Mopping up weak states..." << std::endl;
				#endif
				std::list< scastar_dq::Node* > frontier;
				while ( !weak_dead.empty() ) {
					scastar_dq::Node* n = weak_dead.pop();
					#ifdef DEBUG
					std::cout << "====================================================================================" << std::endl;
					std::cout << "Pruning for determining s.c.'ness" << std::endl;
					print_node( std::cout, n );
					#endif

					if ( n->strong || n->is_dead_end || n->useless ) continue;
					#ifdef DEBUG
					std::cout << "Not Strong, Not dead end" << std::endl;
					#endif
					if ( aptk::dgeq((n->gn + n->hn), m_min_cost_to_go) )continue;
					#ifdef DEBUG
					std::cout << "g(n) + h(n) < min cost to go [" << m_min_cost_to_go << "]" << std::endl;
					#endif
					if ( aptk::dgreater(n->gn,  get_value(n)) ) continue;
					#ifdef DEBUG
					std::cout << "g(n) <= V(n)[" << get_value(n)<< "]"<< std::endl;
					#endif
					#ifdef DEBUG
					std::cout << "======================================================================================================" << std::endl;
					std::cout << "Difference with min cost to go: " << (n->gn + n->hn) - m_min_cost_to_go << std::endl;
					std::cout << "V(s) = " << get_value(n) << std::endl;
					#endif
					if ( n->gn > m_max_cost_to_go ) {
						#ifdef DEBUG
						std::cout << "Processing delayed due to upper bound" << std::endl;
						#endif
						frontier.push_back(n);
						continue;
					}
					if ( !check_strong_dfs( n ) ) {
						#ifdef DEBUG
						std::cout << "Not useful: closed loop" << std::endl;
						#endif
						continue;
					} 
					if ( n->is_dead_end ) {
						#ifdef DEBUG
						std::cout << "Shown to be a dead-end" << std::endl;
						#endif
						if ( is_goal( n->state ) ) {
							std::vector<int> de_actions;
							de_actions.push_back( n->action_idx );
							for ( auto it = m_weak_open_hash.begin(); it != m_weak_open_hash.end(); it++ ) {
								if ( m_model.init.satisfies( it->second->state ) && it->second->is_dead_end ) 
									de_actions.push_back( it->second->action_idx );
							}
							DNF_Clause reason;
							if ( check_terminality( m_model.init, n->gn, de_actions, reason ) ) {
								std::cout << "Initial state proved to be a dead-end!" << std::endl;
								m_init_is_dead_end = true;
								return false;
							}
						}
						continue;
					}
					n->trials++;
					frontier.push_back( n );
					if ( !m_strong_open.empty() )
						break;	
					//if (!n->expanded)
					//	expand_node(n);
				}
				for ( scastar_dq::Node* n : frontier ) {
					weak_dead.insert( n );
				}
				
				changed = !m_strong_open.empty() || !m_weak_open.empty();
			} while ( changed );

			return false;			
		}

		void
		print_node( std::ostream& os, scastar_dq::Node* n, bool show_negative_lits = false ) {
			os << "\t";
			n->state.write( os, m_model, show_negative_lits);
			os << std::endl;
			if ( n->action_idx == -1 ) 
				os << "-> (GOAL)" << std::endl;
			else
				os << "-> " << m_model.actions[n->action_idx]->name << std::endl;

			os << " address(n) = " << n;
			os << " f(n) = " << n->fn;
			os << " g(n) = " << n->gn;
			os << " h(n) = " << n->hn << std::endl;
			os << " trials(n) = " << n->trials;
			os << " strong(n) = " << ( n->strong ? "yes" : "no" );
			os << " deadend(n) = " << ( n->is_dead_end ? "yes" : "no" ) << std::endl;
			os << " critical(n) = " << ( n->critical_path ? "yes" : "no" ) << std::endl;
			os << " useless(n) = " << ( n->useless ? "yes" : "no" );
			os << " hashkey= " << n->hash_key << std::endl;
				
		}

		void
		compute_hash( scastar_dq::Node* n ) {
			aptk::Hash_Key hasher;
			n->state.update_hash( hasher );
			hasher.add( DNF_Clause::num_lits() + ( n->action_idx == -1 ? m_model.actions.size() : n->action_idx ) );
			n->hash_key = (size_t)hasher;
			n->hash_up_to_date = true;
		}		

		bool
		relevance_check( const DNF_Clause& s, const FOND_Model::Action& a ) const {
			for ( auto eff_it = a.effects.begin(); eff_it != a.effects.end(); eff_it++ ) {
				for ( auto eff_it2 = eff_it->begin(); eff_it2 != eff_it->end(); eff_it2++ )
					for ( auto eff_it3 = eff_it2->effect.begin(); eff_it3 != eff_it2->effect.end(); eff_it3++ ) {
						if ( s.entails( *eff_it3 ) ) return true;
						if ( a.effects.size() == 1 && eff_it2->condition.empty() && s.entails( ~(*eff_it3) ) ) return false;	
					}
			}

			return false; // action doesn't mention any of the lits in the formula to be regressed
		}

		bool 		
		is_closed( scastar_dq::Node* n ) { 
			scastar_dq::Node* n2 = this->closed().retrieve(n);
			if ( n2 != nullptr ) {
				#ifdef DEBUG
				std::cout << "Match found for WEAK state-action pair:" << std::endl;
				print_node( std::cout, n, true );
				std::cout << "Matching STRONG state-action pair: " << std::endl;
				print_node( std::cout, n2, true );
				#endif
				if ( n2->gn > n->gn ) {
					n2->gn = n->gn;
					update_value(n2);	
					return true;
				}
	
			}
			return n2 != nullptr;	
		}
		
		bool
		is_goal( const DNF_Clause& c ) {
			return m_model.init.satisfies(c);
		}
	
		scastar_dq::Node* 		
		get_strong_node() {
			scastar_dq::Node* next = nullptr;
			if(! m_strong_open.empty() ) {
				next = m_strong_open.pop();
			}
			return next;
		}

		scastar_dq::Node*
		get_weak_node() {
			scastar_dq::Node* next = nullptr;
			if(! m_weak_open.empty() ) {
				next = m_weak_open.pop();
			}
			return next;			
		}

		void	 	
		open_strong_node( scastar_dq::Node *n ) {
			m_strong_open.insert(n);
			close(n);
			inc_gen();
			m_mem_used += n->nbytes();
		}

		bool	
		is_mutex( const DNF_Clause& c ) const {
			return m_mutex_func.eval( c ) == infty;
		}

		bool	
		is_mutex( const DNF_Clause& c, const DNF_Clause& d ) const {
			return m_mutex_func.eval( c, d ) == infty;
		}

		void
		open_weak_node2( scastar_dq::Node *n ) {
			assert( !already_in_weak_frontier(n) );	
			m_weak_open.insert(n);
			m_weak_open_hash.put(n);
			inc_gen();
			m_mem_used += n->nbytes();
		}

		void	 	
		open_weak_node( scastar_dq::Node *n ) {
			assert( !already_in_weak_frontier(n) );	
			m_weak_open.insert(n);
			if (!already_in_weak_frontier(n) ) {
				m_weak_open_hash.put(n);
				inc_gen();
				m_mem_used += n->nbytes();
			}
		}
		
		bool 			
		already_in_weak_frontier( scastar_dq::Node *n, bool update_cost_to_go = true ) {
			scastar_dq::Node *previous_copy = m_weak_open_hash.retrieve(n);
			if ( previous_copy != nullptr ) {
				#ifdef DEBUG
				std::cout << "Match found for state-action pair:" << std::endl;
				print_node( std::cout, n, true );
				std::cout << "Matching state-action pair: " << std::endl;
				print_node( std::cout, previous_copy, true );
				#endif
				if ( update_cost_to_go && previous_copy->gn > n->gn ) {
					previous_copy->gn = n->gn;
					previous_copy->fn = n->hn + 10.0f* n->gn;
					return true;
				}
			}
			return previous_copy != nullptr;
		}

		void
		expand_node( scastar_dq::Node *head ) {
			assert( !head->expanded );
			#ifdef DEBUG
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Expanding: (min cost to go = " << m_min_cost_to_go << ", max cost to go = " << m_max_cost_to_go << " )" << std::endl;
			print_node( std::cout, head );		
			std::cout << "V(s) = " << get_value(head) << std::endl;
			#endif
			head->expanded = true;
			if ( aptk::dgeq( (head->gn+head->hn), m_min_cost_to_go) ) {
				#ifdef DEBUG
				std::cout << "Predecessors pruned: g(n) + h(n) > V(s0)" << std::endl;
				#endif 
				return;
			}
			if ( head->gn > get_value(head) ) { 
				#ifdef DEBUG
				std::cout << "Predecessors pruned: g(n) > V(s)" << std::endl;
				#endif
				return;
			}
			inc_exp();
			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {	
				if ( !relevance_check( head->state, *(m_model.actions[i]) ) ) continue;

				std::vector<DNF_Clause>  succ;
				procedural_strong_cyclic_regress2( m_model, i, head->state, succ );
				
				for ( auto it = succ.begin(); it != succ.end(); it++ ) {
					scastar_dq::Node* n = new scastar_dq::Node( *it, i, head->gn + m_model.actions[i]->cost, head, false );
					
					eval(n);
					if ( n->hn == infty ) {
						delete n;
						continue;
					}
					
					if (aptk::dgeq( (n->gn+n->hn), m_min_cost_to_go) ) {
						delete n;
						continue;
					}

										
					if ( aptk::dgeq(n->gn, get_value(n) )) {
						#ifdef DEBUG
						std::cout << "Predecessor:" << std::endl;
						print_node( std::cout, n );
						std::cout << "Pruned because V(s) [" << get_value(n) << "] >= g(n)" << std::endl;
						#endif
						delete n;
						continue;
					}
					if ( n->hn < head->hn ) n->critical_path = true;
					DNF_Clause ext_succ;
					m_mutex_func.add_consequences( *it, ext_succ );
					n->state = std::move(ext_succ);

					compute_hash( n );
					if ( is_closed( n ) ) {
						#ifdef DEBUG
						std::cout << "Predecessor:" << std::endl;
						print_node( std::cout, n );
						std::cout << "Pruned because already in the Strong Cyclic Policy" << std::endl;
						#endif

						delete n;
						continue;
					}
					if( already_in_weak_frontier(n) ) {
						#ifdef DEBUG
						std::cout << "Predecessor:" << std::endl;
						print_node( std::cout, n );
						std::cout << "Pruned because already in the Weak Policy" << std::endl;
						#endif
						delete n;
						continue;
					}
					if ( is_redundant( n->state, m_dead_ends ) ) {
						#ifdef DEBUG
						std::cout << "Predecessor:" << std::endl;
						print_node( std::cout, n );
						std::cout << "Pruned because it satisfies a dead-end" << std::endl;
						#endif
						delete n;
						continue;
					}
					open_weak_node( n );
					#ifdef DEBUG
					std::cout << "\t through " << m_model.actions[i]->name << " results in new WEAK predecessor " << std::endl;
					print_node( std::cout, n );
					std::cout << "V(s) = " << get_value( n ) << std::endl;
					std::cout << std::endl;
					#endif
				}	
			} 
		}

		bool
		process_node( scastar_dq::Node* n ) {
			if ( n->strong  ) {
				if ( !n->expanded )
					expand_node( n );
				return true;	
			}
			if ( n->useless ) return false;
			if ( n->gn + n->hn >  m_max_cost_to_go ) {
				#ifdef DEBUG
				std::cout << "================================================================================" << std::endl;
				std::cout << "Node processing delayed due to g(n) + h(n) > UB(s0) [" << m_max_cost_to_go << "]" << std::endl;
				print_node( std::cout, n );
				#endif
				return true;
			}
			if ( !check_strong_dfs(n) ) 
				return false;
			if ( is_goal(n->state) ) 
				n->expanded = true;
			if ( n->is_dead_end ) 
				return false;

			//if (!n->expanded)
			//	expand_node( n );
			
			return true;	
		}

		bool	
		update_value( scastar_dq::Node* n ) {
			#ifdef DEBUG
			std::cout << "V(s) = " << get_value(n) << " V'(s) = " << n->gn << std::endl;
			#endif
			if ( aptk::dgeq( n->gn, get_value(n) ) ) {
				#ifdef DEBUG
				std::cout << "Potential cycle" << std::endl;
				#endif
				return false;
			}
			#ifdef DEBUG
			std::cout << "Needs to be updated" << std::endl;
			#endif
			scastar_dq::Node* new_entry = new scastar_dq::Node( n->state );
			new_entry->gn = n->gn;
			compute_hash( new_entry );
			m_vtable.put( new_entry );
			return true;
		}

		float	
		get_value( DNF_Clause& s ) {
			m_vtable_entry.state = std::move( s );
			compute_hash(&m_vtable_entry);
			auto entry = m_vtable.retrieve( &m_vtable_entry );
			s = std::move( m_vtable_entry.state );
			if ( entry == nullptr )
				return infty;	
			return entry->gn;
		}


		float	
		get_value( scastar_dq::Node* n ) {
			m_vtable_entry.state = std::move( n->state );
			compute_hash(&m_vtable_entry);
			auto entry = m_vtable.retrieve( &m_vtable_entry );
			n->state = std::move( m_vtable_entry.state );
			if ( entry == nullptr )
				return infty;	
			return entry->gn;
		}

		void
		record_deadend( DNF_Clause&& s ) {
			auto tmp = new scastar_dq::Node;
			tmp->state = s;
			tmp->hash_table = &m_dead_ends;
			compute_hash( tmp );
			auto entry = m_dead_ends.retrieve( tmp );
			if ( entry != nullptr ) {
				delete tmp;
				return;
			}
			m_dead_ends.put( tmp );
		}

		void
		record_deadend( const DNF_Clause& s ) {
			auto tmp = new scastar_dq::Node;
			tmp->state = s;
			tmp->hash_table = &m_dead_ends;
			compute_hash( tmp );
			auto entry = m_dead_ends.retrieve( tmp );
			if ( entry != nullptr ) {
				delete tmp;
				return;
			}
			m_dead_ends.put( tmp );
		}

		bool
		is_dead_end( const DNF_Clause& s ) {
			for ( auto it = m_dead_ends.begin();
				it != m_dead_ends.end();
				it++ )
				if ( s.satisfies( it->second->state ) )
					return true;
			return false;
		}

	public:
		void
		print_values( std::ostream& os ) {
			for ( auto it = m_vtable.begin(); it != m_vtable.end(); it++ ) {
				it->second->state.write( os, m_model );
				os << " <- " << it->second->gn << std::endl;
			}
		}

		void
		print_deadends( std::ostream& os ) {
			for ( auto it = m_dead_ends.begin(); it != m_dead_ends.end(); it++ ) {
				it->second->state.write( os, m_model );
				os << std::endl;
			}	
		}

	private:
		const FOND_Model&			m_model;
		AStar_Strong_Open_List			m_strong_open;
		AStar_Weak_Open_List			m_weak_open;
		const Mutex_Heuristic&			m_mutex_func;
		unsigned				m_gen_count;
		unsigned				m_exp_count;
		unsigned				m_strong_cyclic_checks;
		AStar_Closed_List			m_closed;
		AStar_Closed_List			m_weak_open_hash;
		scastar_dq::Node*			m_root;
		scastar_dq::Node*			m_solution;
		size_t					m_mem_used;
		float					m_min_cost_to_go;
		AStar_Closed_List			m_vtable;
		scastar_dq::Node			m_vtable_entry;
		AStar_Closed_List			m_dead_ends;
		float					m_max_cost_to_go;
		bool					m_init_is_dead_end;
		float					m_best_dist_to_init;
	};

}

#endif // ucs.hxx

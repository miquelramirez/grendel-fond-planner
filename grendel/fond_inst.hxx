#ifndef __GRENDEL_FOND_MODEL__
#define __GRENDEL_FOND_MODEL__

#include <gp_atoms.hxx>
#include <iosfwd>

namespace grendel {

	class	FOND_Model {

	public:

		struct Atom {
			std::string	name;
			unsigned	index;
			
			Atom( const std::string& _name, unsigned i = 0 ) 
			: name( _name ), index(i) {
			}

			Atom( const Atom& a ) {
				name = a.name;
				index = a.index;
			}

			const Atom& operator=( const Atom& a ) {
				name = a.name;
				index = a.index;
				return *this;
			}

			bool	operator==( const Atom& other ) const {
				return index == other.index;
			}

			void	print( std::ostream& os, FOND_Model& model ) const;
		};

		typedef	std::vector< Atom* >	Atom_Vec;

		struct Effect {

			DNF_Clause	condition;
			DNF_Clause	effect;

			Effect() {
			}
		};


		typedef std::vector< Effect >	Effect_Vec;

		struct Action {

			std::string			name;
			size_t				index;
			DNF_Clause			precondition;
			std::vector<Effect_Vec>		effects;
			float				cost;

			Action( const std::string& _name, size_t i = 0 ) 
			: name( _name ), index(i), cost( 1.0f ) {
			}
			
			Action( const Action& a ) {
				name = a.name;
				index = a.index;
				precondition = a.precondition;
				effects = a.effects;
				cost = a.cost;
			}

			const Action& operator=( const Action& a ) {
				name = a.name;
				index = a.index;
				precondition = a.precondition;
				effects = a.effects;
				cost = a.cost;
				return *this;
			} 


			bool operator==( const Action& a ) { return index == a.index; }

			void print( std::ostream& os, const FOND_Model& model ) const;
			
			bool	flips( Lit l ) const {
				for ( auto effs : effects ) 
					for ( auto eff : effs )
						if ( eff.effect.entails( ~l ) ) return true;
				return false;	
			}

			void	adds( Lit l, std::vector< unsigned >& adding_effects ) const {
				for ( unsigned i = 0; i < effects.size(); i++ ) {
					for ( auto eff : effects[i] )
						if ( eff.effect.entails( l ) ) 
							adding_effects.push_back(i);
				}
			}

		};

		typedef	std::vector< Action* >		Action_Vec;

		struct Axiom
		{
			DNF_Clause	body;
			DNF_Clause	head;
			
			void		print( std::ostream& os, const FOND_Model& model ) const;
		};

		typedef	std::vector< Axiom* >		Axiom_Vec;

		std::string			domain_name;
		std::string			problem_name;
		Atom_Vec			atoms;
		Action_Vec			actions;
		Axiom_Vec			axioms;
		DNF_Clause			init;
		DNF_Clause			goal;
		std::vector<DNF_Clause>		invariants;
		size_t				max_precondition_size;

		FOND_Model( const std::string& dom, const std::string& prob ) 
			: domain_name( dom ), problem_name( prob ), max_precondition_size(0) {
		}


		virtual 	~FOND_Model();

		 // access instance information
		size_t 		n_atoms() const { return atoms.size(); }
		size_t 		n_actions() const { return actions.size(); }


		Atom&		new_atom( const std::string& name );
		Action&		new_action( const std::string& name );
		
		void		complete_goal();
		void		add_invariant( const DNF_Clause& inv );

		void		add_axiom( const DNF_Clause& body, const DNF_Clause& head );
		void		close_under_axioms( const DNF_Clause& situation, DNF_Clause& closed_situation ) const;

	};

}

#endif // fond_model.hxx

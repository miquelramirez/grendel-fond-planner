#include <search_node.hxx>
#include <hash_table.hxx>

namespace grendel
{
		Search_Node::Search_Node( )
		: action_idx( -1 ), gn(0.0f), parent(nullptr), strong(true), expanded(false), is_dead_end(false), 
		speculative(false), critical_path( false ), useless(false), in_open(false), loopy(false), width(0), novelty(0) {
		}

		Search_Node::Search_Node( const DNF_Clause& c )
		: state(c), action_idx( -1 ), gn(0.0f), parent(nullptr), strong(true), expanded(false), is_dead_end(false), 
		speculative(false), critical_path( false ), useless(false), in_open( false ), loopy(false), width(0), novelty(0) {
		}	

		Search_Node::Search_Node( const DNF_Clause& c, int _action_idx, float _gn, Search_Node* _parent, bool _strong ) 
		: state(c), action_idx( _action_idx ),  gn(_gn), parent(_parent), 
		strong( _strong ), expanded( false ) , is_dead_end(false),  speculative(false), 
		critical_path( false ), useless(false), in_open(false), loopy(false), width(0), novelty(0) {
			hash_up_to_date = false;
			hash_table = nullptr;
		}

		Search_Node::~Search_Node() {
			hash_up_to_date = false;
			gn = -1;
		}

		void
		Search_Node::compute_hash( const FOND_Model& model  ) {
			aptk::Hash_Key hasher;
			state.update_hash( hasher );
			hasher.add( DNF_Clause::num_lits() + ( action_idx == -1 ? model.actions.size() : action_idx ) );
			hash_key = (size_t)hasher;
			hash_up_to_date = true;
		}		

		void
		Search_Node::print_node( std::ostream& os, const FOND_Model& model, bool show_negative_lits ) {
			os << "\t";
			state.write( os, model, show_negative_lits);
			os << std::endl;
			if ( action_idx == -1 ) 
				os << "-> (GOAL)" << std::endl;
			else
				os << "-> " << model.actions[action_idx]->name << std::endl;

			os << " address(n) = " << this;
			os << " novelty = " << novelty;
			os << " f(n) = " << fn;
			os << " g(n) = " << gn;
			os << " h(n) = " << hn;
			os << " strong(n) = " << ( strong ? "yes" : "no" );
			os << " deadend(n) = " << ( is_dead_end ? "yes" : "no" ) ;
			os << " critical(n) = " << ( critical_path ? "yes" : "no" );
			os << " hashkey= " << hash_key;
			os << " speculative= " << speculative;
			os << " useless= " << useless;
			os << " parent= " << parent << std::endl;
				
		}

}


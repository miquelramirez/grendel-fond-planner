#include <h2.hxx>

namespace grendel {

	void	H2_Heuristic::print( std::ostream& os ) const {
		for ( int i = 0; i < (int)(2*m_model.n_atoms()); i++ ) {
			Lit pi = toLit(i);
			Atom idx = atom( pi );
			assert( idx < (int)m_model.n_atoms() );
			const FOND_Model::Atom& ai = *(m_model.atoms[idx]);
			for ( int j = 0; j < (int)(2*m_model.n_atoms()); j++ ) {
				Lit pj = toLit(j);
				idx = atom(pj);
				assert( idx < (int)m_model.n_atoms() );
				const FOND_Model::Atom& aj = *(m_model.atoms[idx]);
				if ( sign(pi) )
					os << "(not (" << ai.name <<  "))";
				else
					os << "(" << ai.name << ")";
				os << ",";
				if ( sign(pj) )
					os << "(not (" << aj.name << "))";
				else
					os << "(" << aj.name << ")";
				os << "," << value( pi, pj ) << std::endl;

			}
		}
	}

	void	H2_Heuristic::initialize( ) {
		for ( unsigned k = 0; k < m_values.size(); k++ )
			m_values[k] = infty;

		for ( auto i = m_model.init.begin(); i != m_model.init.end(); i++ ) 
			for ( auto j = i; j != m_model.init.end(); j++ )
				value( *i, *j ) = 0.0f;
		
	}

	bool	
	H2_Heuristic::process_effect( const FOND_Model::Action& act, float v, unsigned eff_idx ) {
		bool 				unchanged = true;	

		const FOND_Model::Effect_Vec& 		effs = act.effects[eff_idx];
		std::vector< float > 		v_effs( effs.size() );

		for ( unsigned k = 0; k < effs.size(); k++ )
			v_effs[k] = ( effs[k].condition.empty() ? v : eval( act.precondition, effs[k].condition ) );

		for ( unsigned k = 0; k < effs.size(); k++ ) {
			if ( aptk::dequal(v_effs[k], infty) ) continue;
			for ( unsigned l = k;  l < effs.size(); l++ ) {
				float v_eff2 = std::max( v_effs[k], v_effs[l] );
				if ( aptk::dequal(v_eff2,infty) ) continue;
				v_eff2 += act.cost;
				for ( auto k_it = effs[k].effect.begin(); k_it != effs[k].effect.end(); k_it++ ) {
					for ( auto l_it = effs[l].effect.begin(); l_it != effs[l].effect.end(); l_it++ ) {
						float& curr_v_ij = value( *k_it, *l_it );
						if ( v_eff2 < curr_v_ij ) {
							unchanged = false;
							curr_v_ij = v_eff2;
						}
					}
					bool unchanged_by_noops = account_for_noops( act, *k_it, k, v_effs );
					unchanged = unchanged && unchanged_by_noops;
				}
			}
		}	
		return unchanged;	
	}	

	bool
	H2_Heuristic::interferes( const FOND_Model::Effect_Vec& eff, Lit lr, const std::vector<float>& v_effs ) {
		for ( unsigned k = 0; k < eff.size(); k++ ) {
			if ( aptk::dequal(v_effs[k], infty) ) continue;
			if ( eff[k].effect.entails(lr) ) return true;
			if ( eff[k].effect.entails(~lr) ) return true; 
		}
		return false;
	}

	bool	
	H2_Heuristic::account_for_noops( const FOND_Model::Action& act, Lit l, unsigned eff_idx, const std::vector<float>& v_effs ) {
		bool unchanged = true;

		for ( int r_index = 0; r_index < (int)(2*m_model.n_atoms()); r_index++ ) {
			Lit lr = toLit( r_index );
			if ( aptk::dequal(value( lr, lr ), infty) ) continue;
			if ( interferes( act.effects[eff_idx], lr, v_effs ) ) continue;
			float v_noop = infty;
			for ( unsigned k = 0; k < act.effects[eff_idx].size(); k++ )
				v_noop = std::min( v_noop, 
					eval( act.precondition, act.effects[eff_idx][k].condition, lr ) );
			if ( aptk::dequal(v_noop, infty) ) continue;
			v_noop += act.cost;
			if ( v_noop < value( l, lr ) ) {
				unchanged = false;
				value( l, lr ) = v_noop;
			}
		}

		return unchanged;	
	}

	void	
	H2_Heuristic::compute() {
		
		bool fixed_point;
		int  iterations = 0;		

		do { 
			fixed_point = true;
			iterations++;

			for ( auto act_it = m_model.actions.begin();
				act_it != m_model.actions.end(); act_it++ ){

				const FOND_Model::Action& act = **act_it;
				
				float h_pre = eval( act.precondition );

				if ( aptk::dequal(h_pre, infty) ) continue;
			
				for ( unsigned k = 0; k < act.effects.size(); k++ ) {
					bool unchanged = process_effect( act, h_pre, k );
					fixed_point = fixed_point && unchanged;
				}				

				/*	
				if ( act.ndeffects.empty() ) {

					if ( act.when.empty() ) {
						bool changed = process_effect( act, h_pre );
						fixed_point = fixed_point && changed;
						continue;
					}

					std::vector< std::pair< float, int > > fired_cond_effs;

					for ( int k = 0; k < (int)act.when.size(); k++ ) {
						float v_cond_eff = eval( act.precondition, act.when[k].condition );
						if ( v_cond_eff == infty ) continue;
						fired_cond_effs.push_back( std::make_pair( v_cond_eff, k ) );						
					}

					bool changed = process_effect( act, h_pre, fired_cond_effs );
					fixed_point = fixed_point && changed;
					continue;
				}

				if ( act.when.empty() ) {
					for ( unsigned nd_eff_idx = 0; nd_eff_idx < act.ndeffects[0].effs.size(); nd_eff_idx++ ) {
						bool changed = process_effect( act, h_pre, nd_eff_idx );
						fixed_point = fixed_point && changed;
					}
					continue;
				}

				std::vector< std::pair< float, int > > fired_cond_effs;

				for ( int k = 0; k < (int)act.when.size(); k++ ) {
					float v_cond_eff = eval( act.precondition, act.when[k].condition );
					if ( v_cond_eff == infty ) continue;
					fired_cond_effs.push_back( std::make_pair( v_cond_eff, k ) );						
				}

				for ( unsigned nd_eff_idx = 0; nd_eff_idx < act.ndeffects[0].effs.size(); nd_eff_idx++ ) {
					bool changed = process_effect( act, h_pre, fired_cond_effs, nd_eff_idx );
					fixed_point = fixed_point && changed;
				}
				*/
			}	

		} while ( !fixed_point );
		std::cout << "Iterations to compute h^2 = " << iterations << std::endl;
	}

	/*
	bool	H2_Heuristic::interferes( 	const FOND_Model::Action& a,
						Lit l,
						int nd_eff_idx ) {
		int eff_idx_l = effect(l);
		
		// Check regular action effect
		if ( std::find( a.effect.begin(), a.effect.end(), eff_idx_l ) != a.effect.end() )
			return true;

		// Check for non-det effect interference
		if ( nd_eff_idx >= 0 ) {
			const index_set& nd_eff = a.ndeffects[0].effs.at(nd_eff_idx );
			if ( std::find( nd_eff.begin(), nd_eff.end(), eff_idx_l ) != nd_eff.end() )
				return true;
		}

		// Check for conditional effects interference
		for ( auto ce_it = a.when.begin(); ce_it != a.when.end(); ce_it++ ) {
			float v_cond = eval( ce_it->condition );
			if ( v_cond == infty ) continue;
			if ( std::find( ce_it->effect.begin(), ce_it->effect.end(), eff_idx_l ) != ce_it->effect.end() )
				return true;
		}

		int neg_eff_idx_l = effect(~l);

		// Check regular action effect
		if ( std::find( a.effect.begin(), a.effect.end(), neg_eff_idx_l ) != a.effect.end() )
			return true;

		// Check for non-det effect interference
		if ( nd_eff_idx >= 0 ) {
			const index_set& nd_eff = a.ndeffects[0].effs.at(nd_eff_idx );
			if ( std::find( nd_eff.begin(), nd_eff.end(), neg_eff_idx_l ) != nd_eff.end() )
				return true;
		}

		// Check for conditional effects interference
		for ( auto ce_it = a.when.begin(); ce_it != a.when.end(); ce_it++ ) {
			float v_cond = eval( ce_it->condition );
			if ( v_cond == infty ) continue;
			if ( std::find( ce_it->effect.begin(), ce_it->effect.end(), neg_eff_idx_l ) != ce_it->effect.end() )
				return true;
		}

		return false;
	}

	bool	H2_Heuristic::account_for_noops( const FOND_Model::Action& a, Lit li, int nd_eff_idx, int cond_eff_idx ) {

		bool fixed_point = true;
		// Check for noops
		for ( int r_index = 0; r_index < (int)(2*m_model.n_atoms()); r_index++ ) {
			
			Lit lr = toLit( r_index );
			
			if ( value( li, lr ) == 0.0f ) continue;

			if ( interferes( a, lr, nd_eff_idx ) ) {
				std::cout << "Action " << *(a.name) << " INTERFERES with "; 
				Atom idx = atom( li );
				const FOND_Model::Atom& ai = *(m_model.atoms[idx]);
				idx = atom( lr );
				const FOND_Model::Atom& ar = *(m_model.atoms[idx]);
				if ( sign(li) )
					std::cout << "(not " << *(ai.name) <<  ")";
				else
					std::cout << *(ai.name);
				std::cout << " & ";
				if ( sign(lr) )
					std::cout << "(not " << *(ar.name) << ")";
				else
					std::cout << *(ar.name);
				std::cout << std::endl;
				continue;
			}
			float v_noop = ( cond_eff_idx == -1 ? 
						eval( a.precondition, effect(lr) )
						: eval( a.precondition, a.when[cond_eff_idx].condition, effect(lr) ) );
			
			if ( v_noop == infty ) {
				std::cout << "Action " << *(a.name) << " CAN'T SUPPORT "; 
				Atom idx = atom( li );
				const FOND_Model::Atom& ai = *(m_model.atoms[idx]);
				idx = atom( lr );
				const FOND_Model::Atom& ar = *(m_model.atoms[idx]);
				if ( sign(li) )
					std::cout << "(not " << *(ai.name) <<  ")";
				else
					std::cout << *(ai.name);
				std::cout << " & ";
				if ( sign(lr) )
					std::cout << "(not " << *(ar.name) << ")";
				else
					std::cout << *(ar.name);
				std::cout << std::endl;
				continue;
			}
			
			v_noop += a.cost;
			
			if ( v_noop < value(li,lr) ) {
				fixed_point = false;
				value(li,lr) = v_noop;
			}
		}
	
		return fixed_point;
	}


	bool	H2_Heuristic::process_effect( 	const FOND_Model::Action& a, 
						float v,
						int nd_eff_idx ) {
		bool fixed_point = true;
		float v_act = v + a.cost;
		const index_set& eff = a.effect;

		for ( auto add_i = eff.begin(); add_i != eff.end(); add_i++ ) {
			
			Lit li = mkLit2(*add_i);
		
			if ( nd_eff_idx < 1 ) {	
				for ( auto add_j = add_i; add_j != eff.end(); add_j++ ) {
					Lit lj = mkLit2(*add_j);
					float& curr_v_ij = value( li, lj );
					
					if ( curr_v_ij == 0.0f ) continue;
					
					if ( v_act < curr_v_ij ) {
						fixed_point = false;
						curr_v_ij = v_act;
					}
				}
	
				bool fixed_point_noops = account_for_noops( a, li, nd_eff_idx );
				fixed_point = fixed_point && fixed_point_noops;
			}

			if ( nd_eff_idx < 0 ) continue;

			const index_set& nd_eff = a.ndeffects[0].effs.at( nd_eff_idx );

			for ( auto nd_add_j = nd_eff.begin(); nd_add_j != nd_eff.end(); nd_add_j++ ) {

				Lit lj = mkLit2(*nd_add_j);
				float& curr_v_ij = value( li, lj );
				
				if ( curr_v_ij == 0.0f ) continue;
				
				if ( v_act < curr_v_ij ) {
					fixed_point = false;
					curr_v_ij = v_act;
				}				
			}
		}

		if ( nd_eff_idx < 0 ) return fixed_point;

		const index_set& nd_eff = a.ndeffects[0].effs.at( nd_eff_idx );

		for ( auto nd_add_i = nd_eff.begin(); nd_add_i != nd_eff.end(); nd_add_i++ ) {
			Lit li = mkLit2(*nd_add_i);

			for ( auto nd_add_j = nd_add_i; nd_add_j != nd_eff.end(); nd_add_j++ ) {

				Lit lj = mkLit2(*nd_add_j);
				float& curr_v_ij = value( li, lj );
				
				if ( curr_v_ij == 0.0f ) continue;
				
				if ( v_act < curr_v_ij ) {
					fixed_point = false;
					curr_v_ij = v_act;
				}				
			}

			bool fixed_point_noops = account_for_noops( a, li, nd_eff_idx );
			fixed_point = fixed_point && fixed_point_noops;

		}
		
		return fixed_point;
	}

	bool	H2_Heuristic::process_effect( const FOND_Model::Action& a, 
					float v, 
					const Fired_Cond_Effects& ceffs,
					int nd_eff_idx ) {
		
		bool fixed_point = true;
		float v_act = v + a.cost;
		const index_set& eff = a.effect;

		for ( auto add_i = eff.begin(); add_i != eff.end(); add_i++ ) {
			
			Lit li = mkLit2(*add_i);
		
			if ( nd_eff_idx < 1 ) {
	
				for ( auto add_j = add_i; add_j != eff.end(); add_j++ ) {
					Lit lj = mkLit2(*add_j);
					float& curr_v_ij = value( li, lj );
					
					if ( curr_v_ij == 0.0f ) continue;
					
					if ( v_act < curr_v_ij ) {
						fixed_point = false;
						curr_v_ij = v_act;
					}
				}

				bool fixed_point_noops = account_for_noops( a, li, nd_eff_idx );
				fixed_point = fixed_point && fixed_point_noops;

				for ( auto fired_ce_it = ceffs.begin(); fired_ce_it != ceffs.end(); fired_ce_it++ ) {
					float v_cond_eff = fired_ce_it->first;
					const index_set& cond_eff = a.when[fired_ce_it->second].effect;
					for ( auto ce_i = cond_eff.begin(); ce_i != cond_eff.end(); ce_i++ ) {
						Lit lj = mkLit2( *ce_i );
						float& curr_v_ij = value( li, lj );
						
						if ( curr_v_ij == 0.0f ) continue;
				
						float v_ij = v_cond_eff + a.cost;
						if ( v_ij < curr_v_ij ) {
							fixed_point = false;
						}
					}
					
				}
			}

			
			if ( nd_eff_idx < 0 ) continue;

			const index_set& nd_eff = a.ndeffects[0].effs.at( nd_eff_idx );

			for ( auto nd_add_j = nd_eff.begin(); nd_add_j != nd_eff.end(); nd_add_j++ ) {

				Lit lj = mkLit2(*nd_add_j);
				float& curr_v_ij = value( li, lj );
				
				if ( curr_v_ij == 0.0f ) continue;
				
				if ( v_act < curr_v_ij ) {
					fixed_point = false;
					curr_v_ij = v_act;
				}				
			}

		}

		if ( nd_eff_idx < 1 ) {

			for ( auto fired_ce_it = ceffs.begin(); fired_ce_it != ceffs.end(); fired_ce_it++ ) {
				float v_cond_eff = fired_ce_it->first;
				const index_set& cond_eff = a.when[fired_ce_it->second].effect;

				float v_ij = v_cond_eff + a.cost;
				for ( auto ce_i = cond_eff.begin(); ce_i != cond_eff.end(); ce_i++ ) {
					Lit li = mkLit2( *ce_i );

					for ( auto ce_j = ce_i; ce_j != cond_eff.end(); ce_j++ ) {

						Lit lj = mkLit2( *ce_j );
						float& curr_v_ij = value( li, lj );
						
						if ( curr_v_ij == 0.0f ) continue;
				
						if ( v_ij < curr_v_ij ) {
							fixed_point = false;
						}
					}
					bool fixed_point_noops = account_for_noops( a, li, nd_eff_idx, fired_ce_it->second );
					fixed_point = fixed_point && fixed_point_noops;

				}

			}

		}

		if ( nd_eff_idx < 0 ) return fixed_point;

		const index_set& nd_eff = a.ndeffects[0].effs.at( nd_eff_idx );
		for ( auto fired_ce_it = ceffs.begin(); fired_ce_it != ceffs.end(); fired_ce_it++ ) {
			float v_cond_eff = fired_ce_it->first;
			const index_set& cond_eff = a.when[fired_ce_it->second].effect;

			float v_ij = v_cond_eff + a.cost;
			for ( auto ce_i = cond_eff.begin(); ce_i != cond_eff.end(); ce_i++ ) {
				Lit li = mkLit2( *ce_i );
				for ( auto nd_add_j = nd_eff.begin(); nd_add_j != nd_eff.end(); nd_add_j++ ) {

					Lit lj = mkLit2(*nd_add_j);
					float& curr_v_ij = value( li, lj );
					
					if ( curr_v_ij == 0.0f ) continue;
					
					if ( v_ij < curr_v_ij ) {
						fixed_point = false;
						curr_v_ij = v_act;
					}				
				}

			}
		}


		for ( auto nd_add_i = nd_eff.begin(); nd_add_i != nd_eff.end(); nd_add_i++ ) {
			Lit li = mkLit2(*nd_add_i);

			for ( auto nd_add_j = nd_add_i; nd_add_j != nd_eff.end(); nd_add_j++ ) {

				Lit lj = mkLit2(*nd_add_j);
				float& curr_v_ij = value( li, lj );
				
				if ( curr_v_ij == 0.0f ) continue;
				
				if ( v_act < curr_v_ij ) {
					fixed_point = false;
					curr_v_ij = v_act;
				}				
			}

			bool fixed_point_noops = account_for_noops( a, li, nd_eff_idx );
			fixed_point = fixed_point && fixed_point_noops;

		}

		return fixed_point;

	}
	*/
	void
	H2_Heuristic::extract_consequences_table() {
		int num_F = 2*m_model.n_atoms();
		for ( int i = 0; i < num_F; i++ ) {
			//Lit li = toLit( i );
			for ( int j = i+1; j < num_F; j++ ) {
				if ( value( i, j ) < infty ) continue;
				Lit lj = ~toLit( j );
				if ( std::find( m_consequences[i].begin(), m_consequences[i].end(), lj )
					!=  m_consequences[i].end() )
					continue;
				m_consequences[i].push_back( lj );
			}
		}
	}
}

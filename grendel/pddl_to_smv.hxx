#ifndef __PDDL_TO_SMV__
#define __PDDL_TO_SMV__

#include <fond_inst.hxx>
#include <string>

namespace grendel {

	class Rule_Based_H2_Heuristic;

	void direct_translation( const FOND_Model& task, std::string out_filename ); 
	void direct_translation( const FOND_Model& task, const Rule_Based_H2_Heuristic& h, std::string out_filename );
	void ramirez_translation( const FOND_Model& task, std::string out_filename );
	void ramirez_mbp_translation( const FOND_Model& task, std::string out_filename );
}


#endif //pddl_to_smv.hxx

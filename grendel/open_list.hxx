#ifndef __OPEN_LIST__
#define __OPEN_LIST__

#include <vector>
#include <queue>
#include <boost/heap/fibonacci_heap.hpp>

namespace grendel {

	template < class Node_Comp, class Node >
	class Open_List
	{
	public:
	
		typedef	Node		Node_Type;
	
		Open_List();
		~Open_List();
	
		void 		insert( Node* );
		Node* 		pop();
		Node*		first();
		bool		empty() const;
		float		min() const;
		void		clear();
	
	private:
	
		std::priority_queue< Node*, std::vector< Node* >, Node_Comp > m_queue;
	};
	
	template < class Node_Comp, class Node >
	Open_List<Node_Comp, Node>::Open_List()
	{
	}
	
	template < typename Node_Comp, typename Node >
	Open_List<Node_Comp, Node>::~Open_List()
	{
	}
	
	template < typename Node_Comp, typename Node >
	void	Open_List<Node_Comp, Node>::insert( Node* n )
	{
		m_queue.push( n );
	}
	
	template <typename Node_Comp, typename Node >
	Node*	Open_List<Node_Comp, Node>::pop()
	{
		if( empty() ) return nullptr;
		Node* elem = m_queue.top();
		m_queue.pop();
		return elem;
	}

	template <typename Node_Comp, typename Node >
	Node*	Open_List<Node_Comp, Node>::first()
	{
		if( empty() ) return nullptr;
		Node* elem = m_queue.top();
		return elem;
	}

	template < typename Node_Comp, typename Node >
	bool	Open_List<Node_Comp, Node>::empty() const
	{
		return m_queue.empty();
	}
	
	template < typename Node_Comp, typename Node >
	float     Open_List<Node_Comp, Node>::min() const
	{
		if ( empty() ) return 0.0f;
		return m_queue.top()->f;
	}
	
	template < typename Node_Comp, typename Node >
	void	Open_List<Node_Comp, Node>::clear() 
	{
		while ( !empty() )
		{
			Node* elem = pop();
			delete elem;
		}	
	}
	
	template < class Node >
	class Fibonacci_Open_List
	{
		struct	compare_node {
			bool operator()( Node* n1, Node* n2 ) const {
				return *n1 < *n2;
			}
		};

		typedef typename	boost::heap::fibonacci_heap< Node*, boost::heap::compare<compare_node> > Container;

	 	Container m_queue;
		public:
		typedef	typename 	Container::handle_type		     Handle;
	
		typedef	Node		Node_Type;
	
		Fibonacci_Open_List();
		~Fibonacci_Open_List();
	
		void 				insert( Node* );
		void				update( Node* );
		Node* 				pop();
		bool				empty() const;
		float				min() const;
		void				clear();
		Node*				first();
		typename Container::ordered_iterator	
						begin() const 	{ return m_queue.ordered_begin(); }
		typename Container::ordered_iterator
						end() const	{ return m_queue.ordered_end(); }
		void				erase( Node* );	

	};
	
	template < class Node >
	Fibonacci_Open_List<Node>::Fibonacci_Open_List()
	{
	}
	
	template < typename Node >
	Fibonacci_Open_List<Node>::~Fibonacci_Open_List()
	{
	}
	
	template < typename Node >
	void	Fibonacci_Open_List<Node>::insert( Node* n )
	{
		typename Container::handle_type handle = m_queue.push( n );
		n->heap_handle = handle;
		n->current = this;
		n->in_open = true;
	}
		
	template < typename Node >
	void	Fibonacci_Open_List<Node>::update( Node* n )
	{
		m_queue.update( n->heap_handle );
	}

	template < typename Node >
	void	Fibonacci_Open_List<Node>::erase( Node* n )
	{
		m_queue.erase( n->heap_handle );
	}

	template <typename Node >
	Node*	Fibonacci_Open_List<Node>::pop()
	{
		if( empty() ) return NULL;
		Node* elem = m_queue.top();
		m_queue.pop();
		elem->current = nullptr;
		return elem;
	}
	
	template <typename Node >
	bool	Fibonacci_Open_List<Node>::empty() const
	{
		return m_queue.empty();
	}

	template <typename Node >
	Node*     Fibonacci_Open_List<Node>::first()
	{
		if ( empty() ) return nullptr;
		return m_queue.top();
	}

	template <typename Node >
	float     Fibonacci_Open_List<Node>::min() const
	{
		if ( empty() ) return std::numeric_limits<float>::max();
		return m_queue.top()->fn;
	}
	
	template <typename Node >
	void	Fibonacci_Open_List<Node>::clear() 
	{
		while ( !empty() )
		{
			Node* elem = pop();
			delete elem;
		}	
	}
	
}	

#endif // open_list.hxx

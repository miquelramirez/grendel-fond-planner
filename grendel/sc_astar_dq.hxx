#ifndef __STRONG_CYCLIC_ASTAR_SEARCH_DOUBLE_QUEUE__
#define __STRONG_CYCLIC_ASTAR_SEARCH_DOUBLE_QUEUE__

#include <fond_inst.hxx>
#include <common.hxx>
#include <gp_atoms.hxx>
#include <open_list.hxx>
#include <ext_math.hxx>
#include <ex_policy.hxx>
#include <closed_list.hxx>
#include <hash_table.hxx>
#include <proc_nd_regression.hxx>

namespace grendel {

	namespace scastar_dq {
		class Node {
		public:

			typedef DNF_Clause	State_Type;
			
			DNF_Clause			state;
			int				action_idx;
			float				hn;
			float				gn;
			float				fn;
			Node*				parent;
			size_t				hash_key;
			bool				hash_up_to_date;
			int 				trials;
			int				failed_successors;
			bool				strong;
			bool				subsumed;
			void*				hash_table;
			int				num_ag;
			
	
			Node( const DNF_Clause& c, int _action_idx, float _gn = 0.0f, Node* _parent = nullptr, bool _strong = true ) 
			: state(c), action_idx( _action_idx ),  gn(_gn), parent(_parent), 
			trials(0), failed_successors(0), strong( _strong ), subsumed(false), num_ag( 0 ) {

				state.sort();	
				hash_up_to_date = false;
				hash_table = nullptr;
			}

			~Node() {
				assert( hash_table == nullptr );
				hash_up_to_date = false;
				gn = -1;
			}

			size_t	hash( ) const {
				return hash_key;
			}

			bool 	operator==( const Node& o ) const {
				return action_idx == o.action_idx && state == o.state;
			}

			size_t nbytes() const {
				size_t mem_used = sizeof( Node );
				return mem_used + state.nbytes();
			}
		};
	
		class Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				if( (a->fn - b->fn) > 1e-7 ) return true;
				if ( fabs( a->fn - b->fn ) < 1e-7 )
					return (a->gn - b->gn) > 1e-7;
				return false;
			}
		};

		class Weak_Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				if ( a->trials > b->trials ) return true;
				if ( b->trials == a->trials ) {
					if( a->failed_successors > b->failed_successors ) return true;
					if ( a->failed_successors == b->failed_successors ) {
						if( a->fn > b->fn  ) return true;
						if ( fabs( a->fn - b->fn ) < 1e-7 )
							return ( a->gn - b->gn ) > 1e-7;
					}
				}
				return false;
			}
		};


	}

	template <typename Mutex_Heuristic>
	class DQ_Strong_Cyclic_AStar_Search {

		typedef  Open_List< scastar_dq::Comparer, scastar_dq::Node > 	AStar_Strong_Open_List;
		typedef  Open_List< scastar_dq::Weak_Comparer, scastar_dq::Node >	AStar_Weak_Open_List;
		typedef  aptk::search::Closed_List< scastar_dq::Node >		AStar_Closed_List;

	public:
		DQ_Strong_Cyclic_AStar_Search( const FOND_Model& model, const Mutex_Heuristic& h ) 
		: m_model( model ), m_mutex_func( h ), m_gen_count(0), m_exp_count(0) {

		}

		~DQ_Strong_Cyclic_AStar_Search() {
			for ( typename AStar_Closed_List::iterator i = m_closed.begin();
				i != m_closed.end(); i++ ) {
				i->second->hash_table = nullptr;
				delete i->second;
			}
			
			while ( !m_strong_open.empty() ) {
				scastar_dq::Node* n = m_strong_open.pop();
				n->hash_table = nullptr;
				delete n;
			}

			m_closed.clear();
			m_strong_open_hash.clear();

			while ( !m_weak_open.empty() ) {
				scastar_dq::Node* n = m_weak_open.pop();
				n->hash_table = nullptr;
				delete n;
			}

			m_weak_open_hash.clear();

		}

		void	start() {
			/*
			std::cout << "Init: " << std::endl;
			m_model.init.write( std::cout, m_model );
			std::cout << std::endl;
			std::cout << "Goal: " << std::endl;
			m_model.goal.write( std::cout, m_model );
			std::cout << std::endl;
			*/

			DNF_Clause ext_goal;
			m_mutex_func.add_consequences( m_model.goal, ext_goal );

			std::cout << "Goal (extended): " << std::endl;
			ext_goal.write( std::cout, m_model );
			std::cout << std::endl;

			m_mem_used = 0;
			m_root = new scastar_dq::Node( ext_goal, -1 );
			m_root->strong = true;
			eval( m_root );
			compute_hash( m_root );
			m_solution = nullptr;
			open_strong_node( m_root );
			m_get_weak = false;
			m_strong_budget = 10;
			m_weak_budget = 0;
		}

		bool	
		find_strong_cyclic_plan( Explicit_Policy& plan ) {
			bool solved = false;
			
			if ( do_search( ) != nullptr ) {
	
				extract_plan( plan );
				solved = true;

			}

			return solved;
		} 

		void		inc_gen() { m_gen_count++; }
		void		inc_exp() { m_exp_count++; }

		unsigned	generated() const { return m_gen_count; }
		unsigned	expanded() const { return m_exp_count; }
		size_t		mem_used() const { return m_mem_used; }

		void		
		extract_plan( Explicit_Policy& plan ) {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				plan.add( it->second->state, it->second->action_idx );
			}
		}

	protected:

		bool
		check_delete_precondition(scastar_dq::Node* n ) const {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				if ( it->second == n ) return true;
			}
			for ( auto it = m_strong_open_hash.begin(); it != m_strong_open_hash.end(); it++ ) {
				if ( it->second == n ) return true;
			}
			for ( auto it = m_weak_open_hash.begin(); it != m_weak_open_hash.end(); it++ ) {
				if ( it->second == n ) return true;
			}
			return false;
		}

		void 			
		close( scastar_dq::Node* n ) { 
			//std::cout << "CLOSED stats: "; 
			m_closed.put(n); 
		}
	
		AStar_Closed_List&	
		closed() { 
			return m_closed; 
		}
	
		AStar_Closed_List&	
		open_hash() 	{ 
			return m_strong_open_hash; 
		}

		bool
		check_successors2( scastar_dq::Node* n ) {
			const FOND_Model::Action& a = *(m_model.actions[n->action_idx]);
			assert( a.effects.size() > 1 ); // predecessors of det actions shouldn't turn up here
			DNF_Clause   res_state;
			unsigned 	not_implied = 0;
			for ( auto eff_it = a.effects.begin(); eff_it != a.effects.end(); eff_it++ )
			{
				res_state.clear();	
				n->state.apply( a.precondition, *eff_it, res_state );
				if ( n->state == res_state  ) // Self-looping is Okay
					continue;
				if ( !implied_by_closed2( res_state ) )
					not_implied++; // return false;
			}	
			//std::cout << "Not implied: " << not_implied << std::endl;
			n->failed_successors = not_implied;
			return not_implied == 0;
		}

		bool
		implied_by_closed2( const DNF_Clause& c ) const {
			for ( auto it = m_closed.begin(); it != m_closed.end(); it++ ) {
				if ( it->second->state.implies( c ) ) {
					/*
					std::cout << "\t\tImplied by: " << std::endl;
					std::cout << "\t\t";
					it->second->state.write( std::cout, m_model );
					if ( it->second->action_idx == -1 ) 
						std::cout << " -> (GOAL)" << std::endl;
					else
						std::cout << " -> " << m_model.actions[it->second->action_idx]->name << std::endl;
					*/
					return true;
				}
			}
			return false;
		}

		bool
		implied_by_strong_open( const DNF_Clause& c ) const {
			for ( auto it = m_strong_open_hash.begin(); it != m_strong_open_hash.end(); it++ ) {
				if ( it->second->state.implies( c ) ) {
					/*
					std::cout << "\t\tImplied by: " << std::endl;
					std::cout << "\t\t";
					it->second->state.write( std::cout, m_model );
					if ( it->second->action_idx == -1 ) 
						std::cout << " -> (GOAL)" << std::endl;
					else
						std::cout << " -> " << m_model.actions[it->second->action_idx]->name << std::endl;
					*/
					return true;
				}
			}
			return false;
		}

		scastar_dq::Node*
		get_strong_node_first() {
			if ( !m_strong_open.empty() )
				return get_strong_node();
			if ( !m_weak_open.empty() )
				return get_weak_node();
			return nullptr;
		}

		scastar_dq::Node*
		alternate_between_strong_and_weak() {
			if ( !m_get_weak ) {
				m_get_weak = true;
				if ( !m_strong_open.empty() ) 
					return get_strong_node();
			}
			assert( m_get_weak );
			m_get_weak = false;
			if ( !m_weak_open.empty() )
				return get_weak_node();
			if ( !m_strong_open.empty() )
				return get_strong_node();
			return nullptr;
		}

		scastar_dq::Node*
		best_first_then_strong() {
			float best_strong_fn = ( m_strong_open.empty() ? infty : m_strong_open.first()->fn );
			float best_weak_fn = ( m_weak_open.empty() ? infty : m_weak_open.first()->fn );

			if ( best_strong_fn < best_weak_fn ) {
				return get_strong_node();
			}
			if ( best_strong_fn > best_weak_fn && m_weak_open.first()->parent->num_ag > 0 ) {
				return get_weak_node();
			}

			if ( !m_strong_open.empty() )
				return get_strong_node();
			if ( !m_weak_open.empty() )
				return get_weak_node();
			return nullptr;
		}

		scastar_dq::Node*
		first_strong_on_budget() {
			if ( m_strong_budget > 0 ) {
				if (!m_strong_open.empty() ) {
					m_strong_budget--;
					return get_strong_node();
				}
			}
			if ( !m_weak_open.empty() )
				return get_weak_node();
			if ( !m_strong_open.empty() )
				return get_strong_node();
			return nullptr;
		}

		scastar_dq::Node* 
		get_node() {
			scastar_dq::Node* n = nullptr;
			//n = get_strong_node_first();
			// n = alternate_between_strong_and_weak();
			n = best_first_then_strong();
			//n = first_strong_on_budget();
			return n;
		}

		void
		eval( scastar_dq::Node* n ) {
			n->hn = m_mutex_func.eval( n->state );
			n->fn = ( n->hn == infty ? infty : aptk::add( n->gn, 10.0f * n->hn ) );
		}

		scastar_dq::Node*	
		do_search() {
			scastar_dq::Node* head = get_node();
			int 	max_trials = 0;
			size_t	weak_open_size = 0;
			while(head) {
				//if ( expanded() > 100 ) return nullptr;
				if ( head->strong ) {	
					if(is_goal(head->state)) {
						std::cout << "Init reached, execution length is: " << head->gn << std::endl;
						close(head);
						return head;
					}
			
					close(head);
					process_strong_node(head);
					if ( !m_strong_open.empty() )
						m_strong_budget = ( ( head->fn -  m_strong_open.first()->fn) > 1e-7 ? 1 : 0 );
					head = get_node();
					continue;
				}
				process_weak_node( head );
				if ( 	m_strong_open.empty() && !m_weak_open.empty() 
					&& m_weak_open.first()->trials > max_trials ) {
					max_trials = m_weak_open.first()->trials;
					std::cout << "Max trials = " << max_trials << std::endl;
					std::cout << "Fixed point: old size = " << weak_open_size << " current size = " << m_weak_open_hash.size() << std::endl;

					if ( weak_open_size == m_weak_open_hash.size() ) {
						break; // Fixed Point!
					}
					weak_open_size = m_weak_open_hash.size();
				}
				head = get_node();
			}
			return nullptr;			
		}

		void
		print_node( std::ostream& os, scastar_dq::Node* n ) {
			os << "\t";
			n->state.write( os, m_model, false);
			os << std::endl;
			if ( n->action_idx == -1 ) 
				os << "-> (GOAL)" << std::endl;
			else
				os << "-> " << m_model.actions[n->action_idx]->name << std::endl;

			os << "f(n) = " << n->fn << " g(n) = " << n->gn << " h(n) = " << m_mutex_func.eval( n->state ) << " trials(n) = " << n->trials << std::endl;
				
		}

		void
		process_weak_node( scastar_dq::Node* head ) {
			/*
			if ( head->parent->num_ag == 0 ) {
				delete head;
				return;
			}
			*/
			assert( m_strong_open.empty() );
			if ( implied_by_closed2( head->state ) || implied_by_strong_open( head->state ) ) {
				//std::cout << "\t But is actually STRONG" << std::endl;
				delete head;
				return;
			}

			/*
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Expanding potential strong cyclic: " << std::endl;
			print_node( std::cout, head );	
			*/
			if ( check_successors2( head ) ) {
				if ( is_closed( head ) ) {
					delete head;
				}
				else if ( previously_hashed( head ) )
					delete head;
				else {
					head->strong = true;
					open_strong_node( head );
					//std::cout << "Goes into STRONG open" << std::endl;
				}
				return;
			}
			//std::cout << "Delaying expansion" << std::endl;
			head->trials++;
			assert( m_weak_open_hash.retrieve( head ) == nullptr );
			open_weak_node( head );
	
		}

		void
		compute_hash( scastar_dq::Node* n ) {
			aptk::Hash_Key hasher;
			for ( auto it = n->state.begin(); it != n->state.end(); it++ )
				hasher.add( toInt(*it) );
			hasher.add( 2*m_model.n_atoms() + n->action_idx );
			n->hash_key = (size_t)hasher;
			n->hash_up_to_date = true;
		}		

		bool
		relevance_check( const DNF_Clause& s, const FOND_Model::Action& a ) const {
			for ( auto eff_it = a.effects.begin(); eff_it != a.effects.end(); eff_it++ ) {
				for ( auto eff_it2 = eff_it->begin(); eff_it2 != eff_it->end(); eff_it2++ )
					for ( auto eff_it3 = eff_it2->effect.begin(); eff_it3 != eff_it2->effect.end(); eff_it3++ ) {
						if ( s.entails( *eff_it3 ) ) return true;
						if ( a.effects.size() == 1 && eff_it2->condition.empty() && s.entails( ~(*eff_it3) ) ) return false;	
					}
			}

			return false; // action doesn't mention any of the lits in the formula to be regressed
		}

		void 			
		process_strong_node(  scastar_dq::Node *head ) {

			inc_exp();
			/*
			std::cout << "======================================================================================================" << std::endl;
			std::cout << "Expanding strong cyclic: " << std::endl;
			print_node( std::cout, head );		
			*/
			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {	

				if ( !relevance_check( head->state, *(m_model.actions[i]) ) ) continue;

				std::vector<DNF_Clause>  succ;
				lbool res = procedural_strong_cyclic_regress( m_model, i, head->state, succ );

				//const FOND_Model::Action& a = *m_model.actions[i];
				
				assert( res != l_Undef );
				
				if ( res == l_False ) {
					//std::cout << "\t through " << *(a.name) << " results in FALSE" << std::endl;
					for ( auto it = succ.begin(); it != succ.end(); it++ ) {
												
						scastar_dq::Node* n = new scastar_dq::Node( *it, i, head->gn + m_model.actions[i]->cost, head, false );
						eval(n);
						if ( n->hn == infty ) {
							//std::cout << "\t through " << *(a.name) << " results in ";
							//it->write( std::cout, m_model );
							//std::cout << " which is mutex" << std::endl;
							delete n;
							continue;
						}
						DNF_Clause ext_succ;
						m_mutex_func.add_consequences( *it, ext_succ );
						n->state = std::move(ext_succ);
						compute_hash( n );
						if ( is_closed( n ) ) {
							//std::cout << "\t through " << *(a.name) << " already in closed! " << std::endl;
							delete n;
							continue;
						}
						if( weak_previously_hashed(n) ) {
							//std::cout << "\t through " << *(a.name) << " already in WEAK open! " << std::endl;
							delete n;
						}
						else {
							/*
							std::cout << "\t through " << m_model.actions[i]->name << " results in new WEAK predecessor " << std::endl;
							std::cout << "\t";
							it->write( std::cout, m_model, false );
							std::cout << std::endl;
							*/
							if ( implied_by_closed2( n->state ) || implied_by_strong_open( n->state ) ) {
								//std::cout << "\t But is actually STRONG" << std::endl;
								delete n;
								continue;
							}
							open_weak_node( n );
						}
					}	
					continue;
				}
				head->num_ag++;
				for ( auto it = succ.begin(); it != succ.end(); it++ ) {
					scastar_dq::Node* n = new scastar_dq::Node( *it, i, head->gn + m_model.actions[i]->cost, head );
					eval(n);
					if ( n->hn == infty ) {
						//std::cout << "\t through " << *(a.name) << " results in mutex " << std::endl;
						//it->write( std::cout, m_model );
						//std::cout << " which is mutex" << std::endl;
						delete n;
						continue;
					}
					DNF_Clause ext_succ;
					m_mutex_func.add_consequences( *it, ext_succ );
					n->state = std::move(ext_succ);

					compute_hash( n );
					
					if ( is_closed( n ) ) {
						//std::cout << "\t through " << *(a.name) << " already in CLOSED! " << std::endl;
						delete n;
						continue;
					}
					if ( implied_by_closed2( n->state ) || implied_by_strong_open( n->state ) ) {
						//std::cout << "\t But is actually STRONG" << std::endl;
						delete n;
						continue;
					}

					if( previously_hashed(n) ) {
						//std::cout << "\t through " << *(a.name) << " already in STRONG open! " << std::endl;
						delete n;
					}
					else {
						/*
						std::cout << "\t through " << m_model.actions[i]->name << " results in STRONG predecessor " << std::endl;
						std::cout << "\t";
						it->write( std::cout, m_model, false );
						std::cout << std::endl;
						*/
						open_strong_node(n);	
					}
				}	

			} 
		}

		bool 		
		is_closed( scastar_dq::Node* n ) 	{ 
			scastar_dq::Node* n2 = this->closed().retrieve(n);
	
			if ( n2 != NULL ) {
				if ( n2->gn <= n->gn ) {
					// The node we generated is a worse path than
					// the one we already found
					return true;
				}
				// Otherwise, we put it into Open and remove
				// n2 from closed
				this->closed().erase( this->closed().retrieve_iterator( n2 ) );
			}
			return false;
		}
		
		bool
		is_goal( const DNF_Clause& c ) {
			bool satisfied = true;
			for ( auto it = c.begin(); it != c.end(); it++ ) {
				if ( m_model.init.entails( ~(*it) ) ) {
					satisfied = false;
					break;
				}
			}
			return satisfied;
		}
	
		scastar_dq::Node* 		
		get_strong_node() {
			scastar_dq::Node* next = nullptr;
			if(! m_strong_open.empty() ) {
				next = m_strong_open.pop();
				assert( next->hash_table == &m_strong_open_hash );
				assert( m_strong_open_hash.retrieve_iterator( next) != m_strong_open_hash.end() );
				m_strong_open_hash.erase( m_strong_open_hash.retrieve_iterator( next) );
				next->hash_table = nullptr;
			}
			return next;
		}

		scastar_dq::Node*
		get_weak_node() {
			scastar_dq::Node* next = nullptr;
			if(! m_weak_open.empty() ) {
				next = m_weak_open.pop();
				assert( next->hash_table == &m_weak_open_hash );
				assert( m_weak_open_hash.retrieve_iterator( next) != m_weak_open_hash.end() );
				m_weak_open_hash.erase( m_weak_open_hash.retrieve_iterator( next ) );
				next->hash_table = nullptr;
			}
			return next;			
		}

		void	 	
		open_strong_node( scastar_dq::Node *n ) {
			m_strong_open.insert(n);
			assert( m_strong_open_hash.retrieve_iterator( n ) == m_strong_open_hash.end() );
			m_strong_open_hash.put(n);
			/*
			std::cout << "STRONG OPEN STATS:";
			m_strong_open_hash.print_stats( std::cout );
			*/
			assert(  m_strong_open_hash.retrieve_iterator( n ) != m_strong_open_hash.end() );
			inc_gen();
			m_mem_used += n->nbytes();
		}

		bool	
		is_mutex( const DNF_Clause& c ) const {
			return m_mutex_func.eval( c ) == infty;
		}

		bool	
		is_mutex( const DNF_Clause& c, const DNF_Clause& d ) const {
			return m_mutex_func.eval( c, d ) == infty;
		}

		bool 			
		previously_hashed( scastar_dq::Node *n ) {
			scastar_dq::Node *previous_copy = NULL;
			if( (previous_copy = m_strong_open_hash.retrieve(n)) ) {
			
				if(n->gn < previous_copy->gn)
				{
					previous_copy->parent = n->parent;
					previous_copy->action_idx = n->action_idx;
					previous_copy->gn = n->gn;
				}
				return true;
			}

			return false;
		}
		
		void	 	
		open_weak_node( scastar_dq::Node *n ) {	
			m_weak_open.insert(n);
			assert( m_weak_open_hash.retrieve_iterator( n ) == m_weak_open_hash.end() );
			m_weak_open_hash.put(n);
			inc_gen();
			m_mem_used += n->nbytes();
		}
		
		bool 			
		weak_previously_hashed( scastar_dq::Node *n ) {
			scastar_dq::Node *previous_copy = nullptr;
			if( (previous_copy = m_weak_open_hash.retrieve(n)) ) {
			
				if(n->gn < previous_copy->gn)
				{
					previous_copy->parent = n->parent;
					previous_copy->action_idx = n->action_idx;
					previous_copy->gn = n->gn;
				}
				return true;
			}

			return false;
		}

	private:
		const FOND_Model&			m_model;
		AStar_Strong_Open_List			m_strong_open;
		AStar_Weak_Open_List			m_weak_open;
		const Mutex_Heuristic&			m_mutex_func;
		unsigned				m_gen_count;
		unsigned				m_exp_count;
		AStar_Closed_List			m_closed;
		AStar_Closed_List			m_strong_open_hash;
		AStar_Closed_List			m_weak_open_hash;
		scastar_dq::Node*			m_root;
		scastar_dq::Node*			m_solution;
		bool					m_get_weak;
		size_t					m_mem_used;
		int					m_strong_budget;
		int					m_weak_budget;
	};

}

#endif // ucs.hxx

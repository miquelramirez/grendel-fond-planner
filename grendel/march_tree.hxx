#ifndef __MARCH_TREE__
#define __MARCH_TREE__

#include <grendel/gp_atoms.hxx>
#include <grendel/fond_inst.hxx>
#include <grendel/search_node.hxx>
#include <cassert>
#include <list>

namespace grendel {

class March_Tree {

	class Tree_Node {
	public:

		Tree_Node( Atom a, Tree_Node* n )
			: the_atom( a ), parent( n ) {
			l_pos = mkLit( the_atom );
			l_neg = mkLit( the_atom, true );
			children[0] = children[ 1 ] = children[ 2 ] = nullptr;
		}

		lbool	select_child( const DNF_Clause& s ) const {
			assert( !(s.entails(l_pos) && s.entails(l_neg)));
			if ( s.entails( l_pos ) )
				return l_True;
			if ( s.entails( l_neg ) )
				return l_False;
			return l_Undef;
		}

		Tree_Node* get_child( lbool truth_value ) {
			return children[toInt(truth_value)];
		}

		void	set_child( lbool truth_value, Tree_Node* n ) {
			children[toInt(truth_value)] = n;
		}

		Atom				the_atom;
		Lit				l_pos;
		Lit				l_neg;
		Tree_Node*			parent;
		Tree_Node*			children[3];
		std::vector< Search_Node* >	search_nodes;
	};

	public:

		March_Tree( const FOND_Model& model );
		~March_Tree();

		void		add( Search_Node*	n );

		void		retrieve( const DNF_Clause& s, std::list<Search_Node*>& nodes );
	
		unsigned	num_nodes() const { return m_tree_nodes.size(); }

	protected:

		void		add( Search_Node* n, Tree_Node* m, unsigned d );
		void		retrieve( const DNF_Clause& c, Tree_Node* m, std::list<Search_Node*>& nodes );
	
	private:
	
		const		FOND_Model&	m_model;
		Tree_Node*			m_root;
		std::vector< Tree_Node* >	m_tree_nodes;
	
};

}

#endif

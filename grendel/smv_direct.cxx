#include <smv_direct.hxx>
#include <fstream>

namespace grendel {

	std::string g_smv_true = "\"T\"";
	std::string g_smv_false = "\"F\"";


	void	
	SMV_Module::make_vars() {

		// Action vars
		for ( auto action_ptr : m_task.actions ) {
			const FOND_Model::Action& a = *action_ptr;
			std::string smv_var_name = "\"" + a.name + "\"";
			m_action_vars.push_back( smv_var_name );
			smv_var_name = "\"" + a.name + "@pc\"";
			m_prec_vars.push_back( smv_var_name );
		}	

		// Atom vars
		for ( auto atom_ptr : m_task.atoms ) {
			const FOND_Model::Atom& a = *atom_ptr;
			std::string smv_var_name = "\"" + a.name + "\"";
			m_atom_vars.push_back( smv_var_name );
		}			

	}

	void	
	SMV_Module::write_SMV_header( std::ofstream& out ) const {
		out << "--PROBLEM--" << std::endl;
		out << "MODULE main" << std::endl;
		out << "DOMAINNAME " << m_task.domain_name << std::endl;
		out << "PROBLEMNAME " << m_task.problem_name << std::endl;
		out << "PROBLEMDOMAIN " << m_task.domain_name << std::endl;	
	}

	void	
	SMV_Module::write_action_vars( std::ofstream& out ) const {
		out << "IVAR" << std::endl;
		out << "\taction: {" << std::endl;
		for ( unsigned k = 0; k < m_action_vars.size(); k++ ) {
			out << "\t\t" << m_action_vars[k];
			if ( k < m_action_vars.size() - 1 )
				out << ",";
			out << std::endl;
		}
		out << "\t};" << std::endl;
	}

	void	
	SMV_Module::write_atom_vars( std::ofstream& out ) const {
		out << "VAR" << std::endl;
		// Syntax: <var name>: boolean;
		for ( unsigned k = 0; k < m_atom_vars.size(); k++ ) {
			out << "\t\t" << m_atom_vars[k] << ": boolean;" << std::endl;
		}
	}

	void	
	SMV_Module::write_init( std::ofstream& out ) const {
		// Syntax: ( (<var name 1>=value) & ... & (<var name n>=value) )
		out << "INIT" << std::endl;
		out << "(";
		for ( unsigned k = 0; k < m_task.init.size(); k++ ) {
			Lit l = m_task.init[k];
			int atom_idx = atom(l);
			assert( atom_idx < m_task.atoms.size() );
			const std::string var_name = m_atom_vars[ atom_idx ];
			if ( !sign(l) ) 
				out << " " << var_name << " ";
			else
				out << " !" << var_name << " ";
			if ( k < m_task.init.size() - 1 )
				out << " &";
			out << " ";
		}
		out << ")" << std::endl;
	}

	void		
	SMV_Module::write_conjunction( const DNF_Clause& clause, bool next, std::ofstream& out ) const {
		for ( unsigned k = 0; k < clause.size(); k++ ) {
			Lit l = clause[k];
			int atom_idx = atom(l);
			assert( atom_idx < m_task.atoms.size() );
			bool truth_value = !sign(l);
			const std::string var_name = m_atom_vars[ atom_idx ];
			if ( !next ) 
				out << ( truth_value ? " " : " !") << var_name;
			else
				out << ( truth_value ? " " : " !") << "next(" << var_name << ")";
			if ( k < clause.size() - 1 )
				out << " &";
			out << " ";
		} 
	}

	void	
	SMV_Module::write_preconditions( std::ofstream& out ) const {
		out << "DEFINE" << std::endl;
		// Syntax: <var name>:=( (<var name>=value) <op> ... );
		for ( unsigned k = 0; k < m_task.actions.size(); k++ ) {
			out << m_prec_vars[k] << ":=(";
			const FOND_Model::Action& a = *(m_task.actions[k]);
			write_conjunction( a.precondition, false, out );
			out << ");" << std::endl;
		}
	}
	
	void
	SMV_Module::write_frame_effects( std::vector<int>& frame, std::ofstream& out ) const {
		unsigned k = 0;	
		for ( auto it = frame.begin(); it != frame.end(); it++ ) {
			int 	atom_idx = *it;
			const std::string var_name = m_atom_vars[  atom_idx ];
			out << " ( next(" << var_name << ")=" << var_name << ")";
			if ( k < frame.size() - 1 )
				out << " &";
			out << " ";
			k++;
		}
	}

	void
	SMV_Module::write_action_effect( const FOND_Model::Action& a, unsigned index,  std::ofstream& out ) const {
		out <<"\t\t(" << std::endl;
		for ( unsigned k = 0; k < a.effects.size(); k++ ) {
			const FOND_Model::Effect_Vec& nd_effs = a.effects[k];
			std::vector<int> frame_effects;
			DNF_Clause combined;
			for ( auto eff : nd_effs ) {
				combined.add( eff.effect );
			}
			if ( combined.empty() )
				combined.add( a.precondition );
			out << "\t\t\t(";
			out << "\t\t\t";
			write_conjunction( combined, true, out );
			out << std::endl;
			out << "\t\t\t)" << std::endl;
			for ( int p = 0; p < (int)m_task.atoms.size(); p++ ) {
				Lit p_pos = mkLit( p, false );
				Lit p_neg = mkLit( p, true );
				if ( !combined.entails( p_pos ) && !combined.entails( p_neg ) ) 
					frame_effects.push_back( p );
			}
			if (!frame_effects.empty()) {
				out << "\t\t\t&" << std::endl;
				out << "\t\t\t(" << std::endl;
				out << "\t\t\t";
				write_frame_effects( frame_effects, out );
				out << "\t\t\t)" << std::endl;
			}

			if ( k < a.effects.size() - 1 )
				out << "\t\t|" << std::endl;
		}
		out << "\t\t)" << std::endl;
	}

	void
	SMV_Module::write_action_effect( const Rule_Based_H2_Heuristic& h, const FOND_Model::Action& a, unsigned index,  std::ofstream& out ) const {
		out <<"\t\t(" << std::endl;
		for ( unsigned k = 0; k < a.effects.size(); k++ ) {
			const FOND_Model::Effect_Vec& nd_effs = a.effects[k];
			std::vector<int> frame_effects;
			DNF_Clause combined;
			for ( auto eff : nd_effs ) {
				combined.add( eff.effect );
			}
			if ( combined.empty() )
				combined.add( a.precondition );
			
			DNF_Clause closure;
			h.add_consequences( combined, closure );

			out << "\t\t\t(";
			out << "\t\t\t";
			write_conjunction( closure, true, out );
			out << std::endl;
			out << "\t\t\t)" << std::endl;
			for ( int p = 0; p < (int)m_task.atoms.size(); p++ ) {
				Lit p_pos = mkLit( p, false );
				Lit p_neg = mkLit( p, true );
				if ( !closure.entails( p_pos ) && !closure.entails( p_neg ) ) 
					frame_effects.push_back( p );
			}
			if (!frame_effects.empty()) {
				out << "\t\t\t&" << std::endl;
				out << "\t\t\t(" << std::endl;
				out << "\t\t\t";
				write_frame_effects( frame_effects, out );
				out << "\t\t\t)" << std::endl;
			}

			if ( k < a.effects.size() - 1 )
				out << "\t\t|" << std::endl;
		}
		out << "\t\t)" << std::endl;

	}

	void	
	SMV_Module::write_transition_function( std::ofstream& out ) const {
		out << "TRANS" << std::endl;
		for ( unsigned k = 0; k < m_task.actions.size(); k++ ) {
			out << "(" << std::endl;
			out << "\t( ( action=" << m_action_vars[k] << "  &  " << m_prec_vars[k] << " )" << std::endl;
			out << "\t  ->" << std::endl;
			write_action_effect( *(m_task.actions[k]) , k, out );
			out << "\t)" << std::endl;
			out << "\t&" << std::endl;
			out << "\t( action= " << m_action_vars[k] << "  &  !" << m_prec_vars[k] << " -> FALSE )" << std::endl;
			out << " )" << std::endl;
			if ( k < m_task.actions.size() - 1 )
				out << "&" << std::endl;
		}
	}

	void	
	SMV_Module::write_transition_function( const Rule_Based_H2_Heuristic& h, std::ofstream& out ) const {
		out << "TRANS" << std::endl;
		for ( unsigned k = 0; k < m_task.actions.size(); k++ ) {
			out << "(" << std::endl;
			out << "\t( ( action=" << m_action_vars[k] << "  &  " << m_prec_vars[k] << " )" << std::endl;
			out << "\t  ->" << std::endl;
			write_action_effect( h, *(m_task.actions[k]) , k, out );
			out << "\t)" << std::endl;
			out << "\t&" << std::endl;
			out << "\t( action= " << m_action_vars[k] << "  &  !" << m_prec_vars[k] << " -> FALSE )" << std::endl;
			out << " )" << std::endl;
			if ( k < m_task.actions.size() - 1 )
				out << "&" << std::endl;
		}
	}
	/*
	void
	SMV_Module::encode_atom_evolution_rule( int action_idx, int atom_idx, std::ofstream& out ) const {

		int p = atom_idx + 1;
		int not_p = -( atom_idx + 1 );

		const FOND_Model::Action& a = *(m_task.actions[action_idx]);
		assert( a.when.empty() );
		// eff(a) entails p
		bool affected = false;
		if ( std::find( a.effect.begin(), a.effect.end(), p ) != a.effect.end() ) {
			assert( std::find( a.effect.begin(), a.effect.end(), not_p ) == a.effect.end() );
			out << "(action = " << m_action_vars[action_idx] << ") & " << m_prec_vars[action_idx] << ": TRUE;" << std::endl;
			affected = true;
		} 
		if ( std::find( a.effect.begin(), a.effect.end(), not_p ) != a.effect.end() ) {
			assert( std::find( a.effect.begin(), a.effect.end(), p ) == a.effect.end() );
			out << "(action = " << m_action_vars[action_idx] << ") & " << m_prec_vars[action_idx] << ": FALSE;" << std::endl;
			affected = true;
		}

		// Easy case, no n.d. effects, no cond. effects
		if ( !affected && a.ndeffects.empty() ) {
			const index_set& pre = a.precondition;
			auto p_it = std::find( pre.begin(), pre.end(), p );
			auto not_p_it = std::find( pre.begin(), pre.end(), not_p );

			if ( p_it == pre.end() && not_p_it == pre.end() )
				return;
			out << "(action = " << m_action_vars[action_idx] << ") & " << m_prec_vars[action_idx] << ":";
			if ( p_it != pre.end() && not_p_it == pre.end() )
				out << " TRUE";
			else if ( p_it == pre.end() && not_p_it == pre.end() ) 
				out << " FALSE";
			else 
				assert( false );
			out << ";" << std::endl;
			return;
		}
		if ( affected || a.ndeffects.empty() ) return;
		std::vector< int > eff_values;

		bool made_true = false;
		bool made_false = false;
		bool unmentioned = false;

		for ( unsigned k = 0; k < a.ndeffects[0].effs.size(); k++ ) {
			index_set eff = ( a.ndeffects[0].effs[k].empty() ? a.precondition :  a.ndeffects[0].effs[k] );
			auto p_it = std::find( eff.begin(), eff.end(), p );
			auto not_p_it = std::find( eff.begin(), eff.end(), not_p );
			if ( p_it == eff.end() && not_p_it == eff.end() ) {
				unmentioned = true;
				const index_set& pre = a.precondition;
				auto p_it = std::find( pre.begin(), pre.end(), p );
				auto not_p_it = std::find( pre.begin(), pre.end(), not_p );
				if ( !made_true && p_it != pre.end() ) {
					eff_values.push_back( 1 );
					made_true = true;
				}
				if ( !made_false && not_p_it != pre.end() ) {
					eff_values.push_back( 0 );
					made_false = true;
				}

			}
			else if ( p_it != eff.end() && not_p_it == eff.end() ) {
				if ( !made_true ) {
					eff_values.push_back( 1 );
					made_true = true;
				}
			}
			else if ( p_it == eff.end() && not_p_it != eff.end() ) {
				if ( !made_false ) {
					eff_values.push_back( 0 );
					made_false = true;
				}
			}
			else
				assert(false); // Inconsistent problem!
		}

		if ( unmentioned & !made_true & !made_false ) {
			const index_set& pre = a.precondition;
			auto p_it = std::find( pre.begin(), pre.end(), p );
			auto not_p_it = std::find( pre.begin(), pre.end(), not_p );
			unmentioned = made_true = made_false = false;

			if ( p_it == pre.end() && not_p_it == pre.end() ) {
				return;
			}
			else if ( p_it != pre.end() && not_p_it == pre.end() ) {
				if ( !made_true ) {
					eff_values.push_back( 1 );
					made_true = true;
				}
			}
			if ( p_it == pre.end() && not_p_it == pre.end() ) {
				if ( !made_false ) {
					eff_values.push_back( 0 );
					made_false = true;
				}
			}
		}
		
		out << "(action = " << m_action_vars[action_idx] << ") & " << m_prec_vars[action_idx] << ":";
		if ( eff_values.size() == 1 ) {
			if ( eff_values[0] == 0 )
				out << " FALSE";
			else if ( eff_values[0] == 1 )
				out << " TRUE";
			else
				out << " " << m_atom_vars[atom_idx];
			out << ";" << std::endl;
		}
		else {

			out << "{";
			for ( unsigned k = 0; k < eff_values.size(); k++ ) {
				if ( eff_values[k] == 0 )
					out << " FALSE";
				else if ( eff_values[k] == 1 )
					out << " TRUE";
				else
					out << " " << m_atom_vars[atom_idx];
				out << " ";
				if ( k < eff_values.size() - 1 )
					out << ",";
			}
			out << "};" << std::endl;
		}
	}

	void
	SMV_Module::write_evolution_rules( std::ofstream& out ) const {
		for ( unsigned atom_idx = 0; atom_idx < m_task.atoms.size(); atom_idx++ ) {
			out << "ASSIGN next(" << m_atom_vars[atom_idx] << ") :=" << std::endl;
			out << "case" << std::endl;
			for ( unsigned k = 0; k < m_task.actions.size(); k++ ) {
				encode_atom_evolution_rule( k, atom_idx, out );
			} 
			out << "1 : " << m_atom_vars[atom_idx] << ";" << std::endl;
			out << "esac;" << std::endl;
		}
	}
	*/
	void	
	SMV_Module::write_goal( std::ofstream& out ) const {
		out << "FULL_OBS_STRONG_CYCLIC_GOAL" << std::endl;
		// Syntax: ( (<var name 1>=value) & ... & (<var name n>=value) )
		out << "(";
		for ( unsigned k = 0; k < m_task.goal.size(); k++ ) {
			Lit l = m_task.goal[k];
			int atom_idx = atom(l);
			assert( atom_idx < m_task.atoms.size() );
			const std::string var_name = m_atom_vars[ atom_idx ];
			out << ( !sign(l) ? " " : " !" ) << var_name;
			if ( k < m_task.goal.size() - 1 )
				out << " &";
			out << " ";

		}
		out << ")" << std::endl;

	}

	void	
	SMV_Module::write() const {
		std::ofstream	output_stream( m_filename.c_str() );

		write_SMV_header( output_stream );
		write_action_vars( output_stream );
		write_atom_vars( output_stream );
		write_init( output_stream );
		write_preconditions( output_stream );
		write_transition_function( output_stream );
		//write_evolution_rules( output_stream );
		write_goal( output_stream );
	}

	void	
	SMV_Module::write(const Rule_Based_H2_Heuristic& h ) const {
		std::ofstream	output_stream( m_filename.c_str() );

		write_SMV_header( output_stream );
		write_action_vars( output_stream );
		write_atom_vars( output_stream );
		write_init( output_stream );
		write_preconditions( output_stream );
		write_transition_function( h, output_stream );
		//write_evolution_rules( output_stream );
		write_goal( output_stream );
	}

}

#include <smv_mbp_ramirez.hxx>
#include <sstream>
#include <fstream>

namespace grendel {

	const std::string kStartAction = "smv_start";
	const std::string kNoOpAction = "no_op";
	const std::string kStartEffect = "smv_start_eff_0";
	const std::string kEndAction = "smv_end";
	const std::string kWaitAction = "wait";
	const std::string kSelecting = "selecting";
	const std::string kExecuting = "executing";

	void
	Ramirez_MBP_SMV_Program::process_fond_task() {
		process_atoms();
		process_actions();
	}

	void
	Ramirez_MBP_SMV_Program::process_atoms() {
		// Atom vars
		for ( auto atom_ptr : m_task.atoms ) {
			const FOND_Model::Atom& a = *atom_ptr;
			std::string smv_var_name = "\"" + a.name + "\"";
			m_atom_vars.push_back( smv_var_name );
		}
		m_atom_neg_eff.resize( m_task.atoms.size() );
		m_atom_pos_eff.resize( m_task.atoms.size() );
	}

	void
	Ramirez_MBP_SMV_Program::process_actions() {

		for ( auto action_ptr : m_task.actions ) {

			const FOND_Model::Action& a = *action_ptr;
			std::string smv_var_name = "\"" + a.name + "\"";

			m_action_lits.push_back( smv_var_name );
			m_eff_lits.push_back( std::vector<std::string>() );

			for ( unsigned i = 0; i < a.effects.size(); i++ ) {

				std::string smv_eff_name =  "\"" + a.name + "_eff";
				std::stringstream buffer;

				buffer << "_" << i;
				smv_eff_name += buffer.str();
				smv_eff_name += "\"";
				m_eff_lits.back().push_back( smv_eff_name );

				ActionEffect index;

				for ( unsigned k = 0; k < a.effects[i].size(); k++ ) {
					const FOND_Model::Effect& det_eff =a.effects[i][k];
					index.a = a.index;
					index.e = i;
					index.d = k;
					for ( unsigned j = 0; j < det_eff.effect.size(); j++ ) {
						Lit l = det_eff.effect[j];
						if ( sign(l) )
							m_atom_neg_eff[atom(l)].push_back( index );
						else
							m_atom_pos_eff[atom(l)].push_back( index );
					}
				}
			}
		}

	}
	std::string
	Ramirez_MBP_SMV_Program::repr_formula( const DNF_Clause& phi, std::string context, bool next ) const {
		std::string expr = "(";

		for ( unsigned k = 0; k < phi.size(); k++ ) {
			expr += next ? repr_next_lit( phi[k], context ) : repr_lit( phi[k], context );
			if ( k < phi.size() - 1 )
				expr += " & ";
		}

		expr += ")";

		return expr;
	}

	void
	Ramirez_MBP_SMV_Program::write_main_module( std::ostream& out ) const {
		out << "--PROBLEM--" << std::endl;
		out << "MODULE main" << std::endl;
		out << "DOMAINNAME " << m_task.domain_name << std::endl;
		out << "PROBLEMNAME " << m_task.problem_name << std::endl;
		out << "PROBLEMDOMAIN " << m_task.domain_name << std::endl;

		out << "IVAR" << std::endl;
		write_action_variables( out );

		out << "VAR" << std::endl;
		write_general_variables( out );
		write_action_effects( out );

		out << "INIT" << std::endl;
		write_environment_initial_conditions( out );
		write_non_det_initial_conditions( out );

		out << "TRANS" << std::endl;
		write_state_variable_transition_rules( out );
		out << "TRANS" << std::endl;
		write_non_deterministic_execution_rules( out );
		//out << "TRANS" << std::endl;
        //@TODO@: FIXME - Check what's wrong with the invariants being
        // generated
		//write_invariants( out );

		write_goal( out );

		out << "-- end MODULE main" << std::endl;

	}

	void
	Ramirez_MBP_SMV_Program::write() const {
		std::ofstream	output_stream( m_filename.c_str() );
		write_main_module( output_stream );

		output_stream.close();
	}

	void
	Ramirez_MBP_SMV_Program::write_goal( std::ostream& os ) const {
		os << "FULL_OBS_STRONG_CYCLIC_GOAL" << std::endl;
		os << repr_formula( m_task.goal ) << std::endl;
	}

	void
	Ramirez_MBP_SMV_Program::write_action_variables( std::ostream& os ) const {
		std::string indent = "\t";
		std::string indent_lit = "\t";
		os << indent << "action : {" << std::endl;
		//os << indent_lit << kNoOpAction << "," << std::endl;
		os << indent_lit << kWaitAction << "," << std::endl;
		//os << indent_lit << kEndAction << "," << std::endl;

		// write action literals
		for ( unsigned k = 0; k < m_action_lits.size(); k++ ) {
			os << indent_lit << m_action_lits[k];
			if ( k < m_action_lits.size() -1 )
				os << ",";
			os << std::endl;
		}


		os << indent << "};" << std::endl;
	}

	void
	Ramirez_MBP_SMV_Program::write_action_effects( std::ostream& os ) const {
		std::string indent = "\t";
		os << indent << "effect : {" << std::endl;
		os << indent << indent << "none," << std::endl;
		//os << indent << indent << kStartEffect << "," << std::endl;

		for ( unsigned i = 0; i < m_eff_lits.size(); i++ ) {
			os << indent << indent;
			for ( unsigned j = 0; j < m_eff_lits[i].size(); j++ ) {
				os << m_eff_lits[i][j];
				if ( i < m_eff_lits.size() - 1 )
					os << " , ";
				else {
					if ( j < m_eff_lits[i].size() - 1 )
						os << " , ";
				}
			}
			os << std::endl;
		}

		os << indent << "};" << std::endl;
	}

	void
	Ramirez_MBP_SMV_Program::write_state_variables( std::ostream& os ) const {
		std::string indent = "\t";
		for ( auto atom : m_atom_vars ) {
			os << indent << atom << " : boolean;" << std::endl;
		}
	}

	void
	Ramirez_MBP_SMV_Program::write_general_variables( std::ostream& os ) const {
		write_state_variables( os );
	}

	void
	Ramirez_MBP_SMV_Program::write_environment_initial_conditions( std::ostream& os ) const {
		std::string indent = "\t";
		os << indent << repr_formula( m_task.init ) << std::endl;
	}

	void
	Ramirez_MBP_SMV_Program::write_non_det_initial_conditions( std::ostream& os ) const {
		std::string indent = "\t";
		os << indent << "&" << " (effect = none)" << std::endl;
	}

	void
	Ramirez_MBP_SMV_Program::write_state_variable_transition_rules( std::ostream& os ) const {
		std::string indent = "\t";
		for ( unsigned i = 0; i < m_atom_vars.size(); i++ ) {
			os << indent << "case" << std::endl;
			Lit pos_lit = mkLit( i );
			Lit neg_lit = mkLit( i, true );

			// check for init
			/*
			if ( m_task.init.entails( pos_lit ) ) {
				os << indent << indent;
				os << "nd.effect = " << kStartEffect << " : ";
				os << repr_next_lit( pos_lit ) << ";" << std::endl;
			}
			if ( m_task.init.entails( neg_lit ) ) {
				os << indent << indent;
				os << "nd.effect = " << kStartEffect << " : ";
				os << repr_next_lit( neg_lit ) << ";" << std::endl;
			}
			*/
			// positive effects
			for ( auto index : m_atom_pos_eff[i] ) {

				// resolve effect
				const FOND_Model::Effect& eff = m_task.actions[index.a]->effects[index.e][index.d];
				// resolve effect literal
				auto eff_lit_name = m_eff_lits[index.a][index.e];

				os << indent << indent;
				os << "effect = " << eff_lit_name;
				if ( eff.condition.size() > 0 ) {
					os << " & (";
					os<< repr_formula( eff.condition );
					os << ")";
				}
				os << " : ";
				os << repr_next_lit( pos_lit ) << ";" << std::endl;

			}

			// negative effects
			for ( auto index : m_atom_neg_eff[i] ) {

				// resolve effect
				const FOND_Model::Effect& eff = m_task.actions[index.a]->effects[index.e][index.d];
				// resolve effect literal
				auto eff_lit_name = m_eff_lits[index.a][index.e];

				os << indent << indent;
				os << "effect = " << eff_lit_name;
				if ( eff.condition.size() > 0 ) {
					os << " & (";
					os<< repr_formula( eff.condition );
					os << ")";
				}
				os << " : ";
				os << repr_next_lit( neg_lit ) << ";" << std::endl;

			}

			// Frame axiom
			os << indent << indent << "TRUE : next(" << m_atom_vars[i] << ") = " << m_atom_vars[i] << ";" << std::endl;

			os << indent << "esac" << std::endl;
			if ( i < m_atom_vars.size() - 1 )
				os << indent << "&" << std::endl;
		}

	}

	void
	Ramirez_MBP_SMV_Program::write_non_deterministic_execution_rules( std::ostream& os ) const {
		std::string indent = "\t";

		//os << indent << "case" << std::endl;
		//os << indent << indent << "ag.action = " << kStartAction << " : ";
		//os << "next(effect) = " << kStartEffect << ";" << std::endl;
		//os << indent << "next(effect) := case" << std::endl;
		os << indent << "case" << std::endl;
		for ( unsigned action_index = 0; action_index < m_action_lits.size(); action_index++ ) {
			// write action
			os << indent << indent << "action = " << m_action_lits[action_index];
			os << " & ";
			os << repr_formula( m_task.actions[ action_index ]->precondition );
			os << " & ";
			os << "(effect = none )";
			os << " : ";
			if ( m_eff_lits[action_index].size() == 1 ) {
				os << "next(effect) = ";
				os <<  m_eff_lits[action_index][0] << ";" << std::endl;
				continue;
			}
			os << " next(effect) in { ";
			for ( unsigned k = 0; k < m_eff_lits[action_index].size(); k++ ) {
				os <<  m_eff_lits[action_index][k];
				if ( k < m_eff_lits[action_index].size() - 1 )
					os << " , ";
			}
			os << "};" << std::endl;
		}
		//os << indent << indent << "action = " << kEndAction << " :  none;" << std::endl;
		//os << indent << indent << "action = " << kWaitAction << " :  none;" << std::endl;
		os << indent << indent << "action = " << kWaitAction << " & (effect != none ) :  next(effect) = none;" << std::endl;
		//os << indent << indent << "action = " << kNoOpAction << " : none;" << std::endl;
		//os << indent << "esac;" << std::endl;
		os << indent << "esac" << std::endl;
		//os << indent << " & ((ag.action !=" << kStartAction << ") -> (effect != none))" << std::endl;
	}

	void
	Ramirez_MBP_SMV_Program::write_invariants( std::ostream& os ) const {
		std::string indent = "\t";
		for ( unsigned k = 0; k < m_task.invariants.size(); k++ ) {
			os << "-- invariant #" << k << std::endl;
			const DNF_Clause& c = m_task.invariants[k];
			// 1. Write clause c = l_1 \or ... \l_n
			os << indent << "(";
			for ( unsigned i = 0; i < c.size(); i++ ) {
				os << repr_lit( c[i] );
				if ( i < c.size() - 1 )
					os << " | ";
			}
			os << ")" << std::endl;
			os << indent << " & " << std::endl;
			// 2. Write binary clauses \neg l_i \or \neg l_j, i != j
            if ( c.size() == 1 ) continue;
			for ( unsigned i = 0; i < c.size(); i++ ) {
				for ( unsigned j = i+1; j < c.size(); j++ ) {
					os << indent << "(";
					os << repr_lit( ~c[i] ) << " | " << repr_lit( ~c[j] );
					os << ")";
					if (  j < c.size() - 1 )
						os << std::endl << indent << " & " << std::endl;
				}
				if ( i < c.size() - 2 )
					os << std::endl << indent << "&";
				os << std::endl;

			}
			if ( k < m_task.invariants.size()-1 )
				os << indent << "&" << std::endl;
		}
	}
}

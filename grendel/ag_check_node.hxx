#ifndef __AG_CHECK_NODE__
#define __AG_CHECK_NODE__

#include <gp_atoms.hxx>
#include <fond_inst.hxx>
#include <open_list.hxx>
#include <ext_math.hxx>
#include <hash_table.hxx>

namespace grendel
{
	class AG_Check_Node {
		public:
		typedef DNF_Clause	State_Type;

		AG_Check_Node( Search_Node* n, const DNF_Clause& ctx, const FOND_Model& model ) 
			: policy_entry( n ), context(ctx), index( no_idx ), lowlink( no_idx ), 
			parent( nullptr ), AG_connected(false), gn(0.0f), closed(false), hn(infty),
			the_model( model ), greedy_policies(0), in_stack( false ), is_duplicate( false ), failed(false) {
			aptk::Hash_Key hasher;
			context.update_hash( hasher );
			hasher.add( DNF_Clause::num_lits() + ( n->action_idx == -1 ? the_model.actions.size() : n->action_idx ) );
			hash_key = (size_t)hasher;
		}

		void 	compute_hash() {
			aptk::Hash_Key hasher;
			context.update_hash( hasher );
			hasher.add( DNF_Clause::num_lits() + ( policy_entry->action_idx == -1 ? the_model.actions.size() : policy_entry->action_idx ) );
			hash_key = (size_t)hasher;
		}
		
		size_t	hash( ) const {
			return hash_key;
		}

		bool 	operator==( const AG_Check_Node& o ) const {
			return context == o.context && policy_entry->action_idx == o.policy_entry->action_idx;
		}

		const int 		no_idx = std::numeric_limits<int>::max();
		Search_Node*				policy_entry;
		DNF_Clause				context;
		int					index;
		int					lowlink;
		AG_Check_Node*				parent;
		bool					AG_connected;
		float					gn;	
		bool					closed;
		size_t					hash_key;
		std::vector<bool>			delete_succ;
		std::vector<bool>			deadend_succ;
		std::vector<float>			cost_succ;
		float					hn;
		const			FOND_Model&	the_model;
		unsigned				greedy_policies;
		bool					in_stack;
		bool					is_duplicate;
		std::vector<AG_Check_Node*>		successors;
		bool					failed;
		std::vector<DNF_Clause>			assumptions;
	};

	class AG_Check_Node_Comparer {
	public:
		bool	operator()( AG_Check_Node* a, AG_Check_Node* b ) const {
			if ( a->policy_entry->hn < b->policy_entry->hn ) return true;
			if ( aptk::dequal(  a->policy_entry->hn, b->policy_entry->hn ) )
				if ( b->index < a->index ) return true;
			return false;
		}
	};
	typedef  Open_List< AG_Check_Node_Comparer, AG_Check_Node > 		SCC_Container;

	typedef  aptk::search::Closed_List< AG_Check_Node >			AG_Closed_List;

}

#endif

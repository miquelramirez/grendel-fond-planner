#ifndef __UNIFORM_COST_SEARCH__
#define __UNIFORM_COST_SEARCH

#include <problem.hxx>
#include <gp_atoms.hxx>
#include <nd_regression.hxx>
#include <open_list.hxx>
#include <ext_math.hxx>
#include <ex_policy.hxx>
#include <closed_list.hxx>
#include <hash_table.hxx>
#include <nd_regression.hxx>

namespace grendel {

	namespace ucs {
		class Node {
		public:

			typedef DNF_Clause	State_Type;
			
			DNF_Clause			state;
			int				action_idx;
			float				gn;
			Node*				parent;
			size_t				hash_key;
			
	
			Node( const DNF_Clause& c, int _action_idx, float _gn = 0.0f, Node* _parent = nullptr ) 
			: state(c), action_idx( _action_idx ),  gn(_gn), parent(_parent) {
					
				aptk::Hash_Key hasher;
				for ( auto it = c.begin(); it != c.end(); it++ )
					hasher.add( toInt(*it) );
				hash_key = (size_t)hasher;
			}

			size_t	hash( ) const {
				return hash_key;
			}

			bool 	operator==( const Node& o ) const {
				return state == o.state;
			}
		};
	
		class Comparer {
		public:
			bool	operator()( Node* a, Node* b ) const {
				return aptk::dless( b->gn, a->gn );
			}
		};
	}

	template <typename Mutex_Heuristic>
	class Uniform_Cost_Search {

		typedef  Open_List< ucs::Comparer, ucs::Node > 	UCS_Open_List;
		typedef  aptk::search::Closed_List< ucs::Node >		UCS_Closed_List;

	public:
		Uniform_Cost_Search( const Instance& model, const Mutex_Heuristic& h ) 
		: m_model( model ), m_mutex_func( h ), m_gen_count(0), m_exp_count(0) {

			for ( grendel::Atom p = 0; p < (int)m_model.n_atoms(); p++ ) {
				bool found = false;
				for ( auto init_it = m_model.init.literals.begin();
					init_it != m_model.init.literals.end() && !found; init_it++ ) {
					int idx = *init_it > 0 ? *init_it-1 : -*init_it-1;
					found = idx == p;
				}
					
				if ( found ) 
					m_init.add( mkLit(p,false) );
				else
					m_init.add( mkLit(p,true) );
			}

			m_goal = DNF_Clause( m_model.goal_literals );

		}

		~Uniform_Cost_Search() {
			for ( typename UCS_Closed_List::iterator i = m_closed.begin();
				i != m_closed.end(); i++ ) {
				delete i->second;
			}
			
			while ( !m_open.empty() ) {
				ucs::Node* n = m_open.pop();
				delete n;
			}

			m_closed.clear();
			m_open_hash.clear();
		}

		void	start() {
			m_root = new ucs::Node( m_goal, -1 );
			m_solution = nullptr;
			open_node( m_root );
		}

		bool	
		find_strong_plan( Explicit_Policy& plan ) {
			m_solution = do_search();
			if ( m_solution != nullptr ) {
				extract_plan( plan );
				return true;
			}
			return false;
		} 

		void		inc_gen() { m_gen_count++; }
		void		inc_exp() { m_exp_count++; }

		unsigned	generated() const { return m_gen_count; }
		unsigned	expanded() const { return m_exp_count; }

		void		
		extract_plan( Explicit_Policy& plan ) {
			ucs::Node* tmp = m_solution;
			while ( tmp != nullptr ) {
				plan.add( tmp->state, tmp->action_idx );
				tmp = tmp->parent;
			}
		}

	protected:

		void 			
		close( ucs::Node* n ) 	{  
			m_closed.put(n); 
		}
	
		UCS_Closed_List&	
		closed() 			{ 
			return m_closed; 
		}
	
		UCS_Closed_List&	
		open_hash() 	{ 
			return m_open_hash; 
		}

		ucs::Node*	
		do_search() {
			ucs::Node* head = get_node();
			while(head) {
	
				if(is_goal(head->state)) {
					std::cout << "Init reached ";
					head->state.write( std::cout, m_model );
					std::cout << ", execution length is: " << head->gn << std::endl;
					close(head);
					return head;
				}
		
				process(head);
				close(head);
				head = get_node();
			}
			return nullptr;			
		}

		void 			
		process(  ucs::Node *head ) {

			inc_exp();

			for ( int i = 0; i < (int)m_model.n_actions(); i++ ) {	

				DNF_Clause  succ;
				lbool res = strong_regress( m_model, i, head->state, succ );

				assert( res != l_Undef );
				if ( res == l_False || is_mutex( succ ) )
					continue;
	
				ucs::Node* n = new ucs::Node( succ, i, head->gn + m_model.actions[i]->cost, head );
	
				if ( is_closed( n ) ) {
					delete n;
					continue;
				}
				if( previously_hashed(n) ) {
					delete n;
				}
				else 
					open_node(n);	
			} 
		}

		bool 		
		is_closed( ucs::Node* n ) 	{ 
			ucs::Node* n2 = this->closed().retrieve(n);
	
			if ( n2 != NULL ) {
				if ( n2->gn <= n->gn ) {
					// The node we generated is a worse path than
					// the one we already found
					return true;
				}
				// Otherwise, we put it into Open and remove
				// n2 from closed
				this->closed().erase( this->closed().retrieve_iterator( n2 ) );
			}
			return false;
		}
		
		bool
		is_goal( const DNF_Clause& c ) {
			bool satisfied = true;
			for ( auto it = c.begin(); it != c.end(); it++ ) {
				if ( m_init.entails( ~(*it) ) ) {
					satisfied = false;
					break;
				}
			}
			return satisfied;
		}
	
		ucs::Node* 		
		get_node() {
			ucs::Node* next = nullptr;
			if(! m_open.empty() ) {
				next = m_open.pop();
				assert( m_open_hash.retrieve_iterator( next) != m_open_hash.end() );
				m_open_hash.erase( m_open_hash.retrieve_iterator( next) );
			}
			return next;
		}

		void	 	
		open_node( ucs::Node *n ) {
			m_open.insert(n);
			m_open_hash.put(n);
			inc_gen();
		}

		bool	
		is_mutex( const DNF_Clause& c ) const {
			return m_mutex_func.eval( c ) == infty;
		}

		bool 			
		previously_hashed( ucs::Node *n ) {
			ucs::Node *previous_copy = NULL;
			if( (previous_copy = m_open_hash.retrieve(n)) ) {
			
				if(n->gn < previous_copy->gn)
				{
					previous_copy->parent = n->parent;
					previous_copy->action_idx = n->action_idx;
					previous_copy->gn = n->gn;
				}
				return true;
			}

			return false;
		}

	private:
		const Instance&				m_model;
		UCS_Open_List				m_open;
		const Mutex_Heuristic&			m_mutex_func;
		unsigned				m_gen_count;
		unsigned				m_exp_count;
		UCS_Closed_List				m_closed;
		UCS_Closed_List				m_open_hash;
		ucs::Node*				m_root;
		ucs::Node*				m_solution;
		DNF_Clause				m_init;
		DNF_Clause				m_goal;
	};

}

#endif // ucs.hxx

#ifndef __SMV_DIRECT__
#define __SMV_DIRECT__

#include <fond_inst.hxx>
#include <prop_h2.hxx>
#include <gp_atoms.hxx>

namespace grendel {

	class SMV_Module {
	public:

		SMV_Module( const FOND_Model& task, std::string filename ) 
		: m_task( task ), m_filename( filename ) {
		}

		~SMV_Module() {
		}
		
		void		make_vars();
		void		write() 	const;
		void		write( const Rule_Based_H2_Heuristic& h ) 	const;

	protected:
		
		void		write_SMV_header( std::ofstream& out ) const;
		void		write_action_vars( std::ofstream& out ) const;
		void		write_atom_vars( std::ofstream& out ) const;
		void		write_init( std::ofstream& out ) const;
		void		write_preconditions( std::ofstream& out ) const;
		void		write_transition_function( std::ofstream& out ) const;
		void		write_transition_function( const Rule_Based_H2_Heuristic& h, std::ofstream& out ) const;

		void		write_evolution_rules( std::ofstream& out ) const;
		void		write_goal( std::ofstream& out ) const;

		void		write_frame_effects( std::vector<int>& frame, std::ofstream& out ) const;

		void		write_action_effect( const FOND_Model::Action& a, unsigned k, std::ofstream& out ) const;

		void		write_action_effect( const Rule_Based_H2_Heuristic& h, const FOND_Model::Action& a, unsigned k, std::ofstream& out ) const;

		void		write_conjunction( const DNF_Clause& lits, bool next, std::ofstream& out ) const;

		void 		encode_atom_evolution_rule( int action_idx, int atom_idx, std::ofstream& out ) const;
	private:
		const 		FOND_Model& 	m_task;
		std::string			m_filename;
		std::vector< std::string >	m_action_vars;
		std::vector< std::string >	m_atom_vars;
		std::vector< std::string >	m_prec_vars;
	};	
}


#endif // __SMV_DIRECT__

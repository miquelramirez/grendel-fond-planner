#!/usr/bin/python
import 	sys, os, glob
import 	benchmark
from run_tools import parse_report, extract_inputs, extract_inputs_structured,  extract_faults_inputs, extract_gadgets_inputs
import 	mbp

def main() :

	if len(sys.argv) < 2 :
		print >> sys.stderr, "No folder was specified!"
		sys.exit(1)		
	
	if not os.path.exists( sys.argv[1] ) :
		print >> sys.stderr, sys.argv[1], "not a valid path!"
		sys.exit(1)

	if not os.path.isdir( sys.argv[1] ) :
		print >> sys.stderr, sys.argv[1], "not a directory!"
		sys.exit(1)

	inputs = []
	if 'faults' in sys.argv[1] :
		inputs = extract_faults_inputs( sys.argv[1] )
	elif 'gadgets' in sys.argv[1] :
		inputs = extract_gadgets_inputs( sys.argv[1] )
	else :
		inputs = extract_inputs_structured( sys.argv[1] )
		if inputs is None or len(inputs) == 0 :
			inputs = extract_inputs( sys.argv[1] )
			if len(inputs) == 0 :
				print >> sys.stderr, "No inputs found!"
				sys.exit(1)


	#mbp_cmd = "./MBP -sc_local_search -dynamic -thresh 10000 %(smv_file)s"	
	mbp_cmd = './mbp.py %(domain_pddl)s %(instance_pddl)s'
	mbp_logname = "%(domain_name)s-%(instance_name)s-mbp.log"
	results = []
	for domain_name, instance_name, domain_pddl, instance_pddl in inputs :
		results.append( [ domain_name, instance_name, 0, 0, 0 ] )
		mbp_log = benchmark.Log( mbp_logname%locals() )
		mbp_rv, mbp_time = benchmark.run( mbp_cmd%locals(), 1800, 3072, mbp_log )
		results[-1] += [ mbp_rv, mbp_time, mbp_time ]
		found_plan = 2
		bdd_time = None
		search_time = None
		with open( mbp_log.name ) as instream :
			for line in instream :
				if 'The plan has been found' in line :
					found_plan = 1
				if 'The plan has NOT been found' in line :
					found_plan = 0
				if 'Total preprocessing time' in line :
					tmp = line.strip()
					tmp = tmp.replace('*','')
					tmp = tmp.strip()
					tmp = tmp.replace('seconds','')
					tokens = tmp.split('=')
					bdd_time = tokens[1].strip()
				if 'Searching time' in line :
					tmp = line.strip()
					tmp = tmp.replace('*','')
					tmp = tmp.strip()
					tmp = tmp.replace('seconds','')
					tokens = tmp.split('=')
					search_time = tokens[1].strip()

				
		if found_plan == 0 :
			results[-1] += ['No']
		elif found_plan == 1 :
			results[-1] += ['Yes']
		else :
			results[-1] += ['?']
		if bdd_time is None :
			results[-1] += ['?']
		else :
			results[-1] += [ bdd_time ]
		if search_time is None :
			results[-1] += ['?']
		else :
			results[-1] += [ search_time ]


	with open( '%s.mbp.csv'%inputs[0][0], 'w' ) as outstream :
		for res in results :
			res = [ str(field) for field in res ]
			print >> outstream, ",".join(res)

if __name__ == '__main__' :
	main()

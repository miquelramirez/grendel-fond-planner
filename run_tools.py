import os
import sys
import glob

def parse_report() :
	if not os.path.exists( 'report.csv' ) :
		raise RuntimeError, "Could not find file 'report.csv'"
	with open( 'report.csv' ) as instream :
		for line in instream :
			line = line.strip()
			tokens = line.split(',')
	return tokens

def extract_faults_inputs( folder ) :
	pddl_files = glob.glob( folder + "/*.pddl" )
	if len(pddl_files) == 0 :
		print >> sys.stderr, "No PDDL files in folder", folder
		sys.exit(1)
	domains = [ f for f in pddl_files if os.path.split(f)[-1][0] == 'd' ]
	problems = [ f for f in pddl_files if os.path.split(f)[-1][0] == 'p' ]

	inputs = []
	
	for dom in domains :
		pattern = os.path.split(dom)[-1].replace('d_','').replace('-fixed.pddl','')
		match = None
		for prob in problems :
			if pattern + '.' in prob :
				match = prob
				break
		if match is None :
			print >> sys.stderr, "Could not match domain", dom, "(pattern='%s' with any problem!"%pattern
			sys.exit(1)

		inputs.append( ('faults', pattern, dom, prob) )

	inputs.sort()
	return inputs

def extract_gadgets_inputs( folder ) :
	pddl_files = glob.glob( folder + "/*.pddl" )
	if len(pddl_files) == 0 :
		print >> sys.stderr, "No PDDL files in folder", folder
		sys.exit(1)
	domains = [ f for f in pddl_files if 'domain' in os.path.split(f)[-1] ]
	problems = [ f for f in pddl_files if 'problem' in os.path.split(f)[-1] ]

	inputs = []
	
	for dom in domains :
		pattern = os.path.split(dom)[-1].replace('gadget-domain-','').replace('.pddl','')
		match = None
		for prob in problems :
			if pattern + '.' in prob :
				match = prob
				break
		if match is None :
			print >> sys.stderr, "Could not match domain", dom, "(pattern='%s' with any problem!"%pattern
			sys.exit(1)

		inputs.append( ('gadgets', pattern, dom, prob) )

	inputs.sort()
	return inputs


def extract_inputs_structured( folder ) :	
	domain_name = os.path.split(folder)[-1]
	if len(domain_name) == 0 :
		domain_name = os.path.split(os.path.split(folder)[-2])[-1]
	domain_pddl = os.path.join( folder, 'domain.pddl' )
	if not os.path.exists( domain_pddl ) :
		print >> sys.stderr, "No domain file found in folder", folder
		sys.exit(1)

	instances_path = os.path.join( folder, 'instances' )	
	if not os.path.exists( instances_path ) :
		return

	pddl_files = glob.glob( os.path.join( instances_path, '*.pddl' ) )
	
	inputs = []

	for pddl_instance in pddl_files :
		inputs.append( ( domain_name, os.path.split(pddl_instance)[-1].replace('.pddl',''), domain_pddl, pddl_instance ) )

	inputs.sort()

	return inputs


def extract_inputs( folder ) :
	pddl_files = glob.glob( folder + "/*.pddl" )

	if len(pddl_files) == 0 :
		print >> sys.stderr, "No PDDL files in folder", folder
		sys.exit(1)

	domain_name = os.path.split(folder)[-1]
	
	domain_pddl = os.path.join( folder, 'domain.pddl' )
 
	if domain_pddl not in pddl_files :
		print >> sys.stderr, "No domain file found in folder", folder
		sys.exit(1)


	pddl_files.remove( domain_pddl )
	
	inputs = []

	for pddl_instance in pddl_files :
		inputs.append( ( domain_name, os.path.split(pddl_instance)[-1].replace('.pddl',''), domain_pddl, pddl_instance ) )

	inputs.sort()

	return inputs

if __name__ == '__main__' :
	inputs = []
	if 'faults' in sys.argv[1] :
		inputs =  extract_faults_inputs( sys.argv[1] )
	else :
		inputs = extract_inputs( sys.argv[1] )
	for i in inputs :
		print i

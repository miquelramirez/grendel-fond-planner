#!/usr/bin/python
import 	sys, os, glob
import 	benchmark
from run_tools import parse_report, extract_inputs, extract_inputs_structured, extract_faults_inputs, extract_gadgets_inputs

# Successful output of MyND
# Heuristic: FF-heuristic.
# Algorithm: LAO*-search
# INITIAL IS PROVEN!
# 
# Result: Strong cyclic plan found.
# 
# Time needed for preprocess (Parsing, PDBs, ...):    0.052 seconds.
# Time needed for search:                             0.049 seconds.
# Time needed:                                        0.101 seconds.

# Out of 52 nodes, 17 are proven
# Number of node expansions: 17
# Policy entries: 10
# Number of sensing applications in policy: 0
# Total Garbage Collections: 0
# Total Garbage Collection Time (ms): 0

# Unsucessful output of MyND
# Heuristic: FF-heuristic.
# Algorithm: LAO*-search
# INITIAL IS DISPROVEN!

# Result: No strong cyclic plan found.

# Time needed for preprocess (Parsing, PDBs, ...):    0.008 seconds.
# Time needed for search:                             0.004 seconds.
# Time needed:                                        0.012 seconds.

# Total Garbage Collections: 0
# Total Garbage Collection Time (ms): 0


def collect_data( log ) :

	strong_cyclic = '?'
	
	
	with open(log.name ) as instream:
		for line in instream :
			line = line.strip()
			if 'Strong cyclic plan found' in line :
				strong_cyclic = 'Yes'
				continue
			if 'No strong cyclic plan found' in line :
				strong_cyclic = 'No'
				continue
	
		instream.close()
	
	return [strong_cyclic] 

def main() :

	if len(sys.argv) < 2 :
		print >> sys.stderr, "No folder was specified!"
		sys.exit(1)		
	
	if not os.path.exists( sys.argv[1] ) :
		print >> sys.stderr, sys.argv[1], "not a valid path!"
		sys.exit(1)

	if not os.path.isdir( sys.argv[1] ) :
		print >> sys.stderr, sys.argv[1], "not a directory!"
		sys.exit(1)

	inputs = []
	if 'faults' in sys.argv[1] :
		inputs = extract_faults_inputs( sys.argv[1] )
	elif 'gadgets' in sys.argv[1] :
		inputs = extract_gadgets_inputs( sys.argv[1] )
	else :
		inputs = extract_inputs_structured( sys.argv[1] )
		if inputs is None or len(inputs) == 0 :
			inputs = extract_inputs( sys.argv[1] )
			if len(inputs) == 0 :
				print >> sys.stderr, "No inputs found!"
				sys.exit(1)

	
	translate_command = 'translator-fond/translate.py %(domain_pddl)s %(instance_pddl)s'
	command = "java -ea -Xmx4g -classpath src mynd.MyNDPlanner -laostar -ff output.sas"
	logname = "%(domain_name)s-%(instance_name)s-mynd.log"

	results = []
	for domain_name, instance_name, domain_pddl, instance_pddl in inputs :
		log = benchmark.Log( logname%locals() )
		results.append( [ domain_name, instance_name ] )
		rv, time = benchmark.run( translate_command%locals(), 1800, 8192 )
		results[-1] += [ str(rv), str(time) ]
		if rv != 0 : continue
		rv, time = benchmark.run( command%locals(), 1800, 8192, log )
		results[-1] += [ str(rv), str(time) ]
		results[-1] += collect_data(log)

	with open( '%s.mynd.csv'%inputs[0][0], 'w' ) as outstream :
		for res in results :
			res = [ str(field) for field in res ]
			print >> outstream, ",".join(res)

if __name__ == '__main__' :
	main()

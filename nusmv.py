#! /usr/bin/env python

from __future__ import print_function

from collections import defaultdict

import pddl
import timers
from fondparser import *
from libgrendel import SMVCompiler 
import sys, os
import 	benchmark

def read_result_output( filename) :
	txt_buffer = []
	with open( filename ) as instream :
		for line in instream :
			txt_buffer.append(line.strip())
	return txt_buffer


def collect_counterexample( result_buffer ) :
	offset_s = offset_a = offset_s2 = 0
	buffer_s = []
	buffer_a = []
	buffer_s2 = []
	i = 0

	while i < len(result_buffer) :
		if "-> State" in result_buffer[i] :
			if offset_s == 0 :
				offset_s = i
				offset_a = offset_s2 = i
			else :
				if offset_a == offset_s :
					offset_a = i
					offset_s2 = i
				else :
					offset_s2 = i
		i += 1

	# Print counterexample
	state_var_lits_conjunction = []
	print( "s:" )
	for i in range(offset_s+1, offset_a) :
		context, rest = [ tok.strip() for tok in result_buffer[i].split(".") ]
		var, lit = [ tok.strip() for tok in rest.split("=") ]
		if context != "environment" :
			continue
		print (result_buffer[i])
		print ( "context: %s"%context)
		print ( "var: %s"%var )
		print ( "lit: %s"%lit )
		truth_value = True
		if "FALSE" in lit :
			truth_value = False
		state_var_lits_conjunction.append( ( var, truth_value ) )

	forbidden_action = None
	print( "a:" )
	for i in range(offset_a+1, offset_s2) :
		context, rest = [ tok.strip() for tok in result_buffer[i].split(".") ]
		var, lit = [ tok.strip() for tok in rest.split("=") ]

		if context != "agent" : continue
		print (result_buffer[i])
		print ( "context: %s"%context)
		print ( "var: %s"%var )
		print ( "lit: %s"%lit )
		forbidden_action = lit	

	print( "State: %s"%state_var_lits_conjunction )
	print( "Action: %s"%forbidden_action )

#	print( "s':" )
#	for i in range(offset_s2+1, len(result_buffer) ) :
#		print (result_buffer[i])		 

def main( domain_file, problem_file) :
	print("Domain: %s Problem: %s"%(domain_file, problem_file) )

	task = pddl.open( problem_file, domain_file)
	fond_task = SMVCompiler(task.domain_name, task.task_name )

	parse( task, fond_task )
	
	
	fond_task.verify_policy = False 
	fond_task.dump_policy = False 

	fond_task.smv_filename = task.domain_name + '_' + task.task_name + '.smv'
	fond_task.ramirez_smv_trans( )

	# Construct command file for nusmv
	cmd_file = task.domain_name + '_' + task.task_name + '.script'

	result_file = "ctl-check.out"

	with open( cmd_file, 'w' ) as cmd_stream :
		print( "set on_failure_script_quits 1", file= cmd_stream  )
		print( "set verbose_level 1", file=cmd_stream )
		print( "read_model -i %s"%fond_task.smv_filename, file= cmd_stream )
		print( "flatten_hierarchy", file= cmd_stream )
		print( "set dynamic_reorder 1", file= cmd_stream  )
		print( "encode_variables", file= cmd_stream )
		print( "set conj_part_threshold 10000", file= cmd_stream  )
		print( "set partition_method Threshold", file= cmd_stream  )
		print( "build_flat_model", file = cmd_stream )
		print( "build_model", file= cmd_stream  )
		print( "compute_reachable", file = cmd_stream )
		print( "print_reachable_states", file = cmd_stream )
		print( "check_fsm", file= cmd_stream )
		print( "set cone_of_influence 1", file = cmd_stream )
		print( "check_ctlspec -o %s"%result_file, file= cmd_stream  )
		print( "pick_state", file=cmd_stream )
		print( "print_current_state", file=cmd_stream )
		print( "set default_simulation_steps 1000", file=cmd_stream )
		print( "simulate -r ", file=cmd_stream )
		print( "show_traces -t", file = cmd_stream )
		print( "show_traces -v", file = cmd_stream )
		print( "quit", file=cmd_stream )

	nusmv_cmd = 'NuSMV -v 1 -source %s'%cmd_file
	log_name =  task.domain_name + '_' + task.task_name + '.log'
	log = benchmark.Log( log_name )
	rv, time = benchmark.run( nusmv_cmd, 1800, 4096, log )	

	result_buffer = read_result_output( result_file ) 

	# Check policy holds
	if "is true" in result_buffer[0] :
		print >> sys.stdout, "Policy encoded in", fond_task.smv_filename, "is strong cyclic"
		sys.exit(0)
	
	# Collecting counter-example
	collect_counterexample( result_buffer )
	

	

def debug() :
	# edit this to change arguments
	main( 'benchmarks/fond/tireworld/domain.pddl', 'benchmarks/fond/tireworld/p03.pddl' )
	#main( 'benchmarks/fond/gadgets/gadget-domain-01.pddl', 'benchmarks/fond/gadgets/gadget-problem-01.pddl' )
	#main( 'benchmarks/fond/blocksworld/domain.pddl', 'mini-blocks.pddl' )

if __name__ == "__main__":
	main( sys.argv[1], sys.argv[2])


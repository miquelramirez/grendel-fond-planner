;;;  Authors: Andrey Kolobov ;;;
;;;  Encoded into non-deterministic PDDL by Miquel Ramirez, 2013 ;;;

(define (domain gremlin-strong)
	(:requirements :typing :strips :non-deterministic)
	(:predicates
		(alive) ;; the gremlin is alive
		(plane-broken) ;; the plane is broken
		(has-screwdriver) ;; the gremlin has the screwdriver
		(has-wrench) ;; the gremlin holds the wrench
		(has-hammer) ;; the gremlin holds the hammer
	)

	(:action pick-up-screwdriver
		:precondition (and (alive) (not (has-screwdriver)))
		:effect	(has-screwdriver)
	)
	(:action pick-up-hammer
		:precondition (and (alive) (not (has-hammer)))
		:effect (has-hammer)
	)
	(:action pick-up-wrench
		:precondition (and (alive) (not (has-wrench)))
		:effect (has-wrench)
	)
	(:action bang
		:precondition (and (alive) (has-hammer))		
		:effect (oneof (and (alive) (plane-broken)) (and (not (alive))) )
		
	)
	(:action tweak
		:precondition (and (alive) (has-screwdriver) (has-wrench))
		:effect (plane-broken)
	)
)

;; Authors: Michael Littman and David Weissman
;; Modified by: Blai Bonet

;; Comment: Good plans are those that avoid putting blocks on table since the probability of detonation is higher

(define (domain exploding-blocksworld)
  (:requirements :adl :typing :non-deterministic)
  (:types block)
  (:predicates 
	(on ?b1 ?b2 - block) 
	(on-table ?b - block) 
	(clear ?b - block) 
	(holding ?b - block) 
	(emptyhand) 
	(detonated ?b - block) 
	(destroyed ?b - block) 
	(destroyed-table))

  (:action pick-up
   :parameters (?b1 ?b2 - block)
   :precondition (and (not (= ?b1 ?b2)) (emptyhand) (clear ?b1) (on ?b1 ?b2) (not (destroyed ?b1)))
   :effect (and (holding ?b1) (clear ?b2) (not (emptyhand)) (not (on ?b1 ?b2)))
  )

  (:action pick-up-from-table
   :parameters (?b - block)
   :precondition (and (emptyhand) (clear ?b) (on-table ?b) (not (destroyed ?b)))
   :effect (and (holding ?b) (not (emptyhand)) (not (on-table ?b)))
  )
  (:action put-down-nodet
   :parameters (?b - block)
   :precondition (and (not (detonated ?b)) (holding ?b) (not (destroyed-table)))
   :effect (and (emptyhand) (on-table ?b) (not (holding ?b))
                (oneof (and) (and (destroyed-table) (detonated ?b))))
  )
  (:action put-down-det
   :parameters (?b - block)
   :precondition (and (detonated ?b) (holding ?b) (not (destroyed-table)))
   :effect (and (emptyhand) (on-table ?b) (not (holding ?b)))
  )
  (:action put-on-block-nodet
   :parameters (?b1 ?b2 - block)
   :precondition (and (not (= ?b1 ?b2)) (not (detonated ?b1)) (holding ?b1) (clear ?b2) (not (destroyed ?b2)))
   :effect (and (emptyhand) (on ?b1 ?b2) (not (holding ?b1)) (not (clear ?b2)) (clear ?b1)
                (oneof (and) (and (destroyed ?b2) (detonated ?b1))))
  )
  (:action put-on-block-det
   :parameters (?b1 ?b2 - block)
   :precondition (and (detonated ?b1) (holding ?b1) (clear ?b2) (not (destroyed ?b2)))
   :effect (and (emptyhand) (on ?b1 ?b2) (not (holding ?b1)) (not (clear ?b2)))
  )
)


(define (problem very-tiny-tireworld-unsat)

	(:domain tire-adl)
	(:objects n0 n1 n2 - location)
	(:init
		(vehicle-at n0)
		(road n0 n1) (road n1 n0)
		(road n1 n2) (road n2 n1)
	)
	(:goal (vehicle-at n2))
)

(define (domain gadget-3)
	(:requirements :strips :non-deterministic)
	(:predicates (goal) (x_1) (x_2) (x_3) (x_4) (x_5) (x_6) (x_7))
	(:action	o1
		:precondition (and (x_1))
		:effect (oneof	(and (x_2) (not (x_1))) 
				(and (x_4) (not (x_1)))
				(and (x_7) (not (x_1))))
	)

	(:action	o2
		:precondition	(and (x_2))
		:effect	(oneof	(and (goal) (not (x_2)))
				(and (x_3) (not (x_2))))
	)

	(:action	o3
		:precondition	(and (x_3))
		:effect	(and (x_2) (not (x_3)))
	)
	
	(:action	o4
		:precondition	(and (x_4))
		:effect	(oneof	(and (x_5) (not (x_4)))
				(and (x_6) (not	(x_4))))
	)

	(:action	o5
		:precondition	(and (x_5))
		:effect	(and (x_4) (not (x_5)))
	)
	
	(:action	o6
		:precondition	(and (x_6))
		:effect	(and (x_1) (not (x_6)))
	)
)

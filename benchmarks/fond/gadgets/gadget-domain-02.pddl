(define (domain gadget-2)
	(:requirements :strips :non-deterministic)
	(:predicates (goal) (x_1) (x_2) (x_3))
	(:action	get-to-goal
		:precondition (and (x_1))
		:effect (oneof	(and (goal) (not (x_1))) 
				(and (x_2) (not (x_1)))
				(and (x_3) (not (x_1))))
	)

	(:action	x2-to-x1
		:precondition	(and (x_2))
		:effect (and (x_1) (not (x_2)))
	)

	(:action	x3-to-x1
		:precondition	(and (x_3))
		:effect		(and (x_1) (not (x_3)))
	)
)

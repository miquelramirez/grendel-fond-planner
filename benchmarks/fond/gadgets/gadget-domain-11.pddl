(define (domain gadget-11)
	(:requirements :strips :non-deterministic)
	(:predicates (goal) (x_1) (x_2))

	(:action	o1
		:precondition	(and (x_1))
		:effect 	(oneof	
					(and (x_2) (not (x_1)))
					(and ))
	)

	(:action	o2
		:precondition	(and (x_2))
		:effect		(oneof 
					(and (goal) (not (x_2)))
					(and (x_1) (not (x_2))))
	)

)

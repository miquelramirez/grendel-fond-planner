(define (domain gadget-12)
	(:requirements :strips :non-deterministic)
	(:predicates (goal) (x_0) (x_1) (x_2) (x_3))

	(:action	o0
		:precondition	(and (x_0))
		:effect		(oneof
					(and (goal) (not (x_0)))
					(and (x_1) (not (x_0))))	
	)

	(:action	o1
		:precondition	(and (x_1))
		:effect 	(oneof	
					(and (x_2) (not (x_1)))
					(and (x_3) (not (x_1))))
	)

	(:action	o2
		:precondition	(and (x_2))
		:effect		(oneof 
					(and (x_1) (not (x_2)))
					(and (x_3) (not (x_2))))
	)
	(:action	o3
		:precondition	(and (x_3))
		:effect		(oneof
					(and )
					(and (x_2) (not (x_3))))
	)
)

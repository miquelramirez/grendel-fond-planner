(define (domain gadget-1)
	(:requirements :strips :non-deterministic)
	(:predicates (goal) (alpha) (beta))
	(:action	get-to-goal
		:precondition (and (alpha))
		:effect (oneof	(and (goal) (not (alpha))) (and (beta) (not (alpha))))
	)

	(:action	back-to-alpha
		:precondition	(and (beta))
		:effect (and (alpha) (not (beta)))
	)
)

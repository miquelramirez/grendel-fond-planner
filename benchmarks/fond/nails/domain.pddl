;; Authors: Miquel Ramirez and Sebastian Sardina
;; March 2014

(define (domain nails)

	(:requirements :typing :adl :non-deterministic :derived-predicates :equality)
	(:types depth)
	(:predicates
		(at ?d - depth)
		(first ?d - depth)
		(final ?d - depth)
		(next ?d1 - depth ?d2 - depth)
		(bent-nail)
		(has-hammer)
		(has-pliers)
		(empythand)
		(invalid)
	)

	(:action	prepare-nail
		:parameters (?d - depth)
		:precondition (and (not (invalid)) (not (bent-nail)) (has-hammer) (first ?d))
		:effect (and (at ?d))
	)

	(:action	pick-up-hammer
		:precondition (and (not (invalid)) (emptyhand))
		:effect (and (has-hammer) (not (emptyhand)))
	)

	(:action	drop-hammer
		:precondition (and (not (invalid)) (has-hammer))
		:effect (and (emptyhand) (not (has-hammer)))
	)

	(:action	pick-up-pliers
		:precondition (and (not (invalid)) (emptyhand))
		:effect ( and (has-pliers) (not (emptyhand)))
	)

	(:action	drop-pliers
		:precondition (and (not (invalid)) (has-pliers))
		:effect	      (and (emptyhand) (not (has-pliers)))
	)

	(:action	hit-nail
		:parameters (?d0 - depth ?d1 - depth)
		:precondition (and (not (invalid)) (at ?d0) (next ?d0 ?d1) (has-hammer) (not (bent-nail)))
		:effect (oneof
				(and (at ?d1) (not (at ?d0)) (not (bent-nail)))
				(and (at ?d1) (not (at ?d0)) (bent-nail))
				(and )
			)
	)

	(:action	remove-nail
		:parameters (?actual - depth)
		:precondition (and (not (invalid)) (bent-nail) (has-pliers) (at ?actual))
		:effect (oneof
				(and (not (at ?actual)) (not (bent-nail)))
				(and)
			)
	)

	(:derived
		(invalid)
		(exists (?x ?y - depth) (and (not (= ?x ?y)) (at ?x) (at ?y)))
	)
)

(define (problem nails-p01)

	(:domain nails)
	(:objects d0 d1 - depth)
	(:init
		(first d0)
		(final d1)
		(next d0 d1)
	)
	(:goal
		(and
			(at d1)
			(not (bent-nail))
		)
	)
)

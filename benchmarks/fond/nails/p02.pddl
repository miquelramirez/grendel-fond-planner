(define (problem nails-p02)

	(:domain nails)
	(:objects d0 d1 d2 - depth)
	(:init
		(first d0)
		(final d2)
		(next d0 d1)
		(next d1 d2)
		(emptyhand)
	)
	(:goal
		(and
			(at d2)
			(not (bent-nail))
		)
	)
)

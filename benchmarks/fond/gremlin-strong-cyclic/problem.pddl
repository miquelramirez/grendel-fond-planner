(define (problem gremlin-task)
  	(:domain gremlin-strong)
	(:init
		(alive)
	)
	(:goal
		(and (alive) (plane-broken))
	)
)

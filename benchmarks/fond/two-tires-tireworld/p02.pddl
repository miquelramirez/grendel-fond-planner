(define (problem tire_19_0_28845)
  (:domain tire)
  (:objects n0 
            na1 na2 na3
            nb1 
            ng - location)
  (:init (vehicle-at n0)

         (road n0 na1) (road na1 n0)
         (road na1 na2) (road na2 na1)
         (road na2 na3) (road na3 na2)
         (road na3 ng) (road ng na3)

         (road n0 nb1) (road nb1 n0)
         (road nb1 ng) (road ng nb1)

         (spare0-in n0)
         (spare0-in ng)

         (spare1-in na1)
         (spare2-in na2)
         (spare0-in na3)

         (spare0-in nb1)

         (not-flattire)
         (not-hasspare)
  )
  (:goal (vehicle-at ng))
)

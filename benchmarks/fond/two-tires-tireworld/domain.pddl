;;;  Authors: Michael Littman and David Weissman  ;;;
;;;  Modified: Blai Bonet for IPC 2006 ;;;

;;;  Modified: Christian Muise to make it a FOND domain

;;;

(define (domain tire)
  (:requirements :typing :strips :non-deterministic)
  (:types location)
  (:predicates (vehicle-at ?loc - location)
               (spare0-in ?loc - location)
               (spare1-in ?loc - location)
               (spare2-in ?loc - location)
               (road ?from - location ?to - location)
               (not-flattire)
               (flattire)
               (hasspare)
               (not-hasspare))
               
  
  (:action move-car
    :parameters (?from - location ?to - location)
    :precondition (and (vehicle-at ?from) (road ?from ?to) (not-flattire))
    :effect (and (vehicle-at ?to) (not (vehicle-at ?from)) (oneof (and) (and (not (not-flattire)) (flattire))))
  )
  
  (:action loadtire1
    :parameters (?loc - location)
    :precondition (and (vehicle-at ?loc) (spare1-in ?loc) (not-hasspare))
    :effect (and (hasspare) (not (not-hasspare)) (not (spare1-in ?loc)) (spare0-in ?loc))
  )

  (:action loadtire2
    :parameters (?loc - location)
    :precondition (and (vehicle-at ?loc) (spare2-in ?loc) (not-hasspare))
    :effect (and (hasspare) (not (not-hasspare)) (not (spare2-in ?loc)) (spare1-in ?loc))
  )

  (:action fix
    :precondition (and (hasspare) (flattire))
    :effect (and (not-hasspare) (not-flattire) (not (flattire)) (not (hasspare)))
  )
  
)
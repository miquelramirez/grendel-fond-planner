#!/usr/bin/python
import os
import sys
import random

class Instance :

	def __init__( self, size, max_food ) :
		# make positions
		self.grid = {}
		self.adj_list = {}
		for i in range( 0, size ) :
			for j in range(0, size) :
				self.grid[ (i,j) ] = 'p_%d_%d'%(i,j)

		for coordinates, name in self.grid.iteritems() :
			i, j = coordinates	
			self.adj_list[name] = []
			try :
				up = (i - 1, j)
				self.adj_list[name].append( self.grid[ up ] )
			except KeyError :
				pass
			try :
				down = ( i + 1, j )
				self.adj_list[name].append( self.grid[ down ] )
			except KeyError :
				pass
			try :
				left = ( i, j - 1 )
				self.adj_list[name].append( self.grid[ left ] )
			except KeyError :
				pass
			try :
				right = ( i, j + 1 )
				self.adj_list[name].append( self.grid[ right ] )
			except KeyError :
				pass

		locations = self.adj_list.keys()	
		self.food_locations = random.sample( locations, max_food )
		self.prey_pos = random.choice( locations )
		self.hunter_pos = random.choice( locations )
		while self.prey_pos == self.hunter_pos :
			self.hunter_pos = random.choice( locations )
		

def generate( index, size, seed, max_food ) :

	pddl_filename = os.path.join( 'instances', 'p-%d-%d-%d-%d.pddl'%( seed, size, size, index ) )
	i = Instance(size, max_food )

	output_lines = []
	# Read template and substitute as appropiate
	with open( 'instance-template.pddl' ) as instream :
		for line in instream :
			line = line.strip()
			if "@problem_name@" in line :
				out_line = line.replace( "@problem_name@", "hunter-prey-%d-%d-%d-%d"%(seed, size, size, index) )
				output_lines.append( out_line )
				continue
			if "@objects_def@" in line :
				obj_name_list = ' '.join( i.adj_list.keys() )
				out_line = '\t%s - location'%obj_name_list
				output_lines.append( out_line )
				continue
			if "@connected_predicates@" in line :
				for loc, adj_locs in i.adj_list.iteritems() :
					preds = []
					if len(adj_locs) == 1 :
						preds.append( '(one-connected %s)'%loc )
					elif len( adj_locs ) == 2 :
						preds.append( '(two-connected %s)'%loc )
					elif len( adj_locs ) == 3 :
						preds.append( '(three-connected %s)'%loc )
					elif len( adj_locs ) == 4 :
						preds.append( '(four-connected %s)'%loc )
					else :
						assert False
					for loc2 in adj_locs :
						preds.append( '(connected %s %s)'%(loc, loc2) )
					pred_list = ' '.join( preds )
					out_line = '\t%s'%pred_list
					output_lines.append( out_line )
				continue
			if "@has_food_predicates@" in line :
				preds = []
				for loc in i.food_locations :
					preds.append( '(has-food %s)'%loc )
				pred_list = ' '.join( preds )
				out_line = '\t%s'%pred_list
				output_lines.append( out_line )
				continue
			if "@hunter_position@" in line :
				pred = '(hunter-pos %s)'%i.hunter_pos
				out_line = '\t%s'%pred
				output_lines.append( out_line )
				continue
			if "@prey_position@" in line :
				pred = '(prey-pos %s)'%i.prey_pos
				out_line = '\t%s'%pred
				output_lines.append( out_line )
				continue
			if "@player_turn@" in line :
				pred = '(hunter-turn)'
				out_line = '\t%s'%pred
				output_lines.append( out_line )
				continue


			output_lines.append( line )

	# Write generated instance
	output_filename =  pddl_filename 
	with open( output_filename, 'w' ) as outstream :
		for line in output_lines :
			print >> outstream, line
def usage() :

	print >> sys.stderr, "Missing arguments"
	print >> sys.stderr, "./generator <initial size> <end size> <step size> <seed> <max food> <num instances>"
	print >> sys.stderr, "\tinitial size : smallest size of instances to be generated"
	print >> sys.stderr, "\tend size : biggest size of instances to be generated"
	print >> sys.stderr, "\tstep size : increment in size"
	print >> sys.stderr, "\tseed : seed of RNG"
	print >> sys.stderr, "\tmax food: maximum number of locations with food (minimum is 1)"
	print >> sys.stderr, "\tnum instances: number of instances to be generated for a given size"
	

def main() :
	

	if len(sys.argv ) < 7 :
		usage()
		sys.exit(1)

	try :
		start_size = int( sys.argv[1] )
		end_size = int( sys.argv[2] )
		step_size = int( sys.argv[3] )
		seed = int( sys.argv[4] )
		max_food = int( sys.argv[5] )
		num_instances = int( sys.argv[6] )
	except ValueError :
		print >> sys.stderr, "All arguments need to be integers!"
		usage()
		sys.exit(1)

	random.seed(seed)

	for sz in range( start_size, end_size+1, step_size ) :
		for i in range( 1, num_instances + 1 ) :
			generate(i, sz, seed, max_food )
	

if __name__ == '__main__' :
	main()

(define (problem @problem_name@)
(:domain hunter_prey)

(:objects
	@objects_def@
)

(:init
	@connected_predicates@
	@has_food_predicates@
	@hunter_position@
	@prey_position@
	@player_turn@
)

(:goal
	(prey-is-dead)
)

)

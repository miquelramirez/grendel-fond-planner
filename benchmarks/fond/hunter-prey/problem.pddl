(define
	(problem simple)
	(:domain hunter_prey)
	(:init
		(hunter-pos-n1)
		(prey-pos-n2)
		(prey-alive)
	)
	(:goal
		(not (prey-alive))
	)
)

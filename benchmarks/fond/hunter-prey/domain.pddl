(define (domain hunter_prey)
	(:requirements :typing :strips :non-deterministic)
	(:types location )
	(:predicates
		(prey-turn)
		(hunter-turn)
		(hunter-pos ?p - location)
		(prey-pos ?p - location)
		(has-food ?p - location)
		(connected ?p1 ?p2 - location)
		(prey-is-dead)
		(four-connected ?p - location)
		(three-connected ?p - location)
		(two-connected ?p - location)
		(one-connected ?p - location)
	)

	(:action move-hunter
		:parameters ( ?src - location ?dst - location )
		:precondition (and (hunter-turn) (hunter-pos ?src) (connected ?src ?dst))
		:effect (and (prey-turn) (hunter-pos ?dst) (not (hunter-turn)) (not (hunter-pos ?src)))
	)

	(:action hunt
		:parameters ( ?p - location )
		:precondition (and (hunter-turn) (hunter-pos ?p) (prey-pos ?p))
		:effect (oneof
				(and (prey-is-dead) (not (hunter-turn)) (prey-turn))
				(and (not (hunter-turn)) (prey-turn))
			)
	)

	(:action jump-and-hunt
		:parameters ( ?src - location ?dst - location )
		:precondition (and (hunter-turn) (hunter-pos ?src) (prey-pos ?dst) (connected ?src ?dst) )
		:effect (oneof 
				(and (hunter-pos ?dst) (prey-is-dead) (not (hunter-pos ?src)) (not (hunter-turn)) (prey-turn))
				(and (hunter-pos ?dst) (not (hunter-pos ?src)) (not (hunter-turn)) (prey-turn))
			)
	)

	(:action prey-flees-4
		:parameters ( ?p - location ?pp1 - location ?pp2 - location ?pp3 - location ?pp4 - location )
		:precondition (and (prey-turn) (prey-pos ?p) (hunter-pos ?p) (four-connected ?p)
				(connected ?p ?pp1) (connected ?p ?pp2) (connected ?p ?pp3) (connected ?p ?pp4))
		:effect (oneof
				(and (prey-pos ?pp1) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp2) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp3) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp4) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
			)
	)
	
 	(:action prey-flees-3
		:parameters ( ?p - location ?pp1 - location ?pp2 - location ?pp3 - location )
		:precondition (and (prey-turn) (prey-pos ?p) (hunter-pos ?p) (three-connected ?p) 
				(connected ?p ?pp1) (connected ?p ?pp2) (connected ?p ?pp3))
		:effect (oneof
				(and (prey-pos ?pp1) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp2) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp3) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
			)
	)	

	(:action prey-flees-2
		:parameters ( ?p - location ?pp1 - location ?pp2 - location ?pp3 - location ?pp4 - location )
		:precondition (and (prey-turn) (prey-pos ?p) (hunter-pos ?p) (two-connected ?p) 
				(connected ?p ?pp1) (connected ?p ?pp2) )
		:effect (oneof
				(and (prey-pos ?pp1) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp2) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
			)
	)	

	(:action prey-flees-1
		:parameters ( ?p - location ?pp1 - location )
		:precondition (and (prey-turn) (prey-pos ?p) (hunter-pos ?p) (one-connected ?p)
				(connected ?p ?pp1) )
		:effect	(and (prey-pos ?pp1) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
	)

	(:action prey-feeds
		:parameters (?p - location)
		:precondition (and (prey-turn) (has-food ?p) (not (hunter-pos ?p)))
		:effect (and (hunter-turn) (not (prey-turn)))
	)

	(:action prey-wanders-4
		:parameters ( ?p - location ?pp1 - location ?pp2 - location ?pp3 - location ?pp4 - location )
		:precondition (and (prey-turn) (prey-pos ?p) (not (hunter-pos ?p)) (not (has-food ?p)) (four-connected ?p)
				(connected ?p ?pp1) (connected ?p ?pp2) (connected ?p ?pp3) (connected ?p ?pp4))
		:effect (oneof
				(and (prey-pos ?pp1) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp2) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp3) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp4) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
			)
	)
	
 	(:action prey-wanders-3
		:parameters ( ?p - location ?pp1 - location ?pp2 - location ?pp3 - location )
		:precondition (and (prey-turn) (prey-pos ?p) (not (hunter-pos ?p)) (not (has-food ?p)) (three-connected ?p) 
				(connected ?p ?pp1) (connected ?p ?pp2) (connected ?p ?pp3))
		:effect (oneof
				(and (prey-pos ?pp1) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp2) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp3) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
			)
	)	

	(:action prey-wanders-2
		:parameters ( ?p - location ?pp1 - location ?pp2 - location ?pp3 - location ?pp4 - location )
		:precondition (and (prey-turn) (prey-pos ?p) (not (hunter-pos ?p)) (not (has-food ?p)) (two-connected ?p) 
				(connected ?p ?pp1) (connected ?p ?pp2) )
		:effect (oneof
				(and (prey-pos ?pp1) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
				(and (prey-pos ?pp2) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
			)
	)	

	(:action prey-wanders-1
		:parameters ( ?p - location ?pp1 - location )
		:precondition (and (prey-turn) (prey-pos ?p)(not (hunter-pos ?p)) (not (has-food ?p))  (one-connected ?p)
				(connected ?p ?pp1) )
		:effect	(and (prey-pos ?pp1) (not (prey-pos ?p)) (hunter-turn) (not (prey-turn)))
	)
	
)

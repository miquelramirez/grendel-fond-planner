(define (problem hunter-prey-8281-3-3-2)
(:domain hunter_prey)

(:objects
	p_0_1 p_0_0 p_0_2 p_2_2 p_2_1 p_2_0 p_1_0 p_1_1 p_1_2 - location
)

(:init
	(three-connected p_0_1) (connected p_0_1 p_1_1) (connected p_0_1 p_0_0) (connected p_0_1 p_0_2)
	(two-connected p_0_0) (connected p_0_0 p_1_0) (connected p_0_0 p_0_1)
	(two-connected p_0_2) (connected p_0_2 p_1_2) (connected p_0_2 p_0_1)
	(two-connected p_2_2) (connected p_2_2 p_1_2) (connected p_2_2 p_2_1)
	(three-connected p_2_1) (connected p_2_1 p_1_1) (connected p_2_1 p_2_0) (connected p_2_1 p_2_2)
	(two-connected p_2_0) (connected p_2_0 p_1_0) (connected p_2_0 p_2_1)
	(three-connected p_1_0) (connected p_1_0 p_0_0) (connected p_1_0 p_2_0) (connected p_1_0 p_1_1)
	(four-connected p_1_1) (connected p_1_1 p_0_1) (connected p_1_1 p_2_1) (connected p_1_1 p_1_0) (connected p_1_1 p_1_2)
	(three-connected p_1_2) (connected p_1_2 p_0_2) (connected p_1_2 p_2_2) (connected p_1_2 p_1_1)
	(has-food p_1_1) (has-food p_0_0) (has-food p_2_1) (has-food p_0_2)
	(hunter-pos p_2_2)
	(prey-pos p_1_2)
	(hunter-turn)
)

(:goal
(prey-is-dead)
)

)

(define (problem hunter-prey-8281-4-4-1)
(:domain hunter_prey)

(:objects
	p_0_1 p_0_0 p_0_3 p_0_2 p_2_3 p_2_2 p_2_1 p_2_0 p_1_0 p_1_1 p_1_2 p_1_3 p_3_2 p_3_3 p_3_0 p_3_1 - location
)

(:init
	(three-connected p_0_1) (connected p_0_1 p_1_1) (connected p_0_1 p_0_0) (connected p_0_1 p_0_2)
	(two-connected p_0_0) (connected p_0_0 p_1_0) (connected p_0_0 p_0_1)
	(two-connected p_0_3) (connected p_0_3 p_1_3) (connected p_0_3 p_0_2)
	(three-connected p_0_2) (connected p_0_2 p_1_2) (connected p_0_2 p_0_1) (connected p_0_2 p_0_3)
	(three-connected p_2_3) (connected p_2_3 p_1_3) (connected p_2_3 p_3_3) (connected p_2_3 p_2_2)
	(four-connected p_2_2) (connected p_2_2 p_1_2) (connected p_2_2 p_3_2) (connected p_2_2 p_2_1) (connected p_2_2 p_2_3)
	(four-connected p_2_1) (connected p_2_1 p_1_1) (connected p_2_1 p_3_1) (connected p_2_1 p_2_0) (connected p_2_1 p_2_2)
	(three-connected p_2_0) (connected p_2_0 p_1_0) (connected p_2_0 p_3_0) (connected p_2_0 p_2_1)
	(three-connected p_1_0) (connected p_1_0 p_0_0) (connected p_1_0 p_2_0) (connected p_1_0 p_1_1)
	(four-connected p_1_1) (connected p_1_1 p_0_1) (connected p_1_1 p_2_1) (connected p_1_1 p_1_0) (connected p_1_1 p_1_2)
	(four-connected p_1_2) (connected p_1_2 p_0_2) (connected p_1_2 p_2_2) (connected p_1_2 p_1_1) (connected p_1_2 p_1_3)
	(three-connected p_1_3) (connected p_1_3 p_0_3) (connected p_1_3 p_2_3) (connected p_1_3 p_1_2)
	(three-connected p_3_2) (connected p_3_2 p_2_2) (connected p_3_2 p_3_1) (connected p_3_2 p_3_3)
	(two-connected p_3_3) (connected p_3_3 p_2_3) (connected p_3_3 p_3_2)
	(two-connected p_3_0) (connected p_3_0 p_2_0) (connected p_3_0 p_3_1)
	(three-connected p_3_1) (connected p_3_1 p_2_1) (connected p_3_1 p_3_0) (connected p_3_1 p_3_2)
	(has-food p_2_1) (has-food p_0_3) (has-food p_0_2) (has-food p_3_1)
	(hunter-pos p_3_1)
	(prey-pos p_2_2)
	(hunter-turn)
)

(:goal
(prey-is-dead)
)

)

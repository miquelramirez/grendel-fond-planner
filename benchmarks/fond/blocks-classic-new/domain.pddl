(define (domain blocks-domain)

  (:requirements :non-deterministic :negative-preconditions :equality :typing)
  
  (:types block)
  
  (:predicates (holding-one) (holding-two) (holding ?b - block) (emptyhand) (on-table ?b - block) (on ?b1 ?b2 - block) (clear ?b - block))
  
  (:action pick-up
    :parameters (?b1 ?b2 - block)
    :precondition (and (not (= ?b1 ?b2)) (emptyhand) (clear ?b1) (on ?b1 ?b2) (not (clear ?b2)))
    :effect (and 
             (oneof (and (clear ?b2) (not (on ?b1 ?b2)) (holding-one) (holding ?b1) (not (emptyhand)) (not (clear ?b1)))
                    (and (clear ?b2) (not (on ?b1 ?b2)) (on-table ?b1))))
  )
  
  (:action pick-up-from-table
    :parameters (?b - block)
    :precondition (and (emptyhand) (clear ?b) (on-table ?b))
    :effect (oneof (and) (and (holding-one) (holding ?b) (not (emptyhand)) (not (on-table ?b)) (not (clear ?b))))
  )
  
  (:action put-on-block
    :parameters (?b1 ?b2 - block)
    :precondition (and (not (= ?b1 ?b2)) (holding-one) (holding ?b1) (not (clear ?b1)) (clear ?b2))
    :effect (and 
             (oneof (and (emptyhand) (clear ?b1) (not (holding-one)) (not (holding ?b1)) (on ?b1 ?b2) (not (clear ?b2)))
                    (and (emptyhand) (clear ?b1) (not (holding-one)) (not (holding ?b1)) (on-table ?b1))))
  )
  
  (:action put-down
    :parameters (?b - block)
    :precondition (and (holding-one) (holding ?b) (not (clear ?b)))
    :effect (and (on-table ?b) (emptyhand) (clear ?b) (not (holding-one)) (not (holding ?b)))
  )
)

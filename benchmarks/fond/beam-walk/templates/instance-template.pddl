(define (problem @problem_name@)
(:domain beam-walk)
(:objects
	@objects_def@		
)
(:init
	@next_fwd@
	@next_bwd@
	(ladder-at p0)
	(position p0)	
)

(:goal
	(and (up) @goal_position@ )
)

)

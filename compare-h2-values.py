import sys
import os

def main() :
	
	lhs = sys.argv[1]
	rhs = sys.argv[2]

	lhs_values = {}
	with open( lhs ) as instream :
		for line in instream :
			fields = line.strip().split(',')
			lhs_values[ ( fields[0], fields[1] ) ] = fields[2]
	with open ( rhs ) as instream :
		for line in instream :
			fields = line.strip().split(',')
			key = ( fields[0], fields[1] )
			rhs_val = fields[2]
			try :
				lhs_val = lhs_values[key]
				if lhs_val != rhs_val :
					print >> sys.stdout, "Discrepancy on value of", key, "lhs:", lhs_val, "rhs:", rhs_val
			except KeyError :
				print >> sys.stderr, "Could not match rhs", key, "on lhs"
				print >> sys.stderr, "Similar keys on lhs:"
				for key_lhs in lhs_values.keys() :
					if key[0] in key_lhs : print >> sys.stderr, key_lhs
					if key[1] in key_lhs : print >> sys.stderr, key_lhs
				sys.exit(1)

if __name__ == "__main__" :
	main()

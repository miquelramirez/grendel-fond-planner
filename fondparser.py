from __future__ import print_function

from collections import defaultdict

import build_model
import pddl_to_prolog
import pddl
import timers
import fact_groups

USE_PARTIAL_ENCODING = True

def get_fluent_facts(task, model):
	fluent_predicates = set()
	for action in task.actions:
		for effect in action.effects:
			fluent_predicates.add(effect.literal.predicate)
	for axiom in task.axioms:
		fluent_predicates.add(axiom.name)
	return set([fact for fact in model
			if fact.predicate in fluent_predicates])

def get_objects_by_type(typed_objects, types):
	result = defaultdict(list)
	supertypes = {}
	for type in types:
		supertypes[type.name] = type.supertype_names
	for obj in typed_objects:
		result[obj.type].append(obj.name)
		for type in supertypes[obj.type]:
			result[type].append(obj.name)
	return result

def instantiate(task, model):
	relaxed_reachable = False
	fluent_facts = get_fluent_facts(task, model)
	init_facts = set(task.init)
	
	type_to_objects = get_objects_by_type(task.objects, task.types)
	
	instantiated_actions = []
	instantiated_axioms = []
	reachable_action_parameters = defaultdict(list)
	for atom in model:
		if isinstance(atom.predicate, pddl.Action):
			action = atom.predicate
			parameters = action.parameters
			inst_parameters = atom.args[:len(parameters)]
			# Note: It's important that we use the action object
			# itself as the key in reachable_action_parameters (rather
			# than action.name) since we can have multiple different
			# actions with the same name after normalization, and we
			# want to distinguish their instantiations.
			reachable_action_parameters[action].append(inst_parameters)
			variable_mapping = dict([(par.name, arg)
						for par, arg in zip(parameters, atom.args)])
			inst_action = action.instantiate(variable_mapping, init_facts,
							fluent_facts, type_to_objects)
			if inst_action:
				instantiated_actions.append(inst_action)
		elif isinstance(atom.predicate, pddl.Axiom):
			axiom = atom.predicate
			variable_mapping = dict([(par.name, arg)
						for par, arg in zip(axiom.parameters, atom.args)])
			inst_axiom = axiom.instantiate(variable_mapping, init_facts, fluent_facts)
			if inst_axiom:
				instantiated_axioms.append(inst_axiom)
		elif atom.predicate == "@goal-reachable":
			relaxed_reachable = True
	
	return (relaxed_reachable, fluent_facts, instantiated_actions,
		sorted(instantiated_axioms), reachable_action_parameters)

def explore(task):
	prog = pddl_to_prolog.translate(task)
	model = build_model.compute_model(prog)
	with timers.timing("Completing instantiation"):
		return instantiate(task, model)


class PropositionalNonDetAction :
	
	def __init__( self, name, cost ) :
		self.name = name
		self.cost = cost
		self.precondition = []
		self.effects = []
	
	def set_precondition( self, prec, atom_table ) :
		for p in prec :
			self.precondition.append( ( atom_table[p.text()], p.negated ) )
	
	def add_effect( self, adds, dels, atom_table ) :
		effs = []
		for _, lit in adds :
			effs.append( ( atom_table[lit.text()], False )  )
		for _, lit in dels :
			effs.append( (atom_table[lit.text()], True) )
		self.effects.append( effs )

def encode( lits, atom_table ) :
	encoded = []
	if isinstance( lits, pddl.Atom ) or isinstance( lits, pddl.NegatedAtom ) :
		# singleton
		index = atom_table[lits.text()]
		encoded.append( (index, lits.negated) )
		return encoded

	if isinstance( lits, pddl.Conjunction ) :
		lits = [ p for p in lits.parts ]	

	for p in lits :
		try :
			index = atom_table[p.text()]
		except KeyError :
			continue
		encoded.append( (index, p.negated) )
	return encoded

def parse( task, fond_task) :

	parsing_timer = timers.Timer()
	relaxed_reachable, atoms, actions, axioms, reachable_action_params = explore(task)
	print("goal relaxed reachable: %s" % relaxed_reachable)
	if not relaxed_reachable :
		print("No weak plan exists")
		sys.exit(2)
	
	print("%d atoms" % len(atoms))

	with timers.timing("Computing fact groups", block=True):
		groups, mutex_groups, translation_key = fact_groups.compute_groups(
			task, atoms, reachable_action_params,
			partial_encoding=USE_PARTIAL_ENCODING)
	index = 0
	atom_table = {}


	for atom in atoms :
		atom.index = index
		atom_table[ atom.text() ] = index
		fond_task.add_atom( atom.text() )
		index += 1

	print("Invariants %d"%len(mutex_groups))
	for group in mutex_groups :
		if len(group) >= 2 :
			print("{%s}" % ", ".join(map(str, group)))
			fond_task.add_invariant( encode( group, atom_table ) )
			print( encode( group, atom_table ) )

	print("Deterministic %d actions" % len(actions))
	nd_actions = {}
	for action in actions :
		if "_DETDUP_" not in action.name :
			nd_action = PropositionalNonDetAction( action.name, action.cost )
			enc_precs = []
			nd_action.set_precondition( action.precondition, atom_table )
			nd_action.add_effect( action.add_effects, action.del_effects, atom_table )
			nd_actions[ nd_action.name ] = nd_action
			continue

		name_toks = action.name.split('_DETDUP_' )
		name = name_toks[0]
		params = []
		if len( name_toks ) > 1 :
			param_toks = name_toks[1].split(' ')
			params = param_toks[1:]
		name = ' '.join( [ name ] + params )
		try :
			nd_action = nd_actions[name]
			nd_action.add_effect( action.add_effects, action.del_effects, atom_table )

		except KeyError :
			nd_action = PropositionalNonDetAction( name, action.cost )
			nd_action.set_precondition( action.precondition, atom_table )
			nd_action.add_effect( action.add_effects, action.del_effects, atom_table )
			nd_actions[ nd_action.name ] = nd_action
			nd_actions[ name ] = nd_action

	print("Non-deterministic %d actions" % len(nd_actions.values()))
	print()
	index = 0
	for action in nd_actions.values() :
		fond_task.add_action( action.name )
		fond_task.add_precondition( index, action.precondition )
		text_prec = []
		for p, v in action.precondition :
			text_prec.append( "%s=%s"%(fond_task.get_atom_name( p ),  not v) )
		for eff in action.effects :
			fond_task.add_effect( index, eff )
			text_eff = []
			for p, v in eff :
				text_eff.append( "%s=%s"%(fond_task.get_atom_name( p ), not v) )
		index += 1

	fond_task.set_init( encode( task.init, atom_table ) )
	fond_task.set_goal( encode( task.goal, atom_table ) )
	fond_task.parsing_time = parsing_timer.report()

	return fond_task

#!/usr/bin/python
import 	sys, os, glob
import 	benchmark
from run_tools import parse_report, extract_inputs, extract_inputs_structured, extract_faults_inputs, extract_gadgets_inputs

def collect_data( log ) :

	strong_cyclic = '?'
	state_action_pairs = '?'
	score = '?'
	
	
	with open(log.name ) as instream:
		for line in instream :
			line = line.strip()
			if 'Strongly Cyclic' in line :
				toks = line.split(':')
				strong_cyclic = toks[1].strip()
				continue
			if 'State-Action Pairs' in line :
				toks = line.split(':')
				state_action_pairs = toks[1].strip()
				continue
			if 'Policy Score' in line :
				toks = line.split(':')
				score = toks[1].strip()
				continue
	
		instream.close()
	
	return [strong_cyclic, state_action_pairs, score] 

def main() :

	if len(sys.argv) < 2 :
		print >> sys.stderr, "No folder was specified!"
		sys.exit(1)		
	
	if not os.path.exists( sys.argv[1] ) :
		print >> sys.stderr, sys.argv[1], "not a valid path!"
		sys.exit(1)

	if not os.path.isdir( sys.argv[1] ) :
		print >> sys.stderr, sys.argv[1], "not a directory!"
		sys.exit(1)

	inputs = []
	if 'faults' in sys.argv[1] :
		inputs = extract_faults_inputs( sys.argv[1] )
	elif 'gadgets' in sys.argv[1] :
		inputs = extract_gadgets_inputs( sys.argv[1] )
	else :
		inputs = extract_inputs_structured( sys.argv[1] )
		if inputs is None or len(inputs) == 0 :
			inputs = extract_inputs( sys.argv[1] )
			if len(inputs) == 0 :
				print >> sys.stderr, "No inputs found!"
				sys.exit(1)

	
	command = "./prp %(domain_pddl)s %(instance_pddl)s"
	logname = "%(domain_name)s-%(instance_name)s-prp.log"

	results = []
	for domain_name, instance_name, domain_pddl, instance_pddl in inputs :
		log = benchmark.Log( logname%locals() )
		rv, time = benchmark.run( command%locals(), 1800, 4096, log )
		results.append( [ domain_name, instance_name, str(rv), str(time) ] )
		results[-1] += collect_data(log)

	with open( '%s.prp.csv'%inputs[0][0], 'w' ) as outstream :
		for res in results :
			res = [ str(field) for field in res ]
			print >> outstream, ",".join(res)

if __name__ == '__main__' :
	main()

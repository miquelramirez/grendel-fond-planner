#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/graph_utility.hpp>
#include <vector>
#include <string>

int main( int argc, char** argv ) {

	using namespace boost;
	enum V { s1, s2, q1, q2, t1, t2, N };
	std::vector<std::string> name;
	name.push_back("s1");
	name.push_back("s2");
	name.push_back("q1");
	name.push_back("q2");
	name.push_back("t1");
	name.push_back("t2");
	
	typedef std::pair< int, int > E;
	E edges[] = {	E(s1, q1), E(s1, t2), E(s1, s2),
			E(s2, q2),
			E(q1, s1), E(q1, t1),
			E(q2, s2), E(q2, t2),
			E(t1, q2), E(t1, q1),
			E(t2, q2) };
			
	typedef adjacency_list<vecS,vecS,directedS> Graph;
	Graph G( N );

	add_edge( s1, q1, G );
	add_edge( s1, t2, G );
	add_edge( s1, s2, G );
	add_edge( q1, s1, G );
	add_edge( q1, t1, G );
	add_edge( q2, s2, G );
	add_edge( q2, t2, G );
	add_edge( t1, q2, G );
	add_edge( t1, q1, G );
	add_edge( t2, q2, G );


	typedef graph_traits< Graph >::vertex_descriptor Vertex;
	
	std::vector<int> component( num_vertices(G) ), discover_time( num_vertices(G) );
	
	int num = strong_components(G, make_iterator_property_map(component.begin(), get(vertex_index, G)));	

	std::cout << "Total number of components: " << num << std::endl;
	std::vector<int>::size_type i;
	for (i = 0; i != component.size(); ++i)
		std::cout << "Vertex " << name[i]
			<<" is in component " << component[i] << std::endl;

	return 0;
}

#ifndef __INST_BUILDER__
#define __INST_BUILDER__

#include <pyfondtask.hxx>

class Grendel_Planner : public FOND_Task {
public:
	Grendel_Planner( std::string, std::string );
	virtual ~Grendel_Planner();

	void		solve();
	virtual void	write_ground_pddl( std::string domain, std::string instance );


	bool		m_verify_policy;
	bool		m_dump_policy;
	bool		m_use_invariants;
	float		m_fwd_weight;
	float		m_bwd_weight;	

};

#endif // inst_builder.hxx

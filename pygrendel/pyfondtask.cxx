#include <pyfondtask.hxx>
#include <iostream>

using namespace boost::python;

	FOND_Task::FOND_Task( std::string domain, std::string instance ) {
		m_parsing_time = 0.0f;
		m_model = new grendel::FOND_Model( domain, instance );
	}

	FOND_Task::~FOND_Task() {
	}

	void
	FOND_Task::add_atom( std::string name ) {
		m_model->new_atom( name );
	}

	void
	FOND_Task::add_action( std::string name ) {
		m_model->new_action( name );
	}	

	void
	FOND_Task::add_precondition( int index, boost::python::list& lits ) {
		grendel::FOND_Model::Action& action = *(m_model->actions[index]);
		for ( int i = 0; i < len(lits); i++ ) {
			boost::python::tuple li = extract< tuple >( lits[i] );
			action.precondition.add( grendel::mkLit( extract<int>(li[0]), extract<bool>(li[1]) ) );
		}	
		instance()->max_precondition_size = std::max( instance()->max_precondition_size, action.precondition.size() );
	}

	void
	FOND_Task::add_effect( int index, boost::python::list& lits ) {
		grendel::FOND_Model::Action& action = *(m_model->actions[index]);
		grendel::FOND_Model::Effect_Vec effs;
		grendel::FOND_Model::Effect eff;
		for ( int i = 0; i < len(lits); i++ ) {
			boost::python::tuple li = extract< tuple >( lits[i] );
			eff.effect.add( grendel::mkLit( extract<int>(li[0]), extract<bool>(li[1]) ) );
		}	
		effs.push_back( eff );
		action.effects.push_back( effs );

	}

	void
	FOND_Task::add_invariant( boost::python::list& lits ) {
		grendel::DNF_Clause invariant;
		for ( int i = 0; i < len(lits); i++ ) {
			boost::python::tuple li = extract< tuple >( lits[i] );
			invariant.add( grendel::mkLit( extract<int>(li[0]), extract<bool>(li[1]) ) );
		}
		instance()->add_invariant( invariant );
	}

	void
        FOND_Task::add_axiom( boost::python::list& condition, boost::python::list& effect ) {
		grendel::DNF_Clause axiom_body;
		grendel::DNF_Clause axiom_head;
		for ( int i = 0; i < len(condition); i++ ) {
			boost::python::tuple li = extract< tuple >( condition[i] );
			axiom_body.add( grendel::mkLit( extract<int>(li[0]), extract<bool>(li[1]) ) );
		}
		
		for ( int i = 0; i < len(effect); i++ ) {
			boost::python::tuple li = extract< tuple >( effect[i] );
			axiom_head.add( grendel::mkLit( extract<int>(li[0]), extract<bool>(li[1]) ) );
		}	
		instance()->add_axiom( axiom_body, axiom_head );
	}


	void
	FOND_Task::set_init( boost::python::list& lits ) {
		for ( int i = 0; i < len(lits); i++ ) {
			boost::python::tuple li = extract< tuple >( lits[i] );
			instance()->init.add( grendel::mkLit( extract<int>(li[0]), extract<bool>(li[1]) ) );
		}
		// Complete negation
		for ( grendel::Atom p = 0; p < (int)instance()->n_atoms(); p++ ) {
			grendel::Lit l = grendel::mkLit( p, false );
			if ( !instance()->init.contains( l ) )
				instance()->init.add( ~l );
			l = grendel::mkLit( p, true );
			if ( !instance()->init.contains(~l) )
				instance()->init.add( l );
		}
	}

	void
	FOND_Task::set_goal( boost::python::list& lits ) {
		for ( int i = 0; i < len(lits); i++ ) {
			boost::python::tuple li = extract< tuple >( lits[i] );
			instance()->goal.add( grendel::mkLit( extract<int>(li[0]), extract<bool>(li[1]) ) );
		}
	}

	
	void
	FOND_Task::print_action( int index ) {
		instance()->actions[index]->print( std::cout, *instance() );
	}
	
	void
	FOND_Task::write_ground_pddl(std::string domain, std::string instance ) {
	}

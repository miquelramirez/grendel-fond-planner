#include <pygrendel.hxx>
#include <gp_atoms.hxx>
#include <cstring>
#include <fstream>
#include <grendel.hxx>
#include <prop_h2.hxx>

using namespace boost::python;

	Grendel_Planner::Grendel_Planner( std::string domain, std::string instance )
		: FOND_Task( domain, instance ) {
		m_verify_policy = false;
		m_dump_policy = false;
		m_use_invariants = true;
		m_fwd_weight = 1.0f;
		m_bwd_weight = 1.0f;
		m_model = new grendel::FOND_Model( domain, instance );

	}

	Grendel_Planner::~Grendel_Planner() {
	}

	template <typename Search_Engine>
	void
	print_results( const Search_Engine& engine, const grendel::Explicit_Policy& policy ) {
	
		std::ofstream report( "report.csv", std::ofstream::out | std::ofstream::app );
	
	
		std::cout << "Generated: " << engine.generated() << std::endl;
		report <<  engine.generated() << ",";
		std::cout << "Expansions: " << engine.expanded() << std::endl;
		report << engine.expanded() << ",";
		std::cout << "Strong Cyclic checks: " << engine.strong_cyclic_checks() << std::endl;
		report << engine.strong_cyclic_checks() << ",";
		std::cout << "Max Missing size: " << engine.max_missing_size() << std::endl;
		report << engine.max_missing_size() << ",";
		std::cout << "Minimum Cost to Go: " << engine.min_cost_to_go() << std::endl;
		report << engine.min_cost_to_go() << ",";
		std::cout << "Maximum Cost to Go: " << engine.max_cost_to_go() << std::endl;
		report << engine.max_cost_to_go() << ",";
		std::cout << "Dead-ends: " << engine.dead_ends() << std::endl;
		report << engine.dead_ends() << ",";
		unsigned de_sa_pairs = engine.dead_end_action_pairs();
		std::cout << "Dead-end state-action pairs: " << de_sa_pairs << std::endl;
		report << de_sa_pairs << ",";
		std::cout << "Solved States: " << engine.solved() << std::endl;
		report << engine.solved() << ",";	
		report.close();
	}

	void
	print_policy_evaluation( const grendel::Explicit_Policy& policy ) {
	
		std::ofstream report( "report.csv", std::ofstream::out | std::ofstream::app );
	
		if ( policy.num_states() > 0 ) {
			std::cout << "Policy States: " << policy.num_states() << std::endl;
			report << policy.num_states() << ",";
			std::cout << "Policy Visited States: " << policy.num_visited_states() << std::endl;
			report << policy.num_visited_states() << ",";
			std::cout << "Policy Executions: " << policy.num_executions() << std::endl;
			report << policy.num_executions() << ",";
			std::cout << "Policy Min Cost: " << policy.min_cost() << std::endl;
			report << policy.min_cost() << ","; 
			std::cout << "Policy Max Cost: " << policy.max_cost() << std::endl;
			report << policy.max_cost() << std::endl;
		}
	
		report.close();
	}

	void
	Grendel_Planner::solve() {
		float t0, tf;
		std::ofstream report( "report.csv" );
		std::cout << "PDDL parsing time: " << m_parsing_time << std::endl;
		report << m_parsing_time << ",";
		report << instance()->atoms.size() << ",";
		report << instance()->actions.size() << ",";
		report.close();
		
		t0 = grendel::utils::read_time_in_seconds();
		grendel::Rule_Based_H2_Heuristic max_h2( *instance() );
		//max_h2.set_verbose_computation(true);
		if ( m_use_invariants ) {
			std::cout << "Using third-party invariants to flesh out reachability heuristic" << std::endl;
		}
		max_h2.setup(m_use_invariants);
		tf = grendel::utils::read_time_in_seconds();
	
		report.open( "report.csv", std::ofstream::out | std::ofstream::app );
		std::cout << "h^2 Time: " << tf - t0 << std::endl; 
		report << tf - t0 << ",";
		//report << max_h2.num_rules() << ",";
		//report << max_h2.num_rules_fired() << ",";
		//report << max_h2.num_noop_checks() << ",";
		report.close();
	
		/*
		#ifdef DEBUG	
		std::ofstream h2_values( "h2.values.python.csv" );
		max_h2.print( h2_values );
		h2_values.close();
		#endif
		std::ofstream h2_consequences( "h2.consequences.python" );
		max_h2.print_consequences( h2_consequences );
		h2_consequences.close();
		*/		
		
		instance()->complete_goal();

		typedef	grendel::Grendel_Planner< grendel::Rule_Based_H2_Heuristic >	Search_Engine;
		grendel::Explicit_Policy policy( *instance() );
	
		Search_Engine engine( *instance(), max_h2 );	
		engine.set_weight( m_fwd_weight );
		engine.set_weight_2( m_bwd_weight );	
		t0 = grendel::utils::read_time_in_seconds();
		engine.start();
		bool solved = engine.find_strong_cyclic_plan( policy );
		tf = grendel::utils::read_time_in_seconds();
		std::cout << "Memory used: " << engine.mem_used() / (1024.*1024.) << " MBytes" << std::endl;
		std::cout << "March Tree Size: " << engine.march_tree_size() << std::endl;	
		report.open( "report.csv", std::ofstream::out | std::ofstream::app );
		std::cout << "Policy Search Time: " << tf - t0 << std::endl; 
		report << tf - t0 << ",";
		std::cout << "Strong cyclic plan: " << ( solved ? "yes" : "no" ) << std::endl;
		report << ( solved ? "yes" : "no" ) << ",";
		report.close();
		
		print_results(engine, policy);

		#ifdef DEBUG
		std::ofstream	out("value-table");
		engine.print_values( out );
		out.close();
		out.open("dead-ends");
		engine.print_deadends( out );
		out.close();
		#endif
		if ( m_dump_policy ) {
			std::cout << "Policy:" << std::endl;
			std::ofstream policy_file( "policy" );
			policy.write( policy_file, true );
			policy_file.close();
		}

		if ( solved ) {
			if ( m_verify_policy ) {
				std::cout << "Verifying policy..." << std::endl;
	
				t0 = grendel::utils::read_time_in_seconds();
				policy.verify();
				tf = grendel::utils::read_time_in_seconds();
				report.open( "report.csv", std::ofstream::out | std::ofstream::app );
				std::cout << "Policy Verification Time: " << tf - t0 << std::endl;
				report << tf - t0 << ",";
				report.close();
	
				print_policy_evaluation(policy)	;
			}
			else {
				std::cout << "Policy States: " << policy.num_states() << std::endl;
				report << policy.num_states() << ",";
			}
		}
	
	}

	
	void
	Grendel_Planner::write_ground_pddl( std::string domain, std::string problem ) {
		/*
		std::ofstream domain_stream( domain.c_str() );
		m_inst->write_domain( domain_stream );
		std::ofstream problem_stream( problem.c_str() );
		m_inst->write_problem( problem_stream );
		*/
	}


#ifndef __PY_PDDL_TO_SMV__
#define __PY_PDDL_TO_SMV__

#include <pyfondtask.hxx>

class SMV_Compiler : public FOND_Task {
public:

	SMV_Compiler( std::string, std::string );
	~SMV_Compiler();

	void		direct_smv_trans( );
	void		direct_smv_trans_invariants( );
	void		ramirez_smv_trans( );
	void		ramirez_smv_trans_invariants( );
	void		ramirez_mbp_trans();


	bool		m_use_invariants;
	std::string	m_smv_filename;
};


#endif // pypddl2smv.hxx

#include <pypddl2smv.hxx>
#include <pddl_to_smv.hxx>
#include <fstream>
#include <prop_h2.hxx>
#include <iostream>

using namespace boost::python;
	
	SMV_Compiler::SMV_Compiler( std::string domain, std::string instance )
		:  FOND_Task( domain, instance ) {
		m_use_invariants = true;
		m_smv_filename = "problem.smv";
	}

	SMV_Compiler::~SMV_Compiler() {
	}

	
	void
	SMV_Compiler::direct_smv_trans( ) {
		direct_translation( *instance(), m_smv_filename );
	}

	void
	SMV_Compiler::direct_smv_trans_invariants(  ) {
		grendel::Rule_Based_H2_Heuristic max_h2( *instance() );
		//max_h2.set_verbose_computation(true);
		if ( m_use_invariants ) {
			std::cout << "Using third-party invariants to flesh out reachability heuristic" << std::endl;
		}
		max_h2.setup(m_use_invariants);
		direct_translation( *instance(), max_h2, m_smv_filename );
	}

	void
	SMV_Compiler::ramirez_smv_trans() {
		ramirez_translation( *instance(), m_smv_filename );
	}

	void
	SMV_Compiler::ramirez_smv_trans_invariants() {
		
	}

	void	
	SMV_Compiler::ramirez_mbp_trans() {
		ramirez_mbp_translation( *instance(), m_smv_filename );
	}



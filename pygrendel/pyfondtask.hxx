#ifndef __PY_FOND_TASK__
#define __PY_FOND_TASK__

#include <fond_inst.hxx>
#include <boost/python.hpp>
#include <string>

class FOND_Task {
public:
	FOND_Task( std::string, std::string );
	~FOND_Task();

	grendel::FOND_Model*	instance() {
		return m_model;
	}

	void		add_atom( std::string name );
	void		add_action( std::string name );
	void		add_precondition( int index, boost::python::list& lits );
	void		add_effect( int index, boost::python::list& list );
	void		add_invariant( boost::python::list& list );
	void		add_axiom( boost::python::list& condition, boost::python::list& effect );
	void		set_init( boost::python::list& list );
	void		set_goal( boost::python::list& list );
	void		print_action( int index );

	std::string	get_atom_name( int idx ) const { return m_model->atoms[idx]->name; }
		

	size_t		n_atoms() 	const { return m_model->n_atoms(); }
	size_t		n_actions()	const { return m_model->n_actions(); }

	void		write_ground_pddl( std::string domain, std::string instance );

	float		m_parsing_time;

protected:
	grendel::FOND_Model*	m_model;
};


#endif

#include <pypddl2smv.hxx>
#include <pygrendel.hxx>
using namespace boost::python;

BOOST_PYTHON_MODULE( libgrendel )
{
	class_<Grendel_Planner>("GrendelPlanner", init< std::string, std::string >())
		.def( "add_atom", &Grendel_Planner::add_atom )
		.def( "add_action", &Grendel_Planner::add_action )
		.def( "add_invariant", &Grendel_Planner::add_invariant )
		.def( "add_axiom", &Grendel_Planner::add_axiom )
		.def( "num_atoms", &Grendel_Planner::n_atoms )
		.def( "num_actions", &Grendel_Planner::n_actions )
		.def( "get_atom_name", &Grendel_Planner::get_atom_name )
		.def( "add_precondition", &Grendel_Planner::add_precondition )
		.def( "add_effect", &Grendel_Planner::add_effect )
		.def( "set_init", &Grendel_Planner::set_init )
		.def( "set_goal", &Grendel_Planner::set_goal )
		.def( "solve", &Grendel_Planner::solve )
		.def( "write_ground_pddl", &Grendel_Planner::write_ground_pddl )
		.def( "print_action", &Grendel_Planner::print_action )
		.def_readwrite( "parsing_time", &Grendel_Planner::m_parsing_time )
		.def_readwrite( "verify_policy", &Grendel_Planner::m_verify_policy )
		.def_readwrite( "dump_policy", &Grendel_Planner::m_dump_policy )
		.def_readwrite( "use_invariants", &Grendel_Planner::m_use_invariants )
		.def_readwrite( "fwd_weight", &Grendel_Planner::m_fwd_weight )
		.def_readwrite( "bwd_weight", &Grendel_Planner::m_bwd_weight )
	;


	class_<SMV_Compiler>("SMVCompiler", init< std::string, std::string >())
		.def( "add_atom", &SMV_Compiler::add_atom )
		.def( "add_action", &SMV_Compiler::add_action )
		.def( "add_invariant", &SMV_Compiler::add_invariant )
		.def( "add_axiom", &SMV_Compiler::add_axiom )
		.def( "num_atoms", &SMV_Compiler::n_atoms )
		.def( "num_actions", &SMV_Compiler::n_actions )
		.def( "get_atom_name", &SMV_Compiler::get_atom_name )
		.def( "add_precondition", &SMV_Compiler::add_precondition )
		.def( "add_effect", &SMV_Compiler::add_effect )
		.def( "set_init", &SMV_Compiler::set_init )
		.def( "set_goal", &SMV_Compiler::set_goal )
		.def( "write_ground_pddl", &SMV_Compiler::write_ground_pddl )
		.def( "print_action", &SMV_Compiler::print_action )
		.def( "direct_smv_trans", &SMV_Compiler::direct_smv_trans )
		.def( "direct_smv_trans_invariants", &SMV_Compiler::direct_smv_trans_invariants )
		.def( "ramirez_smv_trans", &SMV_Compiler::ramirez_smv_trans )
		.def( "ramirez_smv_trans_invariants", &SMV_Compiler::ramirez_smv_trans_invariants )
		.def( "ramirez_mbp_trans", &SMV_Compiler::ramirez_mbp_trans )
		.def_readwrite( "parsing_time", &SMV_Compiler::m_parsing_time )
		.def_readwrite( "use_invariants", &SMV_Compiler::m_use_invariants )
		.def_readwrite( "smv_filename", &SMV_Compiler::m_smv_filename )
	;
}	

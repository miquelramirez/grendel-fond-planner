#! /usr/bin/env python

from __future__ import print_function

from collections import defaultdict

import pddl
import timers
from fondparser import *
from libgrendel import SMVCompiler 
import sys


def main( domain_file, problem_file) :
	print("Domain: %s Problem: %s"%(domain_file, problem_file) )

	task = pddl.open( problem_file, domain_file)
	fond_task = SMVCompiler(task.domain_name, task.task_name )

	parse( task, fond_task )
	
	
	fond_task.verify_policy = False 
	fond_task.dump_policy = False 

	fond_task.smv_filename = task.domain_name + '_' + task.task_name + '.compact.smv'
	fond_task.ramirez_smv_trans( )
	fond_task.smv_filename = task.domain_name + '_' + task.task_name + '.standard.mbp.smv'
	fond_task.direct_smv_trans_invariants()
	fond_task.smv_filename = task.domain_name + '_' + task.task_name + '.compact.mbp.smv'
	fond_task.ramirez_mbp_trans()

	return fond_task	

def debug() :
	# edit this to change arguments
	main( 'benchmarks/fond/tireworld/domain.pddl', 'benchmarks/fond/tireworld/p03.pddl' )
	#main( 'benchmarks/fond/gadgets/gadget-domain-01.pddl', 'benchmarks/fond/gadgets/gadget-problem-01.pddl' )
	#main( 'benchmarks/fond/blocksworld/domain.pddl', 'mini-blocks.pddl' )

if __name__ == "__main__":
	main( sys.argv[1], sys.argv[2])

